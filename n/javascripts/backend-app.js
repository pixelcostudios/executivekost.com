/* -----------------------
   fade out message
----------------------- */
$(document).ready(function(){
	$(".fadeout").delay(2000).fadeOut();
});
/* -----------------------
   tabs content
----------------------- */
$(document).ready(function(){
	$("#tabs li").click(function() {
		//	First remove class "active" from currently active tab
		$("#tabs li").removeClass('active');

		//	Now add class "active" to the selected/clicked tab
		$(this).addClass("active");

		//	Hide all tab content
		$(".tab_content").hide();

		//	Here we get the href value of the selected tab
		var selected_tab = $(this).find("a").attr("href");

		//	Show the selected tab content
		$(selected_tab).fadeIn();

		//	At the end, we add return false so that the click on the link is not executed
		return false;
	});
});
/* -----------------------
   add redactor into textarea
----------------------- */
/*$(document).ready(
	function(){
		$('.area').redactor()
	}
);*/
function insertHtml(str){
	$('.area').insertHtml(str);
}
$(document).ready(
	function(){
		$('.area').redactor({
			/*focus: true,
			buttonsAdd: ['button1'], 
			buttonsCustom: {
				button1: {
					title: 'Button', 
					callback: function(obj){
						obj.execCommand('inserthtml', '<!-- page --><hr style="border-top: 1px dotted #999;"><!-- break -->');
					}
				}
			}*/
		});
	}
);
/* -----------------------
   toogle "custom field"
----------------------- */
function toggle(){
	var ele = document.getElementById("toggleText");
	var text = document.getElementById("displayText");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "custom field &raquo;";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide";
	}
} 

function toggle_en(){
	var ele = document.getElementById("toggleText_en");
	var text = document.getElementById("displayText_en");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "custom field &raquo;";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide";
	}
}
/* -----------------------
   all check
----------------------- */
function toggleController(oElement){
	oForm = oElement.form; 
	oElement = oForm.elements[oElement.name]; 
	if(oElement.length){ 
		bChecked = true; 
		nChecked = 0; 
		for(i = 1; i < oElement.length; i++) 
			if(oElement[i].checked) 
				nChecked++; 
			if(nChecked < oElement.length - 1) 
				bChecked = false; 
		oElement[0].checked = bChecked; 
	} 
}

function toggleChecked(oElement){ 
	oForm = oElement.form; 
	oElement = oForm.elements[oElement.name];
	if(oElement.length){ 
		bChecked = oElement[0].checked; 
		for(i = 1; i < oElement.length; i++) 
		  oElement[i].checked = bChecked; 
	} 
}
/* -----------------------
   image caption
----------------------- */
$(".image-img").each(function(){
    var $this = $(this);
    var title = $this.attr("title");
	$this.after('<div class="caption">'+ title +'</div>');
});