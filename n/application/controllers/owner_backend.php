<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Owner_backend extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('owner/home');
		$this->load->model('owner/login');
		$this->load->model('owner/setting');
		$this->load->model('owner/kost');
	}
	/* INDEX */
	function index(){
		$mode = strtolower($this->uri->segment(2));
		if($this->session->userdata('owner_login')==TRUE){
			switch($mode){
				default				: $this->home->index(); break;
				case "home"			: $this->home->index(); break;
				case "search"		: $this->home->search(); break;
				case "check"		: $this->home->check(); break;
				case "finish"		: $this->home->finish(); break;
				case "delete"		: $this->home->delete(); break;
				case "accounting"	: $this->load->model('owner/accounting/accounting','acc'); $this->acc->menu();break;
				/* MEMBER */
				case "setting"			:
				$setting = strtolower($this->uri->segment(3));
				if($this->session->userdata('ses_owner_level')!='investor'){
					switch($setting){
						default				: $this->setting->index(); break;
						case "add"			: $this->setting->add(); break;
						case "edit"			: $this->setting->edit(); break;
						case "input"		: $this->setting->input(); break;
						case "delete"		: $this->setting->delete(); break;
					}
				}
				else{
					switch($setting){
						default				: $this->home->index(); break;
						case "edit"			: $this->setting->edit(); break;
					}
				}
				break;
				/* KOST */
				case "kost"			:
				$kost = strtolower($this->uri->segment(3));
				if($this->session->userdata('ses_owner_level')!='investor'){
					switch($kost){
						default				:
						$kost1 = strtolower($this->uri->segment(4));
						switch($kost1){
							default				: $this->kost->index(); break;
							case "add"			: $this->kost->add(); break;
							case "edit"			: $this->kost->edit(); break;
							case "delete"		: $this->kost->delete(); break;
							case "delete_image"	: $this->kost->delete_image(); break;
						}
						break;
						case "input"		: $this->kost->input(); break;
					}
				}
				else{
					switch($kost){
						default				: $this->kost->index(); break;
					}
				}
				break;
				/* LOGOUT */
				case "logout"	: $this->session->unset_userdata('owner_login');
							   	  redirect(base_url().'login/owner', 'refresh');
				break;
			}
		}
		else {
			switch($mode){
				/* LOGIN */
				default					: $this->load->view('owner/login'); break;
				case "process_login"	: $this->login->process_login(); break;
			}
		}
	}
}