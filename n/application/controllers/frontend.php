<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Frontend extends CI_Controller{
	/* GENERAL */
	function __construct(){
		parent::__construct();
		$this->load->model('frontend/home');
		$this->load->model('frontend/page');
		$this->load->model('frontend/post');
		$this->load->model('frontend/kost');
		$this->load->model('frontend/member');
		$this->load->model('frontend/testimoni');
	}
	/* INDEX */
	function index(){
		if($this->session->userdata('lang')=='') $this->session->set_userdata('lang','id');
		$mode = strtolower($this->uri->segment(1));
		switch($mode){
			default : if($mode==''){ $this->home->index(); } else{ $this->post->index($mode); } break;
			case "home"				: $this->home->index(); break;
			case "feed"				: 
			$feed = strtolower($this->uri->segment(2));
			switch($feed){
				default				: $this->home->feed(); break;
				case "subscriber"	: $this->home->subscriber(); break;
			}
			break;
			case "page"				: $this->page->index($this->uri->segment(2)); break;
			case "send_message" 	: $this->page->send_message(); break;
			case "send_comment"		: $this->post->send(); break;
			case "category"			: $this->post->archive(); break;
			case "kost"				: 
			$kost = strtolower($this->uri->segment(2));
			switch($kost){
				default				: $this->kost->index(); break;
				case "search"		: $this->kost->search(); break;
				case "detail"		: $this->kost->detail($this->uri->segment(3)); break;
				case "send_comment"	: $this->kost->send(); break;
				case "room"			: $this->kost->detail_room($this->uri->segment(3)); break;
			}
			break;
			case "member"				: 
			$member = strtolower($this->uri->segment(2));
			switch($member){
				default				: $this->member->index(); break;
				case "reservation"	: $this->member->reservation(); break;
				case "register"		: $this->member->register(); break;
				case "re_order"		: $this->member->re_order(); break;
				case "select_room"	: $this->member->select_room(); break;
				case "confirm"		: $this->member->confirm(); break;
				case "order"		: $this->member->index(); break;
				case "update_info"	: $this->member->update_member_information(); break;
				case "profile"		: $this->member->profile(); break;
				case "update_pass"	: $this->member->update_member_password(); break;
				case "login"		: $this->member->login(); break;
				case "logout"		: $this->session->sess_destroy(); redirect(base_url().'member', 'refresh'); break;
				
			}
			break;
			case "searching"		: $this->search->search(); break;
			case "lang"				: $this->lang(); break;
			case "testimonial"		: $this->testimoni->index(); break;
		}
	}
	// session lang
	function lang(){
		$lang = $this->uri->segment(2);
		$this->session->set_userdata('lang', $lang);
		redirect(base_url()/*.$this->session->userdata('site_url')*/);
	}
	// username check
	function username_check($str){
		if(!$this->site_model->check_data("member_username","tb_members",$str)){
			$this->form_validation->set_message('username_check',$str.' unavailable');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	// name check
	function name_check($str){
		if(!$this->site_model->check_data("member_name","tb_members",mysql_escape_string($str))){
			$this->form_validation->set_message('name_check',$str.' unavailable');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	// identity check
	function identity_check($str){
		if(!$this->site_model->check_data("member_identity_number","tb_members",$str)){
			$this->form_validation->set_message('identity_check','Identity Number : '.$str.' unavailable');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	// email check
	function email_check($str){
		if(!$this->site_model->check_data("member_email","tb_members",$str)){
			$this->form_validation->set_message('email_check',$str.' unavailable');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	// room number check
	/*function room_check($str){
		$where = "kost_id = '".$this->session->userdata('kost_id')."' AND kost_room_number = '".$str."'";
		$data = $this->site_model->get_data('',"tb_kost_room",$where)->num_rows();
		if($data == 0){
			$this->form_validation->set_message('room_check','Room '.$str.' unavailable');
			return FALSE;
		}
		else{		
			$where = "kost_id = '".$this->session->userdata('kost_id')."' AND kost_room_number = '".$str."' AND kost_room_status = '0'";
			$data = $this->site_model->get_data('',"tb_kost_room",$where)->num_rows();
			if($data == 0){
				$this->form_validation->set_message('room_check','Room '.$str.' unavailable');
				return FALSE;
			}
			else{
				return TRUE;
			}
		}
	}*/
	// room check in
	function room_check_in($str,$kost_number){
		$data = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$kost_number."' AND check_in <= '".$str."' AND check_out >= '".$str."' AND rent_status != '4'")->num_rows();
		if($data == 1){
			$this->form_validation->set_message('room_check_in',$str.' unavailable');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	// room check out
	function room_check_out($str,$callback_room_check_out){
		$x = explode(",",$callback_room_check_out);
		$data = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$x[0]."' AND check_in <= '".$str."' AND check_out >= '".$str."' AND rent_status != '4'")->num_rows();
		if($data == 1){
			$this->form_validation->set_message('room_check_out',$str.' unavailable');
			return FALSE;
		}
		else{
			$data = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$x[0]."' AND check_in BETWEEN '".$x[1]."' AND '".$str."' AND check_out BETWEEN '".$x[1]."' AND '".$str."' AND rent_status != '4'")->num_rows();
			if($data == 1){
				$this->form_validation->set_message('room_check_out',$str.' unavailable');
				return FALSE;
			}
			else{
				return TRUE;
			}
		}
	}
}