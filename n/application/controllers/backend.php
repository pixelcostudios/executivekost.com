<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Backend extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('backend/home');
		$this->load->model('backend/login');
		$this->load->model('backend/user');
		$this->load->model('backend/post');
		$this->load->model('backend/page');
		$this->load->model('backend/daerah');
		$this->load->model('backend/kost');
		$this->load->model('backend/setting');
		$this->load->model('backend/notification');
		$this->load->model('backend/category');
		$this->load->model('backend/banner');
		$this->load->model('backend/backup');
	}
	/* INDEX */
	function index(){
		$mode = strtolower($this->uri->segment(2));
		if($this->session->userdata('admin_login')==TRUE){
			switch($mode){
				default: $this->home->index(); break;
				case "home"			: $this->home->index(); break;
				case "check_title"	: $this->check_title(); break;
				/* USER */
				case "user"			: 
				$user = strtolower($this->uri->segment(3));
				switch($user){
					default				: $this->user->index(); break;
					case "add"			: $this->user->add(); break;
					case "edit"			: $this->user->edit(); break;
					case "input"		: $this->user->input(); break;
					case "delete"		: $this->user->delete(); break;
				}
				break;
				/* POST */
				case "post":
				$post = strtolower($this->uri->segment(3));
				switch($post){
					default						: $this->post->comment(); break;
					case "publish_comment"		: $this->post->$post(); break;
					case "delete_comment"		: $this->post->$post(); break;
					case "view"					: $this->post->$post(); break;
					case "search"				: $this->post->$post(); break;
					case "add"					: $this->post->$post(); break;
					case "edit"					: $this->post->$post(); break;
					case "input"				: $this->post->$post(); break;
					case "delete"				: $this->post->$post(); break;
					case "delete_image"			: $this->post->$post(); break;
				}
				break;
				/* PAGE */
				case "page":
				$page = strtolower($this->uri->segment(3));
				switch($page){
					default						: $this->page->home(); break;
					case "view"					: $this->page->$page(); break;
					case "add"					: $this->page->$page(); break;
					case "edit"					: $this->page->$page(); break;
					case "input"				: $this->page->$page(); break;
					case "delete"				: $this->page->$page(); break;
					case "delete_image"			: $this->page->$page(); break;
				}
				break;
				/* PRODUCT */
				case "kost":
				$kost = strtolower($this->uri->segment(3));
				switch($kost){
					default						: $this->kost->comment(); break;
					case "publish_comment"		: $this->kost->$kost(); break;
					case "delete_comment"		: $this->kost->$kost(); break;
					case "list"					: $this->kost->view(); break;
					case "add"					: $this->kost->$kost(); break;
					case "edit"					: $this->kost->$kost(); break;
					case "input"				: $this->kost->$kost(); break;
					case "delete"				: $this->kost->$kost(); break;
					case "delete_image"			: $this->kost->$kost(); break;
					case "daerah"				: 
					$daerah = strtolower($this->uri->segment(4));
					switch($daerah){
						default					: $this->daerah->index(); break;
						case "add"				: $this->daerah->$daerah(); break;
						case "edit"				: $this->daerah->$daerah(); break;
						case "input"			: $this->daerah->$daerah(); break;
						case "delete"			: $this->daerah->$daerah(); break;
					}
					break;
				}
				break;
				/* SETTING */
				case "setting":
				$setting = strtolower($this->uri->segment(3));
				switch($setting){
					// setting
					default						: $this->setting->general(); break;
					case "update_setting"		: $this->setting->$setting(); break;
					// notivication
					case "notification"			:
					$notivication = strtolower($this->uri->segment(4));
					switch($notivication){
						default						: $this->notification->index(); break;
						case "add"					: $this->notification->add(); break;
						case "edit"					: $this->notification->edit(); break;
						case "input"				: $this->notification->input(); break;
						case "delete"				: $this->notification->delete(); break;
					}
					break;
					// category
					case "category"				: 
					$category = strtolower($this->uri->segment(4));
					switch($category){
						default 					: $this->category->index(); break;
						case "set_category"			: $this->category->set_category(); break;
						case "add"					: $this->category->add(); break;
						case "edit"					: $this->category->edit(); break;
						case "input"				: $this->category->input(); break;
						case "delete"				: $this->category->delete(); break;
						case "delete_image"			: $this->category->delete_image(); break;
					}
					break;
					// banner
					case "banner"				: 
					$banner1 = strtolower($this->uri->segment(4));
					switch($banner1){
						default						:
						$banner2 = strtolower($this->uri->segment(5));
						switch($banner2){
							default						: $this->banner->index(); break;
							case "add"					: $this->banner->add(); break;
							case "edit"					: $this->banner->edit(); break;
							case "delete"				: $this->banner->delete(); break;
						}
						break;
						case "input"				: $this->banner->input(); break;
					}
					break;
					// backup database
					case "backup"				: $this->backup->index(); break;
					case "backup_database"		: $this->backup->$setting(); break;
					case "download"				: $this->backup->$setting(); break;
					case "download_database"	: $this->backup->$setting(); break;
				}
				break;
				/* LOGOUT */
				case "logout"					:					
					$data_session = array('ses_admin_id'=>'','admin_username'=>'','admin_level'=>'','admin_login'=>FALSE);
					$this->session->unset_userdata($data_session);
					redirect(base_url().'login/', 'refresh'); break;
			}
		}
		else {
			switch($mode){
				/* LOGIN */
				default: $this->load->view('backend/login'); break;
				case "process_login"			: $this->login->process_login(); break;
			}
		}
	}
	// title check
	function check_title($str="",$check=""){
		if($str==''){
			$title = $this->uri->segment(3);
			$table = $this->uri->segment(4);
			$title_check = $this->uri->segment(5);
			
			if($this->site_model->check_data($title,$table,$title_check)==TRUE){
				echo "";
			}
			else{
				if($this->session->userdata('form_mode')=='add')	
					echo "title already exists in the database";
				else
					echo "";
			}
		}
		else{
			$x = explode(",",$check);
			$title = $x[0];
			$table = $x[1];
			$title_check = $str;
			
			if($this->site_model->check_data($title,$table,$title_check)==TRUE){
				return TRUE;
			}
			else{
				if($this->session->userdata('form_mode')=='add'){	
					$this->form_validation->set_message('check_title','title already exists in the database');
					return FALSE;
				}
				else
					return TRUE;
			}
		}
	}
	// check new & confirm password
	function check_confirm_password($str,$new_password){
		if($str==$new_password)		
			return TRUE;
		else
			$this->form_validation->set_message('check_confirm_password','Confirm Pass do not match with new password');
			return FALSE;
	}
}