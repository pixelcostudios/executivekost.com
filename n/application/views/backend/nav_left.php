<?php
echo '<div class="msg_list">';
if($this->uri->segment(2)=='home'){
	echo '&nbsp';
}
if($this->uri->segment(2)=='user'){
	echo '<p class="msg_head">User</p>';
	echo '<div class="msg_body">';
	echo '<ul>';
	echo '<li><a href="'.base_url().'backend/user/">Manage User</a></li>';
	echo '</ul>';
	echo '</div>';
}
if($this->uri->segment(2)=='post'){
	$parent_doc = $this->site_model->get_data("*","tb_category","category_type = '1' and category_group = '0' and category_status = 'active'")->result();
	if(count($parent_doc)>0){
		foreach($parent_doc as $p){
			echo '<p class="msg_head">'.$p->category_title.'</p>
			<div class="msg_body">
			<ul>';
			$child = $this->site_model->get_data('',"tb_category","category_group = '".$p->category_id."' and category_status = 'active'")->result();
			foreach($child as $c){
				echo '<li><a href="'.base_url().'backend/post/view/'.$c->category_id.'">'.$c->category_title.' / '.$c->category_title_en.'</a></li>';
			}
			echo '</ul></div>';
		}
	}
	else{
		echo '<p class="msg_head">&nbsp;</p>';
	}
}
if($this->uri->segment(2)=='page'){
	echo '<p class="msg_head"><a href="'.base_url().'backend/page/view">Page</a></p>';
	echo '<div class="msg_body">';
	echo '<ul>';
	$menupage = $this->site_model->get_data('',"tb_pages")->result();
	if(count($menupage)>0){
		foreach($menupage as $m){
			echo '<li><a href="'.base_url().'backend/page/edit/'.$m->page_id.'">'.$m->page_title.'</a></li>';
		}
	}
	else{
	}
	echo '</ul>';
	echo '</div>';
}
if($this->uri->segment(2)=='kost' OR $this->uri->segment(2)=='daerah'){
	echo '<p class="msg_head">Kost</p>';
	echo '<div class="msg_body">';
	echo '<ul>';
	echo '<li><a href="'.base_url().'backend/kost/list">Daftar Kost</a></li>';
	echo '<li><a href="'.base_url().'backend/kost/daerah">Daerah Kost</a></li>';
	echo '</ul></div>';
}
if($this->uri->segment(2)=='setting'){
	echo '<p class="msg_head">Setting</p>';
	echo '<div class="msg_body">';
	echo '<ul>';
	echo '<li><a href="'.base_url().'backend/setting/general">General</a></li>';
	echo '<li><a href="'.base_url().'backend/setting/notification">Notification</a></li>';
	echo '<li><a href="'.base_url().'backend/setting/category">Category</a></li>';
	echo '<li><a href="#">Banner & Partner</a></li>';
	echo '<li class="sub-menu"><a href="'.base_url().'backend/setting/banner/1">Banner</a></li>';
	echo '<li class="sub-menu"><a href="'.base_url().'backend/setting/banner/2">Partner</a></li>';
	echo '<li><a href="'.base_url().'backend/setting/backup">Backup Database</a></li>';
	echo '</ul>';
	echo '</div>';
}
echo '</div>';
?>