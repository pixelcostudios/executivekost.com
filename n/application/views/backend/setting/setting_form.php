<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div style="width:538px; float:left; margin-right: 10px;">
<?php
echo form_open_multipart(''.$action.'');
echo form_hidden('oldimg',''.set_value('oldimg', isset($default['image']) ? $default['image'] : '').'');
echo '<p><label>Contact email</label>'.form_input('email',''.set_value('email', isset($default['email_admin']) ? $default['email_admin'] : '').'','size="50"').'</p>';
$title = array('name'=>'title','value'=>''.isset($default['title']) ? $default['title'] : ''.'','rows'=>'3');
echo '<p><label class="lang-id">Title</label>'.form_textarea($title).'</p>';
$title_en = array('name'=>'title_en','value'=>''.isset($default['title_en']) ? $default['title_en'] : ''.'','rows'=>'3');
echo '<p><label class="lang-en">Title</label>'.form_textarea($title_en).'</p>';
$des = array('name'=>'des','value'=>''.isset($default['description']) ? $default['description'] : ''.'','rows'=>'3');
echo '<p><label class="lang-id">Description</label>'.form_textarea($des).'</p>';
$des_en = array('name'=>'des_en','value'=>''.isset($default['description_en']) ? $default['description_en'] : ''.'','rows'=>'3');
echo '<p><label class="lang-en">Description</label>'.form_textarea($des_en).'</p>';
$keyword = array('name'=>'keyword','value'=>''.isset($default['keyword']) ? $default['keyword'] : ''.'','rows'=>'3');
echo '<p><label class="lang-id">Keyword</label>'.form_textarea($keyword).'</p>';
$keyword_en = array('name'=>'keyword_en','value'=>''.isset($default['keyword_en']) ? $default['keyword_en'] : ''.'','rows'=>'3');
echo '<p><label class="lang-en">Keyword</label>'.form_textarea($keyword_en).'</p>';
echo '<p>'.form_submit('','Update','class="button"').'</p>';
?>
</div>
<div style="width:230px; float:left;">
<?php
if(!empty($default['image'])){
echo '<p><label>Logo</label>'.img(array('src'=>'uploads/'.thumb($default['image']).'','style'=>'max-width:200px;')).'</p>';
}
echo '<p>'.form_upload('image').'</p>';
echo '<p><label>Operator 1 *)</label>'.form_input('op_1',''.set_value('op_1', isset($default['op_1']) ? $default['op_1'] : '').'','size="25"').'</p>';
echo '<p><label>Operator 2 *)</label>'.form_input('op_2',''.set_value('op_2', isset($default['op_2']) ? $default['op_2'] : '').'','size="25"').'</p>';
echo '<p><label>*) Operator Name, Yahoo ID</label></p>';
?>
</div>
</div>