<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<div id="navigation">
<ul>
	<li><a href="<?php echo $data_link?>">List Data</a></li>
    <li class="active"><a href="<?php echo $form_link?>">Form</a></li>
</ul>
<div class="clr"></div>
</div>
<?php
echo form_open_multipart($action);
echo '<p><label>Name</label>'.form_input('title',set_value('title', isset($default['title']) ? $default['title']:'')).'</p>';
echo form_error('title', '<p class="error">', '</p>');
$value = array('name'=>'value','value'=>set_value('value',isset($default['value']) ? $default['value'] : ''),'class'=>'area','style'=>'height: 300px;');
echo '<p><label class="lang-id">Value</label>'.form_textarea($value).'</p>';
$value_en = array('name'=>'value_en','value'=>set_value('value_en',isset($default['value_en']) ? $default['value_en'] : ''),'class'=>'area','style'=>'height: 300px;');
echo '<p><label class="lang-en">Value</label>'.form_textarea($value_en).'</p>';
echo '<p>'.form_submit('','Submit','class="button"').'</p>';
?>
</div>
</div>