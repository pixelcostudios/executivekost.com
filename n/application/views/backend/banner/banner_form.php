<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<div id="navigation">
<ul>
	<li><a href="<?php echo $data_link?>">List Data</a></li>
    <li class="active"><a href="<?php echo $form_link?>">Form</a></li>
</ul>
<div class="clr"></div>
</div>
<?php
echo form_open_multipart(''.$action.'');
echo form_hidden('oldimg',''.set_value('oldimg', isset($default['oldimg']) ? $default['oldimg'] : '').'');
echo form_hidden('position',''.set_value('position', isset($default['position']) ? $default['position'] : $this->session->userdata('position')).'');
?>
<div style="width: 515px; float: left; margin-right: 10px;">
<?php
echo '<p><label>Title</label>'.form_input('title',set_value('title', isset($default['title']) ? $default['title'] : '')).'</p>';
echo form_error('title', '<p class="error">', '</p>');
echo '<p><label>Url</label>'.form_input('url',set_value('url', isset($default['url']) ? $default['url'] : '')).'</p>';
echo form_error('url', '<p class="error">', '</p>');
echo '<p><label>Order</label>'.form_input('sequence',set_value('sequence', isset($default['sequence']) ? $default['sequence'] : ''),'style="width: 30px;"').'</p>';
echo '<br />';
if($this->session->userdata('form_mode')=='edit'){
	echo '<p><label>Status</label>';
	echo '<font size="-1">publish</font> '.form_radio('publish', '1', set_radio('post_status', '1',isset($default['banner_status']) && $default['banner_status'] == '1' ? TRUE : FALSE)).' ';
	echo '<font size="-1">unpublish</font> '.form_radio('publish', '0', set_radio('post_status', '0', isset($default['banner_status']) && $default['banner_status'] == '0' ? TRUE : FALSE));
	echo '</p>';
}
echo '<p>'.form_submit('','Submit','class="button"').'</p>';
?>
</div>
<div style="width: 230px; float: left;">
<?php
if($this->session->userdata('form_mode')=='edit'){
	$c = explode(".",$default['oldimg']);
	if($c[0]!='swf'){
		echo '<p><img src="'.base_url().'uploads/banners/'.thumb($default['oldimg']).'" /></p>';
	}
	else {
		echo "<p><object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" 
		codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0\" width=\"200\">
		<param name=\"movie\" value=\"".base_url()."uploads/banners/".$default['oldimg']."\" />
		<param name=\"quality\" value=\"high\" />
		<embed src=\"".base_url()."uploads/banners/".$default['oldimg']."\" quality=\"high\" 
		pluginspage=\"http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash\"	type=\"application/x-shockwave-flash\" width=\"200\">
		</embed>
		</object></p>";
	}
}
echo '<p><label>Image</label>'.form_upload('image').'</p>';
?>
</div>
<div class="clr"></div>
</div>
</div>