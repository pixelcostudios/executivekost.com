<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<div id="navigation">
<ul>
	<li><a href="<?php echo $data_link?>">List Data</a></li>
    <li class="active"><a href="<?php echo $form_link?>"><?php echo $this->session->userdata('form_mode')?> Data</a></li>
</ul>
<div class="clr"></div>
</div>
<?php
echo form_open_multipart(''.$action.'');
?>
<div id="tabs_container">
<ul id="tabs">
    <li class="active"><a href="#tab1">ID</a></li>
    <li><a href="#tab2">EN</a></li>
</ul>
</div>
<div id="tabs_content_container">
    <div id="tab1" class="tab_content" style="display: block;">
    <p><label class="lang-id">Title</label><?php echo form_input('title',''.set_value('title', isset($title) ? $title : '').'')?></p>
    <?php echo form_error('title', '<p class="error">', '</p>')?>
    <?php $des = array('name'=>'des','value'=>''.set_value('des',isset($des) ? $des : '').'','style'=>'height:250px;','class'=>'area')?>
    <p><label class="lang-id">Description</label><?php echo form_textarea($des)?></p>
    <?php $facility = array('name'=>'facility','value'=>''.set_value('facility',isset($facility) ? $facility : '').'','rows'=>'2')?>
    <p><label class="lang-id">Facility *)</label><?php echo form_textarea($facility)?></p>
    <p><label>*) example : LED 32", Kulkas, AC, Bath Tub</label></p>
    </div>
    <div id="tab2" class="tab_content">
    <p><label class="lang-en">Title</label><?php echo form_input('title_en',''.set_value('title_en', isset($title_en) ? $title_en : '').'')?></p>
    <?php echo form_error('title', '<p class="error">', '</p>')?>
    <?php $des_en = array('name'=>'des_en','value'=>''.set_value('des',isset($des_en) ? $des_en : '').'','style'=>'height:250px;','class'=>'area')?>
    <p><label class="lang-en">Description</label><?php echo form_textarea($des_en)?></p>
    <?php $facility_en = array('name'=>'facility_en','value'=>''.set_value('facility',isset($facility_en) ? $facility_en : '').'','rows'=>'2')?>
    <p><label class="lang-en">Facility *)</label><?php echo form_textarea($facility_en)?></p>
    <p><label>*) example : LED 32", Kulkas, AC, Bath Tub</label></p>
    </div>
</div>
<p><label>Kota</label><select name="kota" style="width: auto; padding: 4px;">
<option value="0">Kota</option>
<?php
if(count($kota)>0){
    foreach($kota as $k){
    	echo '<option value="'.$k->daerah_id.'" '.set_select('kota', ''.$k->daerah_id.'', isset($daerah_id) && $daerah_id == $k->daerah_id ? TRUE : FALSE).'>'.$k->daerah_title.'</option>';
    }
}
?>
</select></p>
<p><label>Virtual Tour</label><?php echo form_input('virtual_tour',''.set_value('virtual_tour', isset($virtual_tour) ? $virtual_tour : '').'','style="width:530px;"')?></p>
<?php $x = explode("/",$old_sketch)?>
<p><?php echo !empty($old_sketch)?img(array('src'=>base_url().$x[0]."/".$x[1]."/".$x[2]."/".$x[3]."/".thumb($x[4]).'','style'=>'max-width:200px;')):'';?></p>
<?php echo form_hidden('old_sketch',''.set_value('old_sketch', isset($old_sketch) ? $old_sketch : '').'')?>
<p><label>Denah Kost</label><?php echo form_upload('kost_sketch')?></p>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<div id="canvas" style="float: left; margin-right: 10px; width: 545px; height: 200px; border: 1px solid #CCC;"></div>
<p><label class="title">Latitude :</label><?php echo form_input('latitude',''.set_value('latitude', isset($latitude) ? $latitude : ''),'id="latitude" style="width: 200px;"')?></p>
<?php echo form_error('latitude', '<p class="error">', '</p>')?>
<p><label class="title">Longitude :</label><?php echo form_input('longitude',''.set_value('longitude', isset($longitude) ? $longitude : ''),'id="longitude" style="width: 200px;"')?></p>
<?php echo form_error('longitude', '<p class="error">', '</p>')?>
<script type="text/javascript">
// configuration
var myZoom = 12;
var myMarkerIsDraggable = true;
var myCoordsLenght = 6;
var defaultLat = <?php echo isset($latitude) ? $latitude : '-7.784472';?> 
var defaultLng = <?php echo isset($longitude) ? $longitude : '110.36824';?>

// creates the map
// zooms
// centers the map
// sets the map's type 
var map = new google.maps.Map(document.getElementById('canvas'), {
	zoom: myZoom,
	center: new google.maps.LatLng(defaultLat, defaultLng),
	mapTypeId: google.maps.MapTypeId.ROADMAP
});

// creates a draggable marker to the given coords
var myMarker = new google.maps.Marker({
	position: new google.maps.LatLng(defaultLat, defaultLng),
	draggable: myMarkerIsDraggable
});

// adds a listener to the marker
// gets the coords when drag event ends
// then updates the input with the new coords
google.maps.event.addListener(myMarker, 'dragend', function(evt){
	document.getElementById('latitude').value = evt.latLng.lat().toFixed(myCoordsLenght);
	document.getElementById('longitude').value = evt.latLng.lng().toFixed(myCoordsLenght);
});

// centers the map on markers coords
map.setCenter(myMarker.position);

// adds the marker on the map
myMarker.setMap(map);
</script>
<div class="clr"></div>
<script type="text/javascript" language="javascript">
var upload_number = <?php echo count($images)+1 ?>;
function addFileInput(){
	if(upload_number > 5) {
		alert('sorry you can only upload 5 files')
		exit(0);
	}
	var d = document.createElement("div");
	var a = document.createElement("a");
	var file = document.createElement("input");
	var file2 = document.createElement("input");
	var img = document.createElement("img");
	d.setAttribute("id", "f"+upload_number);
	d.setAttribute("style", "clear: both;");
	d.appendChild(file);
	d.appendChild(file2);
	d.appendChild(a);
	a.setAttribute("href", "javascript:removeFileInput('f"+upload_number+"');");
	a.appendChild(img);
	file.setAttribute("type", "file");
	file.setAttribute("name", "image"+upload_number);
	file.setAttribute("style", "margin: 4px 0; float: left;");
	file2.setAttribute("type", "text");
	file2.setAttribute("style", "float: left;");
	file2.setAttribute("name", "caption"+upload_number);
	file2.setAttribute("placeholder", "caption "+upload_number+" . . . .");
	img.setAttribute("src", "<?php echo base_url()?>stylesheets/backend/remove.png");
	img.setAttribute("style", "margin: 4px 0 4px 5px;");
	document.getElementById("moreUploads").appendChild(d);
	upload_number++;
}

function removeFileInput(i) {
	var elm = document.getElementById(i);
	document.getElementById("moreUploads").removeChild(elm);
	upload_number = upload_number -1; // decrement the max file upload counter if the file is removed 
}
</script>
<?php
echo br(1);
if($this->session->userdata('form_mode')=='edit'){
	foreach($images as $a){
		echo '<div class="image-frame"><div class="image">';
		echo '<img src="'.base_url().$a->dir.'/'.thumb($a->image).'" width="145" alt="'.$a->caption.'" title="'.$a->caption.'" />';
		echo '</div>';
		echo anchor(base_url().'backend/kost/delete_image/'.$a->image_id.'/'.$a->parent_id.'',img(array('src'=>base_url().'stylesheets/backend/remove.png')));
		echo '</div>';
	}
}
?>
<div class="clr">&nbsp;</div>
<p><label class="left">Images</label><a onclick="addFileInput();" style="cursor: pointer;"><img src="<?php echo base_url()?>stylesheets/backend/plus.png" /></a></p>
<div id="moreUploads"></div>
<p class="error">filetype allowed : .gif .jpg .jpeg .png</p>
<?php
if($this->session->userdata('form_mode')=='edit'){
	echo '<p><label>Status</label><font size="-1">publish</font> '.form_radio('kost_status', '1', set_radio('kost_status', '1', isset($kost_status) && $kost_status == '1' ? TRUE : FALSE)).' <font size="-1">unpublish</font> '.form_radio('kost_status', '0', set_radio('kost_status', '0', isset($kost_status) && $kost_status == '0' ? TRUE : FALSE)).'</p>';
}
echo '<p>'.form_submit('','Submit','class="button"').'</p>';
?>
</div>
</div>