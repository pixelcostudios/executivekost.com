<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<div id="navigation">
<ul>
	<li class="active"><a href="<?php echo $data_link?>">List Data</a></li>
    <li><a href="<?php echo $form_link?>">Add Data</a></li>
</ul>
<div class="clr"></div>
</div>
<?php
echo ! empty($pagination) ? "<div class=\"page_num\">" . $pagination . '</div>' : '';
echo ! empty($table) ? $table : '';
echo ! empty($pagination) ? "<div class=\"page_num\">" . $pagination . '</div>' : '';
?>
</div>
<?php
echo '<table style="width: auto;">';
echo '<tr>';
echo '<td><span class="edit"></span> edit</td>';
echo '<td><span class="delete"></span> delete</td>';
echo '</tr>';
echo '</table>';
?>
</div>