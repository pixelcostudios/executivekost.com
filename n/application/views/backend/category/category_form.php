<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<?php
echo form_open_multipart(''.$action.'');
echo form_hidden('oldimg',''.set_value('oldimg', isset($default['oldimg']) ? $default['oldimg'] : '').'');
?>
<div style="width:538px; float:left; margin-right: 10px;">
<?php
echo '<p><label class="lang-id">Title</label>'.form_input('title',''.set_value('title', isset($default['title']) ? $default['title'] : '').'').'</p>'.form_error('title', '<p class="error">', '</p>');
echo '<p><label class="lang-en">Title</label>'.form_input('title_en',''.set_value('title_en', isset($default['title_en']) ? $default['title_en'] : '').'').'</p>'.form_error('title_en', '<p class="error">', '</p>');
$des = array('name'=>'des','value'=>''.set_value('des',isset($default['des']) ? $default['des'] : '').'','cols'=>'54','rows'=>'5');
echo '<p><label class="lang-id">Description</label>'.form_textarea($des).'</p>';
$des_en = array('name'=>'des_en','value'=>''.set_value('des_en',isset($default['des_en']) ? $default['des_en'] : '').'','cols'=>'54','rows'=>'5');
echo '<p><label class="lang-en">Description</label>'.form_textarea($des_en).'</p>';
echo '<p>'.form_submit('','Submit','class="button"').'</p>';
?>
</div>
<div style="width:230px; float:left;">
<?php
echo '<p><label>Type</label><select name="type" style="padding:4px;">
<option value="1" '.set_select('type', '1', isset($default['type']) && $default['type'] == '1' ? TRUE : FALSE).'>Post</option>
<option value="2" '.set_select('type', '2', isset($default['type']) && $default['type'] == '2' ? TRUE : FALSE).'>Gallery</option>';
echo '</select></p>';
echo '<p><label>Parent Category</label><select name="category" style="padding:4px;" class="fieldtitle hover">
<option value="0">Parent Category</option>';
if(count($kategori)>0){
    foreach($kategori as $k){
    echo '<option value="'.$k->category_id.'" '.set_select('category', ''.$k->category_id.'', isset($default['category_group']) && $default['category_group'] == $k->category_id ? TRUE : FALSE).'>'.$k->category_title.'</option>';
    }
}
echo '</select></p>';
if(!empty($default['image'])){
echo '<p><label>Image</label></p>
<div style="width:150px; height:100px; margin:0 5px 10px; float:left; position:relative;">
<div style="width:150px; height:100px; overflow:hidden;">
<img src="'.base_url().'uploads/category/'.$default['image'].'" width="150" />
<a href="'.$url.'delete_image/'.$this->session->userdata('category_id').'">      
<img src="'.base_url().'stylesheets/backend/remove.png" style="position:absolute; bottom:-5px; right:-5px;" /></a>
</div></div>';
}
echo '<p>'.form_upload('image').'</p>';
echo '<p class="error">filetype allowed : .gif .jpg .jpeg .png</p>';
?>
</div>
</div>