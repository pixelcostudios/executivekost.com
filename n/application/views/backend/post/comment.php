<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
?>
</div>
<div id="data">
<?php
echo ! empty($table) ? $table : '';
echo ! empty($pagination) ? "<div class=\"page_num\">" . $pagination . '</div>' : '';
?>
</div>