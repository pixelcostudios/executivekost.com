<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<div id="navigation">
<ul>
	<li class="active"><a href="<?php echo $data_link?>">List Data</a></li>
    <li><a href="<?php echo $form_link?>"><?php echo $this->session->userdata('form_mode')?> Data</a></li>
</ul>
<div class="clr"></div>
</div>
<!--<input type="button" value="add" class="button" style="float:left; margin:0 5px 0 0;" onclick="location.href='<?php echo $action?>'" />
<form action="<?php echo base_url();?>backend/post/search" method="post" style="float:right;">
<input name="post_category_id" type="hidden" value="<?php echo $this->session->userdata('post_category_id'); ?>" />
<input type="text" name="search" class="auto" />
<input type="submit" name="check" value="Search ..." class="button" />
</form>
<div class="clr"></div>-->
<?php
echo ! empty($pagination) ? "<div class=\"page_num\">" . $pagination . '</div>' : '';
echo ! empty($table) ? $table : '';
echo ! empty($pagination) ? "<div class=\"page_num\">" . $pagination . '</div>' : '';
?>
</div>
<?php
echo '<table style="width: auto;">';
echo '<tr>';
echo '<td><span class="edit"></span> edit</td>';
echo '<td><span class="delete"></span> delete</td>';
echo '</tr>';
echo '</table>';
?>
</div>