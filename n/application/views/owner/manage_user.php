<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<input type="button" value="add" class="button" style="float:left; margin:0 5px 0 0;" onclick="location.href='<?php echo $action?>'" />
<div class="clr"></div>
<?php
echo ! empty($table) ? $table : '';
echo ! empty($pagination) ? "<div class=\"page_num\">" . $pagination . '</div>' : '';
?>
</div>