<div id="head">
<?php
echo ! empty($h3_title) ? '<h3>' . $h3_title . '</h3>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="invoice">
<div style="width: 49%; float:left; margin-right: 2%">
<?php
echo '<table>
	  <tr><th colspan="3">Personal Information</th>
	  <tr><td>Name</td>					<td> : </td><td>'.$member_name.'</td></tr>
	  <tr><td>Identity</td>				<td> : </td><td>'.$member_identity.' [ '.$member_identity_number.' ]</td></tr>
	  <tr><td>Birthday</td>				<td> : </td><td>'.date("d F Y",strtotime($member_birthday)).'</td></tr>
	  <tr><td>Phone</td>				<td> : </td><td>'.$member_phone.'</td></tr>
	  <tr><td>Email</td>				<td> : </td><td>'.$member_email.'</td></tr>
	  <tr><td>Address</td>				<td> : </td><td>'.$member_address.'</td></tr>
	  </table>';
	  
// confirm
$confirm = $this->site_model->get_data('',"tb_confirms","member_id = '".$member_id."'")->row();
if(count($confirm)!=0 AND $rent_status=='1'){
echo '<table>
	  <tr><th colspan="3">Member Confirm</th>
	  <tr><td>Booking Code</td>			<td> : </td><td>'.$confirm->booking_code.'</td></tr>
	  <tr><td>Confirm Date</td>			<td> : </td><td>'.date("d F Y | H:i:s",strtotime($confirm->confirm_date)).'</td></tr>
	  <tr><td>Transfer Date</td>		<td> : </td><td>'.date("d F Y | H:i:s",strtotime($confirm->transfer_date)).'</td></tr>
	  <tr><td>Total Pay</td>			<td> : </td><td>Rp. '.strrev(implode('.',str_split(strrev(strval($confirm->total_pay)),3))).',-</td></tr>
	  <tr><td>Bank</td>					<td> : </td><td style="text-transform: uppercase;">'.$confirm->bank.'</td></tr>
	  <tr><td>Bank Account Name</td>	<td> : </td><td>'.$confirm->bank_account_number.'</td></tr>
	  <tr><td>Bank Account Number</td>	<td> : </td><td>'.$confirm->bank_account_name.'</td></tr>
	  <tr><td>Transfer Message</td>		<td> : </td><td>'.$confirm->transfer_message.'</td></tr>
	  </table>';
}
?>
</div>
<div style="width: 49%; float:left;">
<?php
$exp_date = strtotime("".$time_order."+3 hours");
if($rent_status=='2'){
	$style = "background-color: #00FF00; color: #FFF";
	$status = "paid";
}
else{
	if($exp_date < now()){
		$style = "background-color: #FF0000; color: #FFF";
		$status = "expired";
	}
	else{
		$style = "background-color: #FFA500; color: #FFF";
		$status = "pending";
	}
}
echo '<table>';
echo '<tr><th colspan="3">Kost Order Information</th>';
echo '<tr><td>Kost Name</td>			<td> : </td><td>'.$kost_title.'</td></tr>';
echo '<tr><td>Kost Room Number</td>		<td> : </td><td>'.$kost_room_number.'</td></tr>';
echo '<tr><td>Check In</td>				<td> : </td><td>'.date("d F Y",strtotime($check_in)).'</td></tr>';
echo '<tr><td>Check Out</td>			<td> : </td><td>'.date("d F Y",strtotime($check_out)).'</td></tr>';
echo '<tr><td>Special Request</td>		<td> : </td><td>'.$special_request.'</td></tr>';
	if($rent_status!='3'){		
		echo '<tr><td>Time Order</td>	<td> : </td><td>'.date("d F Y | H:i:s",strtotime($time_order)).'</td></tr>';
		echo '<tr><td>Order Expired</td><td> : </td><td>'.date("d F Y | H:i:s",$exp_date).'</td></tr>';
		echo '<tr><td>Order Status</td>	<td> : </td><td style="'.$style.'">'.$status.'';
		echo '<form method="post" action="'.base_url().'owner_backend/finish">';
		echo '<input type="hidden" name="booking_id" value="'.$rent_kost_id.'" />';
		echo '<input type="hidden" name="member_id" value="'.$member_id.'" />';
		if($exp_date < now() OR $rent_status=='2'){}else{
			echo '<tr><td>Order.Code</td><td> : </td><td><input type="text" name="order_code" /></td></tr>';
			echo '<tr><td colspan="2"></td><td><input type="submit" value="finish" class="button" /></td></tr>';
		}
	}
echo '</table>';
?>
</div>
<div class="clr"></div>
</div>
<?php
if($exp_date < now() OR $rent_status=='2'){}else{
	echo '<table style="width: auto;">';
	echo '<tr>';
	echo '<td colspan="2">status : </td>';
	echo '<td><span style="padding:0 5px; background-color: #00FF00;">&nbsp;</span> confirm</td>';
	echo '<td><span style="padding:0 5px; background-color: #FFA500;">&nbsp;</span> pending</td>';
	echo '<td><span style="padding:0 5px; background-color: #FF0000;">&nbsp;</span> expired</td>';
	echo '</tr>';
	echo '</table>';
}
?>
</div>