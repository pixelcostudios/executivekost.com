<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<?php
echo form_open_multipart(''.$action.'');
echo form_hidden('user_login_id',''.set_value('user_login_id', isset($user_login_id) ? $user_login_id : $this->uri->segment(4)).'');
?>
<div style="width:350px; float:left; margin-right: 10px;">
<?php
if($this->session->userdata('form_mode')=='edit'){
echo '<p><label>Enter your present password to continue:</label>'.form_password('old_password',''.set_value('old_password').'','style="width: 300px"').'</p><br />';
}
echo '<p><label>:: Nick Name & Username</label></p>';
echo '<p><label>Nick Name</label>'.form_input('nickname',''.set_value('nickname', isset($nickname) ? $nickname : '').'','style="width: 300px"').'</p>'.form_error('nickname', '<p class="error">', '</p>');
echo '<p><label>Username</label>'.form_input('username',''.set_value('username', isset($username) ? $username : '').'','style="width: 300px"').'</p>';
echo '<br /><p><label>:: Password</label></p>';
echo '<p><label>New Password</label>'.form_password('new_password',''.set_value('new_password').'','style="width: 300px"').'</p>';
echo '<p><label>Confirm Password</label>'.form_password('conf_password',''.set_value('conf_password').'','style="width: 300px"').'</p>';
echo '<p>'.form_submit('','Submit','class="button"').'</p>';
?>
</div>
<div style="width:300px; float:left; margin-right: 10px;">
<?php
if($user_login_id != $this->session->userdata('ses_owner_id')){
	/*echo '<p><label>User Type</label><select name="type" style="padding:4px;">
	<option value="investor" '.set_select('type', 'investor', isset($default['type']) && $default['type'] == 'investor' ? TRUE : FALSE).'>Investor</option>
	<option value="guard" '.set_select('type', 'guard', isset($default['type']) && $default['type'] == 'guard' ? TRUE : FALSE).'>Guard</option>';
	echo '</select></p>';*/
	echo '<p><label>Kost Name</label><select name="kost_id" style="padding:4px;" class="fieldtitle hover">';
	if(count($kosts)>0){
		foreach($kosts as $k){
		echo '<option value="'.$k->kost_id.'" '.set_select('kost_id', ''.$k->kost_id.'', isset($kost_id) && $kost_id == $k->kost_id ? TRUE : FALSE).'>'.$k->kost_title.'</option>';
    	}
	}
	echo '</select></p>';
}
else{
	echo form_hidden('kost_id',''.set_value('kost_id', isset($kost_id) ? $kost_id : '').'');
}
?>
</div>
<div class="clr"></div>
</div>