<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Owner Administrator</title>
<link type="image/x-icon" rel="shortcut icon" href="<?php echo base_url();?>images/favicon.ico" />
<link href="<?php echo base_url();?>stylesheets/owner.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>javascripts/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>javascripts/backend-app.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>javascripts/redactor/redactor.js"></script>	
<link rel="stylesheet" href="<?php echo base_url();?>javascripts/redactor/redactor.css" />
</head>

<body>
<div id="main">
<div id="menu">
<ul id="navbar">
    <li><a href="<?php echo base_url()?>owner_backend/home"><img src="<?php echo base_url()?>stylesheets/images/logo.png" width="50" /></a>
    <li><a href="<?php echo base_url()?>owner_backend/home"><img src="<?php echo base_url()?>stylesheets/backend/home.png" />Kost</a>    
    <ul>
    <?php
	$kost = $this->site_model->get_data('',"tb_kosts k,tb_user_kost uk","k.kost_id = uk.kost_id AND user_id = '".$this->session->userdata('owner_id')."'")->result();
	foreach($kost as $a){
	?>
    	<li><a href="<?php echo base_url().'owner_backend/kost/'.$a->kost_id?>"><?php echo $a->kost_title?></a></li>
    <?php
	}
	?>
    </ul>
    </li>
    <li><a href="http://bausasran.dyndns.org/" target="_blank"><img src="<?php echo base_url()?>stylesheets/backend/camera.png" />Camera</a></li>
    <li><a href="<?php echo base_url()?>owner_backend/setting/"><img src="<?php echo base_url()?>stylesheets/backend/user.png" /><?php echo $this->session->userdata('owner_username')?></a>
    <ul>
        <li><a href="<?php echo base_url()?>owner_backend/setting/edit/<?php echo $this->session->userdata('ses_owner_id')?>"><img src="<?php echo base_url()?>stylesheets/backend/secure.png" />Change Password</a>
        <li><a href="<?php echo base_url()?>owner_backend/logout"><img src="<?php echo base_url()?>stylesheets/backend/logout.png" />log out</a>
    </ul>
    </li>
    <?php $now = date("d F Y");?>
    <li style="float: right;"><a href="#"><font style="color: #666;">Date</font> <?php echo $now?></a></li>
</ul>
</div>
<div class="clr"></div>
</div>
<div id="content"><?php $this->load->view($content)?></div>
</body>
</html>