<?php
$this->load->view('owner/room_menu');
?>
<div class="head">
<h3><?php echo $kost_title ?></h3>
<h3>NERACA</h3>
<h3>Tanggal: <?php echo date('d-m-Y') ?></h3>
</div>
<div id="data">
<table width="600" border="0" class="table_list">
  <tr>
    <th width="91">Kode Akun</th>
    <th width="286">Nama Akun</th>
    <th width="99">&nbsp;</th>
    <th width="106">&nbsp;</th>
  </tr>
  <?php if(count($neraca)==0){ ?>
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <?php 
  }else{
	$totdebit=0;
	$totkredit=0;
	$totpajak=0;
	$total=0;
	$totaljenis=0;
	$namajenisakun='';
  foreach($neraca as $list){
	  if($list['possaldo']=='DB'){
		$debit=$list['jmlbesaran'];
		$kredit='';
	  }else{
		$debit='';
		$kredit=$list['jmlbesaran'];
	  }
		  
	  $totdebit+=$debit;
	  $totkredit+=$kredit;
	  //$totpajak+=$list['pajak'];
	  $total+=($debit-$kredit);//-$list['pajak']);
	  
		if($namajenisakun=='' OR $namajenisakun==$list['namajenisakun']){
	  ?>
  <tr>
    <td><?php echo $list['kodeakun'] ?></td>
    <td><?php echo $list['namaakun'] ?></td>
    <td align="right"><?php echo number_format($list['jmlbesaran'],0,',','.') ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <?php }else{?>
  <tr style="font-weight:bold;">
    <td>&nbsp;</td>
    <td> <?php echo strtoupper('TOTAL '.$namajenisakun) ?></td>
    <td align="right">&nbsp;</td>
    <td align="right"><?php echo number_format($totaljenis,0,',','.') ?></td>
  </tr>
  <tr>
    <td><?php echo $list['kodeakun'] ?></td>
    <td><?php echo $list['namaakun'] ?></td>
    <td align="right"><?php echo number_format($list['jmlbesaran'],0,',','.') ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <?php
  
  }
  if($namajenisakun==$list['namajenisakun'])
	  	$totaljenis+=$list['jmlbesaran'];
	  else
	  	$totaljenis=$list['jmlbesaran'];
  $namajenisakun=$list['namajenisakun'];
  
  ?>
  
  
  <?php }}  
  if(count($neraca)>0){
  ?>
  
  <tr style="font-weight:bold;">
    <td>&nbsp;</td>
    <td> <?php echo strtoupper('TOTAL '.$namajenisakun) ?></td>
    <td align="right">&nbsp;</td>
    <td align="right"><?php echo number_format($totaljenis,0,',','.') ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr style="font-weight:bold;">
    <td>&nbsp;</td>
    <td>Laba Bersih</td>
    <td align="right">&nbsp;</td>
    <td align="right">
	<?php 
	if($total<0)
	echo "<div style=\"color:#f00;\">".number_format($total,0,',','.')."</div>";
    else
    echo number_format($total,0,',','.');
	?>
    </td>
  </tr>
  <?php } ?>
</table>
</div>