<?php
$this->load->view('owner/room_menu');
?>
<div class="head"><h3>Setting Akun Transaksi</h3></div>
<div id="data">
<form name="form1" method="post" action="<?php echo base_url()."owner_backend/accounting/".$this->uri->segment(3)."/settingpajak/proses" ?>">
  <table width="700" border="0" class="table_list">
    <tr>
      <td width="153">Kode Akun Pajak</td>
      <td width="10">&nbsp;</td>
      <td width="528"><select name="idjenisakunpajak" id="idjenisakunpajak">
        <option value="">[-pilih dahulu-]</option>
        <?php foreach($akun as $list){ ?>
        <option value="<?php echo $list['idakun'] ?>" <?php echo ($list['idakun']==$idakunpajak)?'selected':'' ?>><?php echo $list['kodejenisakun'] ?>-<?php echo $list['kodeakun'] ?> <?php echo $list['namaakun'] ?></option>
        <?php } ?>
      </select>
        <input name="mode" type="hidden" id="mode" value="<?php echo $mode ?>" />
      <input name="idsetting" type="hidden" id="idsetting" value="<?php echo $idsetting ?>" /></td>
    </tr>
    <tr>
      <td>Kode Akun Sewa Kamar</td>
      <td>&nbsp;</td>
      <td><select name="idjenisakunsewa" id="idjenisakunsewa">
        <option value="">[-pilih dahulu-]</option>
        <?php foreach($akun as $list){ ?>
        <option value="<?php echo $list['idakun'] ?>" <?php echo ($list['idakun']==$idakunsewa)?'selected':'' ?>><?php echo $list['kodejenisakun'] ?>-<?php echo $list['kodeakun'] ?> <?php echo $list['namaakun'] ?></option>
        <?php } ?>
      </select></td>
    </tr>
    <tr>
      <td>Kode Akun Penjualan</td>
      <td>&nbsp;</td>
      <td><select name="idjenisakunpenjualan" id="idjenisakunpenjualan">
        <option value="">[-pilih dahulu-]</option>
        <?php foreach($akun as $list){ ?>
        <option value="<?php echo $list['idakun'] ?>" <?php echo ($list['idakun']==$idakunpenjualan)?'selected':'' ?>><?php echo $list['kodejenisakun'] ?>-<?php echo $list['kodeakun'] ?> <?php echo $list['namaakun'] ?></option>
        <?php } ?>
      </select></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="submit" name="button" id="button" value="Submit"></td>
    </tr>
  </table>
</form>
</div>