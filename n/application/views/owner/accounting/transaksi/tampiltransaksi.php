<?php
$this->load->view('owner/room_menu');
?>
<div class="head"><h3>Transaksi</h3></div>
<div id="data">
<input type="button" value="add" class="button" style="float:left; margin:0 5px 0 0;" onclick="location.href='<?php echo base_url().'owner_backend/accounting/'.$this->uri->segment(3).'/transaksi/input-transaksi'?>'" />
<div class="clr"></div>
<br />

<form id="form1" name="form1" method="post" action="<?php echo base_url().'owner_backend/accounting/'.$this->uri->segment(3).'/transaksi/tampil-transaksi'?>">
Tanggal Transaksi:<input type="text" name="tanggaltransaksi" id="tanggaltransaksi" />  <input type="submit" name="button" id="button" value="Submit" class="button" />
</form>
<table width="880" border="0" class="table_list">
  <tr>
    <th width="84" height="22" align="center">Tanggal</th>
    <th width="83" align="center">Bukti Transaksi</th>
    <th width="239" align="center">Uraian</th>
    <th width="80" align="center">Debet(Rp.)</th>
    <th width="81" align="center">Kredit(Rp.)</th>
    <th width="83" align="center">Pajak(10%)</th>
    <th width="166" align="center">Saldo</th>
    <!--<th width="30" align="center">&nbsp;</th>-->
  </tr>
  <?php
  if(count($transaksi)==0){
  ?>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <?php
  }else{
	  $totdebit=0;
	  $totkredit=0;
	  $totpajak=0;
	  $total=0;
	  foreach($transaksi as $list){
		  
		  $debit=$list['debit'];
		  $kredit=$list['kredit'];
		  $totdebit+=$debit;
	  	  $totkredit+=$kredit;
	  	  $totpajak+=$list['pajak'];
	  	  $total+=($debit-$kredit-$list['pajak']);
	  ?>
  <tr>
    <td><?php echo $list['tanggaltransaksi'] ?></td>
    <td><?php echo $list['notransaksi'] ?></td>
    <td><?php echo $list['uraian'] ?></td>
    <td align="right"><?php echo ($debit!='')?number_format($debit,0,',','.'):$debit ?></td>
    <td align="right"><?php echo ($kredit!='')?number_format($kredit,0,',','.'):$kredit ?></td>
    <td align="right"><?php echo ($list['pajak']==0)?'':number_format($list['pajak'],0,',','.') ?></td>
    <td align="right">
	<?php 
	if($total<0)
	echo "<div style=\"color:#f00;\">".number_format($total,0,',','.')."</div>";
    else
    echo number_format($total,0,',','.');
	?>
    
    </td>
    <!--<td align="right"><a href="<?php echo base_url()."owner_backend/accounting/sewakamar/".$this->uri->segment(4)."/edit-transaksi/".$list['idtransaksi'] ?>" class="edit">edit</a><br />
    <a href="<?php echo base_url()."owner_backend/accounting/sewakamar/".$this->uri->segment(4)."/delete-transaksi/".$list['idtransaksi'] ?>" class="delete">del</a></td>-->
  </tr>
  
  <?php
  }}
  
  if(count($transaksi)>0){
  ?>
  
  <tr style="font-weight:bold">
    <td colspan="3" align="right">Jumlah Penerimaan/Pengeluaran</td>
    <td align="right"><?php echo number_format($totdebit,0,',','.') ?></td>
    <td align="right"><?php echo number_format($totkredit,0,',','.') ?></td>
    <td align="right"><?php echo number_format($totpajak,0,',','.') ?></td>
    <td align="right">
	<?php 
	if($total<0)
	echo "<div style=\"color:#f00;\">".number_format($total,0,',','.')."</div>";
    else
    echo number_format($total,0,',','.');
	?>
    </td>
    <!--<td align="right">&nbsp;</td>-->
  </tr>
  <?php } ?>
</table>
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/date-picker/jquery-ui.css" />
<script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#tanggaltransaksi" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
</script>