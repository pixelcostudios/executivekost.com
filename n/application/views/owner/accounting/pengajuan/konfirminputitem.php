<?php
$this->load->view('owner/room_menu');
?>
<div class="head"><h3>Pengajuan Belanja</h3></div>
<div id="data">
<form name="form1" method="post" action="<?php echo base_url().'owner_backend/accounting/'.$this->uri->segment(3).'/belanja/proses-input-items' ?>">
  Tanggal:
    <input name="tanggalpengajuan" type="text" id="tanggalpengajuan" readonly="readonly" value="<?php echo $tanggal ?>">
    <input name="jmlitem" type="hidden" id="jmlitem" value="<?php echo $jmlitem ?>">
<table width="700" border="0" class="table_list">
    <tr>
      <th width="241" align="center">Uraian</th>
      <th width="130" align="center">Satuan</th>
      <th width="107" align="center">Jumlah</th>
      <th width="204" align="center">Harga Satuan</th>
      <th width="204" align="center">Total</th>
    </tr>
    <?php 
	$totals=0;
	for($i=1;$i<=$jmlitem;$i++){ 
	if($uraian[$i]!='' AND $satuan[$i]!='' AND $jumlah[$i]!='' AND $harga[$i]!=''){
	?>
    <tr>
      <td><input type="hidden" name="uraian[<?php echo $i ?>]" id="uraian" value="<?php echo $uraian[$i] ?>" /><?php echo $uraian[$i] ?></td>
      <td align="center"><input type="hidden" name="satuan[<?php echo $i ?>]" id="satuan" value="<?php echo $satuan[$i] ?>"><?php echo $satuan[$i] ?></td>
      <td align="center"><input name="jumlah[<?php echo $i ?>]" type="hidden" id="jumlah" value="<?php echo $jumlah[$i] ?>"><?php echo $jumlah[$i] ?></td>
      <td align="right"><input type="hidden" name="harga[<?php echo $i ?>]" id="harga" value="<?php echo $harga[$i] ?>">Rp. <?php echo number_format($harga[$i],0,',','.') ?></td>
      <td align="right">
      Rp. <?php
	  $total=$jumlah[$i]*$harga[$i];
	  $totals+=$total;
	  echo number_format($total,0,',','.');
	  ?>
      </td>
    </tr>
    <?php } } ?>
    <tr>
      <td colspan="5" align="right">Rp. <?php echo number_format($totals,0,',','.'); ?></td>
    </tr>
    <tr>
      <td colspan="5" align="right"><input type="submit" name="Submit" id="Submit" value="Proses"></td>
    </tr>
  </table>
</form>
</div>