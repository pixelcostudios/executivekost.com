<div id="sidebar">
	<div id="sidebar-header">
	<?php $forhead = $this->site_model->get_data('',"tb_options","option_id = '1'")->row(); ?>
    <a href="#"><img src="<?php echo base_url()?>uploads/<?php echo $forhead->image?>" /></a>
    </div>
    
	<div id="sidebar-content">
        <div id="how-to-order">
        <?php
		$page = $this->site_model->get_data('',"tb_pages","page_id = '5' AND page_status = '1'")->row();
		?>
        <h1><?php echo $page->page_title?></h1>
        <p><?php echo $page->page_content?></p>
        <a href="<?php echo base_url()?>page/<?php echo $page->page_name?>.html"><img src="<?php echo base_url()?>stylesheets/images/see-detail-button.jpg" /></a>
        </div>
                
        <div id="operator">
            <?php
			$op = $this->site_model->get_data("op_1","tb_options","option_id = '1'")->row();
			$x = explode(",",$op->op_1);	
			echo '<div class="operator">';	
			ymstatus("".$x[1]."",base_url()."stylesheets/images/yahoo-online.png",base_url()."stylesheets/images/yahoo-offline.png","".$x[0]."");
			echo '<p>Customer Service : '.$x[0].'</p>';
			echo '</div>';
			
			$op = $this->site_model->get_data("op_2","tb_options","option_id = '1'")->row();
			$x = explode(",",$op->op_2);	
			echo '<div class="operator">';	
			ymstatus("".$x[1]."",base_url()."stylesheets/images/yahoo-online.png",base_url()."stylesheets/images/yahoo-offline.png","".$x[0]."");
			echo '<p>Customer Service : '.$x[0].'</p>';
			echo '</div>';
			?>
        </div>
        <?php if($this->uri->segment(2)=='detail' OR $this->uri->segment(2)=='room' OR $this->uri->segment(1)=='member' AND $this->session->userdata('kost_id')!=''){?>
        <div id="information-room">
        <h1>Information Room</h1>
        <h2>Room of <b><?php echo $this->session->userdata('kost_title')?></b></h2>
        <div id="room-number">
        <p>Monthly Room :</p>
        <ul>
        <?php
		$kost_room = $this->site_model->get_data('',"tb_kost_room","kost_id = '".$this->session->userdata('kost_id')."' AND kost_room_type = '1'",'',"kost_room_number ASC")->result();
		foreach($kost_room as $a){
			$date = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$a->kost_room_id."'")->row();
			if($a->kost_room_status!='0'){
				$class = 'class="available"';
				$date = '<span>'.date("M",strtotime($date->check_out)).'</span>';
			}
			else{
				$class = ''; $date = '';
			}
			echo '<li><a href="'.base_url().'kost/room/'.$a->kost_room_id.'" '.$class.'>'.$date.$a->kost_room_number.'</a></li>';
		}
		?>
        </ul>
        <div class="clr"></div>
        <p>Daily Room :</p>
        <ul>
        <?php
		$kost_room = $this->site_model->get_data('',"tb_kost_room","kost_id = '".$this->session->userdata('kost_id')."' AND kost_room_type = '2'",'',"kost_room_number ASC")->result();
		foreach($kost_room as $a){
			$date = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$a->kost_room_id."'")->row();
			if($a->kost_room_status!='0'){
				$class = 'class="available"';
				$date = '<span>'.date("M",strtotime($date->check_out)).'</span>';
			}
			else{
				$class = ''; $date = '';
			}
			echo '<li><a href="'.base_url().'kost/room/'.$a->kost_room_id.'" '.$class.'>'.$date.$a->kost_room_number.'</a></li>';
		}
		?>
        </ul>
        <div class="clr"></div>
        <div id="status">
        	<div class="status"><img src="<?php echo base_url()?>stylesheets/images/room-number-status.jpg" />Unavailable</div>
            <div class="status"><img src="<?php echo base_url()?>stylesheets/images/room-number-status-2.jpg" />Available</div>
        </div>
        <div class="clr"></div>
        </div>
        </div>
        <?php } ?>
    </div>
</div>