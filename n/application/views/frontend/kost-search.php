<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a class="current">Search Kost</a></span>
        <?php
		if(count($search)!=0){
			foreach($search as $a){
				$img = $this->site_model->get_img("parent_id = '".$a['kost_id']."' AND relation = 'kost'","1")->row();
				if(count($img)!=0) $image = $img->dir.'/'.thumb($img->image); else $image = 'stylesheets/images/empty_image.jpg';
				echo '<div class="search">';
				echo heading($a['kost_title'],1);
				echo img(array('src'=>base_url().$image,'alt'=>$a['kost_title'],'title'=>$a['kost_title'],'width'=>'125'));
				echo '<div class="search-desc">';
				//echo '<h2>Room Available : <span>3 Maret 2013</span></h2>';
				echo '<p>'.$a['kost_content'].'</p>';
				echo '<p><a href="'.base_url().'kost/detail/'.$a['kost_name'].'.html">Lihat Sekarang</a></p>';
				echo '</div>';
				echo '<div class="clr"></div>';
				echo '</div>';
			}
		}
		else{
			echo '<div class="search">';
			echo heading('not found ...',1);
			echo '</div>';
		}
		?>
        <?php echo ! empty($pagination) ? '<div class="page_num">' . $pagination . '</div>' : ''; ?> 
    	</div>        
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>