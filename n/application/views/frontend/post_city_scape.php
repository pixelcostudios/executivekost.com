<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">
    	<div id="city-scape">
        <?php echo heading($post_title,1); ?>
        <div id="social-media">
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
        <a class="addthis_button_tweet"></a>
        <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
        <a class="addthis_button_compact"></a>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50f0cbe33d8a334b"></script>
        <!-- AddThis Button END -->
        </div>
		<?php echo $post_content; ?>
        <script type="text/javascript">
		//$("#city-scape img").wrap('<div class="image-frame"></div>');
		//$("#city-scape img").after('<label>'+$("#city-scape img").attr("title")+'</label><div class="clr"></div>');
		</script>
        </div>
    </div>
</div>
<div class="clr"></div>
</div>