<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">      
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/lightbox/jquery.lightbox-0.5.css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>javascripts/lightbox/jquery.lightbox-0.5.js"></script>
        <script type="text/javascript">
        $(function() {
            $('.image a').lightBox({
                imageLoading: '<?php echo base_url(); ?>javascripts/lightbox/images/lightbox-ico-loading.gif',
                imageBtnPrev: '<?php echo base_url(); ?>javascripts/lightbox/images/lightbox-btn-prev.gif',
                imageBtnNext: '<?php echo base_url(); ?>javascripts/lightbox/images/lightbox-btn-next.gif',
           });
        });
		</script>
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url();?>">Home</a> / <a href="<?php echo base_url();?>kost">Kost</a> / <a href="<?php echo base_url();?>kost/detail/<?php echo $this->session->userdata('kost_name')?>.html"><?php echo $this->session->userdata('kost_title')?></a> / <a class="current">Detail Kost Room</a></span>       
        <div id="social-media">
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
        <a class="addthis_button_tweet"></a>
        <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
        <a class="addthis_button_compact"></a>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50f0cbe33d8a334b"></script>
        <!-- AddThis Button END -->
        </div>
        <h1><?php echo $kost_room_title?></h1>
        <?php echo $kost_room_content?>
    	</div>        
        
		<div id="content-sidebar">
            <a href="<?php echo base_url()?>member" class="booking-button">BOOKING NOW</a>
        	<div class="content-sidebar">
			<?php 
            echo '<div class="image"><a href="'.base_url().$img1->dir.'/'.$img1->image.'">';
            echo isset($img1->image)?img(array('src'=>base_url().$img1->dir.'/'.$img1->image,'alt'=>$kost_title,'title'=>$kost_title)):'';
            echo '</a></div>';
            if(count($img2) > 0){
                foreach($img2 as $a){
                    echo '<div class="rel-img image"><a href="'.base_url().$img1->dir.'/'.$a->image.'">';
                    echo img(array('src'=>''.base_url().$img1->dir.'/'.thumb($a->image).'','class'=>'thumb','alt'=>$kost_title,'title'=>$kost_title));
                    echo '</a></div>';
                }
            }
            ?>
        	<div class="clr"></div>
        	</div>
        	<div class="content-sidebar">     
            <h1>DENAH EKOST</h1>
            <?php echo img(array('src'=>base_url().$kost_sketch,'alt'=>$kost_title,'title'=>$kost_title))?>
        	</div>
        </div>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>