<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="archive">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a class="current"><?php echo $category->category_title?></a></span>
        <?php
		foreach($archive as $a){
			echo '<div class="archive">';
			echo heading(anchor(base_url().$a['post_name'].'.html',$a['post_title'],array('title'=>$a['post_title'])),1);
			echo '<p>'.$a['post_content'].'</p>';
			echo '</div>';
		}
		?>
        <?php echo ! empty($pagination) ? '<div class="page_num">' . $pagination . '</div>' : ''; ?> 
    	</div>        
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>