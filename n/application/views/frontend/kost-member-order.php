<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a href="<?php echo base_url()?>kost/detail/<?php echo $this->session->userdata('kost_name')?>.html">Kost</a> / <a class="current">Reservation</a></span>
        <?php
		$flashmessage = $this->session->flashdata('message');
		echo !empty($flashmessage)?'<p class="message fadeout">'.$flashmessage.'</p>': '';
		?>
        <div id="reservation">
        <ul>
        <li class="active"><a href="<?php echo base_url()?>member/order">Order Status</a></li>
        <li><a href="<?php echo base_url()?>member/profile">Personal Information</a></li>
        <div class="clr"></div>
        </ul>
        <div class="clr"></div>
                
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/date-picker/jquery-ui.css" />
        <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url()?>javascripts/date-picker/jquery-ui-timepicker-addon.css" />
        <script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui-sliderAccess.js"></script>
		<script type="text/javascript">
			$(function() {
				$("#transfer_date").datetimepicker({timeFormat: 'HH:mm:ss',showSecond: true, changeMonth: true,changeYear: true});
				$("#check_in").datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
				$("#check_out").datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
			});
        </script>
        <?php
        //$exp_date = strtotime("".$time_order."+3 hours");
		//echo $exp_date.br(1).now();
        if($rent_status=='2'){
            $style = "background-color: #55FF55; color: #FFF";
            $status = "terbayar";
        }
        else{
            if($rent_status=='3'){
                $style = "background-color: #036; color: #FFF";
                $status = "expired";
            }
            else{
                $style = "background-color: #FF5555; color: #FFF";
                $status = "belum terbayar";
            }
        }
        ?>
        <fieldset>
        <legend>Kost Order Detail</legend>
        <table class="reservation">
        <tr><td width="120">Kost Name</td>				<td> : </td><td><?php echo anchor(base_url().'kost/detail/'.$kost_name.'.html',$kost_title) ?></td></tr>
        <tr><td>Room Number</td>						<td> : </td><td><?php echo $kost_room_number ?></td></tr>
        <tr><td>Check In</td>							<td> : </td><td><?php echo date("d F Y",strtotime($check_in))?></td></tr>
        <tr><td>Check Out</td>							<td> : </td><td><?php echo date("d F Y",strtotime($check_out)) ?></td></tr>
        <tr><td>Special Request <i>(optional)</i></td>	<td> : </td><td><?php echo $special_request ?></td></tr>
        <tr><td>Status</td>								<td> : </td><td <?php echo 'style="'.$style.'"'?>><?php echo $status?></td></tr>
        </table>
        </fieldset>
        <?php
		if(strtotime(date("H:i")) >= strtotime(date("H:i",mktime(20,0,0,0,0,0))) OR strtotime(date("H:i")) <= strtotime(date("H:i",mktime(8,0,0,0,0,0)))){
			if($this->session->userdata("lang")=="id"){
				echo "<p>maaf batas waktu reservasi 20:00 - 08.00, kembali lagi besok ....</p>";
				echo "<p>terima kasih.</p>";
			}
			else{
				echo "sorry ";
			}
		}
		else{
        if($rent_status=='3'){
		?>
        <fieldset>
        <legend>Reorder Kost</legend>
        <form method="post" action="<?php echo base_url()?>member/re_order">
        <input type="hidden" name="kost_id" value="<?php echo $this->session->userdata('kost_id')?>" />
        <input type="hidden" name="email" value="<?php echo $email?>" />
        <script type="text/javascript">
		// select room type
		function select_room(str){
			var xmlhttp;    
			if(str==""){
				document.getElementById("txtHint").innerHTML="";
				return;
			}
			if(window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function(){
				if(xmlhttp.readyState==4 && xmlhttp.status==200){
					document.getElementById("room_number").innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","<?php echo base_url()?>member/select_room/"+str,true);
			xmlhttp.send();
		}
		</script>
        <table class="reservation">
        <tr><td width="120">Kost Name</td><td> : </td><td><?php if($this->session->userdata('kost_title')!='') echo $this->session->userdata('kost_title'); else echo 'select kost first ...';?></td></tr>
        <tr><td>Select Room Type</td><td> : </td>
        <td>
        <select name="room_type" onchange="select_room(this.value)">
		<option value="">Room Type</option>
		<option value="1" <?php //echo set_select('room_type', '1');?>>Monthly</option>
		<option value="2" <?php //echo set_select('room_type', '2');?>>Daily</option>
        </select>
        <?php echo form_error('room_type', '<label class="error">', '</label>')?>
        </td>
        </tr>
        </table>
        <table class="reservation" id="room_number">
        <tr>
        <td width="120">Room Number</td><td> : </td>
        <td><select name="kost_number"></select><?php echo form_error('kost_number', '<label class="error">', '</label>')?></td>
        </tr>
        <tr><td>Check In</td><td> : </td><td><?php echo form_input('check_in',''.set_value('check_in').'','id="check_in"')?><?php echo form_error('check_in', '<label class="error">', '</label>')?></td></tr>
        <tr><td>Check Out</td><td> : </td><td><?php echo form_input('check_out',''.set_value('check_out').'','id="check_out"')?><?php echo form_error('check_out', '<label class="error">', '</label>')?></td></tr>
        <?php $special_request = array('name'=>'special_request','value'=>''.set_value('special_request').'','cols'=>'20','rows'=>'3'); ?>
        <tr><td>Special Request <i>(optional)</i></td><td> : </td><td><?php echo form_textarea($special_request)?></td></tr>
        <tr><td></td><td> : </td><td><?php echo $cap_img?><br /><?php echo form_input('captcha',''.set_value('captcha').'','id="captcha" maxlength="6"')?><?php echo $cap_msg?></td></tr>
        <tr><td colspan="2"></td><td><input type="submit" name="submit" value="register" /></tr>
        </table>
        </form>
        </fieldset>
        <?php } elseif($rent_status=='1'){?>            
        <fieldset>
        <legend>Confim</legend>
        <form method="post" action="<?php echo base_url()?>member/confirm">
        <input type="hidden" name="kost_id" value="<?php echo $kost_id?>" />
        <input type="hidden" name="kost_room_id" value="<?php echo $kost_room_id?>" />
        <table class="reservation">
        <tr><td width="120">Booking Code</td>	<td> : </td><td><?php echo form_input('booking_code',''.set_value('booking_code').'')?><?php echo form_error('booking_code', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>Transfer Date</td>				<td> : </td><td><?php echo form_input('transfer_date',''.set_value('transfer_date').'','id="transfer_date"')?><?php echo form_error('transfer_date', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>Total Pay</td>					<td> : </td><td><?php echo form_input('total_pay',''.set_value('total_pay').'')?>*)<br />*) 3500000<?php echo form_error('total_pay', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>Bank</td><td> : </td>
        <td>
        <select name="bank">
        <option value="bca" <?php echo set_select('bank', 'bca')?>>BCA</option>
        <option value="bni" <?php echo set_select('bank', 'bni');?>>BNI</option>
        <option value="bri" <?php echo set_select('bank', 'bri');?>>BRI</option>
        <option value="mandiri" <?php echo set_select('bank', 'mandiri');?>>MANDIRI</option>
        </select>
        </td>
        </tr>
        <tr><td>Bank Account Name</td>			<td> : </td><td><?php echo form_input('bank_account_name',''.set_value('bank_account_name').'')?><?php echo form_error('bank_account_name', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>Bank Account Number</td>		<td> : </td><td><?php echo form_input('bank_account_number',''.set_value('bank_account_number').'')?><?php echo form_error('bank_account_number', '<br /><label class="error">', '</label>')?></td></tr>
        <?php $transfer_message = array('name'=>'transfer_message','value'=>''.set_value('transfer_message').'','cols'=>'20','rows'=>'3'); ?>
        <tr><td>Transfer Message <i>(optional)</i></td><td> : </td><td><?php echo form_textarea($transfer_message)?></td></tr>
        <tr><td colspan="2"></td><td><input type="submit" name="submit" value="confirm" /></tr>
        </table>
        </form>
        </fieldset>
        <?php } } ?>
        </div>
        </div> 
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>