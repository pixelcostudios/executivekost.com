<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a href="<?php echo base_url()?>kost/detail/<?php echo $this->session->userdata('kost_name')?>.html">Kost</a> / <a class="current">Reservation</a></span>
        <h1>Reservation</h1>
        <?php
		$flashmessage = $this->session->flashdata('message');
		echo !empty($flashmessage)?'<p class="message fadeout">'.$flashmessage.'</p>': '';
		if(strtotime(date("H:i")) >= strtotime(date("H:i",mktime(20,0,0,0,0,0))) OR strtotime(date("H:i")) <= strtotime(date("H:i",mktime(8,0,0,0,0,0)))){
			if($this->session->userdata("lang")=="id"){
				echo "<p>maaf batas waktu reservasi 20:00 - 08.00, kembali lagi besok ....</p>";
				echo "<p>terima kasih.</p>";
			}
			else{
				echo "sorry ";
			}
		}
		else{
		?>
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/date-picker/jquery-ui.css" />
        <script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui.js"></script>
		<script type="text/javascript">
			$(function() {
				$( "#birthday" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true,});
				$( "#check_in" ).datepicker({
					dateFormat: 'yy-mm-dd',
					changeMonth: true,
					changeYear: true,
					minDate: 0,
					onClose: function( selectedDate ){
        				$( "#check_out" ).datepicker( "option", "minDate", selectedDate )
					}
				});
				$( "#check_out" ).datepicker({
					dateFormat: 'yy-mm-dd',
					changeMonth: true,
					changeYear: true,
					onClose: function( selectedDate ) {
						$( "#check_in" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
			});
            /*new datepickr('birthday',{'dateFormat': 'Y-m-d'});      
            new datepickr('check_in',{'dateFormat': 'Y-m-d'});    
            new datepickr('check_out',{'dateFormat': 'Y-m-d'});  */
        </script>
        <div id="reservation">
        <form method="post" action="<?php echo base_url()?>member/register">
        <fieldset>
        <legend>Login Information</legend>
        <table class="reservation">
        <tr><td width="110">Username</td><td> : </td><td><?php echo form_input('username',''.set_value('username').'')?><?php echo form_error('username', '<label class="error">', '</label>')?></td></tr>
        <tr><td>Password</td><td> : </td><td><?php echo form_password('password',''.set_value('password').'')?><?php echo form_error('password', '<label class="error">', '</label>')?></td></tr>
        </table>
        </fieldset>
        <fieldset>
        <legend>Personal Information</legend>
        <table class="reservation">
        <tr><td width="120">Name</td><td> : </td><td><?php echo form_input('name',''.set_value('name').'')?><?php echo form_error('name', '<label class="error">', '</label>')?></td></tr>
        <tr><td>Identity</td><td> : </td>
        <td>
        <select name="identity">
		<option value="ktm" <?php echo set_select('identity', 'ktm');?>>KTM</option>
		<option value="ktp" <?php echo set_select('identity', 'ktp');?>>KTP</option>
		<option value="sim" <?php echo set_select('identity', 'sim');?>>SIM</option>
		<option value="passport" <?php echo set_select('identity', 'passport');?>>Passport</option>
        </select>
        </td>
        </tr>
        <tr><td>Identity Number</td><td> : </td><td><?php echo form_input('identity_no',''.set_value('identity_no').'')?><?php echo form_error('identity_no', '<label class="error">', '</label>')?></td></tr>
        <tr><td>Birthday</td><td> : </td><td><?php echo form_input('birthday',''.set_value('birthday').'','id="birthday"')?><?php echo form_error('birthday', '<label class="error">', '</label>')?></td></tr>
        <tr><td>Phone</td><td> : </td><td><?php echo form_input('phone',''.set_value('phone').'')?><?php echo form_error('phone', '<label class="error">', '</label>')?></td></tr>
        <tr><td>Email</td><td> : </td><td><?php echo form_input('email',''.set_value('email').'')?><?php echo form_error('email', '<label class="error">', '</label>')?></td></tr>
        <?php $address = array('name'=>'address','value'=>''.set_value('address').'','cols'=>'20','rows'=>'3'); ?>
        <tr><td>Address</i></td><td> : </td><td><?php echo form_textarea($address)?></td></tr>
        </table>
        </fieldset>
        <fieldset>
        <legend>Kost Detail</legend>
        <input type="hidden" name="kost_id" value="<?php echo $this->session->userdata('kost_id')?>" /> 
        <table class="reservation">
        <tr><td width="120">Kost Name</td><td> : </td><td><?php if($this->session->userdata('kost_title')!='') echo $this->session->userdata('kost_title'); else echo 'select kost first ...';?></td></tr>
        <tr><td>Select Room Type</td><td> : </td>
        <td>
        <script type="text/javascript">
		function select_room(str){
			var xmlhttp;
			if(str==""){
				document.getElementById("txtHint").innerHTML="";
				return;
			}
			if(window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function(){
				if(xmlhttp.readyState==4 && xmlhttp.status==200){
					document.getElementById("room_number").innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","<?php echo base_url()?>member/select_room/"+str,true);
			xmlhttp.send();
		}
		</script>
        <select name="room_type" onchange="select_room(this.value)">
		<option value="">Room Type</option>
		<option value="1" <?php //echo set_select('room_type', '1');?>>Monthly</option>
		<option value="2" <?php //echo set_select('room_type', '2');?>>Daily</option>
        </select>
        <?php echo form_error('room_type', '<label class="error">', '</label>')?>
        </td>
        </tr>
        <tr id="room_number"><td>Room Number</td><td> : </td>
        <td>
        <select name="kost_number">
        </select><?php echo form_error('kost_number', '<label class="error">', '</label>')?>
        </td>
        </tr>
        <tr><td>Room Number</td><td> : </td><td><?php echo form_input('kost_number',''.set_value('kost_number').'','size="1"')?><?php echo form_error('kost_number', '<label class="error">', '</label>')?></td></tr>
        <tr><td>Check In</td><td> : </td><td><?php echo form_input('check_in',''.set_value('check_in').'','id="check_in"')?><?php echo form_error('check_in', '<label class="error">', '</label>')?></td></tr>
        <tr><td>Check Out</td><td> : </td><td><?php echo form_input('check_out',''.set_value('check_out').'','id="check_out"')?><?php echo form_error('check_out', '<labesl class="error">', '</label>')?></td></tr>
        <?php $special_request = array('name'=>'special_request','value'=>''.set_value('special_request').'','cols'=>'20','rows'=>'3'); ?>
        <tr><td>Special Request <i>(optional)</i></td><td> : </td><td><?php echo form_textarea($special_request)?></td></tr>
        <tr><td></td><td> : </td><td><?php echo $cap_img?><?php echo form_input('captcha',''.set_value('captcha').'','id="captcha" maxlength="6"')?><?php echo $cap_msg?></td></tr>
        <tr><td colspan="2"></td><td><input type="submit" name="submit" value="register" /></tr>
        </table>
        </fieldset>
        </form>
        </div>
        <?php } ?>
    	</div>        
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>