<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kost extends CI_Model{
	var $metahead;
	var $content;
	var $footer;
	function __construct(){
		parent::__construct();
	}
	// index
	function index(){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		$kota = html($this->uri->segment(2));
		
		if($kota==''){
			$condition = "";
		}
		else{
			$condition = " AND daerah_name = '".$kota."'";
		}
		$uri = 3;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$select = "k.kost_id,k.kost_title".$lang." as kost_title,k.kost_name".$lang." as kost_name,k.kost_content".$lang." as kost_content";
		$search = $this->site_model->get_data($select,"tb_kosts k,tb_daerah d","k.daerah_id = d.daerah_id".$condition."",'','',"$limit,$offset")->result();
		$num_rows = $this->site_model->get_data($select,"tb_kosts k,tb_daerah d","k.daerah_id = d.daerah_id".$condition."")->num_rows();
		if($num_rows > 0){
			$config['base_url'] 			= base_url()."kost/".$kota."/";
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			$this->content['pagination'] 	= $this->pagination->create_links();
			// data kosts
			$this->content['search'] = array();
			foreach($search as $a){
				$data['kost_id']			= $a->kost_id;
				$data['kost_title']			= $a->kost_title;
				$data['kost_name']			= $a->kost_name;
				$data['kost_content']		= summary($a->kost_content,20);
				$this->content['search'][]	= $data;
			}
		}
		
		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/kost-search',$this->content);
		$this->load->view('frontend/footer',$this->footer);
	}
	// search
	function search(){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		
		$daerah = $this->input->post('daerah');
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		//$date = 
		$select = "k.kost_id,k.kost_title".$lang." as kost_title,k.kost_name".$lang." as kost_name,k.kost_content".$lang." as kost_content";
		$where = "rk.kost_room_id = kr.kost_room_id AND kr.kost_id = k.kost_id AND k.daerah_id = '".$daerah."' AND (MONTH(rk.check_out) != ".$bulan." OR MONTH(rk.check_out) = 0) AND YEAR(rk.check_out) != ".$tahun."";
		//$where = "k.kost_id = kr.kost_id AND k.daerah_id = '".$daerah."'";
		$search = $this->site_model->get_data($select,"tb_rent_kost rk,tb_kost_room kr,tb_kosts k",$where,"k.kost_title")->result();
		$this->content['search'] = array();
		foreach($search as $a){
			$data['kost_id']			= $a->kost_id;
			$data['kost_title']			= $a->kost_title;
			$data['kost_name']			= $a->kost_name;
			$data['kost_content']		= summary($a->kost_content,20);
			$this->content['search'][]	= $data;
		}
		
		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/kost-search',$this->content);
		$this->load->view('frontend/footer',$this->footer);
	}
	// detail
	function detail($mode,$cap_msg=""){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		$slug = html($mode);		
		
		$select = "kost_id,kost_title".$lang." as kost_title,kost_name".$lang." as kost_name,kost_content".$lang." as kost_content,";
		$select.= "kost_facility".$lang." as kost_facility,kost_virtual_tour,kost_sketch,kost_map,kost_count";
		$kost = $this->site_model->get_data($select,"tb_kosts","kost_status = '1' AND kost_name = '".$slug."'")->row();
		if($slug=='' OR $kost==''){
			redirect(base_url());
		}
		else{
			// meta head
			$this->metahead['content_title']	= $kost->kost_title;
			// data kost
			$this->content['kost_id']			= $kost->kost_id;
			$this->content['kost_title']		= $kost->kost_title;
			$this->content['kost_name']			= $kost->kost_name;
			$this->content['kost_content']		= $kost->kost_content;
			$this->content['kost_facility']		= $kost->kost_facility;
			$this->content['kost_virtual_tour']	= $kost->kost_virtual_tour;
			$this->content['kost_sketch']		= $kost->kost_sketch;
			$this->content['kost_map']			= $kost->kost_map;
			
			$this->session->set_userdata('kost_id',$kost->kost_id);
			$this->session->set_userdata('kost_title',$kost->kost_title);
			$this->session->set_userdata('kost_name',$kost->kost_name);
			// image
			$this->content['img1'] = $this->site_model->get_img("parent_id = '".$kost->kost_id."' AND relation = 'kost'","1")->row();
			$this->content['img2'] = $this->site_model->get_img("parent_id = '".$kost->kost_id."' AND relation = 'kost'","1,4")->result();
			// comments
			$this->content['comments'] = $this->site_model->get_data('',"tb_comments","parent_id = '".$kost->kost_id."' AND parent_type = 'kost' AND comment_status = '1' AND comment_lang = '".$this->session->userdata('lang')."'",'',"comment_id desc")->result();
			// add count
			$this->site_model->update_data("tb_kosts",array('kost_count'=>$kost->kost_count+1),"kost_name = '".$slug."'");
			// captcha
			$captcha_result = '';
			$this->content["cap_img"] = $this->fm->_make_captcha();
			$this->content["cap_msg"] = $cap_msg;
			// room check
			$room_check = $this->site_model->get_data('',"tb_rent_kost rk,tb_kost_room kr","rk.kost_room_id = kr.kost_room_id AND kost_id = '".$kost->kost_id."' AND kost_room_type = '1' AND kr.kost_room_status = '1' AND DATE_ADD(rk.time_order,INTERVAL 3 HOUR) <= NOW()")->result();
			foreach($room_check as $x){
				$this->site_model->update_data("tb_kost_room",array('kost_room_status'=>'0'),"kost_room_id = '".$room_check->kost_room_id."'");
			}
			$order_check = $this->site_model->get_data('',"tb_rent_kost","DATE_ADD(time_order,INTERVAL 3 HOUR) <= NOW() AND rent_status != '2'")->result();
			foreach($order_check as $x){
				$this->site_model->update_data("tb_rent_kost",array('rent_status'=>'3'),"rent_kost_id = '".$x->rent_kost_id."'");
			}
			
			$this->load->view('frontend/header',$this->metahead);
			$this->load->view('frontend/kost-detail',$this->content);
			$this->load->view('frontend/footer',$this->footer);
		}
	}
	// send comment
	function send(){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
			
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'valid_email');
		$this->form_validation->set_rules('comment', 'comment', 'required');
		
		$flash_message = "Terima Kasih atas Kritik & Saran Anda.";
		$flash_message_en = "Thank you for your Feedback.";
		
		if($this->form_validation->run()==TRUE){
			if($this->input->post('submit')){
				if($this->fm->_check_capthca()){
					$data = array(
						'parent_id'			=> $this->input->post('post_id'),
						'parent_type'		=> 'kost',
						'name'				=> $this->input->post('name'),
						'email'				=> $this->input->post('email'),
						'comment_content'	=> $this->input->post('comment'),
						'comment_lang'		=> $this->input->post('lang'),
						'comment_date'		=> date("Y-m-d H:i:s")
					);
					$this->site_model->input_data("tb_comments",$data);
					$this->session->set_flashdata('message',$flash_message.$lang);
					redirect(base_url().'kost/detail/'.$this->input->post('slug').'.html');
				}
				else {
        			$cap_msg = '<span class="error">Security Code Wrong!</span>';
				}
			}
			$this->detail($this->input->post('slug'),$cap_msg);
		}
		else {
			$this->detail($this->input->post('slug'));
		}
	}
	// room detail
	function detail_room($mode){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		
		$select = "kost_title".$lang." as kost_title,kost_sketch,";
		$select.= "kost_room_id,kost_room_number,kost_room_title".$lang." as kost_room_title,kost_room_content".$lang." as kost_room_content";
		$kost = $this->site_model->get_data($select,"tb_kost_room kr,tb_kosts k","kr.kost_id = k.kost_id AND kost_room_id = '".$mode."'")->row();
		if($mode=='' OR $kost==''){
			redirect(base_url().'kost/detail/'.$this->session->userdata('kost_name').'.html');
		}
		else{
			// meta head
			$this->metahead['content_title']	= $this->session->userdata('kost_title');
			// data kost
			$this->content['kost_title']		= $kost->kost_title;
			$this->content['kost_sketch']		= $kost->kost_sketch;
			$this->content['kost_room_id']		= $kost->kost_room_id;
			$this->content['kost_room_number']	= $kost->kost_room_number;
			$this->content['kost_room_title']	= $kost->kost_room_title;
			$this->content['kost_room_content']	= $kost->kost_room_content;
			// image
			$this->content['img1'] = $this->site_model->get_img("parent_id = '".$kost->kost_room_id."' AND relation = 'kost_room'","1")->row();
			$this->content['img2'] = $this->site_model->get_img("parent_id = '".$kost->kost_room_id."' AND relation = 'kost_room'","1,4")->result();
			
			$this->load->view('frontend/header',$this->metahead);
			$this->load->view('frontend/kost-detail-room',$this->content);
			$this->load->view('frontend/footer',$this->footer);
		}
	}
}