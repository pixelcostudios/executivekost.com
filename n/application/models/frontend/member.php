<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Member extends CI_Model{
	var $metahead;
	var $content;
	var $footer;
	function __construct(){
		parent::__construct();
	}
	// login
	function index($cap_msg=""){
		// order check
		$order_check = $this->site_model->get_data('',"tb_rent_kost","DATE_ADD(time_order,INTERVAL 90 MINUTE) <= NOW() AND rent_status != '2'")->result();
		foreach($order_check as $x){
			$this->site_model->update_data("tb_rent_kost",array('rent_status'=>'3'),"rent_kost_id = '".$x->rent_kost_id."'");
		}
		$order_check2 = $this->site_model->get_data('',"tb_rent_kost")->result();
		foreach($order_check2 as $x){
			if($x->check_out < date("Y-m-d"))
			$this->site_model->update_data("tb_rent_kost",array('rent_status'=>'3'),"rent_kost_id = '".$x->rent_kost_id."'");
		}
		
		if($this->session->userdata('member_login')==TRUE){
			// data order
			$select = "k.kost_id,k.kost_title,k.kost_name,kr.kost_room_id,kr.kost_room_number,";
			$select.= "rk.check_in,rk.check_out,rk.rent_status,rk.special_request,kr.kost_room_status,rk.time_order";
			$where = "rk.kost_room_id = kr.kost_room_id AND kr.kost_id = k.kost_id AND rk.member_id = '".$this->session->userdata('ses_member_id')."'";
			$data_order = $this->site_model->get_data($select,"tb_rent_kost rk,tb_kosts k,tb_kost_room kr",$where)->row();
			$this->content['kost_id']			= $data_order->kost_id;
			$this->content['kost_title']		= $data_order->kost_title;
			$this->content['kost_name']			= $data_order->kost_name;
			$this->content['kost_room_id']		= $data_order->kost_room_id;
			$this->content['kost_room_number']	= $data_order->kost_room_number;
			$this->content['check_in']			= $data_order->check_in;
			$this->content['check_out']			= $data_order->check_out;
			$this->content['rent_status']		= $data_order->rent_status;
			$this->content['special_request']	= $data_order->special_request;
			$this->content['room_status']		= $data_order->kost_room_status;
			$this->content['time_order']		= $data_order->time_order;
			$data_member = $this->site_model->get_data('',"tb_members","member_id = '".$this->session->userdata('ses_member_id')."'")->row();
			$this->content['email']				= $data_member->member_email;
			
			// captcha
			$captcha_result = '';
			$this->content["cap_img"] = $this->fm->_make_captcha();
			$this->content["cap_msg"] = $cap_msg;		
			
			$this->load->view('frontend/header',$this->metahead);
			$this->load->view('frontend/kost-member-order',$this->content);
			$this->load->view('frontend/footer',$this->footer);
		}
		else{
			$this->load->view('frontend/header',$this->metahead);
			$this->load->view('frontend/kost-login',$this->content);
			$this->load->view('frontend/footer',$this->footer);
		}
	}
	// prosses login
	function login(){
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
		
		if($this->form_validation->run() == TRUE){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			if($this->site_model->check_login_member($username,$password) == TRUE){
				$user = $this->site_model->get_data('',"tb_members","member_username = '".$username."'")->row();
				$data = array('ses_member_id'=>$user->member_id,'member_username'=>$user->member_name,'member_login'=>TRUE);
				$this->session->set_userdata($data);
				redirect(base_url().'member/');
			}
			else{
				$this->session->set_flashdata('message', 'Invalid Username / Password');
				redirect(base_url().'member/login');
			}
		}
		else{
			if($this->session->userdata('member_login')){
				redirect(base_url().'member');
			}
			else{
				$this->load->view('frontend/header');
				$this->load->view('frontend/kost-login');
				$this->load->view('frontend/footer');
			}
		}
	}
	// reservation
	function reservation($cap_msg=""){
		$this->content['action']		= base_url().'member/register';
		$this->content['title_member']	= 'Reservation';
		// captcha
		$captcha_result = '';
		$this->content["cap_img"] = $this->fm->_make_captcha();
		$this->content["cap_msg"] = $cap_msg;
		
		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/kost-reservation',$this->content);
		$this->load->view('frontend/footer',$this->footer);
	}
	// select room
	function select_room(){
		echo '<td>Room Number</td><td> : </td>';
        echo '<td>';
        echo '<select name="kost_number">';
		$room_number = $this->site_model->get_data('',"tb_kost_room","kost_id = '".$this->session->userdata('kost_id')."' AND kost_room_type = '".$this->uri->segment(3)."' AND kost_room_status = '0'")->result();
		foreach($room_number as $r){			
			echo '<option value="'.$r->kost_room_id.'" '.set_select('kost_number', '1').'">'.$r->kost_room_number.'</option>';
		}
        echo '</select>';
		echo form_error('kost_number', '<label class="error">', '</label>');
        echo '</td>';
	}
	// register
	function register(){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		$kost_number = $this->input->post('kost_number');
		$callback_room_check_out = $this->input->post('kost_number').','.$this->input->post('check_in');
		
		$this->form_validation->set_rules('username', 'username', 'required|callback_username_check');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('kost_id', 'kost_id', '');
		$this->form_validation->set_rules('room_type', 'room_type', 'required');
		$this->form_validation->set_rules('kost_number', 'room number', 'required');	
		$this->form_validation->set_rules('name', 'name', 'required|callback_name_check');	
		$this->form_validation->set_rules('identity', 'identity', 'required');
		$this->form_validation->set_rules('identity_no', 'identity_no', 'required|callback_identity_check');
		$this->form_validation->set_rules('birthday', 'birthday', '');
		$this->form_validation->set_rules('phone', 'phone', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|callback_email_check');
		$this->form_validation->set_rules('address', 'address', '');
		$this->form_validation->set_rules('check_in', 'check_in', 'required|callback_room_check_in['.$kost_number.']');
		$this->form_validation->set_rules('check_out', 'check_out', 'required|callback_room_check_out['.$callback_room_check_out.']');
		$this->form_validation->set_rules('special_request', 'special_request', '');
		
		$option = $this->site_model->get_data('',"tb_options","option_id = '1'")->row();
		$flash_message = "Terima Kasih telah memesan kamar,waktu anda <b>3 jam</b> untuk melakukan pembayaran & konfirmasi pembayaran.";
		$flash_message.= "<br />Setelah anda melakukan pembayaran, gunakan username dan password anda untuk login dan konfirmasi pembayaran.";
		$flash_message_en = "Thank you for your Feedback.";
		
		if($this->form_validation->run() == TRUE){
			if($this->fm->_check_capthca()){	
				// to tb_members				
				$data = array(
					'member_username'			=> $this->input->post("username"),
					'member_password'			=> md5($this->input->post("password")),
					'member_b'					=> $this->input->post("password"),
					'member_name'				=> $this->input->post("name"),
					'member_identity'			=> $this->input->post("identity"),
					'member_identity_number'	=> $this->input->post("identity_no"),
					'member_birthday'			=> $this->input->post("birthday"),
					'member_phone'				=> $this->input->post("phone"),
					'member_email'				=> $this->input->post("email"),
					'member_address'			=> mysql_escape_string($this->input->post("address")));
				$this->site_model->input_data("tb_members",$data);
				$member_id = $this->db->insert_id();
				
				// to tb_kost_room		
				if($this->input->post('room_type'=='1')) $status = '1'; else $status = '0';
				$data = array('kost_room_status' => $status);
				$where = "kost_id = '".$this->input->post("kost_id")."' AND kost_room_number = '".$this->input->post("kost_number")."'";
				$this->site_model->update_data("tb_kost_room",$data,$where);
				// to tb_booking			
				$order_code = gen_rand(8);
				$kost_room_id = $this->site_model->get_data('',"tb_kost_room","kost_room_number = '".$this->input->post("kost_number")."'")->row();	
				$data = array(
					'booking_code'				=> $order_code,
					'member_id'					=> $member_id,
					'kost_room_id'				=> $kost_room_id->kost_room_id,
					'time_order'				=> date("Y-m-d H:i:s"),
					'check_in'					=> $this->input->post("check_in"),
					'check_out'					=> $this->input->post("check_out"),
					'rent_status'				=> '1',
					'special_request'			=> $this->input->post("special_request"));
				$this->site_model->input_data("tb_rent_kost",$data);
				
				// mail message
				$message = '<style type=text/css>
							body { font-family: Tahoma, Geneva, sans-serif; }
							a { text-decoration: none; color: #333; font-size: 12px; }
							#invoice { clear: both; } 
							#invoice h1 { font-size: 16px; color: #333; }
							#invoice h2 { font-size: 12px; font-weight: normal; color: #900; }
							#invoice fieldset { border: 1px solid #CCC; }
							#invoice legend { font-size: 12px; color: #999; }
							#invoice table { border-collapse: collapse; }
							#invoice td { padding: 5px 5px 5px 0; font-size: 12px; color: #333; }
							#invoice p { font-size: 12px; color: #333; }
							</style>';								
				$message.= '<div id="invoice">
							<h1>Pemesanan Kamar</h1>
							<h2>Terima kasih telah memesan kamar, waktu Anda <b>3 jam</b> untuk melakukan pembayaran & konfirmasi pembayaran.</h2>
							<h2>Setelah anda melakukan pembayaran, gunakan username dan password anda untuk login dan konfirmasi pembayaran.</h2>
							<fieldset>
							<legend>Kost Order Information</legend>
							<h1>'.$this->session->userdata('kost_title').'</h1>
							<table>
							<tr><td>Booking Code</td><td>: #'.$order_code.'</td></tr>
							<tr><td>Kost Room Number</td><td>: '.$this->input->post("kost_number").'</td></tr>
							<tr><td>Check In</td><td>: '.date("d F Y",strtotime($this->input->post("check_in"))).'</td></tr>
							<tr><td>Check Out</td><td>: '.date("d F Y",strtotime($this->input->post("check_out"))).'</td></tr>
							<tr><td>Special Request</td><td>: '.$this->input->post("special_request").'</td></tr>
							</table>
							</fieldset>
							<fieldset>
							<legend>Personal Information</legend>
							<table>
							<tr><td>Name</td><td>: '.$this->input->post("name").'</td></tr>
							<tr><td>Username</td><td>: '.$this->input->post("username").'</td></tr>
							<tr><td>Password</td><td>: '.$this->input->post("password").'</td></tr>
							<tr><td>Identity</td><td>: '.$this->input->post("identity").' [ '.$this->input->post("identity_no").' ]</td></tr>
							<tr><td>Birthday</td><td>: '.date("d F Y",strtotime($this->input->post("birthday"))).'</td></tr>
							<tr><td>Phone</td><td>: '.$this->input->post("phone").'</td></tr>
							<tr><td>Email</td><td>: '.$this->input->post("email").'</td></tr>
							<tr><td>Address</td><td>: '.$this->input->post("address").'</td></tr>
							</table>
							</fieldset>
							</div>';
				$config['mailtype'] = 'html';
				$this->email->initialize($config);					
				$this->email->from($option->email, 'Executive Kost');
				$this->email->to($this->input->post('email'));					
				$this->email->subject("Pemesanan Kamar, ".$this->session->userdata('kost_title'));
				$this->email->message($message);					
				$this->email->send();
				
				$this->session->unset_userdata('kost_id');
				$this->session->unset_userdata('kost_title');
				$this->session->unset_userdata('kost_name');
				$this->session->set_flashdata('message', $flash_message.$lang);
				redirect(base_url().'member/reservation');	
			}
			else {
				$cap_msg = '<span class="error">Security Code Wrong!</span>';
			}
			$this->reservation($cap_msg);
		}
		else {		
			$this->reservation();						
		}
	}
	// re order kost
	function re_order(){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		$kost_number = $this->input->post('kost_number');
		$callback_room_check_out = $this->input->post('kost_number').','.$this->input->post('check_in');
		
		$this->form_validation->set_rules('kost_id', 'kost_id', '');	
		$this->form_validation->set_rules('room_type', 'room type', 'required');
		$this->form_validation->set_rules('kost_number', 'kost number', 'required');
		$this->form_validation->set_rules('check_in', 'check_in', 'required|callback_room_check_in['.$kost_number.']');
		$this->form_validation->set_rules('check_out', 'check_out', 'required|callback_room_check_out['.$callback_room_check_out.']');
		$this->form_validation->set_rules('special_request', 'special_request', '');
		
		$option = $this->site_model->get_data('',"tb_options","option_id = '1'")->row();
		$flash_message = "Terima Kasih telah memesan kamar,waktu anda <b>3 jam</b> untuk melakukan pembayaran & konfirmasi pembayaran.";
		$flash_message.= "<br />Setelah anda melakukan pembayaran, gunakan username dan password anda untuk login dan konfirmasi pembayaran.";
		$flash_message_en = "Thank you for your Feedback.";
	
		if($this->form_validation->run() == TRUE){
			if($this->fm->_check_capthca()){					
				// to tb_kost_room	
				if($this->input->post('room_type'=='1')) $status = '1'; else $status = '0';			
				$data = array('kost_room_status' => $status);
				$where = "kost_id = '".$this->input->post("kost_id")."' AND kost_room_number = '".$this->input->post("kost_number")."'";
				$this->site_model->update_data("tb_kost_room",$data,$where);
				// to tb_booking			
				$order_code = gen_rand(8);
				$kost_room_id = $this->site_model->get_data('',"tb_kost_room","kost_room_number = '".$this->input->post("kost_number")."'")->row();	
				$data = array(
					'booking_code'				=> $order_code,
					'member_id'					=> $this->session->userdata('ses_member_id'),
					'kost_room_id'				=> $kost_room_id->kost_room_id,
					'time_order'				=> date("Y-m-d H:i:s"),
					'check_in'					=> $this->input->post("check_in"),
					'check_out'					=> $this->input->post("check_out"),
					'rent_status'				=> '1',
					'special_request'			=> $this->input->post("special_request"));
				$this->site_model->input_data("tb_rent_kost",$data);
				
				// mail message
				$message = '<style type=text/css>
							body { font-family: Tahoma, Geneva, sans-serif; }
							a { text-decoration: none; color: #333; font-size: 12px; }
							#invoice { clear: both; } 
							#invoice h1 { font-size: 16px; color: #333; }
							#invoice h2 { font-size: 12px; font-weight: normal; color: #900; }
							#invoice fieldset { border: 1px solid #CCC; }
							#invoice legend { font-size: 12px; color: #999; }
							#invoice table { border-collapse: collapse; }
							#invoice td { padding: 5px 5px 5px 0; font-size: 12px; color: #333; }
							#invoice p { font-size: 12px; color: #333; }
							</style>';								
				$message.= '<div id="invoice">
							<h1>Pemesanan Kamar</h1>
							<h2>Terima kasih telah memesan kamar, waktu Anda <b>3 jam</b> untuk melakukan pembayaran & konfirmasi pembayaran.</h2>
							<h2>Setelah anda melakukan pembayaran, gunakan username dan password anda untuk login dan konfirmasi pembayaran.</h2>
							<fieldset>
							<legend>Kost Order Information</legend>
							<h1>'.$this->session->userdata('kost_title').'</h1>
							<table>
							<tr><td>Booking Code</td><td>: #'.$order_code.'</td></tr>
							<tr><td>Kost Room Number</td><td>: '.$this->input->post("kost_number").'</td></tr>
							<tr><td>Check In</td><td>: '.date("d F Y",strtotime($this->input->post("check_in"))).'</td></tr>
							<tr><td>Check Out</td><td>: '.date("d F Y",strtotime($this->input->post("check_out"))).'</td></tr>
							<tr><td>Special Request</td><td>: '.$this->input->post("special_request").'</td></tr>
							</table>
							</fieldset>
							</div>';
				$config['mailtype'] = 'html';
				$this->email->initialize($config);					
				$this->email->from($option->email, 'Executive Kost');
				$this->email->to($this->input->post('email'));					
				$this->email->subject("Pemesanan Kamar, ".$this->session->userdata('kost_title'));
				$this->email->message($message);					
				$this->email->send();
				
				$this->session->unset_userdata('kost_id');
				$this->session->unset_userdata('kost_title');
				$this->session->unset_userdata('kost_name');
				$this->session->set_flashdata('message', $flash_message.$lang);
				redirect(base_url().'member');	
			}
			else {
				$cap_msg = '<span class="error">Security Code Wrong!</span>';
			}
			$this->index($cap_msg);
		}
		else {		
			$this->index();						
		}
	}
	// confirm
	function confirm(){		
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		
		$this->form_validation->set_rules('kost_id', 'kost_id', 'required');	
		$this->form_validation->set_rules('kost_room_id', 'kost_room_id', 'required');	
		$this->form_validation->set_rules('booking_code', 'booking code', 'required');	
		$this->form_validation->set_rules('transfer_date', 'transfer date', 'required');
		$this->form_validation->set_rules('total_pay', 'total pay', 'required');
		$this->form_validation->set_rules('bank', 'bank', 'required');
		$this->form_validation->set_rules('bank_account_name', 'bank account name', 'required');
		$this->form_validation->set_rules('bank_account_number', 'bank account number', 'required');
		$this->form_validation->set_rules('transfer_message', 'transfer message', '');		
		
		$flash_message = "Terima kasih, konfirmasi Anda akan segera kami proses.";
		$flash_message_en = "Thanks, we will process your confirmation.";
		
		if($this->form_validation->run() == TRUE){
			$data = array(
				'member_id'				=> $this->session->userdata('ses_member_id'),
				'kost_id'				=> $this->input->post("kost_id"),
				'kost_room_id'			=> $this->input->post("kost_room_id"),
				'booking_code'			=> $this->input->post("booking_code"),
				'transfer_date'			=> $this->input->post("transfer_date"),
				'total_pay'				=> $this->input->post("total_pay"),
				'bank'					=> $this->input->post("bank"),
				'bank_account_name'		=> $this->input->post("bank_account_name"),
				'bank_account_number'	=> $this->input->post("bank_account_number"),
				'transfer_message'		=> $this->input->post("transfer_message"),
				'confirm_date'			=> date("Y-m-d H:i:s"));
			$this->site_model->input_data("tb_confirms",$data);
			
			$this->session->set_flashdata('message', $flash_message.$lang);
			redirect(base_url().'member');	
		}
		else {		
			$this->index();						
		}
	}
	// profile
	function profile(){
		// data member
		$data_member = $this->site_model->get_data('',"tb_members","member_id = '".$this->session->userdata('ses_member_id')."'")->row();
		$this->content['member_id']			= $data_member->member_id;
		$this->content['username']			= $data_member->member_username;
		$this->content['password']			= $data_member->member_password;
		$this->content['name']				= $data_member->member_name;
		$this->content['identity']			= $data_member->member_identity;
		$this->content['identity_no']		= $data_member->member_identity_number;
		$this->content['birthday']			= $data_member->member_birthday;
		$this->content['phone']				= $data_member->member_phone;
		$this->content['email']				= $data_member->member_email;
		$this->content['address']			= $data_member->member_address;	
		
		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/kost-member-profile',$this->content);
		$this->load->view('frontend/footer',$this->footer);
	}
	// update member information
	function update_member_information(){	
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
			
		$this->form_validation->set_rules('name', 'name', 'required');	
		$this->form_validation->set_rules('identity', 'identity', 'required');	
		$this->form_validation->set_rules('identity_no', 'identity_no code', 'required');
		$this->form_validation->set_rules('birthday', 'birthday', 'required');
		$this->form_validation->set_rules('phone', 'phone', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('address', 'address', '');	
		
		$flash_message = "Data Anda sudah terupdate.";
		$flash_message_en = "Your data already updated.";
		
		if($this->form_validation->run() == TRUE){
			$data = array(
				'member_name'			=> $this->input->post("name"),
				'member_identity'		=> $this->input->post("identity"),
				'member_identity_number'=> $this->input->post("identity_no"),
				'member_birthday'		=> $this->input->post("birthday"),
				'member_phone'			=> $this->input->post("phone"),
				'member_email'			=> $this->input->post("email"),
				'member_address'		=> $this->input->post("address"));
			$this->site_model->update_data("tb_members",$data,"member_id = '".$this->input->post("member_id")."'");
			
			$this->session->set_flashdata('message', $flash_message.$lang);
			redirect(base_url().'member/profile');	
		}
		else {
			$this->profile();						
		}
	}
	// update member password
	function update_member_password(){	
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
			
		$member_id 		= $this->input->post("member_id");
		$username 		= $this->input->post("username");
		$old_password 	= $this->input->post("old_password");
		$new_password 	= $this->input->post("new_password");
		$conf_password 	= $this->input->post("conf_password");
		$password 		= md5($new_password);	
		
		$this->form_validation->set_rules('name', 'name', '');
		
		$flash_message = "Data Anda sudah terupdate.";
		$flash_message_en = "Your data already updated.";
		
		if($this->form_validation->run() == TRUE){
			if($this->site_model->check_password_member($member_id,$old_password)==TRUE){
				if(!empty($new_password)){
					if($new_password==$conf_password){
						$data = array('member_username'=>$username,'member_password'=>$password,'member_b'=>$new_password);
						$this->site_model->update_data("tb_members",$data,"member_id = '".$member_id."'");
						$this->session->set_flashdata('message', 'Data was update!');
						redirect(base_url().'member/profile');
					}
					else {
						$this->content['message']='New Password & Confirm Pass do not match!';
						$this->load->view('backend/template',$this->content);
					}
				}
				else{
					$data = array('member_username'=>$username);
					$this->site_model->update_data("tb_members",$data,"member_id = '".$member_id."'");
					$this->session->set_flashdata('message', 'Data was update!');
					redirect(base_url().'member/profile');
				}
			}
			else {
				$this->session->set_flashdata('message', 'Wrong Present Password!');
				redirect(base_url().'member/profile');
			}
		}
		else{
			$this->profile();	
		}
	}
}