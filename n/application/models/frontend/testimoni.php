<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Testimoni extends CI_Model{
	var $metahead;
	var $content;
	var $footer;
	function __construct(){
		parent::__construct();
	}
	// index
	function index(){		
		$uri = 2;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		// testimoni
		$testimoni = $this->site_model->get_data('',"tb_comments","parent_type = 'kost' AND comment_lang = '".$this->session->userdata('lang')."' AND comment_status = '1'",'',"comment_date desc","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_comments","parent_type = 'kost' AND comment_lang = '".$this->session->userdata('lang')."' AND comment_status = '1'")->num_rows();
		if($num_rows > 0){
			$config['base_url'] 			= base_url()."testimonial/";
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			$this->content['pagination'] 	= $this->pagination->create_links();
			// data testimoni
			$this->content['testimoni'] = array();
			foreach($testimoni as $a){
				$data['name']					= $a->name;
				$data['comment_date']			= fdate($a->comment_date);
				$data['comment_content']		= $a->comment_content;
				$this->content['testimoni'][]	= $data;
			}
		}
		
		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/testimoni',$this->content);
		$this->load->view('frontend/footer',$this->footer);
	}
}