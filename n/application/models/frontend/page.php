<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends CI_Model{
	var $metahead;
	var $content;
	var $footer;
	function __construct(){
		parent::__construct();
	}
	// index
	function index($mode){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		$slug = html($mode);
		
		// page
		$select = "page_id,page_title".$lang." as page_title,page_name".$lang." as page_name,page_content".$lang." as page_content";
		$page = $this->site_model->get_data($select,"tb_pages","page_name".$lang." = '".$slug."' AND page_status = '1'")->row();
		if(count($page)==0){
			redirect(base_url());
		}
		else{
			// meta head
			$this->metahead['content_title']	= $page->page_title;
			// data page
			$this->content['page_title']	= $page->page_title;
			$this->content['page_name']		= $page->page_name;
			$this->content['page_content']	= $page->page_content;
			$this->content['image']			= $this->site_model->get_img("parent_id = '".$page->page_id."' AND relation = 'page'","1")->row();
			
			$this->load->view('frontend/header',$this->metahead);
			$this->load->view('frontend/page',$this->content);
			$this->load->view('frontend/footer',$this->footer);
		}
	}
}