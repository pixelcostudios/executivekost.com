<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Model{
	var $url = 'owner_backend/';
	var $content;
	function __construct(){
		parent::__construct();
		$this->load->model('site_model','',TRUE);
	}
	
	// view
	function index(){
		// room check
		$room_check = $this->site_model->get_data('',"tb_rent_kost rk,tb_kost_room kr","rk.kost_room_id = kr.kost_room_id AND kost_room_type = '1' AND kr.kost_room_status = '1' AND DATE_ADD(rk.time_order,INTERVAL 90 MINUTE) <= NOW()")->result();
		foreach($room_check as $x){
			$this->site_model->update_data("tb_kost_room",array('kost_room_status'=>'0'),"kost_room_id = '".$room_check->kost_room_id."'");
		}
		// order check
		$order_check = $this->site_model->get_data('',"tb_rent_kost","DATE_ADD(time_order,INTERVAL 90 MINUTE) <= NOW() AND rent_status != '2'")->result();
		foreach($order_check as $x){
			$this->site_model->update_data("tb_rent_kost",array('rent_status'=>'3'),"rent_kost_id = '".$x->rent_kost_id."'");
		}
		$order_check2 = $this->site_model->get_data('',"tb_rent_kost")->result();
		foreach($order_check2 as $x){
			if($x->check_out < date("Y-m-d"))
			$this->site_model->update_data("tb_rent_kost",array('rent_status'=>'3'),"rent_kost_id = '".$x->rent_kost_id."'");
		}
		
		$this->content['h3_title']		= 'daftar reservasi executive kost';
		$this->content['content'] 		= 'owner/home';
		
		$uri = 3;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
				
		$select = "rk.rent_kost_id,rk.booking_code,m.member_id,m.member_name,k.kost_title,kr.kost_room_number,rk.time_order,rk.check_in,rk.rent_status";
		$table = "tb_rent_kost rk ";
		$table.= "LEFT JOIN tb_members m ON m.member_id = rk.member_id ";
		$table.= "LEFT JOIN tb_kost_room kr ON kr.kost_room_id = rk.kost_room_id ";
		$table.= "LEFT JOIN tb_kosts k ON k.kost_id = kr.kost_id ";
		$table.= "LEFT JOIN tb_user_kost uk ON uk.kost_id = k.kost_id";
		$where = "uk.user_id = '".$this->session->userdata('owner_id')."' AND rk.rent_status != '3'";
		$order = $this->site_model->get_data($select,$table,$where,"rk.rent_kost_id","rk.time_order DESC","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data($select,$table,$where,"rk.rent_kost_id","rk.time_order DESC")->num_rows();
		
		if($num_rows > 0){
			$config['base_url']				= base_url().$this->url.'/';
			$config['total_rows']			= $num_rows;
			$config['per_page']				= $limit;
			$config['uri_segment']			= $uri;
			$this->pagination->initialize($config);
			$this->content['pagination']	= $this->pagination->create_links();
			
			// Set template tabel, untuk efek selang-seling tiap baris
			$tmpl = array('table_open'=>'<table>');
			$this->table->set_template($tmpl);
	
			// Set heading untuk tabel
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('Order.Code','Name','Date Order','Kost Name','Room','Check In','Option');
			
			$i = 0 + $offset;
			foreach($order as $o){
				$time_order = fdate($o->time_order);
				$check_in = fdate($o->check_in,2);
				$confirm = $this->site_model->get_data('',"tb_confirms","member_id = '".$o->member_id."'")->num_rows();
				if($o->rent_status == '2'){
					$cell = array('data'=>$o->member_name,'style'=>'background-color: #00FF00; color: #FFF;');
				}
				else {
					if($confirm == '1'){
						$cell = array('data'=>$o->member_name,'style'=>'background-color: #FF0000; color: #FFF;');
					}
					else{
						$cell = array('data'=>$o->member_name);
					}
				}
				
				if($o->rent_status == '1') $cell2 = array('data'=>$time_order,'style'=>'background-color: #FFA500; color: #FFF;');
				if($o->rent_status == '2') $cell2 = array('data'=>$time_order,'style'=>'background-color: #00FF00; color: #FFF;');
					
				$this->table->add_row('#'.$o->booking_code,$cell,$cell2,$o->kost_title,array('data'=>$o->kost_room_number,'align'=>'center'),$check_in,
					anchor(base_url().$this->url.'check/'.$o->rent_kost_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor(base_url().$this->url.'delete/'.$o->rent_kost_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')")));
			}
			$this->table->add_row();
			$this->content['table'] = $this->table->generate();
		}
		else{
			$this->content['message'] = 'Empty Data!';
		}
		
		$this->load->view('owner/template',$this->content);
	}
	// search ticket
	function search(){
		$this->content['h3_title']		= 'result ticket';
		$this->content['content'] 		= 'ticket/home';
		
		if($this->input->post("search")!=""){
			$hasilcari = $this->site_model->search("tb_booking",$this->input->post("type"),$this->input->post("search"))->result();		
			if(count($hasilcari)>0){	
				// Set template tabel, untuk efek selang-seling tiap baris
				$tmpl = array('table_open'=>'<table class="table_list">','row_alt_start'=>'<tr class="zebra">','row_alt_end'=>'</tr>');
				$this->table->set_template($tmpl);
		
				// Set heading untuk tabel
				$this->table->set_empty("&nbsp;");
				$this->table->set_heading('Order.Code','Name','Kost Name','Room Number','Date Order','Option');
				
				$i = 0 + $offset;
				foreach($order as $o){
					$time_order = date("d F Y H:i:s", strtotime($o->time_order));
									
					$this->table->add_row('#'.$o->booking_code,$o->member_name,$o->kost_title,$o->kost_room_number,$time_order,
						anchor(base_url().$this->url.'check/'.$o->booking_id,'check',array('class'=>'edit')).' '.
						anchor(base_url().$this->url.'delete/'.$o->booking_id,'delete',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')")));
				}
				$this->content['table'] = $this->table->generate();
			}
			else {
				$this->content['message'] = 'maaf, data yang anda cari tidak ditemukan';
			}
		}
		else {
			$this->content['message'] = 'maaf, data yang anda cari tidak ditemukan';
		}
		$this->load->view('owner/template',$this->content);
	}
	// check ticket
	function check(){
		$id = $this->uri->segment(3);
		$this->content['h3_title']		= 'check order';
		$this->content['content']		= 'owner/check';
		$this->content['action']		= base_url().$this->url.'input';
		$this->session->set_userdata('form_mode', 'edit');
		
		$select = "rk.rent_kost_id,rk.booking_code,rk.time_order,rk.check_in,rk.check_out,rk.rent_status,rk.special_request,";
		$select.= "m.member_id,m.member_name,m.member_identity,m.member_identity_number,m.member_birthday,m.member_phone,m.member_email,m.member_address,";
		$select.= "k.kost_title,kr.kost_room_number,kr.kost_room_status";
		$table = "tb_rent_kost rk ";
		$table.= "LEFT JOIN tb_members m ON m.member_id = rk.member_id ";
		$table.= "LEFT JOIN tb_kost_room kr ON kr.kost_room_id = rk.kost_room_id ";
		$table.= "LEFT JOIN tb_kosts k ON k.kost_id = kr.kost_id ";
		$order = $this->site_model->get_data($select,$table,"rk.rent_kost_id = '".$id."'")->row();
		// order information
		$this->content['rent_kost_id']			 	= $order->rent_kost_id;
		$this->content['booking_code'] 				= $order->booking_code;
		$this->content['time_order'] 				= $order->time_order;
		$this->content['check_in'] 					= $order->check_in;
		$this->content['check_out']					= $order->check_out;
		$this->content['rent_status']				= $order->rent_status;
		$this->content['special_request']			= $order->special_request;
		$this->content['kost_title'] 				= $order->kost_title;
		$this->content['kost_room_number']			= $order->kost_room_number;
		$this->content['kost_room_status']			= $order->kost_room_status;
		// member information
		$this->content['member_id']					= $order->member_id;
		$this->content['member_name']				= $order->member_name;
		$this->content['member_identity']			= $order->member_identity;
		$this->content['member_identity_number']	= $order->member_identity_number;
		$this->content['member_birthday']			= $order->member_birthday;
		$this->content['member_phone'] 				= $order->member_phone;
		$this->content['member_email'] 				= $order->member_email;
		$this->content['member_address']			= $order->member_address;
		
		$this->load->view('owner/template',$this->content);			
	}
	// finish
	function finish(){
		$x = $this->site_model->get_data('',"tb_rent_kost","rent_kost_id = '".$this->input->post('booking_id')."'")->row();
		$room = $this->site_model->get_data('',"tb_kost_room","kost_room_id = '".$x->kost_room_id."'")->row();
		if($room->kost_room_type=='1') $status = '2'; else $status = '0';
		$this->site_model->update_data("tb_kost_room",array('kost_room_status'=>$status),"kost_room_id = '".$x->kost_room_id."'");
		$this->site_model->update_data("tb_rent_kost",array('rent_status'=>'2'),"rent_kost_id = '".$this->input->post('booking_id')."'");
		redirect(base_url().'/owner_backend/home');
	}
	// delete
	function delete(){
		$this->site_model->del_data("tb_order_ticket","order_ticket_id = '".$this->uri->segment(3)."'");
		redirect(base_url().'/owner_backend/home');
	}
}