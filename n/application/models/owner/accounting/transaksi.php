<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class transaksi extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('owner/accounting/mfunction','fungsi');
		$this->kost_id=$this->uri->segment(3);
    }
	
	function getakun(){
		
		$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id));
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$qr=$this->db->get('tb_akun');
		return $qr->result_array();
	}
		
		
	function tampiltransaksi(){
		$tanggaltransaksi=trim($this->input->post('tanggaltransaksi'));
		if(!preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $tanggaltransaksi))
			$tanggaltransaksi=date('Y-m-d');
		
		$this->db->join('tb_akun','tb_akun.idakun=tb_transaksi.idakun');
		$this->db->order_by('tb_transaksi.tanggaltransaksi','asc');
		$this->db->where(array('tb_transaksi.kost_id'=>$this->kost_id,'tb_transaksi.tanggaltransaksi'=>$tanggaltransaksi));
		$qr=$this->db->get('tb_transaksi');
		$a=$qr->result_array();
		
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		$idakunsewa=$h['idakunsewa'];
		$idakunpajak=$h['idakunpajak'];
		
		$h=array();
		foreach($a as $list){
			if($list['idakun']!=$idakunpajak){
				$notransaksi=$list['notransaksi'];
				$idakun=$list['idakun'];
				
				$b=array();
				$b['idtransaksi']=$list['idtransaksi'];
				$b['tanggaltransaksi']=$this->fungsi->tgl($list['tanggaltransaksi']);
				$b['notransaksi']=$list['notransaksi'];
				$b['idakun']=$list['idakun'];
				$b['uraian']=$list['uraian'];
				$b['possaldo']=$list['possaldo'];
				$besaran=$list['besaran'];
				
				if($idakun==$idakunsewa){
					$this->db->select('besaran');
					$this->db->where(array('notransaksi'=>$notransaksi,'idakun'=>$idakunpajak));
					$qr=$this->db->get('tb_transaksi');
					$h2=$qr->row_array();
					if(count($h2)>0)
						$b['pajak']=$h2['besaran'];
					else{
						$b['pajak']=0;
					}
					//$b['besaran']=$besaran+$b['pajak'];
					$besaran+=$b['pajak'];
				}else{
					//$b['besaran']=$besaran;
					$b['pajak']=0;
				}
				
				if($list['possaldo']=='DB'){
		  			$b['debit']=$besaran;
					$b['kredit']='';
		  		}else{
			  		$b['debit']='';
					$b['kredit']=$besaran;
		  		}
		  
				$h[]=$b;
			}
		}
		
		$this->content['transaksi']=$h;
		$this->content['content']='owner/accounting/transaksi/tampiltransaksi';
		$this->load->view('owner/template',$this->content);
	}
	
	function inputtransaksi(){
		$this->content['akun']=$this->getakun();
		$this->content['mode']='input';
		$this->content['tanggaltransaksi']=date('Y-m-d');
		$this->content['idtransaksi']='';
		$this->content['idakun']='';
		$this->content['nobukti']='';
		$this->content['uraian']='';
		$this->content['besaran']='';
		
		$this->content['content']='owner/accounting/transaksi/forminputtransaksi';
		$this->load->view('owner/template',$this->content);
	}
	
	function edittransaksi(){
		
		$this->content['akun']=$this->getakun();
		$idtransaksi=$this->uri->segment(6);
		$this->db->where(array('idtransaksi'=>$idtransaksi));
		$qr=$this->db->get('tb_transaksi');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner_backend/accounting/'.$this->kost_id.'/transaksi/tampil-transaksi','refresh');
		else{
			$this->content['mode']='edit';
			$this->content['tanggaltransaksi']=$h['tansggaltransaksi'];
			$this->content['idtransaksi']=$h['idtransaksi'];
			$this->content['nobukti']=$h['notransaksi'];
			$this->content['uraian']=$h['uraian'];
			$this->content['idakun']=$h['idakun'];
			$this->content['besaran']=$h['besaran'];

		
			$this->content['content']='owner/accounting/transaksi/forminputtransaksi';
			$this->load->view('owner/template',$this->content);
		}
	}
	
	function hapustransaksi(){
		$idtransaksi=$this->uri->segment(6);
		$this->db->where(array('idtransaksi'=>$idtransaksi));
		$qr=$this->db->get('tb_transaksi');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner_backend/accounting/'.$this->kost_id.'/transaksi/tampil-transaksi','refresh');
		else{
			$where=array('idtransaksi'=>$idtransaksi);
			$this->db->delete('tb_transaksi', $where); 
			redirect(base_url().'owner_backend/accounting/'.$this->kost_id.'/transaksi/tampil-transaksi','refresh');
		}
	}
	
	function prosestransaksi(){
		$mode=$this->input->post('mode');
		$idtransaksi=$this->input->post('idtransaksi');
		$nobukti=$this->input->post('nobukti');
		$uraian=$this->input->post('uraian');
		$idakun=$this->input->post('idakun');
		$besaran=$this->input->post('besaran');
		$tanggaltransaksi=$this->input->post('tanggaltransaksi');
		
		$this->content['mode']=$mode;
		$this->content['tanggaltransaksi']=$tanggaltransaksi;
		$this->content['idtransaksi']=$idtransaksi;
		$this->content['nobukti']=$nobukti;
		$this->content['uraian']=$uraian;
		$this->content['idakun']=$idakun;
		$this->content['besaran']=$besaran;
		
		$this->form_validation->set_rules('nobukti', 'No Bukti', 'required');
		$this->form_validation->set_rules('uraian', 'Uraian', 'required');
		$this->form_validation->set_rules('idakun', 'Jenis Akun', 'required');
		$this->form_validation->set_rules('besaran', 'Besaran', 'required');
		$this->form_validation->set_rules('tanggaltransaksi', 'Tanggal transaksi', 'required');
		$this->form_validation->set_message('required', '%s masih kosong');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->content['akun']=$this->getakun();
			$this->content['content']='owner/accounting/transaksi/forminputtransaksi';
			$this->load->view('owner/template',$this->content);
		}else{
			
			$userid=$this->session->userdata('ses_owner_id');
			//$this->db->where(array('guard_id'=>$userid));
			//$qr=$this->db->get('tb_kosts');
			//$h=$qr->row_array();
			//$kost_id=$h['kost_id'];
			$kost_id=$this->uri->segment(3);
			$namfield=array();
			$namafield=array('kost_id'=>$kost_id,'tanggaltransaksi'=>$tanggaltransaksi,'notransaksi'=>$nobukti,'uraian'=>$uraian,'idakun'=>$idakun,'besaran'=>$besaran);
			//if($pajak=='Y')
			//	$jmlpajak=$besaran*0.01;
			//else
			//	$jmlpajak=0;
						
			//$namafield['pajak']=$jmlpajak;
			
			if($mode=='input'){
				$this->db->insert('tb_transaksi', $namafield);
			}
			else{
				$where=array('idtransaksi'=>$idtransaksi);
				$this->db->where($where);
				$this->db->update('tb_transaksi', $namafield);
			}
			redirect(base_url().'owner_backend/accounting/'.$this->kost_id.'/transaksi/tampil-transaksi','refresh');
		}
	}
}