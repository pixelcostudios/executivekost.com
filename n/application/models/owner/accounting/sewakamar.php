<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class sewakamar extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
		$this->load->model('owner/accounting/mfunction','fungsi');
    }
	
	function getnokamar($kost_id){
		$this->db->where(array('kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_kost_room');
		return $qr->result_array();	
	}
	
	function getnamamember($member_id){
		$this->db->where(array('member_id'=>$member_id));
		$qr=$this->db->get('tb_members');
		$h=$qr->row_array();	
		if(count($h)==0)
			return '';
		else
			return $h['member_name'];
	}

	
	function carimember(){
		$this->content['kost_id']=$this->kost_id;
		$this->content['content']='owner/accounting/sewa/formcarimember';
		$this->load->view('owner/template',$this->content);
	}
	
	function prosescarimember(){
		$cari=$this->input->post('cari');
		$kost_id=$this->kost_id;
		if($cari==''){
			redirect('owner_backend/accounting/'.$this->kost_id.'/sewakamar');
		}else{
			$this->db->like('member_name',$cari);
			$qr=$this->db->get('tb_members');
			$h=$qr->result_array();
			
			$this->content['kost_id']=$kost_id;
			$this->content['pencarian']=$h;
			$this->content['content']='owner/accounting/sewa/pencarianmember';
			$this->load->view('owner/template',$this->content);
		}
		
	}
	
	function tampilsewakamar(){
		$this->db->order_by('tb_sewakamar.tanggalsewakamar','asc');
		$this->db->join('tb_members','tb_sewakamar.member_id=tb_members.member_id');
		$this->db->join('tb_kost_room','tb_sewakamar.kost_room_id=tb_kost_room.kost_room_id');
		$this->db->where(array('tb_kost_room.kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_sewakamar');
		$h=$qr->result_array();
		
		$hasil=array();
		foreach($h as $list){
			$b=array();
			$b['idsewakamar']=$list['idsewakamar'];
			$b['tanggalsewakamar']=$this->fungsi->tgl($list['tanggalsewakamar']);
			$b['tanggalmasuk']=$this->fungsi->tgl($list['tanggalmasuk']);
			$b['tanggalkeluar']=$this->fungsi->tgl($list['tanggalkeluar']);
			$b['kost_room_id']=$list['kost_room_id'];
			$b['besaran']=$list['besaran'];
			$b['notransaksi']=$list['notransaksi'];
			$b['member_name']=$list['member_name'];
			$b['kost_room_number']=$list['kost_room_number'];
			$hasil[]=$b;
		}
		
		$this->content['sewakamar']=$hasil;
		$this->content['content']='owner/accounting/sewa/tampilsewa';
		$this->load->view('owner/template',$this->content);
	}
	
	function inputsewa(){
		//$kost_id=$this->uri->segment(3);
		$member_id=$this->uri->segment(6);
		$this->content['mode']='input';
		$this->content['kost_id']=$this->kost_id;
		$this->content['nokamar']=$this->getnokamar($this->kost_id);
		$this->content['member_id']=$member_id;
		$this->content['namamember']=$this->getnamamember($member_id);
		$this->content['idsewakamar']='';
		$this->content['kost_room_id']='';
		$this->content['tanggaltransaksi']=date('Y-m-d');
		$this->content['hari']=date('d');
		$this->content['bulan']=date('m');
		$this->content['tahun']=date('Y');
		$this->content['tanggalmasuk']=date('Y-m-d');
		$this->content['harimasuk']='';
		$this->content['bulanmasuk']='';
		$this->content['tahunmasuk']='';
		$this->content['tanggalkeluar']=date('Y-m-d');
		$this->content['harikeluar']='';
		$this->content['bulankeluar']='';
		$this->content['tahunkeluar']='';
		$this->content['besaran']='';
		
		$this->content['content']='owner/accounting/sewa/forminputsewa';
		$this->load->view('owner/template',$this->content);
	}
	
	function editsewa(){
		
		$idsewakamar=$this->uri->segment(6);
		$this->db->where(array('idsewakamar'=>$idsewakamar));
		$qr=$this->db->get('tb_sewakamar');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner_backend/accounting/'.$this->kost_id.'/sewakamar/tampil-sewakamar','refresh');
		else{
			//$this->content['jenisakun']=$this->getjenisakun();
			$idsewakamar=$h['idsewakamar'];
			$member_id=$h['member_id'];
			$kost_room_id=$h['kost_room_id'];
			$tanggalsewakamar=$h['tanggalsewakamar'];
			$a=explode('-',$tanggalsewakamar);
			$hari=$a[2];
			$bulan=$a[1];
			$tahun=$a[0];
			$tanggalmasuk=$h['tanggalmasuk'];
			$a=explode('-',$tanggalmasuk);
			$harimasuk=$a[2];
			$bulanmasuk=$a[1];
			$tahunmasuk=$a[0];
			$tanggalkeluar=$h['tanggalkeluar'];
			$a=explode('-',$tanggalkeluar);
			$harikeluar=$a[2];
			$bulankeluar=$a[1];
			$tahunkeluar=$a[0];
			$besaran=$h['besaran'];
		
			$this->content['mode']='edit';
			$this->content['kost_id']=$this->kost_id;
			$this->content['nokamar']=$this->getnokamar($this->kost_id);
			$this->content['member_id']=$member_id;
			$this->content['namamember']=$this->getnamamember($member_id);
			$this->content['idsewakamar']=$idsewakamar;
			$this->content['kost_room_id']=$kost_room_id;
			$this->content['tanggaltransaksi']=$tanggalsewakamar;
			$this->content['hari']=$hari;
			$this->content['bulan']=$bulan;
			$this->content['tahun']=$tahun;
			$this->content['tanggalmasuk']=$tanggalmasuk;
			$this->content['harimasuk']=$harimasuk;
			$this->content['bulanmasuk']=$bulanmasuk;
			$this->content['tahunmasuk']=$tahunmasuk;
			$this->content['tanggalkeluar']=$tanggalkeluar;
			$this->content['harikeluar']=$harikeluar;
			$this->content['bulankeluar']=$bulankeluar;
			$this->content['tahunkeluar']=$tahunkeluar;
			$this->content['besaran']=$besaran;
		
			$this->content['content']='owner/accounting/sewa/forminputsewa';
			$this->load->view('owner/template',$this->content);
		}
	}
	
	function hapussewa(){
		$idakun=$this->uri->segment(5);
		$this->db->where(array('idakun'=>$idakun));
		$qr=$this->db->get('tb_akun');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner_backend/accounting/akun','refresh');
		else{
			$where=array('idakun'=>$idakun);
			$this->db->delete('tb_akun', $where); 
			redirect(base_url().'owner_backend/accounting/akun','refresh');
		}
	}
	
	function prosessewa(){
		$mode=$this->input->post('mode');
		$idsewakamar=$this->input->post('idsewakamar');
		$member_id=$this->input->post('member_id');
		$kost_room_id=$this->input->post('kost_room_id');
		$tanggaltransaksi=$this->input->post('tanggaltransaksi');
		$hari=$this->input->post('hari');
		$bulan=$this->input->post('bulan');
		$tahun=$this->input->post('tahun');
		$tanggalmasuk=$this->input->post('tanggalmasuk');
		$harimasuk=$this->input->post('harimasuk');
		$bulanmasuk=$this->input->post('bulanmasuk');
		$tahunmasuk=$this->input->post('tahunmasuk');
		$tanggalkeluar=$this->input->post('tanggalkeluar');
		$harikeluar=$this->input->post('harikeluar');
		$bulankeluar=$this->input->post('bulankeluar');
		$tahunkeluar=$this->input->post('tahunkeluar');
		$besaran=$this->input->post('besaran');
		
		$this->content['mode']=$mode;
		$this->content['kost_id']=$this->kost_id;
		$this->content['nokamar']=$this->getnokamar($this->kost_id);
		$this->content['member_id']=$member_id;
		$this->content['namamember']=$this->getnamamember($member_id);
		$this->content['idsewakamar']=$idsewakamar;
		$this->content['kost_room_id']=$kost_room_id;
		$this->content['tanggaltransaksi']=$tanggaltransaksi;
		$this->content['hari']=$hari;
		$this->content['bulan']=$bulan;
		$this->content['tahun']=$tahun;
		$this->content['tanggalmasuk']=$tanggalmasuk;
		$this->content['harimasuk']=$harimasuk;
		$this->content['bulanmasuk']=$bulanmasuk;
		$this->content['tahunmasuk']=$tahunmasuk;
		$this->content['tanggalkeluar']=$tanggalkeluar;
		$this->content['harikeluar']=$harikeluar;
		$this->content['bulankeluar']=$bulankeluar;
		$this->content['tahunkeluar']=$tahunkeluar;
		$this->content['besaran']=$besaran;
		
		$this->form_validation->set_rules('kost_room_id', 'No Kamar', 'required');
		$this->form_validation->set_rules('besaran', 'Pembayaran', 'required');
		$this->form_validation->set_rules('tanggaltransaksi', 'Tanggal transaksi', 'required');
		$this->form_validation->set_rules('tanggalmasuk', 'Tanggal masuk', 'required');
		$this->form_validation->set_rules('tanggalkeluar', 'Tanggal keluar', 'required');
		/*$this->form_validation->set_rules('hari', 'hari', 'required');
		$this->form_validation->set_rules('bulan', 'bulan', 'required');
		$this->form_validation->set_rules('tahun', 'tahun', 'required');
		$this->form_validation->set_rules('harimasuk', 'Hari masuk', 'required');
		$this->form_validation->set_rules('bulanmasuk', 'Bulan masuk', 'required');
		$this->form_validation->set_rules('tahunmasuk', 'Tahun masuk', 'required');
		$this->form_validation->set_rules('harikeluar', 'Hari keluar', 'required');
		$this->form_validation->set_rules('bulankeluar', 'Bulan keluar', 'required');
		$this->form_validation->set_rules('tahunkeluar', 'Tahun keluar', 'required');*/
		$this->form_validation->set_message('required', '%s masih kosong');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->content['content']='owner/accounting/sewa/forminputsewa';
			$this->load->view('owner/template',$this->content);
		}else{
			//$tgltransaksi="$tahun-$bulan-$hari";
			//$tglmasuk="$tahunmasuk-$bulanmasuk-$harimasuk";
			//$tglkeluar="$tahunkeluar-$bulankeluar-$harikeluar";
			
			$namfield=array();
			$namafield=array('tanggalsewakamar'=>$tanggaltransaksi,'kost_room_id'=>$kost_room_id,'member_id'=>$member_id,'tanggalmasuk'=>$tanggalmasuk,'tanggalkeluar'=>$tanggalkeluar,'besaran'=>$besaran);
					
			if($mode=='input'){
				$notransaksi=$this->fungsi->createnotransaksi();
				$namafield['notransaksi']=$notransaksi;
				$this->db->insert('tb_sewakamar', $namafield);
				
				$pajak=($besaran*10/100);
				$pendapatan=$besaran-$pajak;
				
				
				$qr=$this->db->get('tb_settingakun');
				$h=$qr->row_array();
				$idakun=$h['idakunsewa'];
				$idakunpajak=$h['idakunpajak'];
				$namafield2=array();
				$namafield2['kost_id']=$this->kost_id;
				$namafield2['tanggaltransaksi']=$tanggaltransaksi;
				$namafield2['notransaksi']=$notransaksi;
				$namafield2['idakun']=$idakun;
				$namafield2['besaran']=$pendapatan;
				$namafield2['uraian']='Pendapatan sewa kamar';
				$this->db->insert('tb_transaksi', $namafield2);
				
				$namafield2=array();
				$namafield2['kost_id']=$this->kost_id;
				$namafield2['tanggaltransaksi']=$tanggaltransaksi;
				$namafield2['notransaksi']=$notransaksi;
				$namafield2['idakun']=$idakunpajak;
				$namafield2['besaran']=$pajak;
				$namafield2['uraian']='Pajak sewa kamar';
				$this->db->insert('tb_transaksi', $namafield2);
				
			}
			else{
				$where=array('idsewakamar'=>$idsewakamar);
				
				$this->db->select('notransaksi');
				$this->db->where($where);
				$qr=$this->db->get('tb_sewakamar');
				$h=$qr->row_array();
				$notransaksi=$h['notransaksi'];
				
				$qr=$this->db->get('tb_settingakun');
				$h=$qr->row_array();
				$idakun=$h['idakunsewa'];
				$idakunpajak=$h['idakunpajak'];
				
				$pajak=($besaran*10/100);
				$pendapatan=$besaran-$pajak;
				
				$namafield2=array();
				$namafield2['tanggaltransaksi']=$tanggaltransaksi;
				$namafield2['besaran']=$pendapatan;
				
				$this->db->where(array('notransaksi'=>$notransaksi,'idakun'=>$idakun));
				$temp=$this->db->get('tb_transaksi');
				$a=$temp->row_array();				
				if(count($a)==0){
					$namafield2['kost_id']=$this->kost_id;
					$namafield2['idakun']=$idakun;
					$namafield2['uraian']='Pendapatan sewa kamar';
					$namafield2['notransaksi']=$notransaksi;
					$this->db->insert('tb_transaksi', $namafield2);
				}else{
					$this->db->where(array('notransaksi'=>$notransaksi,'idakun'=>$idakun));
					$this->db->update('tb_transaksi', $namafield2);
				}
				
				$namafield2=array();
				$namafield2['tanggaltransaksi']=$tanggaltransaksi;
				$namafield2['besaran']=$pajak;
				
				$this->db->where(array('notransaksi'=>$notransaksi,'idakun'=>$idakunpajak));
				$temp=$this->db->get('tb_transaksi');
				$a=$temp->row_array();				
				if(count($a)==0){
					$namafield2['kost_id']=$this->kost_id;
					$namafield2['idakun']=$idakunpajak;
					$namafield2['uraian']='Pajak sewa kamar';
					$namafield2['notransaksi']=$notransaksi;
					$this->db->insert('tb_transaksi', $namafield2);
				}else{
					$this->db->where(array('notransaksi'=>$notransaksi,'idakun'=>$idakunpajak));
					$this->db->update('tb_transaksi', $namafield2);			
				}
				$this->db->where($where);
				$this->db->update('tb_sewakamar', $namafield);
			}
			redirect(base_url().'owner_backend/accounting/'.$this->kost_id.'/sewakamar/tampil-sewakamar','refresh');
		}
	}
}