<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class mfunction extends CI_Model{
	var $kost_id;
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
		
    }
	
	function createnotransaksi(){
		$year=date('Y');
		$month=date('m');
		
		$qr2=$this->db->query('select notransaksi from tb_sewakamar where year(tanggalsewakamar)='.$year.' and kost_id='.$this->kost_id.' order by notransaksi desc');
		$h2=$qr2->row_array();
		if(count($h2)>0){
			//$notransaksi=substr($h2['notransaksi'],-10);
			$notransaksi=$year.'/'.$month.'/'.str_pad(substr($h2['notransaksi'],-10)+1,10,'0', STR_PAD_LEFT);
		}
		else
			$notransaksi=$year.'/'.$month.'/0000000001';
		//echo $notransaksi;
		return $notransaksi;
		
	}
	
	function createnobelanja(){
		$year=date('Y');
		$month=date('m');
		
		$qr2=$this->db->query('select nopengajuan from tb_pengajuanbelanja where year(tglpengajuan)='.$year.' and kost_id='.$this->kost_id.' order by nopengajuan desc');
		$h2=$qr2->row_array();
		if(count($h2)>0){
			//$notransaksi=substr($h2['notransaksi'],-10);
			$nopengajuan=str_pad(substr($h2['notransaksi'],10)+1,10,'0', STR_PAD_LEFT).'/BLJ/'.$month.'/'.$year;
		}
		else
			$nopengajuan='0000000001/BLJ/'.$month.'/'.$year;
		//echo $notransaksi;
		return $nopengajuan;
		
	}
	
	function tgl($tgl){
		$day=array("Sun"=>"Minggu", "Mon"=>"Senin", "Tue"=>"Selasa", "Wed"=>"Rabu", "Thu"=>"Kamis", "Fri"=>"Jumat", "Sat"=>"Sabtu");
		$bulan=array("00"=>"00","01"=>"Januari", "02"=>"Februari", "03"=>"Maret", "04"=>"April", "05"=>"Mei", "06"=>"Juni", "07"=>"Juli", "08"=>"Agustus", "09"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
		$a=explode("-",$tgl);
		$bln=$bulan[$a[1]];
		return "$a[2] $bln $a[0]";
	}
	
	function getnamekost($kost_id){
		$this->db->select('kost_title');
		$this->db->where(array('kost_id'=>$kost_id));
		$qr=$this->db->get('tb_kosts');
		$h=$qr->row_array();
		if(count($h)==0)
			$nama='';
		else
			$nama=$h['kost_title'];
			
		return $nama;
	}
}