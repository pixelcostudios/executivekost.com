<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class laporan extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
		$this->load->model('owner/accounting/mfunction','fungsi');
    }
	
	function rugilaba(){
		//$this=
		
		
		$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id,'tb_akun.poslaporan'=>'LB'));
		$qr=$this->db->get('tb_akun');
		$h=$qr->result_array();
		
		$hasil=array();
		$namajenisakun='';
		$total=0;
		foreach($h as $list){
			$idakun=$list['idakun'];
			$kodeakun=$list['kodejenisakun'].'-'.$list['kodeakun'];
			$namaakun=$list['namaakun'];
			$namajenisakun=$list['namajenisakun'];
			$possaldo=$list['possaldo'];
			//if($namajenisakun==$list['namajenisakun'])
			{
				
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakun.' and kost_id='.$this->kost_id);
				$h2=$qr2->row_array();
				//print_r($h2);
				if($h2['jmlbesaran']=='')
					$jmlbesaran=0;
				else
					$jmlbesaran=$h2['jmlbesaran'];
				$a=array();
				$a['idakun']=$idakun;
				$a['kodeakun']=$kodeakun;
				$a['namaakun']=$namaakun;
				$a['namajenisakun']=$namajenisakun;
				$a['possaldo']=$possaldo;
				$a['jmlbesaran']=$jmlbesaran;
				$hasil[]=$a;
			}
			
		}
		$this->content['kost_title']=$this->fungsi->getnamekost($this->kost_id);
		$this->content['rugilaba']=$hasil;
		$this->content['content']='owner/accounting/laporan/labarugi';
		$this->load->view('owner/template',$this->content);
		
	}
	
	function neraca(){
		
		$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id,'tb_akun.poslaporan'=>'NRC'));
		$qr=$this->db->get('tb_akun');
		$h=$qr->result_array();
		
		$hasil=array();
		$namajenisakun='';
		$total=0;
		foreach($h as $list){
			$idakun=$list['idakun'];
			$kodeakun=$list['kodejenisakun'].'-'.$list['kodeakun'];
			$namaakun=$list['namaakun'];
			$namajenisakun=$list['namajenisakun'];
			$possaldo=$list['possaldo'];
			//if($namajenisakun==$list['namajenisakun'])
			{
				
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakun.' and kost_id='.$this->kost_id);
				$h2=$qr2->row_array();
				//print_r($h2);
				if($h2['jmlbesaran']=='')
					$jmlbesaran=0;
				else
					$jmlbesaran=$h2['jmlbesaran'];
				$a=array();
				$a['idakun']=$idakun;
				$a['kodeakun']=$kodeakun;
				$a['namaakun']=$namaakun;
				$a['namajenisakun']=$namajenisakun;
				$a['possaldo']=$possaldo;
				$a['jmlbesaran']=$jmlbesaran;
				$hasil[]=$a;
			}
			
		}
		$this->content['kost_title']=$this->fungsi->getnamekost($this->kost_id);
		$this->content['neraca']=$hasil;
		$this->content['content']='owner/accounting/laporan/neraca';
		$this->load->view('owner/template',$this->content);
		
	}
	
}
?>