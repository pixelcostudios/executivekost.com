<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class akun extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
    }
	
	function getjenisakun(){
		$this->db->order_by('kodejenisakun','asc');
		$this->db->where(array('kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_jenisakun');
		return $qr->result_array();	
	}
	
	function tampilakun(){
		$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_akun');
		$h=$qr->result_array();
		$this->content['akun']=$h;
		$this->content['content']='owner/accounting/akun/tampilakun';
		$this->load->view('owner/template',$this->content);
	}
	
	function inputakun(){
		$this->content['mode']='input';
		$this->content['jenisakun']=$this->getjenisakun();
		$this->content['idakun']='';
		$this->content['idjenisakun']='';
		$this->content['kodeakun']='';
		$this->content['namaakun']='';
		$this->content['possaldo']='';
		$this->content['poslaporan']='';
		
		$this->content['content']='owner/accounting/akun/forminputakun';
		$this->load->view('owner/template',$this->content);
	}
	
	function editakun(){
		
		$idakun=$this->uri->segment(6);
		$this->db->where(array('idakun'=>$idakun));
		$qr=$this->db->get('tb_akun');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner_backend/accounting/akun','refresh');
		else{
			$this->content['jenisakun']=$this->getjenisakun();
			$this->content['mode']='edit';
			$this->content['idakun']=$h['idakun'];
			$this->content['idjenisakun']=$h['idjenisakun'];
			$this->content['kodeakun']=$h['kodeakun'];
			$this->content['namaakun']=$h['namaakun'];
			$this->content['possaldo']=$h['possaldo'];
			$this->content['poslaporan']=$h['poslaporan'];
		
			$this->content['content']='owner/accounting/akun/forminputakun';
			$this->load->view('owner/template',$this->content);
		}
	}
	
	function hapusakun(){
		$idakun=$this->uri->segment(6);
		$this->db->where(array('idakun'=>$idakun));
		$qr=$this->db->get('tb_akun');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner_backend/accounting/'.$this->kost_id.'/akun','refresh');
		else{
			$where=array('idakun'=>$idakun);
			$this->db->delete('tb_akun', $where); 
			redirect(base_url().'owner_backend/accounting/'.$this->kost_id.'/akun','refresh');
		}
	}
	
	function prosesakun(){
		$mode=$this->input->post('mode');
		$idakun=$this->input->post('idakun');
		$idjenisakun=$this->input->post('idjenisakun');
		$kodeakun=$this->input->post('kodeakun');
		$namaakun=$this->input->post('namaakun');
		$possaldo=$this->input->post('possaldo');
		$poslaporan=$this->input->post('poslaporan');

		$this->content['jenisakun']=$this->getjenisakun();
		$this->content['mode']=$mode;
		$this->content['idakun']=$idakun;
		$this->content['idjenisakun']=$idjenisakun;
		$this->content['kodeakun']=$kodeakun;
		$this->content['namaakun']=$namaakun;
		$this->content['possaldo']=$possaldo;
		$this->content['poslaporan']=$poslaporan;
		
		$this->form_validation->set_rules('kodeakun', 'Kode akun', 'required');
		$this->form_validation->set_rules('idjenisakun', 'Jenis akun', 'required');
		$this->form_validation->set_rules('namaakun', 'Nama akun', 'required');
		$this->form_validation->set_message('required', '%s masih kosong');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->content['content']='owner/accounting/akun/forminputakun';
			$this->load->view('owner/template',$this->content);
		}else{
			
			$namfield=array();
			$namafield=array('kodeakun'=>$kodeakun,'idjenisakun'=>$idjenisakun,'namaakun'=>$namaakun,'possaldo'=>$possaldo,'poslaporan'=>$poslaporan);
					
			if($mode=='input'){
				$this->db->insert('tb_akun', $namafield);
			}
			else{
				$where=array('idakun'=>$idakun);
				$this->db->where($where);
				$this->db->update('tb_akun', $namafield);
			}
			redirect(base_url().'owner_backend/accounting/'.$this->kost_id.'/akun','refresh');
		}
	}
}