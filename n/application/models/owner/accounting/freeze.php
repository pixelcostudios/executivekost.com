<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class freeze extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
    }
	
	function freezing(){
		$qr=$this->db->update('tb_settingakun',array('freezestatus'=>'1'));
		$this->session->set_userdata('ses_freeze', '1');
		
		$content['content']='owner/accounting/freeze/freeze';
		$this->load->view('owner/template',$content);
	}
	
	
	function unfreezing(){
		$qr=$this->db->update('tb_settingakun',array('freezestatus'=>'0'));
		$this->session->set_userdata('ses_freeze', '0');
		
		$content['content']='owner/accounting/freeze/unfreeze';
		$this->load->view('owner/template',$content);
	}
	
	
	function tampilfreezing(){
		$content['content']='owner/accounting/freeze/message';
		$this->load->view('owner/template',$content);
		
		
	}
	
	
}