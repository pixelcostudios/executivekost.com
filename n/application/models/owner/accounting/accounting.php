<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class accounting extends CI_Model{
	
	function menu(){
		$ses_freeze=$this->session->userdata('ses_freeze');
		$ses_userlevel=$this->session->userdata('ses_owner_level');
		if($ses_freeze=='1' and $ses_userlevel=='guard'){
			$this->load->model('owner/accounting/freeze');
			$this->freeze->tampilfreezing();
		}else{
		
			$mode=$this->uri->segment(4);
			switch($mode){
				case 'belanja':$this->load->model('owner/accounting/permintaanbelanja','pb');
										  $mode2=$this->uri->segment(5);
										  switch($mode2){
											case 'delete-pengajuan-belanja':$this->pb->deletepengajuanbelanja();break;
											case 'input-jumlah-items':$this->pb->inputjumlahitem();break;
											case 'input-items':$this->pb->inputitem();break;
											case 'confirm-input-items':$this->pb->konfirminputitem();break;
											case 'proses-input-items':$this->pb->prosespengajuan();break;
											case 'guard-detail': $mode3=$this->uri->segment(6);
																  switch($mode3){
																	case 'delete-detail-pengajuan':$this->pb->deleteitemdetailpengajuan();break;
																	case 'input-detail-pengajuan':$this->pb->inputdetailpengajuan();break;
																	default:$this->pb->guarddetail();break;
																  }break;
											case 'owner-detail': $mode3=$this->uri->segment(6);
																  switch($mode3){
																	case 'delete-detail-pengajuan':$this->pb->deleteitemdetailpengajuan();break;
																	default:$this->pb->ownerdetail();break;
																  }break;
											default:
													$userlevel=$this->session->userdata('ses_owner_level');
													if($userlevel=='guard')
														$this->pb->tampilpengajuan();
													elseif($userlevel=='owner')
														$this->pb->tampilownerpengajuan();
														
													break;
										  }break;
				case 'transaksi':$this->load->model('owner/accounting/transaksi','tr');
								 $mode2=$this->uri->segment(5);
								 switch($mode2){
									 case 'input-transaksi':$this->tr->inputtransaksi();break;
									 case 'edit-transaksi':$this->tr->edittransaksi();break;
									 case 'proses-transaksi':$this->tr->prosestransaksi();break;
									 case 'delete-transaksi':$this->tr->hapustransaksi();break;
									 default :$this->tr->tampiltransaksi();break;
								 }break;
				case 'setting':$this->load->model('owner/accounting/settingpajak','sp');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input':$this->sp->input();break;
								 case 'edit':$this->sp->edit();break;
								 case 'proses':$this->sp->proses();break;
								 case 'delete':$this->sp->hapus();break;
								 default :$this->sp->tampil();break;
							 }break;			  
				case 'jenisakun':$this->load->model('owner/accounting/jenisakun');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input-jenisakun':$this->jenisakun->inputjenisakun();break;
								 case 'edit-jenisakun':$this->jenisakun->editjenisakun();break;
								 case 'proses-jenisakun':$this->jenisakun->prosesjenisakun();break;
								 case 'delete-jenisakun':$this->jenisakun->hapusjenisakun();break;
								 default :$this->jenisakun->tampiljenisakun();break;
							 }break;			  
				case 'akun':$this->load->model('owner/accounting/akun');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input-akun':$this->akun->inputakun();break;
								 case 'edit-akun':$this->akun->editakun();break;
								 case 'proses-akun':$this->akun->prosesakun();break;
								 case 'delete-akun':$this->akun->hapusakun();break;
								 default :$this->akun->tampilakun();break;
							 }break;			  
				case 'sewakamar':$this->load->model('owner/accounting/sewakamar','sewa');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'carimember':$this->sewa->carimember();break;
								 case 'proses-carimember':$this->sewa->prosescarimember();break;
								 case 'input-sewakamar':$this->sewa->inputsewa();break;
								 case 'edit-sewakamar':$this->sewa->editsewa();break;
								 case 'proses-sewakamar':$this->sewa->prosessewa();break;
								 //case 'delete':$this->sewa->hapus();break;
								 default :$this->sewa->tampilsewakamar();break;
							 }break;			  
				case 'transaksi':$this->load->model('owner/accounting/transaksi','tr');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input-transaksi':$this->tr->inputtransaksi();break;
								 case 'edit-transaksi':$this->tr->edittransaksi();break;
								 case 'delete-transaksi':$this->tr->hapustransaksi();break;
								 case 'proses-transaksi':$this->tr->prosestransaksi();break;
								 default:$this->tr->tampiltransaksi();
							 };break;
				case 'laporan':$this->load->model('owner/accounting/laporan','lp');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'rugi-laba':$this->lp->rugilaba();break;
								 default:$this->lp->neraca();
							 };break;
				case 'freeze':$this->load->model('owner/accounting/freeze');
							$this->freeze->freezing();
							break;
				case 'unfreeze':$this->load->model('owner/accounting/freeze');
							$this->freeze->unfreezing();
							break;
			}
		}
	
	
	}
}
