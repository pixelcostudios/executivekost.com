<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class settingpajak extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
    }
	
	function getakun(){
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_akun');
		return $qr->result_array();	
	}
	
	function tampil(){
		$this->db->where(array('kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		
		if(count($h)>0){
			$idakunpajak=$h['idakunpajak'];
			$idakunsewa=$h['idakunsewa'];
			$idakunpenjualan=$h['idakunpenjualan'];
			$idsetting=$h['idsetting'];
			$a['idsetting']=$idsetting;
			$this->db->where(array('tb_akun.idakun'=>$idakunpajak));
			$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
			$qr=$this->db->get('tb_akun');
			$h=$qr->row_array();
			if(count($h)>0)
				$a['akunpajak']=$h['kodejenisakun'].'-'.$h['kodeakun'];
			else
				$a['akunpajak']='';
			
			$this->db->where(array('tb_akun.idakun'=>$idakunsewa));
			$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
			$qr=$this->db->get('tb_akun');
			$h=$qr->row_array();
			if(count($h)>0)
				$a['akunsewa']=$h['kodejenisakun'].'-'.$h['kodeakun'];
			else
				$a['akunsewa']='';
				
			$this->db->where(array('tb_akun.idakun'=>$idakunpenjualan));
			$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
			$qr=$this->db->get('tb_akun');
			$h=$qr->row_array();
			if(count($h)>0)
				$a['akunpenjualan']=$h['kodejenisakun'].'-'.$h['kodeakun'];
			else
				$a['akunpenjualan']='';
		}else
			$a=array();
		$this->content['settingpajak']=$a;
		$this->content['content']='owner/accounting/settingpajak/tampilsettingpajak';
		$this->load->view('owner/template',$this->content);
	}
	
	function input(){
		$this->content['akun']=$this->getakun();
		$this->content['mode']='input';
		$this->content['idakunpajak']='';
		$this->content['idakunsewa']='';
		$this->content['idakunpenjualan']='';
	
		$this->content['content']='owner/accounting/settingpajak/forminputsettingpajak';
		$this->load->view('owner/template',$this->content);
	}
	
	function edit(){
		
		$idsetting=$this->uri->segment(6);
		$this->db->where(array('idsetting'=>$idsetting));
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner_backend/accounting/'.$this->uri->segment(3).'/settingpajak','refresh');
		else{
			$this->content['akun']=$this->getakun();
			$this->content['mode']='edit';
			$this->content['idsetting']=$h['idsetting'];
			$this->content['idakunpajak']=$h['idakunpajak'];
			$this->content['idakunsewa']=$h['idakunsewa'];
			$this->content['idakunpenjualan']=$h['idakunpenjualan'];
		
			$this->content['content']='owner/accounting/settingpajak/forminputsettingpajak';
			$this->load->view('owner/template',$this->content);
		}
	}
	
	function hapus(){
		$idakun=$this->uri->segment(5);
		$this->db->where(array('idakun'=>$idakun));
		$qr=$this->db->get('tb_akun');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner_backend/accounting/akun','refresh');
		else{
			$where=array('idakun'=>$idakun);
			$this->db->delete('tb_akun', $where); 
			redirect(base_url().'owner_backend/accounting/akun','refresh');
		}
	}
	
	function proses(){
		$mode=$this->input->post('mode');
		$idsetting=$this->input->post('idsetting');
		$idjenisakunpajak=$this->input->post('idjenisakunpajak');
		$idjenisakunsewa=$this->input->post('idjenisakunsewa');
		$idjenisakunpenjualan=$this->input->post('idjenisakunpenjualan');

		$this->content['akun']=$this->getakun();
		$this->content['mode']=$mode;
		$this->content['idjenisakunpajak']=$idjenisakunpajak;
		$this->content['idjenisakunsewa']=$idjenisakunsewa;
		$this->content['idjenisakunpenjualan']=$idjenisakunpenjualan;
		
		$this->form_validation->set_rules('idjenisakunpajak', 'Jenis akun pajak', 'required');
		$this->form_validation->set_rules('idjenisakunsewa', 'Jenis akun sewa kamar', 'required');
		$this->form_validation->set_rules('idjenisakunpenjualan', 'Jenis akun penjualan', 'required');
		$this->form_validation->set_message('required', '%s masih kosong');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->content['content']='owner/accounting/settingpajak/forminputsettingpajak';
			$this->load->view('owner/template',$this->content);
		}else{
			
			$namfield=array();
			$namafield=array('idakunpajak'=>$idjenisakunpajak,'idakunsewa'=>$idjenisakunsewa,'idakunpenjualan'=>$idjenisakunpenjualan);
					
			if($mode=='input'){
				$this->db->insert('tb_settingakun', $namafield);
			}
			else{
				$where=array('idsetting'=>$idsetting);
				$this->db->where($where);
				$this->db->update('tb_settingakun', $namafield);
			}
			redirect(base_url().'owner_backend/accounting/'.$this->uri->segment(3).'/settingpajak','refresh');
		}
	}
}