<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting extends CI_Model{
	var $url = 'owner_backend/setting/';
	var $content;
	function __construct(){
		parent::__construct();
	}
	// manage user
	function index(){
		$this->content['h1_title']		= 'Manage User';
		$this->content['content'] 		= 'owner/manage_user';
		$this->content['action'] 		= base_url().$this->url.'add';
		
		$uri = 4;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		//$where = "u.user_id = uk.user_id AND uk.kost_id AND k.kost_id AND user_level IN ('investor','guard')";
		//$posts = $this->site_model->get_data('',"tb_users u,tb_user_kost uk,tb_kosts k",$where,'',"user_name ASC","$limit,$offset")->result();
		//$num_rows = $this->site_model->get_data('',"tb_users u,tb_user_kost uk,tb_kosts k",$where)->num_rows();
		
		$posts = $this->site_model->get_data('',"tb_users","user_level AND user_level IN ('investor','guard')",'',"user_name ASC","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_users","user_level AND user_level IN ('investor','guard')")->num_rows();
		
		if($num_rows > 0){
			$config['base_url'] = base_url().$this->url.'manage/';
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $limit;
			$config['uri_segment'] = $uri;
			$this->pagination->initialize($config);
			$this->content['pagination'] = $this->pagination->create_links();
			
			// Set template tabel, untuk efek selang-seling tiap baris
			$tmpl = array('table_open'=>'<table>');
			$this->table->set_template($tmpl);
	
			// Set heading untuk tabel
			$this->table->set_empty("&nbsp;");
			$no = array('data'=>'No','width'=>'40');
			$actions = array('data'=>'Actions','width'=>'80');
			$this->table->set_heading($no,'User','Kost Guard','Status','Username','Password',$actions);
			
			$i = 0 + $offset;
			foreach($posts as $p){		
				$owner_kost = $this->site_model->get_data('',"tb_user_kost uk,tb_kosts k","uk.kost_id = k.kost_id AND user_id = '".$p->user_id."'")->result();
				$data = array();
				foreach($owner_kost as $o){
					$data[] = $o->kost_title;					
				}
				if(count($owner_kost)) $kost_title = implode(", ",$data); else $kost_title = "";		
				$this->table->add_row(
					++$i,$p->user_name,$kost_title,$p->user_level,$p->user_login,$p->user_b,
					anchor(base_url().$this->url.'edit/'.$p->user_login_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor(base_url().$this->url.'delete/'.$p->user_login_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"))
				);
			}
			$this->content['table'] = $this->table->generate();
		}
		else{
			$this->content['message'] = 'Empty Data!';
		}
		$this->load->view('owner/template',$this->content);
	}
	// add user
	function add(){
		$this->content['h1_title'] 		= 'Add User';
		$this->content['content'] 		= 'owner/owner_form';
		$this->content['action'] 		= base_url().$this->url.'input';
		$this->content['user_login_id'] = '';
		$this->content['kosts'] 		= $this->site_model->get_data('',"tb_kosts k,tb_user_kost uk","k.kost_id = uk.kost_id AND uk.user_id = '".$this->session->userdata('owner_id')."'")->result();
		$this->session->set_userdata('form_mode', 'add');
		
		$this->load->view('owner/template',$this->content);		
	}
	// edit user
	function edit(){
		$this->content['h1_title'] 		= 'Edit Profile';
		$this->content['content'] 		= 'owner/owner_form';
		$this->content['action'] 		= base_url().$this->url.'input';
		$this->content['kosts'] 		= $this->site_model->get_data('',"tb_kosts k,tb_user_kost uk","k.kost_id = uk.kost_id AND uk.user_id = '".$this->session->userdata('owner_id')."'")->result();;
		$this->session->set_userdata('form_mode', 'edit');
		
		$user = $this->site_model->get_data('',"tb_users u,tb_user_kost uk","u.user_id = uk.user_id AND u.user_login_id = '".$this->uri->segment(4)."'")->row();
		$this->content['user_login_id'] = $user->user_login_id;
		$this->content['nickname'] 		= $user->user_name;
		$this->content['username'] 		= $user->user_login;
		$this->content['kost_id'] 		= $user->kost_id;
		
		$this->load->view('owner/template',$this->content);		
	}
	// update user
	function input(){
		$this->content['h1_title'] 		= 'Edit Profile';
		$this->content['content'] 		= 'owner/owner_form';
		$this->content['action'] 		= base_url().$this->url.'input';
		$this->content['user_login_id'] = $this->input->post('user_login_id');
		
		$user_login_id 	= $this->input->post('user_login_id');
		$nickname 		= $this->input->post("nickname");
		$username 		= $this->input->post("username");
		$old_password 	= $this->input->post("old_password");
		$password 		= md5($this->input->post("new_password"));	
		$new_password 	= $this->input->post("new_password");
		$conf_password 	= $this->input->post("conf_password");	
		$type 			= $this->input->post("type");
		$kost 			= $this->input->post("kost_id");
		
		$this->form_validation->set_rules('user_login_id', 'user_login_id', '');
		$this->form_validation->set_rules('nickname', 'nickname', '');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('type', 'type', '');
		$this->form_validation->set_rules('kost_id', 'kost_id', '');
		
		if($this->session->userdata('form_mode')=='add'){
			if($this->form_validation->run() == TRUE){
				// data for tb_users
				$data = array(
					'user_login_id'	=> md5(gen_rand(8)),
					'user_login'	=> $username,
					'user_pass'		=> $password,
					'user_b'		=> $new_password,
					'user_name'		=> $nickname,
					'user_level'	=> 'guard');
				$this->site_model->input_data("tb_users",$data);
				$user_id = $this->db->insert_id();				
				// data for tb_user_kost
				$data = array(
					'kost_id'		=> $kost,
					'user_id'		=> $user_id);
				$this->site_model->input_data("tb_user_kost",$data);
				// message and redirect
				$this->session->set_flashdata('message', '1 Data has been saved!');
				redirect(base_url().$this->url);
			}
		}
		elseif($this->session->userdata('form_mode')=='edit'){
			if($this->form_validation->run() == TRUE){
				if($this->site_model->check_password($user_id,$old_password)==TRUE){
					if(!empty($new_password)){
						if($new_password==$conf_password){
							// data for tb_users
							$data = array(
								'user_login'	=> $username,
								'user_pass'		=> $password,
								'user_b'		=> $new_password,
								'user_name'		=> $nickname);
							$this->site_model->update_data("tb_users",$data,"user_login_id = '".$user_login_id."'");
							// data for tb_user_kost
							$data = array('kost_id'=>$kost);
							$this->site_model->update_data("tb_user_kost",$data,"user_id = '".$user_login_id."'");
							// message and redirect
							$this->session->set_flashdata('message', 'Data was update!');
							redirect(base_url().$this->url.'edit/'.$user_id);
						}
						else {
							$this->content['message']='New Password & Confirm Pass do not match!';
							$this->load->view('owner/template',$this->content);
						}
					}
					else{
						// data for tb_users
						$data = array('user_login'=>$username,'user_name'=>$nickname);
						$this->site_model->update_data("tb_users",$data,"user_login_id = '".$user_id."'");
						// data for tb_user_kost
						$id = $this->site_model->get_data('',"tb_users","user_login_id = '".$user_id."'")->row();
						$data = array('kost_id'=>$kost);
						$this->site_model->update_data("tb_user_kost",$data,"user_id = '".$id->user_id."'");
						// message and redirect
						$this->session->set_flashdata('message', 'Data was update!');
						redirect(base_url().$this->url.'edit/'.$user_id);
					}
				}
				else {
					$this->content['message']='Wrong Present Password!';
					$this->load->view('owner/template',$this->content);
				}
			}
			else{
				$this->load->view('owner/template',$this->content);
			}
		}
	}/*
	// edit member
	function edit(){
		$this->content['h3_title']		= 'update user data';
		$this->content['content'] 		= 'owner/owner_form';
		$this->content['action']		= base_url().$this->url.'update_user';
		
		$admin = $this->site_model->get_data('',"tb_users","user_id = '".$this->session->userdata('ses_owner_id')."'")->row();
		$this->content['user_id'] 		= $admin->user_id;
		$this->content['user_login'] 	= $admin->user_login;
		$this->content['user_nickname'] = $admin->user_name;
		
		$this->load->view('owner/template',$this->content);			
	}	
	
	
	// update user
	function update_user(){
		$this->content['h3_title'] 		= 'edit profile';
		$this->content['content'] 		= 'owner/owner_form';
		$this->content['action'] 		= base_url().$this->url.'update_user';
		
		$user_id = $this->session->userdata('ses_owner_id');
		$nickname = $this->input->post("nickname");
		$username = $this->input->post("username");
		$old_password = $this->input->post("old_password");
		$new_password = $this->input->post("new_password");
		$conf_password = $this->input->post("conf_password");
		$password = md5($new_password);		
		
		$this->form_validation->set_rules('nickname', 'nickname', '');
		$this->form_validation->set_rules('username', 'username', 'required');
				
		if($this->form_validation->run() == TRUE){
			if($this->site_model->check_password($user_id,$old_password)==TRUE){
				if(!empty($new_password)){
					if($new_password==$conf_password){
						$data = array('user_nickname'=>$nickname,'user_login'=>$username,'user_pass'=>$password,'user_b'=>$new_password);
						$this->site_model->update_data("tb_users",$data,"user_id = '$user_id'");
						$this->session->set_flashdata('message', 'Data was update!');
						redirect(base_url().$this->url.'edit');
					}
					else {
						$this->content['message']='New Password & Confirm Pass do not match!';
						$this->load->view('owner/template',$this->content);
					}
				}
				else{
					$data = array('user_nickname'=>$nickname,'user_login'=>$username);
					$this->site_model->update_data("tb_users",$data,"user_id = '$user_id'");
					$this->session->set_flashdata('message', 'Data was update!');
					redirect(base_url().$this->url.'edit');
				}
			}
			else {
				$this->content['message']='Wrong Present Password!';
				$this->load->view('owner/template',$this->content);
			}
		}
		else{
			$this->load->view('owner/template',$this->content);
		}
	}*/
}