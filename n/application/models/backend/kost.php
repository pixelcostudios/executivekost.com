<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kost extends CI_Model{
	var $url;
	var $content;
	function __construct(){
		parent::__construct();
		$this->url = base_url()."backend/kost/";
	}
	
	/* COMMENT */
	// view comment
	function comment(){
		$this->content['h1_title']		= "Comments";
		$this->content['content'] 		= "backend/post/comment";
		
		$uri = 4;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$where = "c.parent_id = k.kost_id and parent_type = 'kost'";
		$comments = $this->site_model->get_data('',"tb_comments c,tb_kosts k",$where,'',"comment_id desc","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_comments c,tb_kosts k",$where)->num_rows();
		
		if($num_rows > 0){
			// Set heading
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No','Name','Comment');
			
			$i = 0 + $offset;
			foreach($comments as $c){
				$date = date("d/m/Y", strtotime($c->comment_date));
				$time = date("H:i", strtotime($c->comment_date));
				$comment_date = "<b>".$date."</b> at <b>".$time."</b>";
				if($c->comment_status=='0') $status = anchor($this->url."publish_comment/".$c->comment_id."/1","Approve");
				if($c->comment_status=='1') $status = anchor($this->url."publish_comment/".$c->comment_id."/0","Unapprove");
				if($c->comment_status=='0') $status_type = "unapprove"; else $status_type = "approve";
				if($c->comment_lang=='id') $comment_content = '<label class="lang-id">'.$c->kost_title.'</label>';
				if($c->comment_lang=='en') $comment_content = '<label class="lang-en">'.$c->kost_title_en.'</label>';
				
				$this->table->add_row(++$i,$c->name.br(1).$c->email.br(1).$status,$comment_content.br(1).'Submitted on '.$comment_date.', comment status ['.$status_type.']'.' '.anchor($this->url.'delete_comment/'.$c->comment_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')")).br(1).'<font color="#999999">'.$c->comment_content.'</font>'
				);
			}
			
			$config['base_url'] 			= $this->url."comment/";
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination'] 	= $this->pagination->create_links();
			$this->content['table'] 		= $this->table->generate();
		}
		else{
			$this->content['message'] = "Empty Data!";
		}
		
		$this->load->view('backend/template',$this->content);
	}
	// publish comment
	function publish_comment(){
		$id = $this->uri->segment(4);
		$set = $this->uri->segment(5);
		$this->site_model->update_data("tb_comments",$data=array('comment_status'=>$set),"comment_id = '$id'");
		redirect($this->url);
	}
	// delete comment
	function delete_comment(){
		$id = $this->uri->segment(4);
		$this->site_model->del_data("tb_comments",$data=array('comment_id'=>$id));	
		redirect($this->url);
	}
	// view
	function view(){
		$this->content['h1_title']		= "Manage Kost";
		$this->content['content'] 		= "backend/default";
		$this->content['data_link'] 	= $this->url."list/";
		$this->content['form_link'] 	= $this->url."add";
		
		$uri = 4;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$posts = $this->site_model->get_data('',"tb_kosts k,tb_daerah d","k.daerah_id = d.daerah_id",'',"kost_id desc","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_kosts k,tb_daerah d","k.daerah_id = d.daerah_id")->num_rows();
		
		if($num_rows > 0){
			$config['base_url'] 			= $this->url."view/";
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			$this->content['pagination'] 	= $this->pagination->create_links();
			
			// Set template tabel, untuk efek selang-seling tiap baris
			$tmpl = array('table_open'=>'<table>');
			$this->table->set_template($tmpl);
	
			// Set heading untuk tabel
			$this->table->set_empty("&nbsp;");
			$no = array('data'=>'No','width'=>'40');
			$actions = array('data'=>'Actions','width'=>'80');
			$this->table->set_heading($no,'Kost Name','Owner','Kota','Status',$actions);
			
			$i = 0 + $offset;
			foreach($posts as $p){
				$owner = $this->site_model->get_data('',"tb_users u,tb_user_kost uk","u.user_id = uk.user_id AND uk.kost_id = '".$p->kost_id."'")->row();
				if($p->kost_status==1) $kost_status	= "publish"; else $kost_status = "unpublish";	
				$this->table->add_row(
					++$i.'.','<label class="lang-id">'.$p->kost_title.br(1).'</label><label class="lang-en">'.$p->kost_title_en.'</label>',
					isset($owner->user_name)?$owner->user_name:'',$p->daerah_title,$kost_status,
					anchor($this->url.'edit/'.$p->kost_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor($this->url.'delete/'.$p->kost_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"))
				);
			}
			$this->content['table'] = $this->table->generate();
		}
		else{
			$this->content['message'] = "Empty Data!";
		}
		
		$this->load->view('backend/template',$this->content);
	}
	// add
	function add(){
		$this->content['h1_title'] 		= "Manage Kost";
		$this->content['content'] 		= "backend/kost/kost_form";
		$this->content['data_link'] 	= $this->url."list/";
		$this->content['form_link'] 	= $this->url."add";
		$this->content['action'] 		= $this->url."input";
		$this->content['kota']			= $this->site_model->get_data('',"tb_daerah")->result();
		$this->content['old_sketch'] 	= "";
		$this->content['images'] 		= array();
		$this->session->set_userdata('form_mode', 'add');
		
		$this->load->view('backend/template',$this->content);
	}
	// edit
	function edit(){
		$id = $this->uri->segment(4);
		$this->content['h1_title'] 		= "Manage Kost";
		$this->content['content'] 		= "backend/kost/kost_form";
		$this->content['data_link'] 	= $this->url."list/";
		$this->content['form_link'] 	= $this->url."add";
		$this->content['action'] 		= $this->url."input";
		$this->content['kota']			= $this->site_model->get_data('',"tb_daerah")->result();
		$this->session->set_userdata('form_mode', 'edit');
		// data kost
		$data = $this->site_model->get_data('',"tb_kosts","kost_id = '".$id."'")->row();
		$this->session->set_userdata('kost_id', $data->kost_id);
		$this->content['daerah_id']			= $data->daerah_id;
		$this->content['title'] 			= $data->kost_title;
		$this->content['title_en']			= $data->kost_title_en;
		$this->content['des'] 				= $data->kost_content;
		$this->content['des_en']			= $data->kost_content_en;
		$this->content['facility'] 			= $data->kost_facility;
		$this->content['facility_en']		= $data->kost_facility_en;
		$this->content['virtual_tour'] 		= $data->kost_virtual_tour;
		$this->content['old_sketch'] 		= $data->kost_sketch;
		$data_map = explode(',',$data->kost_map);
		$this->content['latitude']			= $data_map[0];
		$this->content['longitude'] 		= $data_map[1];
		$this->content['kost_status']		= $data->kost_status;
		$this->content['images']			= $this->site_model->get_img("parent_id = '".$data->kost_id."' and relation = 'kost'","0,10")->result();
		
		$this->load->view('backend/template',$this->content);			
	}
	// input
	function input(){
		$this->content['h1_title'] 		= "Manage Kost";
		$this->content['content']		= "backend/kost/kost_form";
		$this->content['action']		= $this->url."input";
		$this->content['kota']			= $this->site_model->get_data('',"tb_daerah")->result();
		$type_file 						= "gif|jpg|jpeg|png";
		$this->content['old_sketch'] 	= "";
		
		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('title_en', 'title_en', '');
		$this->form_validation->set_rules('des', 'des', '');		
		$this->form_validation->set_rules('des_en', 'des_en', '');
		$this->form_validation->set_rules('facility', 'facility', '');		
		$this->form_validation->set_rules('facility_en', 'facility_en', '');
		$this->form_validation->set_rules('kota', 'kota', '');	
		$this->form_validation->set_rules('virtual_tour', 'virtual_tour', '');		
		$this->form_validation->set_rules('old_sketch', 'old_sketch', '');
		$this->form_validation->set_rules('latitude', 'latitude', '');		
		$this->form_validation->set_rules('longitude', 'longitude', '');	
		$this->form_validation->set_rules('kost_status', 'kost_status', '');
		
		$this->content['latitude'] = $this->input->post("latitude");
		$this->content['longitude'] = $this->input->post("longitude");
		
		create_dir('kosts');
		$dir = date("Y").'/'.date("m");
		
		if($this->session->userdata('form_mode')=='add'){	
			$this->content['images'] = array();
			if($this->form_validation->run() == TRUE){
				if($this->site_model->check_data("kost_title","tb_kosts",$this->input->post("title"))){
					$config['upload_path'] 		= "./uploads/kosts/".$dir."/";
					$config['allowed_types'] 	= $type_file;
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload('kost_sketch')){
						$this->content['message'] = $this->upload->display_errors('','');
						$this->load->view('backend/template',$this->content);
					}	
					else {
						// kost sketch
						$sketch = $this->fm->upload_img($type_file,$this->input->post("title")."_sketch");
						$sketch_name = slug($this->input->post("title")).'_sketch'.$sketch['ext'];
						// kost map
						$map = $this->input->post("latitude").','.$this->input->post("longitude");
						// data to database
						$data = array(
							'daerah_id'				=> $this->input->post("kota"),
							'kost_title'			=> $this->input->post("title"),
							'kost_title_en'			=> $this->input->post("title_en"),
							'kost_name'				=> slug($this->input->post("title")),
							'kost_name_en'			=> slug($this->input->post("title_en")),
							'kost_content'			=> $this->input->post("des"),
							'kost_content_en'		=> $this->input->post("des_en"),
							'kost_facility'			=> $this->input->post("facility"),
							'kost_facility_en'		=> $this->input->post("facility_en"),
							'kost_virtual_tour'		=> $this->input->post("virtual_tour"),
							'kost_map'				=> $map,
							'kost_sketch'			=> "uploads/kosts/".$dir."/".$sketch_name);
						// save to database
						$this->site_model->input_data("tb_kosts",$data);
						$data_id = $this->db->insert_id();
						// data to database
						$data = array(
							'kost_id'				=> $data_id,
							'user_id'				=> $this->input->post("owner"));
						// save to database
						$this->site_model->input_data("tb_user_kost",$data);
						// upload image
						$this->fm->multi_upload_img("uploads/kosts/".$dir,$type_file,$this->input->post("title"),$data_id,"kost");	
						// message			
						$this->session->set_flashdata('message', '1 Data has been saved!');
						redirect($this->url."list");
					}
				}
				else {
					$this->content['message'] = "Title already exists in the database";
					$this->load->view('backend/template',$this->content);
				}		
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
		elseif($this->session->userdata('form_mode')=='edit'){
			$id = $this->session->userdata('kost_id');
			$this->content['images'] = $this->site_model->get_img("parent_id = '".$this->session->userdata('kost_id')."' and relation = 'kost'","0,10")->result();
			if($this->form_validation->run() == TRUE){	
				if(!empty($_FILES['kost_sketch']['name'])){
					$config['upload_path'] 		= "./uploads/kosts/".$dir."/";
					$config['allowed_types'] 	= $type_file;
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload('kost_sketch')){
						$this->content['message'] = $this->upload->display_errors('','');
						$this->load->view('backend/template',$this->content);
					}	
					else {
						if($this->input->post("old_sketch")!=''){
							$image = $this->site_model->get_data('',"tb_kosts","kost_id = '$id'")->row();
							$x = explode("/",$image->kost_sketch);
							$del	= "./".$image->kost_sketch;
							$del2	= "./".$x[0]."/".$x[1]."/".$x[2]."/".$x[3]."/".thumb($x[4]);
							if(file_exists($del)) unlink($del);
							if(file_exists($del2)) unlink($del2);
						}
						// kost sketch
						$sketch = $this->fm->upload_img($type_file,$this->input->post("title")."_sketch");
						$sketch_name = "uploads/kosts/".$dir."/".slug($this->input->post("title"))."_sketch".$sketch['ext'];
					}
				}
				else {
					$sketch_name = $this->input->post("old_sketch");
				}
				// kost map
				$map = $this->input->post("latitude").",".$this->input->post("longitude");
				// data to database
				$data = array(
					'daerah_id'				=> $this->input->post("kota"),
					'kost_title'			=> $this->input->post("title"),
					'kost_title_en'			=> $this->input->post("title_en"),
					'kost_name'				=> slug($this->input->post("title")),
					'kost_name_en'			=> slug($this->input->post("title_en")),
					'kost_content'			=> $this->input->post("des"),
					'kost_content_en'		=> $this->input->post("des_en"),
					'kost_facility'			=> $this->input->post("facility"),
					'kost_facility_en'		=> $this->input->post("facility_en"),
					'kost_virtual_tour'		=> $this->input->post("virtual_tour"),
					'kost_map'				=> $map,
					'kost_sketch'			=> $sketch_name,
					'kost_status'			=> $this->input->post("kost_status"));
				// save to database
				$this->site_model->update_data("tb_kosts",$data,"kost_id = '".$id."'");
				// data to database
				$data = array(
					'user_id'				=> $this->input->post("owner"));
				// save to database
				$this->site_model->update_data("tb_user_kost",$data,"kost_id = '".$id."'");
				// upload image
				$this->fm->multi_upload_img("uploads/kosts/".$dir,$type_file,$this->input->post("title"),"$id","kost");						
				// message
				$this->session->set_flashdata('message', 'Data ( '.$this->input->post("title").' / '.$this->input->post("title_en").' ) has been update!');
				$this->session->unset_userdata('kost_id');
				redirect($this->url."edit/".$id);
			}
			else{				
				$this->load->view('backend/template',$this->content);	
			}
		}
	}
	// delete post
	function delete(){
		$id = $this->uri->segment(4);
		$kost_sketch = $this->site_model->get_data('',"tb_kosts","kost_id = '$id'")->row();		
		$x = explode("/",$kost_sketch->kost_sketch);
		$del	= "./".$kost_sketch->kost_sketch;
		$del2	= "./".$x[0]."/".$x[1]."/".$x[2]."/".$x[3]."/".thumb($x[4]);
		if(file_exists($del)) unlink($del);
		if(file_exists($del2)) unlink($del2);
		$images = $this->site_model->get_data('',"tb_images","parent_id = '$id' and relation = 'kost'")->result();
		foreach($images as $image){
			if(!empty($image->image)){
				$del	= "./".$image->dir."/".$image->image;
				$del2	= "./".$image->dir."/".thumb($image->image);
				if(file_exists($del)) unlink($del);
				if(file_exists($del2)) unlink($del2);
			}
			$this->site_model->del_data("tb_images",$data=array('image_id'=>$image->image_id));	
		}
		$this->site_model->del_data("tb_comments",$data=array('parent_id'=>$id,'parent_type'=>'kost'));
		$this->site_model->del_data("tb_kosts",$data=array('kost_id'=>$id));
		$this->site_model->del_data("tb_user_kost",$data=array('kost_id'=>$id));
		$this->session->set_flashdata('message', 'Data has been delete!');
		redirect($this->url."list");		
	}
	// delete post image
	function delete_image(){
		$id = $this->uri->segment(4);
		$idpost = $this->uri->segment(5);
		$image = $this->site_model->get_img("image_id = '$id'")->row();
		$del	= "./".$image->dir."/".$image->image;
		$del2	= "./".$image->dir."/".thumb($image->image);
		if(file_exists($del)) unlink($del);
		if (file_exists($del2)) unlink($del2);
		$this->site_model->del_data("tb_images",$data=array('image_id'=>$id));				
		$this->session->set_flashdata('message', 'Image has been delete!');
		redirect($this->url."edit/".$idpost);	
	}
}