<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends CI_Model{
	var $url;
	var $content;
	function __construct(){
		parent::__construct();
		$this->url = base_url()."backend/page/";
	}
	
	function home(){
		$this->content['h1_title']		= "<h1>Use left menu to Manage Page</h1>";
		$this->content['text']		    = "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>";
		$this->content['content'] 		= 'backend/empty';
		$this->load->view('backend/template',$this->content);		
	}
	/* PAGE */
	// view post
	function view(){
		$this->content['h1_title']		= "Manage Page";
		$this->content['content'] 		= "backend/default";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url.'add';
		
		$uri = 4;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$posts = $this->site_model->get_data('',"tb_pages",'','',"page_id desc","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_pages",'')->num_rows();
		
		if($num_rows > 0){
			// Set heading
			$this->table->set_empty("&nbsp;");
			$no = array('data'=>'No','width'=>'40');
			$date = array('data'=>'Status','width'=>'100');
			$actions = array('data'=>'Actions','width'=>'80');
			$this->table->set_heading($no,'Title',$date,$actions);
			
			$i = 0 + $offset;
			foreach($posts as $p){	
				if($p->page_status==1) $kost_status	= "publish"; else $kost_status = "unpublish";			
				$this->table->add_row(
					++$i.'.',
					'<label class="lang-id">'.$p->page_title.br(1).'</label><label class="lang-en">'.$p->page_title_en.'</label>',
					$kost_status,
					anchor($this->url.'edit/'.$p->page_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor($this->url.'delete/'.$p->page_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"))
				);
			}
			
			$config['base_url'] 			= $this->url."view/";
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination'] 	= $this->pagination->create_links();
			$this->content['table']			= $this->table->generate();
		}
		else{
			$this->content['message'] = "Empty Data!";
		}		
		$this->load->view('backend/template',$this->content);
	}
	// add page
	function add(){
		$this->content['h1_title']		= "Manage Page";
		$this->content['content'] 		= "backend/page/page_form";
		$this->content['action'] 		= $this->url."input";
		$this->content['images'] 		= array();
		$this->session->set_userdata('form_mode', 'add');
		
		$this->load->view('backend/template',$this->content);
	}
	// edit page
	function edit(){
		$id = $this->uri->segment(4);
		$this->content['h1_title']		= "Manage Page";
		$this->content['content'] 		= "backend/page/page_form";
		$this->content['action'] 		= $this->url."input";
		$this->session->set_userdata('form_mode', 'edit');
		
		$page = $this->site_model->get_data('',"tb_pages","page_id = '$id'")->row();
		
		$this->session->set_userdata('page_id', $page->page_id);
		$this->content['default']['title'] 			= $page->page_title;
		$this->content['default']['title_en']		= $page->page_title_en;
		$this->content['default']['des'] 			= $page->page_content;
		$this->content['default']['des_en']			= $page->page_content_en;
		$this->content['default']['page_status']	= $page->page_status;
		$this->content['images']					= $this->site_model->get_img("parent_id = '".$page->page_id."' and relation = 'page'","0,10")->result();
		
		$this->load->view('backend/template',$this->content);			
	}
	// input post
	function input(){
		$this->content['h1_title']		= "Manage Page";
		$this->content['content']		= "backend/page/page_form";
		$this->content['action']		= $this->url."input";
		$type_file 						= "gif|jpg|jpeg|png";
		
		$check = "page_title,tb_pages";
		$this->form_validation->set_rules('title', 'title', 'required|callback_check_title['.$check.']');
		$this->form_validation->set_rules('title_en', 'title_en', '');
		$this->form_validation->set_rules('des', 'des', '');		
		$this->form_validation->set_rules('des_en', 'des_en', '');
		
		create_dir('pages');
		$dir = date("Y").'/'.date("m");
		
		if($this->session->userdata('form_mode')=='add'){	
			$this->content['images'] = array();
			if($this->form_validation->run() == TRUE){
				// data to database
				$data = array(
					'page_title'			=> $this->input->post("title"),
					'page_title_en'			=> $this->input->post("title_en"),
					'page_name'				=> slug($this->input->post("title")),
					'page_name_en'			=> slug($this->input->post("title_en")),
					'page_content'			=> $this->input->post("des"),
					'page_content_en'		=> $this->input->post("des_en"));
				// save to database
				$this->site_model->input_data("tb_pages",$data);
				// upload image
				$data_id = $this->db->insert_id();
				$this->fm->multi_upload_img("uploads/pages/".$dir,$type_file,$this->input->post("title"),$data_id,"page");		
				// message						
				$this->session->set_flashdata('message', '1 Data has been saved!');
				redirect($this->url."view");		
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
		elseif($this->session->userdata('form_mode')=='edit'){
			$id = $this->session->userdata('page_id');
			$this->content['images'] = $this->site_model->get_img("parent_id = '".$id."' and relation = 'page'","0,10")->result();
			if($this->form_validation->run() == TRUE){
				$data = array(
					'page_title'			=> $this->input->post("title"),
					'page_title_en'			=> $this->input->post("title_en"),
					'page_name'				=> slug($this->input->post("title")),
					'page_name_en'			=> slug($this->input->post("title_en")),
					'page_content'			=> $this->input->post("des"),
					'page_content_en'		=> $this->input->post("des_en"),
					'page_status'			=> $this->input->post("page_status"));
				// save to database
				$this->site_model->update_data("tb_pages",$data,"page_id = '".$id."'");
				$this->fm->multi_upload_img("uploads/pages/".$dir,$type_file,$this->input->post("title"),"$id","page");
				// message					
				$this->session->set_flashdata('message', 'Data ( '.$this->input->post("title").' / '.$this->input->post("title_en").' ) has been update!');
				$this->session->unset_userdata('page_id');
				redirect($this->url."edit/".$id);
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
	}
	// delete post
	function delete(){
		$id = $this->uri->segment(4);
		$images = $this->site_model->get_data('',"tb_images","parent_id = '$id' and relation = 'page'")->result();
		foreach($images as $image){
			if(!empty($image->image)){
				$del	= "./".$image->dir."/".$image->image;
				$del2	= "./".$image->dir."/".thumb($image->image);
				if(file_exists($del)) unlink($del);
				if(file_exists($del2)) unlink($del2);
			}
			$this->site_model->del_data("tb_images",$data=array('image_id'=>$image->image_id));	
		}
		$this->site_model->del_data("tb_pages",$data=array('page_id'=>$id));					
		$this->session->set_flashdata('message', 'Data has been delete!');
		redirect($this->url."view");		
	}
	// delete post image
	function delete_image(){
		$id = $this->uri->segment(4);
		$page_id = $this->uri->segment(5);
		$image = $this->site_model->get_img("image_id = '$id'")->row();
		$del	= "./".$image->dir."/".$image->image;
		$del2	= "./".$image->dir."/".thumb($image->image);
		if(file_exists($del)) unlink($del);
		if (file_exists($del2)) unlink($del2);
		$this->site_model->del_data("tb_images",$data=array('image_id'=>$id));				
		$this->session->set_flashdata('message', 'Image has been delete!');
		redirect($this->url."edit/".$page_id);	
	}
}