<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Backup extends CI_Model{
	var $url = "backend/setting/";
	var $content;
	function __construct(){
		parent::__construct();
	}
	// backup
	function index(){
		$del = "./uploads/mybackup.sql";
		if(file_exists($del))unlink($del);
		$this->content['h1_title'] 		= "backup database";
		$this->content['action']		= base_url().$this->url."backup_database";
		$this->content['content']		= "backend/setting/backup_database";
			
		// Set template tabel, untuk efek selang-seling tiap baris
		$tmpl = array('table_open'=>'<table class="auto">');
		$this->table->set_template($tmpl);

		// Set heading untuk tabel
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('','Table Name','Total Rows','Data Size','Last Update');
		
		$table = "db_ekost";
		$databases = $this->db->query("SHOW TABLE STATUS FROM ".$table."")->result_array();
		foreach($databases as $db){
			$total = $db['Data_length']+$db['Index_length'];
			if($total < 1024)
				$total_lenght = number_format($total,1,'.',',').' KB';
			elseif(($total > 1024) && ($total < 1048576))
				$total_lenght = number_format($total/1024,1,'.',',').' KB';
			elseif ($total >= 1048576)
				$total_lenght = number_format($total/1048576,1,'.',',').' MB';
			$row_cell = array('data'=>$db['Rows'],'align'=>'right');
			$size_cell = array('data'=>$total_lenght,'align'=>'right');
			$this->table->add_row(
				form_checkbox('cek[]', $db['Name'], '','onClick="toggleController(this)"'),$db['Name'],$row_cell,$size_cell,date("d m Y | H:i:s",strtotime($db['Update_time']))
			);
		}
		$this->content['table'] = $this->table->generate();
		
		$this->load->view('backend/template',$this->content);	
	}
	// backup database
	function backup_database(){
		$check = $this->input->post("cek");
		// Load the DB utility class
		$this->load->dbutil();		
		// Backup your entire database and assign it to a variable
		$prefs = array(
                'tables'      => $check, // Array of tables to backup.
                'ignore'      => array(),           		// List of tables to omit from the backup
                'format'      => 'zip',             		// gzip, zip, txt
                'filename'    => 'mybackup.sql',    		// File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              		// Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              		// Whether to add INSERT data to backup file
                'newline'     => "\n"               		// Newline character used in backup file
              );
		$backup = $this->dbutil->backup($prefs);
		
		// Load the file helper and write the file to your server
		$this->load->helper('file');
		write_file('./uploads/mybackup.sql', $backup); 
		
		redirect(base_url().$this->url.'download/');		
	}
	// download
	function download(){
		$this->content['h1_title']		= "<h1>Download ....</h1>";
		$this->content['text']		    = "<p>Click <b><a href=\"".base_url()."backend/setting/download_database\" onclick=\"setTimeout('history.go(-1);',2000);\">here</a></b> for download backup.</p>";
		$this->content['content'] 		= 'backend/empty';
		$this->load->view('backend/template',$this->content);	
	}
	// download database
	function download_database(){
		// Load the download helper and send the file to your desktop
		$this->load->helper('download');
		$data = file_get_contents("./uploads/mybackup.sql"); // Read the file's contents
		force_download('mybackup.sql', $data);
	}
}