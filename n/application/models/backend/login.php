<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Model{
	var $content;
	function __construct(){
		parent::__construct();
	}
	
	function index(){		
		if($this->session->userdata('admin_login')==TRUE){
			redirect(base_url().'backend/home');
		}
	}
	
	function process_login(){
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
		
		if($this->form_validation->run() == TRUE){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$user_level = $this->site_model->check_login($username,$password);
			
			if($user_level!=''){
				if($user_level=='admin'){
					$user = $this->site_model->get_data('',"tb_users","user_login = '".$username."' AND user_level = '".$user_level."'")->row();
					$data = array('ses_admin_id'=>$user->user_login_id,'admin_username'=>$user->user_name,'admin_level'=>$user->user_level,'admin_login'=>TRUE);
					$this->session->set_userdata($data);
					redirect(base_url().'backend/home');
				}
				else{
					$this->session->set_flashdata('message', 'You are not allowed to access this page!');
					redirect(base_url().'login');
				}
			}
			else{
				$this->session->set_flashdata('message', 'Invalid username / password');
				redirect(base_url().'login');
			}
		}
		else{
			$this->load->view('backend/login');
		}
	}
}