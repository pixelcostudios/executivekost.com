<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Post extends CI_Model{
	var $url;
	var $content;
	function __construct(){
		parent::__construct();
		$this->url = base_url()."backend/post/";
	}
	
	/* COMMENT */
	// view
	function comment(){
		$this->content['h1_title']		= "Post Comments";
		$this->content['content'] 		= "backend/post/comment";
		
		$uri = 4;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$where = "c.parent_id = p.post_id and parent_type = 'post' and p.category_id = ct.category_id";
		$comments = $this->site_model->get_data('',"tb_comments c,tb_posts p,tb_category ct",$where,'',"comment_id desc","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_comments c,tb_posts p,tb_category ct",$where)->num_rows();
		
		if($num_rows > 0){	
			// Set heading
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No','Name','Comment');
			
			$i = 0 + $offset;
			foreach($comments as $c){
				$date = date("d/m/Y", strtotime($c->comment_date));
				$time = date("H:i", strtotime($c->comment_date));
				$comment_date = "<b>".$date."</b> at <b>".$time."</b>";
				if($c->comment_status=='0') $status = anchor($this->url.'publish_comment/'.$c->comment_id.'/1','Approve');
				if($c->comment_status=='1') $status = anchor($this->url.'publish_comment/'.$c->comment_id.'/0','Unapprove');
				if($c->comment_status=='0') $status_type = "unapprove"; else $status_type = "approve";
				if($c->comment_lang=='id') $comment_content = '<label class="lang-id">'.$c->post_title.' ['.$c->category_title.']</label>';
				if($c->comment_lang=='en') $comment_content = '<label class="lang-en">'.$c->post_title_en.' ['.$c->category_title_en.']</label>';
				
				$this->table->add_row(++$i,$c->name.br(1).$c->email.br(1).$status,$comment_content.br(1).'Submitted on '.$comment_date.', comment status ['.$status_type.']'.' '.anchor($this->url.'delete_comment/'.$c->comment_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')")).br(1).'<font color="#999999">'.$c->comment_content.'</font>'
				);
			}
			
			$config['base_url'] 			= $this->url."comment/";
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination'] 	= $this->pagination->create_links();
			$this->content['table'] 		= $this->table->generate();
		}
		else{
			$this->content['message'] = "Empty Data!";
		}
		
		$this->load->view('backend/template',$this->content);
	}
	// publish
	function publish_comment(){
		$id = $this->uri->segment(4);
		$set = $this->uri->segment(5);
		$this->site_model->update_data("tb_comments",$data=array('comment_status'=>$set),"comment_id = '$id'");
		redirect($this->url );
	}
	// delete
	function delete_comment(){
		$id = $this->uri->segment(4);
		$this->site_model->del_data("tb_comments",$data=array('comment_id'=>$id));	
		redirect($this->url );
	}
	/* POST */
	// view
	function view(){
		$cat_id = $this->uri->segment(4);
		$category = $this->site_model->get_data('',"tb_category","category_id = '".$cat_id."'")->row();
		$this->content['h1_title']		= $category->category_title." / ".$category->category_title_en;
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add/".$category->category_id;
		$this->content['content'] 		= "backend/default";
		
		$uri = 5;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$posts = $this->site_model->get_data('',"tb_posts","category_id = '".$cat_id."'",'',"post_id desc","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_posts","category_id = '".$cat_id."'")->num_rows();
		
		if($num_rows > 0){
			// Set heading
			$this->table->set_empty("&nbsp;");
			$no 		= array('data'=>'No','width'=>'40');
			$date 		= array('data'=>'Date','width'=>'230');
			$actions	= array('data'=>'Actions','width'=>'80');
			$this->table->set_heading($no,'Title',$date,$actions);
			
			$i = 0 + $offset;
			foreach($posts as $p){
				if($p->post_status==1) $post_status	= "publish"; else $post_status = "unpublish";
				$date = date("d/m/Y", strtotime($p->post_date));
				$time = date("H:i", strtotime($p->post_date));
				$post_date = "<b>".$date."</b> at <b>".$time."</b>";
				$this->table->add_row(
					++$i.'.',
					'<label class="lang-id">'.anchor(base_url().$p->post_name,$p->post_title.br(1),array('target'=>'_blank')).'</label>
					<label class="lang-en">'.anchor(base_url().$p->post_name_en,$p->post_title_en,array('target'=>'_blank')).'</label>',
					$post_status.'.'.$post_date,
					anchor($this->url.'edit/'.$p->category_id.'/'.$p->post_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor($this->url.'delete/'.$p->category_id.'/'.$p->post_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')")));
			}
			
			$config['base_url'] 			= $this->url."view/".$cat_id."/";
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination'] 	= $this->pagination->create_links();
			$this->content['table'] 		= $this->table->generate();
		}
		else{
			$this->content['message'] = "Empty Data!";
		}
		
		$this->load->view('backend/template',$this->content);
	}
	// search post
	function search(){
		$this->content['content'] 		= "backend/post/post";
		
		if($this->input->post("search")!=""){
			$hasilcari = $this->site_model->search($this->input->post("search"),$this->input->post("post_category_id"))->result();			
			if(count($hasilcari)>0){		
				// Set heading
				$this->table->set_empty("&nbsp;");
				$no 		= array('data'=>'No','width'=>'40');
				$date 		= array('data'=>'Date','width'=>'230');
				$actions 	= array('data'=>'Actions','width'=>'80');
				$this->table->set_heading($no,'Title',$date,$actions);
				
				$i = 0;
				foreach($hasilcari as $p){
					$date = date("d/m/Y", strtotime($p->post_date));
					$time = date("H:i", strtotime($p->post_date));
					$post_date = "<b>".$date."</b> at <b>".$time."</b>";
					
					$this->table->add_row(
						++$i,
						'<label class="lang-id">'.$p->post_title.br(1).'</label><label class="lang-en">'.$p->post_title_en.'</label>',
						$p->post_status.'.'.$post_date,
						anchor($this->url.'edit/'.$p->post_id,'edit',array('class'=>'edit')).' '.
						anchor($this->url.'delete/'.$p->post_id,'delete',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"))
					);
				}
				$this->content['table'] = $this->table->generate();
			}
			else {
				$this->content['message'] = "Sorry, the data you are looking for could not be found";
			}
		}
		else {
			$this->content['message'] = "Sorry, the data you are looking for could not be found";
		}
		$this->load->view('backend/template',$this->content);
	}
	// add post
	function add(){
		$cat_id = $this->uri->segment(4);
		$category = $this->site_model->get_data('',"tb_category","category_id = '".$cat_id."'")->row();
		$this->content['h1_title'] 		= $category->category_title." / ".$category->category_title_en;
		$this->content['content'] 		= "backend/post/post_form";
		$this->content['data_link'] 	= $this->url."view/".$category->category_id;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['action'] 		= $this->url."input";
		$this->content['images'] 		= array();
		$this->session->set_userdata('form_mode', 'add');
		
		$this->load->view('backend/template',$this->content);
	}
	// edit post
	function edit(){
		$cat_id = $this->uri->segment(4);
		$post_id = $this->uri->segment(5);
		$category = $this->site_model->get_data('',"tb_category","category_id = '".$cat_id."'")->row();
		$this->content['h1_title'] 		= $category->category_title." / ".$category->category_title_en;
		$this->content['content']		= "backend/post/post_form";
		$this->content['data_link'] 	= $this->url."view/".$category->category_id;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['action']		= $this->url."input";
		$this->session->set_userdata('form_mode', 'edit');
		
		$post = $this->site_model->get_data('',"tb_posts","post_id = '".$post_id."'")->row();		
		$this->content['default']['post_id'] 		= $post->post_id;
		$this->content['default']['category_id'] 	= $post->category_id;
		$this->content['default']['title'] 			= $post->post_title;
		$this->content['default']['title_en']		= $post->post_title_en;
		$this->content['default']['des'] 			= $post->post_content;
		$this->content['default']['des_en']			= $post->post_content_en;
		$this->content['default']['blockquote'] 	= $post->post_blockquote;
		$this->content['default']['blockquote_en']	= $post->post_blockquote_en;
		$this->content['default']['meta_des'] 		= $post->meta_des;
		$this->content['default']['meta_des_en']	= $post->meta_des_en;
		$this->content['default']['meta_key'] 		= $post->meta_key;
		$this->content['default']['meta_key_en']	= $post->meta_key_en;
		$this->content['default']['post_status']	= $post->post_status;
		$this->content['images']					= $this->site_model->get_img("parent_id = '".$post->post_id."' and relation = 'post'","0,10")->result();
		
		$this->load->view('backend/template',$this->content);			
	}
	// input post
	function input(){
		$category = $this->site_model->get_data('',"tb_category","category_id = '".$this->input->post('post_category_id')."'")->row();
		$this->content['h1_title'] 		= anchor($this->url.'view/'.$category->category_id.'',$category->category_title.' / '.$category->category_title_en).' &raquo; '.$this->session->userdata('form_mode');
		$this->content['content']		= 'backend/post/post_form';
		$this->content['action']		= $this->url."input";
		$type_file 						= "gif|jpg|jpeg|png";
		
		$check = "post_title,tb_posts";
		$this->form_validation->set_rules('post_id', 'post_id', '');
		$this->form_validation->set_rules('post_category_id', 'post_category_id', '');
		$this->form_validation->set_rules('title', 'title', 'required|callback_check_title['.$check.']');
		$this->form_validation->set_rules('title_en', 'title_en', '');
		$this->form_validation->set_rules('des', 'des', '');		
		$this->form_validation->set_rules('des_en', 'des_en', '');	
		$this->form_validation->set_rules('blockquote', 'blockquote', '');	
		$this->form_validation->set_rules('blockquote_en', 'blockquote_en', '');	
		$this->form_validation->set_rules('meta_des', 'meta_des', '');	
		$this->form_validation->set_rules('meta_des_en', 'meta_des_en', '');	
		$this->form_validation->set_rules('meta_key', 'meta_key', '');	
		$this->form_validation->set_rules('meta_key_en', 'meta_key_en', '');
		$this->form_validation->set_rules('post_status', 'post_status', '');
		
		create_dir('posts');
		$dir = date("Y").'/'.date("m");
		
		if($this->session->userdata('form_mode')=='add'){
			$this->content['images'] = array();
			if($this->form_validation->run() == TRUE){
				// data to database
				$data = array(
					'category_id'			=> $this->input->post("post_category_id"),
					'user_id'				=> $this->session->userdata("ses_admin_id"),
					'post_title'			=> $this->input->post("title"),
					'post_title_en'			=> $this->input->post("title_en"),
					'post_name'				=> slug($this->input->post("title")),
					'post_name_en'			=> slug($this->input->post("title_en")),
					'post_content'			=> $this->input->post("des"),
					'post_content_en'		=> $this->input->post("des_en"),
					'post_blockquote'		=> $this->input->post("blockquote"),
					'post_blockquote_en'	=> $this->input->post("blockquote_en"),
					'meta_des'				=> $this->input->post("meta_des"),
					'meta_des_en'			=> $this->input->post("meta_des_en"),
					'meta_key'				=> $this->input->post("meta_key"),
					'meta_key_en'			=> $this->input->post("meta_key_en"),
					'post_date'				=> date("Y-m-d H:i:s"));
				// save to database
				$this->site_model->input_data("tb_posts",$data);
				// upload image
				$data_id = $this->db->insert_id();
				$this->fm->multi_upload_img("uploads/posts/".$dir,$type_file,$this->input->post("title"),$data_id,"post");	
				// message			
				$this->session->set_flashdata('message', '1 Data has been saved!');
				redirect($this->url."view/".$this->input->post("post_category_id"));	
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
		elseif($this->session->userdata('form_mode')=='edit'){
			$id = $this->input->post('post_id');
			$this->content['images'] = $this->site_model->get_img("parent_id = '".$id."' and relation = 'post'","0,10")->result();
			if($this->form_validation->run() == TRUE){
				$data = array(
					'category_id'			=> $this->input->post("post_category_id"),
					'post_title'			=> $this->input->post("title"),
					'post_title_en'			=> $this->input->post("title_en"),
					'post_name'				=> slug($this->input->post("title")),
					'post_name_en'			=> slug($this->input->post("title_en")),
					'post_content'			=> $this->input->post("des"),
					'post_content_en'		=> $this->input->post("des_en"),
					'post_blockquote'		=> $this->input->post("blockquote"),
					'post_blockquote_en'	=> $this->input->post("blockquote_en"),
					'meta_des'				=> $this->input->post("meta_des"),
					'meta_des_en'			=> $this->input->post("meta_des_en"),
					'meta_key'				=> $this->input->post("meta_key"),
					'meta_key_en'			=> $this->input->post("meta_key_en"),
					'post_modified'			=> date("Y-m-d H:i:s"),
					'post_status'			=> $this->input->post("post_status"));
				// save to database
				$this->site_model->update_data("tb_posts",$data,"post_id = '".$id."'");
				// upload image
				$this->fm->multi_upload_img("uploads/posts/".$dir,$type_file,$this->input->post("title"),"$id","post");		
				// message				
				$this->session->set_flashdata('message', 'Data ( '.$this->input->post("title").' / '.$this->input->post("title_en").' ) has been update!');
				redirect($this->url."edit/".$this->input->post("post_category_id")."/".$id);
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
	}
	// delete post
	function delete(){
		$id = $this->uri->segment(4);
		$images = $this->site_model->get_data('',"tb_images","parent_id = '$id' and relation = 'post'")->result();
		foreach($images as $image){
			if(!empty($image->image)){
				$del	= "./".$image->dir."/".$image->image;
				$del2	= "./".$image->dir."/".thumb($image->image);
				if(file_exists($del)) unlink($del);
				if(file_exists($del2)) unlink($del2);
			}
			$this->site_model->del_data("tb_images",$data=array('image_id'=>$image->image_id));	
		}
		$this->site_model->del_data("tb_comments",$data=array('parent_id'=>$id,'parent_type'=>'post'));
		$this->site_model->del_data("tb_posts",$data=array('post_id'=>$id));					
		$this->session->set_flashdata('message', 'Data has been delete!');
		redirect($this->url."view/".$this->session->userdata('post_category_id'));		
	}
	// delete post image
	function delete_image(){
		$id = $this->uri->segment(4);
		$post_id = $this->uri->segment(5);
		$image = $this->site_model->get_img("image_id = '$id'")->row();
		$del	= "./".$image->dir."/".$image->image;
		$del2	= "./".$image->dir."/".thumb($image->image);
		if(file_exists($del)) unlink($del);
		if (file_exists($del2)) unlink($del2);
		$this->site_model->del_data("tb_images",$data=array('image_id'=>$id));				
		$this->session->set_flashdata('message', 'Image has been delete!');
		redirect($this->url."edit/".$post_id);	
	}
}