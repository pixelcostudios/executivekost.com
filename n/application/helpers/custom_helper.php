<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------
// Create Dir
if ( ! function_exists('create_dir'))
{
	// create dir
	function create_dir($dir){		
		$year = './uploads/'.$dir.'/'.date('Y');
		if(!file_exists($year)){
			mkdir('./uploads/'.$dir.'/'.date('Y').'/', 0777);
			$month = './uploads/'.$dir.'/'.date('Y').'/'.date('m');
			if(!file_exists($month)){
				mkdir('./uploads/'.$dir.'/'.date('Y').'/'.date('m').'/', 0777);
			}
		}
		else{
			$month = './uploads/'.$dir.'/'.date('Y').'/'.date('m');
			if(!file_exists($month)){
				mkdir('./uploads/'.$dir.'/'.date('Y').'/'.date('m').'/', 0777);
			}			
		}
	}
}

// ------------------------------------------------------------------------
// Format Date
if ( ! function_exists('fdate'))
{	
	// date
	function fdate($tgl,$set="1"){
		$a = date("j F Y", strtotime($tgl));
		$b = date("H:i", strtotime($tgl));
		if($set=="1") return "$a, $b WIB";
		if($set=="2") return "$a";
		if($set=="3") return "$b WIB";
	}
}

// ------------------------------------------------------------------------
// Generate Random
if ( ! function_exists('gen_rand'))
{	
	// get random character
	function gen_rand($length = 6) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
}

// ------------------------------------------------------------------------
// Rewrite HTML
if ( ! function_exists('html'))
{	
	// html
	function html($slug){
		$a = explode(".",$slug);
		return $a[0];
	}
}

// ------------------------------------------------------------------------
// Create Slug
if ( ! function_exists('slug'))
{	
	// slug
	function slug($slug){
		$title = str_replace("-"," ",$slug);
		$replace = array("/",".",",","\\",":","?","*","\"","\'","'","<",">","|","(",")","!","@","#","$","%","^","&","+","=","`","´");
		$title = strtolower(str_replace($replace,"",$title));
		return strtolower(str_replace(" ","-",trim($title)));
	}
}

// ------------------------------------------------------------------------
// Summary
if ( ! function_exists('summary'))
{	
	// summary
	function summary($str,$length="20",$trailing=". . ."){
		$ket = "";
		$katanya = $length;
		$a = explode(" ",strip_tags($str));
		if(count($a)<$length)
			$katanya = count($a);
		for($i=0; $i<$katanya; $i++)
			$ket.= $a[$i]." ";
		return $ket.$trailing;
	}
}

// ------------------------------------------------------------------------
// Read Thumb
if ( ! function_exists('thumb'))
{	
	// thumb
	function thumb($img){
		if(!empty($img)){
			$a = explode(".",$img);
			return $a[0]."_thumb.".$a[1];
		}
	}
}

// ------------------------------------------------------------------------
// Custom Icon Yahoo IM "ymstatus('ID_YM_ANDA', 'url/online.png', 'url/offline.png', 'Hello');"
if ( ! function_exists('ymstatus'))
{	// ymstatus('ID_YM_ANDA', 'catalog/view/theme/< nama_theme >/image/online.png', 'catalog/view/theme/< nama_theme >/image/offline.png', 'Hello');
	function ymstatus($yahooid, $img_ol_path, $img_off_path, $title){
		$yahoo_url = "http://opi.yahoo.com/online?u={$yahooid}&m=a&t=1";				 
		if(ini_get('allow_url_fopen')){
			error_reporting(0);
			$yahoo = file_get_contents($yahoo_url);
		}
		elseif(function_exists('curl_init')){
			$ch = curl_init($yahoo_url);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_HEADER, 0);
			$yahoo = curl_exec($ch);
			curl_close($ch);
		}
		$yahoo = trim($yahoo);
		if(empty($yahoo))
			$imgsrc = $img_off_path; /* Maybe failed connection.*/
		elseif($yahoo == "01")
			$imgsrc = $img_ol_path;
		elseif($yahoo == "00")
			$imgsrc = $img_off_path;
		else
			$imgsrc = $img_off_path;
			 
		echo '<a href="ymsgr:sendim?' . $yahooid . '" title="' . $title . '">';
		echo '<img src="' . $imgsrc . '" alt="' . $title . '" title="' . $title . '" /></a>';
	}
}
	
/* End of file date_helper.php */
/* Location: ./system/helpers/date_helper.php */