<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Owner extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('owner/home');
		$this->load->model('owner/login');
		$this->load->model('owner/user');
		$this->load->model('owner/kost');
	}
	/* INDEX */
	function index(){
		date_default_timezone_set('Asia/Jakarta');
		$mode = strtolower($this->uri->segment(2));
		if($this->session->userdata('owner_login')==TRUE){
			switch($mode){
				default				: $this->home->index(); break;
				case "home"			: $this->home->index(); break;
				case "check_title"	: $this->check_title(); break;
				case "check"		: $this->home->check(); break;
				case "finish"		: $this->home->finish(); break;
				case "update"		: $this->home->update(); break;
				case "checkout"		: $this->home->checkout(); break;
				case "delete"		: $this->home->delete(); break;
				case "accounting"	: $this->load->model('owner/accounting/accounting','acc'); $this->acc->menu();break;
				/* MEMBER */
				case "user"			:
				$user = strtolower($this->uri->segment(3));
				switch($user){
					default				: $this->user->index(); break;
					case "edit"			: $this->user->edit(); break;
					case "input"		: $this->user->input(); break;
				}
				break;
				/* KOST */
				case "kost"			:
				$kost = strtolower($this->uri->segment(3));
				if($this->session->userdata('ses_owner_level')!='investor'){
					switch($kost){
						default				:
						$kost1 = strtolower($this->uri->segment(4));
						switch($kost1){
							default				: $this->kost->index(); break;
							case "add"			: $this->kost->add(); break;
							case "edit"			: $this->kost->edit(); break;
							case "delete"		: $this->kost->delete(); break;
							case "delete_image"	: $this->kost->delete_image(); break;
						}
						break;
						case "input"		: $this->kost->input(); break;
					}
				}
				else{
					switch($kost){
						default				: $this->kost->index(); break;
					}
				}
				break;
				/* LOGOUT */
				case "logout"	: 				
					$data_session = array('owner_id'=>'','ses_owner_id'=>'','owner_username'=>'','ses_owner_level'=>'','owner_login'=>FALSE);
					$this->session->unset_userdata($data_session);
					redirect(base_url().'login/owner', 'refresh');
				break;
			}
		}
		else {
			switch($mode){
				/* LOGIN */
				default					: $this->load->view('owner/login'); break;
				case "process_login"	: $this->login->process_login(); break;
			}
		}
	}
	// title check
	function check_title($str="",$check=""){
		if($str==''){
			$title = $this->uri->segment(3);
			$table = $this->uri->segment(4);
			if($this->uri->segment(6)==''){
				$title_check = mysql_escape_string($this->uri->segment(5));
				$old_title = "";
			}
			else{
				$title_check = mysql_escape_string($this->uri->segment(6));
				$old_title = $this->uri->segment(5);
			}
			
			if($this->session->userdata('form_mode')=='add'){
				if($this->site_model->check_data($title,$table,$title_check)==TRUE)
					echo "";
				else
					echo "".$title_check." already exists in the database";
			}
			else{
				if($old_title!=''){
					if($old_title==$title_check)
						echo "";
					else
						if($this->site_model->check_data($title,$table,$title_check)==TRUE)
							echo "";
						else
							echo "".$title_check." title already exists in the database";
				}
				else
					echo "";
			}
		}
		else{
			$x = explode("/",$check);
			$title = $x[0];
			$table = $x[1];
			$title_check = mysql_escape_string($str);
			$old_title = $x[2];
			
			if($this->session->userdata('form_mode')=='add'){
				if($this->site_model->check_data($title,$table,$title_check)==TRUE){
					return TRUE;
				}
				else{
					$this->form_validation->set_message("check_title","".$title_check." already exists in the database");
					return FALSE;
				}
			}
			else{
				if($old_title==$title_check)
					return TRUE;
				else{
					if($this->site_model->check_data($title,$table,$title_check)==TRUE)
						return TRUE;
					else{
						$this->form_validation->set_message("check_title","".$title_check." already exists in the database");
						return FALSE;
					}
				}
			}
		}
	}
	// check password
	function check_password($str,$user_login_id){
		if($this->site_model->check_password($user_login_id,$str)==TRUE)
			return TRUE;
		else
			$this->form_validation->set_message('check_password','Wrong Present Password!');
			return FALSE;
	}
	// check new & confirm password
	function check_confirm_password($str,$new_password){
		if($str==$new_password)		
			return TRUE;
		else
			$this->form_validation->set_message('check_confirm_password','Confirm Pass do not match with new password');
			return FALSE;
	}
}