<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Frontend extends CI_Controller{

	function __construct(){
		parent::__construct();
		if($_SERVER['HTTP_HOST']=='localhost') $this->output->enable_profiler(TRUE);
		$this->load->model('frontend/home');
		$this->load->model('frontend/page');
		$this->load->model('frontend/post');
		$this->load->model('frontend/kost');
		$this->load->model('frontend/member');
		$this->load->model('frontend/testimoni');
		$this->load->model('frontend/search');
	}
	
	/* ------------------
	index
	------------------ */
	function index(){
		date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('lang')=='') $this->session->set_userdata('lang','id');
		$mode = strtolower($this->uri->segment(1));
		switch($mode){
			default : if($mode==''){ $this->home->index();}else{ $this->post->index($mode);} break;
			case "home"				: $this->home->index(); break;
			case "feed"				: 
			$feed = strtolower($this->uri->segment(2));
			switch($feed){
				default				: $this->home->feed(); break;
				case "subscriber"	: $this->home->subscriber(); break;
			}
			break;
			case "site_map"			: $this->home->site_map(); break;
			case "page"				: $this->page->index($this->uri->segment(2)); break;
			case "send_message" 	: $this->page->send_message(); break;
			case "send_comment"		: $this->post->send(); break;
			case "category"			: $this->post->archive(); break;
			case "kost"				: 
			$kost = strtolower($this->uri->segment(2));
			switch($kost){
				default				: $this->kost->index(); break;
				case "search"		: $this->kost->search(); break;
				case "detail"		: $this->kost->detail($this->uri->segment(3)); break;
				case "send_comment"	: $this->kost->send(); break;
				case "room"			: $this->kost->detail_room($this->uri->segment(3)); break;
			}
			break;
			case "member"				: 
			$member = strtolower($this->uri->segment(2));
			switch($member){
				default				: $this->member->index(); break;
				case "reservation"	: $this->member->reservation(); break;
				case "price_check"	: $this->room_check_out(); break;
				case "register"		: $this->member->register(); break;
				case "re_order"		: $this->member->re_order(); break;
				case "select_room"	: $this->member->select_room(); break;
				case "confirm"		: $this->member->confirm(); break;
				case "order"		: $this->member->index(); break;
				case "update_info"	: $this->member->update_member_information(); break;
				case "profile"		: $this->member->profile(); break;
				case "update_pass"	: $this->member->update_member_password(); break;
				case "login"		: $this->member->login(); break;
				case "logout"		: 
					$data_session = array('ses_member_id'=>'','member_username'=>'','member_login'=>FALSE);				
					$this->session->unset_userdata($data_session); redirect(base_url().'member', 'refresh');
				break;
				
			}
			break;
			case "search"			: $this->search->index(); break;
			case "searching"		: $this->search->search(); break;
			case "testimonial"		: $this->testimoni->index(); break;
			case "lang"				: $this->lang(); break;
			case "price_check"		: $this->price_check(); break;
		}
	}

	/* ------------------
	session languange
	------------------ */
	function lang(){
		$lang = $this->uri->segment(2);
		$this->session->set_userdata('lang', $lang);
		redirect(base_url()/*.$this->session->userdata('site_url')*/);
	}

	/* ------------------
	* callback
	------------------ */
	// username check
	function username_check($str){
		if(!$this->site_model->check_data("member_username","tb_members",$str)){
			$this->form_validation->set_message('username_check',$str.' unavailable');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	// name check
	function name_check($str){
		if(!$this->site_model->check_data("member_name","tb_members",mysql_real_escape_string($str))){
			$this->form_validation->set_message('name_check',$str.' unavailable');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	// identity check
	function identity_check($str){
		if(!$this->site_model->check_data("member_identity_number","tb_members",$str)){
			$this->form_validation->set_message('identity_check','Identity Number : '.$str.' unavailable');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	// email check
	function email_check($str){
		if(!$this->site_model->check_data("member_email","tb_members",$str)){
			$this->form_validation->set_message('email_check',$str.' unavailable');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	// room check in
	function room_check_in($str,$kost_room_id){
		$this->db->where('kost_room_id', $kost_room_id);
		$this->db->where('check_in <=', $str);
		$this->db->where('check_out >=', $str);
		$this->db->where('rent_status !=', '3');
		$this->db->or_where('rent_status !=', '4');
		$data = $this->db->get('tb_rent_kost')->num_rows();
		//$data = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$kost_room_id."' AND check_in <= '".$str."' AND check_out >= '".$str."' AND rent_status != '4'")->num_rows();
		if($data == 1){
			$this->form_validation->set_message('room_check_in',$str.' unavailable');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	
	// room check out
	function room_check_out($str="",$callback_room_check_out=""){
		/*if($this->session->userdata('lang')=='id') $lang = "";else $lang = "_en";
		$notif = $this->site_model->get_data('',"tb_notifications","notification_name = 'limit'")->row();*/
		//if($str != ''){
			$x = explode(",",$callback_room_check_out);
			$this->db->where('kost_room_id', $x[0]);
			$this->db->where('check_in <=', $str);
			$this->db->where('check_out >=', $str);
			$this->db->where('rent_status !=', '3');
			$this->db->or_where('rent_status !=', '4');
			$data = $this->db->get('tb_rent_kost')->num_rows();
			//$data = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$x[0]."' AND check_in <= '".$str."' AND check_out >= '".$str."' AND rent_status != '4'")->num_rows();
			if($data == 1){
				$this->form_validation->set_message('room_check_out',$str.' unavailable');
				return FALSE;
			}
			else{
				$this->db->where('kost_room_id', $x[0]);
				$this->db->where('check_in >=', $x[1]);
				$this->db->where('check_in <=', $str);
				$this->db->where('check_out >=', $x[1]);
				$this->db->where('check_out <=', $str);
				$this->db->where('rent_status !=', '3');
				$this->db->or_where('rent_status !=', '4');
				$data = $this->db->get('tb_rent_kost')->num_rows();
				//$data = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$x[0]."' AND check_in BETWEEN '".$x[1]."' AND '".$str."' AND check_out BETWEEN '".$x[1]."' AND '".$str."' AND rent_status != '4'")->num_rows();
				if($data == 1){
					$this->form_validation->set_message('room_check_out',$str.' unavailable');
					return FALSE;
				}
				else{
					/*$kost = $this->site_model->get_data('',"tb_kost_room","kost_room_id = '".$x[0]."'")->row();
					$harga_harian 	= $kost->kost_room_price_day;
					$harga_bulanan 	= $kost->kost_room_price_month;
					
					$data = $this->fm->selisih_tgl($x[1],$str);
					
					$result = $data['result'];
					$days 	= $data['hari'];
					$months = $data['bulan'];
					
					// harga
					$bayar_harian = $harga_harian * $days;
					if($months != '0')
					$bayar_bulanan = $harga_bulanan * $months;
					else
					$bayar_bulanan = 0;
					$total_bayar = $bayar_harian + $bayar_bulanan;
					
					if($total_bayar > $kost->kost_room_price_month AND ($months == 0 AND $days < $result))
					$this->form_validation->set_message('room_check_out',$months." month ".$days." days<br />total = Rp. ".rupiah($total_bayar).",-".$notif->notification_value.$lang);
					else
					$this->form_validation->set_message('room_check_out',$months." month ".$days." days<br />total = Rp. ".rupiah($total_bayar).",-");*/
					return TRUE;
				}
			}
		/*
		}
		else{
			$check_in 		= $_POST['check_in'];
			$check_out 		= $_POST['check_out'];
			$kost_room_id 	= $_POST['kost_room_id'];
			
			$data = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$kost_room_id."' AND check_in <= '".$check_out."' AND check_out >= '".$check_out."' AND rent_status != '4'")->num_rows();
			if($data == 1){
				echo $check_out.' unavailable';
			}
			else{
				$data = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$kost_room_id."' AND check_in BETWEEN '".$check_in."' AND '".$check_out."' AND check_out BETWEEN '".$check_in."' AND '".$check_out."' AND rent_status != '4'")->num_rows();
				if($data == 1){
					echo $check_out.' unavailable';
				}
				else{
					$kost = $this->site_model->get_data('',"tb_kost_room","kost_room_id = '".$kost_room_id."'")->row();
					$harga_harian 	= $kost->kost_room_price_day;
					$harga_bulanan 	= $kost->kost_room_price_month;
					
					$data = $this->fm->selisih_tgl($check_in,$check_out);
					
					$result = $data['result'];
					$days 	= $data['hari'];
					$months = $data['bulan'];
					
					// harga
					$bayar_harian = $harga_harian * $days;
					if($months != '0')
					$bayar_bulanan = $harga_bulanan * $months;
					else
					$bayar_bulanan = 0;
					$total_bayar = $bayar_harian + $bayar_bulanan;
					
					if($total_bayar > $kost->kost_room_price_month AND ($months == 0 AND $days < $result))
					echo $months." month ".$days." days<br />total = Rp. ".rupiah($total_bayar).",-".br(1).$notif->notification_value.$lang;	
					else
					echo $months." month ".$days." days<br />total = Rp. ".rupiah($total_bayar).",-";
				}
			}			
		}
		*/
	}
	// booking code
	function check_booking_code($str){
		if($this->session->userdata('booking_code')==$str)
		return TRUE;
		else
		$this->form_validation->set_message('check_booking_code','booking code is wrong');
		return FALSE;
	}
}