<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    <?php if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";?>
    
    <div id="single">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url();?>">Home</a> / <a href="<?php echo base_url();?>site_map">Search</a></span>  
        <h1><?php if($this->session->userdata('lang') == 'id') echo 'Search ....'; else echo 'Pencarian .....'; ?></h1>
        <p><i><?php $a = explode(" ",$word); echo implode(", ",$a)?></i></p>
        
        <?php
		if(count($search)!=0){
			foreach($search as $a){
				echo '<div class="archive">';
				echo heading(anchor(base_url().$a['post_name'].'.html',$a['post_title'],array('title'=>$a['post_title'])),1);
				echo '<p>'.$a['post_content'].'....</p>';
				echo '</div>';
			}
		}
		else{
			echo 'not found ....';
		}
		?>
    	</div>        
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>
