		<div id="content-sidebar">
        <h1>LOCATION</h1>
        <div id="menu-sidebar">
        <ul>
            <?php
			$kota = $this->site_model->get_data('',"tb_daerah",'','',"daerah_title ASC")->result();
			foreach($kota as $a){
				echo '<li>'.anchor(base_url().'kost/'.$a->daerah_name.'.html',$a->daerah_title,array('title'=>$a->daerah_title)).'</li>';
			}
			?>
        </ul>
        </div>
		<div class="clr"></div>
        
        <div class="content-sidebar">
        <?php
		$banner = $this->site_model->get_data('',"tb_banners","position = '1' AND banner_status = '1'",'',"sequence DESC")->result();
		foreach($banner as $a){
			echo '<div class="image">';
			echo anchor($a->banner_url,img(array('src'=>base_url().'uploads/banners/'.$a->banner_image,'alt'=>$a->banner_title,'title'=>$a->banner_title)),array('title'=>$a->banner_title));
			echo '</div>';
		}
		?>
        </div>
        </div>