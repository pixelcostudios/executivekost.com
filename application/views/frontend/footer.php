<div id="footer-bg" <?php if($this->uri->segment(1)=='') echo 'style="background-position: top !important;"'?>>
<div class="clr"></div>
<div id="footer">
    <?php if($this->uri->segment(1)==''){?>
    <div id="footer-news">
    <?php
	foreach($news as $a){
		$img = $this->site_model->get_img("parent_id = '".$a['post_id']."' AND relation = 'post'","1")->row();
		if(count($img)!=0) $image = $img->dir.'/'.thumb($img->image); else $image = 'stylesheets/images/empty_image.jpg';
		echo '<div class="footer-news">';
		echo '<div class="footer-news-img">'.img(array('src'=>base_url().$image,'alt'=>$a['post_title'],'title'=>$a['post_title'])).'</div>';
		echo '<div class="footer-news-desc">';
		echo heading(anchor(base_url().'category/'.$a['category_name'].'.html',$a['category_title'],array('title'=>$a['category_title'])),1);
		echo heading(anchor(base_url().$a['post_name'].'.html',$a['post_title'],array('title'=>$a['post_title'])),2);
		echo '<p>'.$a['post_content'].''.anchor(base_url().$a['post_name'].'.html','read more',array('title'=>$a['post_title'])).'</p>';
		echo '</div></div>';
	}
	foreach($artikel as $a){
		$img = $this->site_model->get_img("parent_id = '".$a['post_id']."' AND relation = 'post'","1")->row();
		if(count($img)!=0) $image = $img->dir.'/'.thumb($img->image); else $image = 'stylesheets/images/empty_image.jpg';
		echo '<div class="footer-news">';
		echo '<div class="footer-news-img">'.img(array('src'=>base_url().$image,'alt'=>$a['post_title'],'title'=>$a['post_title'])).'</div>';
		echo '<div class="footer-news-desc">';
		echo heading(anchor(base_url().'category/'.$a['category_name'].'.html',$a['category_title'],array('title'=>$a['category_title'])),1);
		echo heading(anchor(base_url().$a['post_name'].'.html',$a['post_title'],array('title'=>$a['post_title'])),2);
		echo '<p>'.$a['post_content'].''.anchor(base_url().$a['post_name'].'.html','read more',array('title'=>$a['post_title'])).'</p>';
		echo '</div></div>';
	}
	foreach($comments as $c){
		echo '<div class="footer-news">';
		echo '<div class="footer-news-desc">';
		echo heading(anchor('#','Testimoni',array('title'=>'Testimoni')),1);
		echo heading(anchor('#',$c['name'].' | '.$c['comment_date'],array('title'=>$c['name'])),2);
		echo '<p>'.$c['comment_content'].'</p>';
		echo '</div></div>';
	}
	?>
	<div class="clr"></div>
    </div>
    <?php } ?>
    <div id="footer-mitra">
        <?php
		$banner = $this->site_model->get_data('',"tb_banners","position = '2' AND banner_status = '1'",'',"sequence DESC")->result();
		foreach($banner as $a){
			echo anchor($a->banner_url,img(array('src'=>base_url().'uploads/banners/'.$a->banner_image,'alt'=>$a->banner_title,'title'=>$a->banner_title)),array('title'=>$a->banner_title));
		}
		?>
    </div>
    
    <div id="footer-menu">
    <a href="#"><img src="<?php echo base_url()?>stylesheets/images/footer-logo.jpg" /></a>
    <ul>
    	<li><a href="<?php echo base_url()?>page/privacy-policy.html">Privacy Policy</a></li>
        <li><a href="<?php echo base_url()?>page/term-of-services.html">Term of Services</a></li>
        <li><a href="<?php echo base_url()?>site_map">Site map</a></li>
        <li>Copyright &copy; 2012 E Kost. All Right reserved</li>
        <li>
        <!-- Site Meter -->
		<script type="text/javascript" src="http://s51.sitemeter.com/js/counter.js?site=s51executivekost">
        </script>
        <noscript>
        <a href="http://s51.sitemeter.com/stats.asp?site=s51executivekost" target="_blank">
        <img src="http://s51.sitemeter.com/meter.asp?site=s51executivekost" alt="Site Meter" border="0"/></a>
        </noscript>
        <!-- Copyright (c)2009 Site Meter -->
        </li>
    </ul>
	<div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>
</body>
</html>