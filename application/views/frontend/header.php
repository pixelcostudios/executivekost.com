<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
// meta head
$forhead = $this->site_model->get_data('',"tb_options","option_id = '1'")->row();
$site_title 		= $forhead->title;
$site_des 			= $forhead->des;
$site_keyword 		= $forhead->keyword;
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Copyright" content="Executive Kost" />
<meta http-equiv="imagetoolbar" content="no" />
<title><?php echo !empty($content_title) ? $content_title." ".$site_title : $site_title; ?></title>
<meta name="robots" content="follow, all" />
<meta name="distribution" content="global" />
<meta name="description" content="<?php echo !empty($content_des) ? $content_des." ".$site_des : $site_des; ?>" />
<meta name="keywords" content="<?php echo !empty($content_keyword) ? $content_keyword." ".$site_keyword : $site_keyword; ?>" />
<meta name="author" content="Executive Kost" />
<meta name="language" content="en" />
<meta name="revisit-after" content="7" />
<meta name="webcrawlers" content="all" />
<meta name="rating" content="general" />
<meta name="spiders" content="all" />
<link href="<?php echo base_url()?>stylesheets/style.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>javascripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>javascripts/frontend-app.js"></script>
</head>

<body>