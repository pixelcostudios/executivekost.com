<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="archive">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a class="current">Testimonial</a></span>
        <?php
		foreach($testimoni as $a){
			echo '<div class="archive">';
			echo heading($a['name'],1);
			echo heading($a['comment_date'],2);
			echo '<p>'.$a['comment_content'].'</p>';
			echo '</div>';
		}
		?>
        <?php echo ! empty($pagination) ? '<div class="page_num">' . $pagination . '</div>' : ''; ?> 
    	</div>
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>