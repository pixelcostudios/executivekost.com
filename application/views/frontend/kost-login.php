<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a href="<?php echo base_url()?>kost/detail/<?php echo $this->session->userdata('kost_name')?>">Kost</a> / <a class="current">Reservation</a></span>
        <h1>Login</h1>
        <form method="post" action="<?php echo base_url()?>member/login">
        <table class="reservation">
        <tr><td>Username</td><td> : </td><td><input type="text" name="username" value="<?php echo set_value('username');?>" /><?php echo form_error('username','<p class="error">','</p>'); ?></td></tr>
        <tr><td>Password</td><td> : </td><td><input type="password" name="password" /><?php echo form_error('password'); ?><?php echo form_error('password','<p class="error">','</p>'); ?></td></tr>
        <tr><td colspan="2"></td><td><input type="submit" value="LOGIN" /></td></tr>
        </table>
        </form>
		<?php 
        $message = $this->session->flashdata('message');
        echo $message == '' ? '' : '<p class="message fadeout">' . $message . '</p>';
        ?>
        <h1>Sign Up</h1>
        <a href="<?php echo base_url()?>member/reservation" class="booking-button">SIGN UP</a>
    	</div>        
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>