<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/lightbox/jquery.lightbox-0.5.css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>javascripts/lightbox/jquery.lightbox-0.5.js"></script>
        <script type="text/javascript">
        $(function() {
            $('.image a').lightBox({
                imageLoading: '<?php echo base_url(); ?>javascripts/lightbox/images/lightbox-ico-loading.gif',
                imageBtnPrev: '<?php echo base_url(); ?>javascripts/lightbox/images/lightbox-btn-prev.gif',
                imageBtnNext: '<?php echo base_url(); ?>javascripts/lightbox/images/lightbox-btn-next.gif',
			});
            $('.sketch a').lightBox();
        });
		</script>
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url();?>">Home</a> / <a href="<?php echo base_url();?>kost">Kost</a> / <a class="current"><?php echo $this->session->userdata('kost_title')?></a></span>       
        <div id="social-media">
        <!-- AddThis Button BEGIN --
        <div class="addthis_toolbox addthis_default_style ">
        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
        <a class="addthis_button_tweet"></a>
        <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
        <a class="addthis_button_compact"></a>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50f0cbe33d8a334b"></script>
        <!-- AddThis Button END -->
        </div>
        <?php 
		echo heading($kost_title,1);
		if(!empty($img1->image)) $url = base_url().$img1->dir.'/'.$img1->image; else $url = "";
        echo '<div class="image"><a href="'.$url.'">';
		echo isset($img1->image)?img(array('src'=>base_url().$img1->dir.'/'.$img1->image,'alt'=>$kost_title,'title'=>$kost_title)):'';
		echo '</a></div>';
		if(count($img2) > 0){
			foreach($img2 as $a){
				echo '<div class="rel-img image"><a href="'.base_url().$img1->dir.'/'.$a->image.'">';
				echo img(array('src'=>''.base_url().$img1->dir.'/'.thumb($a->image).'','class'=>'thumb','alt'=>$kost_title,'title'=>$kost_title));
				echo '</a></div>';
			}
		}
        ?>
        <div class="clr"></div>
        <?php echo isset($kost_content)?$kost_content:''?>
        <br />
        <h1><span>Virtual Tour :</span></h1>
        <?php if(isset($kost_virtual_tour)){?>
        <iframe width="418" height="235" src="<?php echo $kost_virtual_tour?>" frameborder="0" allowfullscreen></iframe>
        <?php } ?>
        <br />
        <br />
        <h1><span>MAP :</span></h1>
            <div id="map_canvas" style="width:418px; height:230px; margin: 0 auto;"></div>
            <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
			<script>
            var myLatlng = new google.maps.LatLng(<?php echo $kost_map?>);
            var mapOptions = {
				zoom: 15,
				center: myLatlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
            }
            var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);            
            var marker = new google.maps.Marker({
                position: myLatlng,
                title:"<?php echo $kost_title?>"
            });            
            // To add the marker to the map, call setMap();
            marker.setMap(map);
            </script>
    	</div>        
        
		<div id="content-sidebar">
            <div class="content-sidebar">  
            <h1>FACILITY</h1>
            <div id="fasilitas">
            <table>
            <?php
            $facility = explode(",",$kost_facility);
            foreach($facility as $a){
                echo '<tr><td>'.img(array('src'=>base_url().'stylesheets/images/check_button.jpg')).'</td><td width="205"><p>'.$a.'</p></td></tr>';
            }
            ?>
            </table>
            </div>
            </div>
            <!--
            <a href="<?php //echo base_url()?>member" class="booking-button">BOOKING NOW</a>
            <div class="content-sidebar">
            <h1>VIRTUAL TOUR</h1>
            <?php //if(isset($kost_virtual_tour)){?>
            <iframe width="230" height="150" src="<?php //echo $kost_virtual_tour?>" frameborder="0" allowfullscreen></iframe>
            <?php //} ?>
            </div>
            <div class="content-sidebar">
            <h1>MAP EKOST</h1>
            <div id="map_canvas" style="width:230px; height:230px; margin: 0 auto;"></div>
            <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
			<script>
            var myLatlng = new google.maps.LatLng(<?php //echo $kost_map?>);
            var mapOptions = {
				zoom: 15,
				center: myLatlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
            }
            var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);            
            var marker = new google.maps.Marker({
                position: myLatlng,
                title:"<?php //echo $kost_title?>"
            });            
            // To add the marker to the map, call setMap();
            marker.setMap(map);
            </script>
            </div>
            -->
            <div class="content-sidebar">        
            <h1>SKETCH</h1>
            <?php
            echo '<div class="sketch"><a href="'.base_url().$kost_sketch.'">';
			echo img(array('src'=>base_url().$kost_sketch,'alt'=>$kost_title,'title'=>$kost_title,'width'=>'230'));
            echo '</a></div>';
			?>
            </div>
            
			<?php if($this->uri->segment(2)=='detail' OR $this->uri->segment(2)=='room' OR $this->uri->segment(1)=='member' AND $this->session->userdata('kost_id')!=''){?>
            <div class="content-sidebar"> 
            <h1>SELECT ROOM</h1> 
            <div id="information-room">
            <!--<h2>Room of <b><?php //echo $this->session->userdata('kost_title')?></b></h2>-->
            <div id="room-number">
            <ul>
	            <?php
				$this->db->where('kost_id', $this->session->userdata('kost_id'));
				$this->db->order_by('kost_room_number', 'ASC');
				$kost_room = $this->db->get('tb_kost_room')->result();
	            foreach($kost_room as $a){
                $rent_kost = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$a->kost_room_id."' AND rent_status = '2'")->row();
                if(count($rent_kost)=='1'){
                    $class = 'class="unavailable"';
                    $date = '<span>&nbsp;'/*.date("M",strtotime($rent_kost->check_out))*/.'</span>';
                    $date2 = 'available in '.date("d M Y",strtotime($rent_kost->check_out));
                    $url = '#';
                }
                else{
                    $class = ''; $date = ''; $date2 = ''; $url = base_url().'kost/room/'.$a->kost_room_id;
                }
                echo '<li>';
				if(!empty($date2)){
					echo '<div>'.$date2.'</div>';					
				}
				echo '<a href="'.$url.'" '.$class.'>'.$date.$a->kost_room_number.'</a>';
				echo '</li>';
            }																	
	            ?>
	            <div class="clr"></div>
            </ul>
            <!--
            <p>Daily Room :</p>
            <ul>
	            <?php
				/*$this->db->where('kost_id', $this->session->userdata('kost_id'));
				$this->db->where('kost_room_type', '2');
				$this->db->order_by('kost_room_number', 'ASC');
				$kost_room = $this->db->get('tb_kost_room')->result();
	            foreach($kost_room as $a){
                $rent_kost = $this->site_model->get_data('',"tb_rent_kost","kost_room_id = '".$a->kost_room_id."' AND rent_status = '2'")->row();
                if(count($rent_kost)=='1'){
                    $class = 'class="unavailable"';
                    $date = '<span>&nbsp;'.date("M",strtotime($rent_kost->check_out)).'</span>';
                    $date2 = 'available in '.date("d M Y",strtotime($rent_kost->check_out));
                    $url = '#';
                }
                else{
                    $class = ''; $date = ''; $date2 = ''; $url = base_url().'kost/room/'.$a->kost_room_id;
                }
                echo '<li>';
				if(!empty($date2)){
					echo '<div>'.$date2.'</div>';					
				}
					echo '<a href="'.$url.'" '.$class.'>'.$date.$a->kost_room_number.'</a>';
					echo '</li>';
	            }*/															
	            ?>
	            <div class="clr"></div>
            </ul>
			-->
            <div id="status">
                <div class="status"><img src="<?php echo base_url()?>stylesheets/images/room-number-status-2.jpg" />Available</div>
                <div class="status"><img src="<?php echo base_url()?>stylesheets/images/room-number-status.jpg" />Unavailable ( hover to show info/available )</div>
            </div>
            <div class="clr"></div>
            </div>
            </div>
            </div>
            <?php } ?>
        </div>
        <div class="clr"></div>
        <div id="testimoni">
            <h1>Testimoni</h1>
            <?php
			/*foreach($comments as $a){
            	echo '<div class="testimoni">';
            	echo '<h2>'.$a->name.', '.date("F j, Y",strtotime($a->comment_date)).'</h2>';
            	echo '<p>'.$a->comment_content.'</p>';
            	echo '</div>';
			}	*/		
			?>
        	<div id="comment-form">
            <form method="post" action="<?php echo base_url()?>kost/send_comment">
            <?php			
			echo form_hidden('post_id',''.$kost_id.'');
			echo form_hidden('slug',''.$kost_name.'');
			echo form_hidden('lang',$this->session->userdata('lang'));
			?>
            <table>
            <tr>
            <td>
			<?php echo form_input('name',''.set_value('name').'','placeholder="NAMA"')?>
			<?php echo form_error('name', '<br /><label class="error">', '</label>')?></td>
            <td>
            <?php echo form_input('email',''.set_value('email').'','placeholder="EMAIL"')?>
			<?php echo form_error('email', '<br /><label class="error">', '</label>')?>
            </td>
            </tr>
            <tr>
            <td colspan="2"><img src="<?php echo base_url()?>stylesheets/images/testimoni-icon.jpg" /><textarea name="comment" rows="3"placeholder="COMMENT ..."><?php echo set_value('comment')?></textarea><?php echo '<br />'.form_error('comment', '<label class="error">', '</label>')?>
            </td>
            </tr>
            <tr>
            <td colspan="2" style="padding-left: 65px;"><?php echo $cap_img?><?php echo form_input('captcha',''.set_value('captcha').'','id="captcha" maxlength="6"')?><input type="submit" name="submit" value="SEND" /></td>
            </tr>
            <tr>
            <td colspan="2" style="padding-left: 65px;"><?php echo $cap_msg?></td>
            </tr>
            </table>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="clr"></div>
</div>