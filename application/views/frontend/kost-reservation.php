<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a href="<?php echo base_url()?>kost/detail/<?php echo $this->session->userdata('kost_name')?>.html">Kost</a> / <a class="current">Reservation</a></span>
        <h1>Reservation</h1>
        <?php
		// flash message
		$flashmessage = $this->session->flashdata('message');
		echo !empty($flashmessage) ? '<div class="message fadeout">'.$flashmessage.'</div>' : '';
		
		// kost detail		
		$this->db->join('tb_kosts', 'tb_kost_room.kost_id = tb_kosts.kost_id');
		$this->db->where('kost_room_id', $this->session->userdata('kost_room_id'));
		$kost = $this->db->get('tb_kost_room')->row();
		
		// notification
		$notif = $this->site_model->get_data('',"tb_notifications","notification_id = '2'")->row();
		preg_match_all('/[0-9]{1,2}:[0-9]{1,2}/', $notif->notification_value, $matches);
		$time = $matches[0];
		//echo "buka : ".strtotime(date("H:i"))." - tutup : ".strtotime(date("H:i")).br(1);
		//echo "buka : ".strtotime($time[0])." - tutup : ".strtotime($time[1]);
		if(strtotime(date("H:i")) < strtotime($time[0]) OR strtotime(date("H:i")) > strtotime($time[1])){
			if($this->session->userdata("lang")=="id"){
				echo '<div class="message">'.strip_tags($notif->notification_value).'</div>';
			}
			else{
				echo '<div class="message">'.strip_tags($notif->notification_value_en).'</div>';
			}
		}
		elseif(count($kost) == '0'){
			echo '<div class="message">Anda belum memilih kost / Select kost first.</div>';
		}
		else{
			?>
	        <div id="reservation">
	        <form method="post" action="<?php echo base_url()?>member/register" id="reservation">
	        <input type="hidden" value="<?php echo base_url("member/price_check")?>" id="data_url" />
	        <input type="hidden" name="kost_room_id" value="<?php echo $kost->kost_room_id?>" id="data_url" />
	        <fieldset>
	        <legend>Login Information</legend>
	        <table class="reservation">
		        <tr>
					<td width="110">Username</td><td> : </td>
		        	<td><?php echo form_input('username',set_value('username'))?>
						<?php echo form_error('username', '<label class="error">', '</label>')?>
					</td>
				</tr>
		        <tr><td>Password</td><td> : </td>
		        	<td><?php echo form_password('password',set_value('password'))?>
						<?php echo form_error('password', '<label class="error">', '</label>')?>
				</td>
				</tr>
	        </table>
	        </fieldset>
	        <fieldset>
	        <legend>Personal Information</legend>
	        <table class="reservation">
		        <tr>
					<td width="120">Name</td><td> : </td>
		            <td><?php echo form_input('name',set_value('name'))?>
						<?php echo form_error('name', '<label class="error">', '</label>')?>
					</td>
				</tr>
		        <tr>
					<td>Identity</td><td> : </td>
		            <td>
		            <select name="identity">
		            <option value="ktm" <?php echo set_select('identity', 'ktm');?>>KTM</option>
		            <option value="ktp" <?php echo set_select('identity', 'ktp');?>>KTP</option>
		            <option value="sim" <?php echo set_select('identity', 'sim');?>>SIM</option>
		            <option value="passport" <?php echo set_select('identity', 'passport');?>>Passport</option>
		            </select>
		            </td>
		        </tr>
		        <tr>
					<td>Identity Number</td><td> : </td>
		            <td><?php echo form_input('identity_no',set_value('identity_no'))?>
						<?php echo form_error('identity_no', '<label class="error">', '</label>')?>
					</td>
				</tr>
		        <tr>
					<td>Birthday</td><td> : </td>
		            <td><?php echo form_input('birthday',set_value('birthday'),'readonly="true" id="birthday"')?>
						<?php echo form_error('birthday', '<label class="error">', '</label>')?>
					</td>
				</tr>
		        <tr>
					<td>Phone</td><td> : </td>
		            <td><?php echo form_input('phone',set_value('phone'))?>
						<?php echo form_error('phone', '<label class="error">', '</label>')?>
					</td>
				</tr>
		        <tr>
					<td>Email</td><td> : </td>
		            <td><?php echo form_input('email',set_value('email'))?>
						<?php echo form_error('email', '<label class="error">', '</label>')?>
					</td>
				</tr>
		        <tr>
					<td>Address</i></td><td> : </td>
		        	<td><?php $address = array('name'=>'address','value'=>set_value('address'),'cols'=>'20','rows'=>'3')?>
						<?php echo form_textarea($address)?>
					</td>
				</tr>
	        </table>
	        </fieldset>
	        <fieldset>
	        <legend>Kost Detail</legend>
	        <table class="reservation">
		        <tr>
					<td width="120">Kost Name</td><td> : </td>
		            <td><?php echo $kost->kost_title?></td>
				</tr>
		        <tr>
					<td>Room Number</td><td> : </td>
		            <td><?php echo $kost->kost_room_number?>
						<?php
						if($kost->kost_room_type == '1') echo ' ( Rp. '.rupiah($kost->kost_room_price_month).' / Month )';
						elseif($kost->kost_room_type == '2') echo ' ( Rp. '.rupiah($kost->kost_room_price_day).' / Day )';
						else{
							echo br(1).' - Rp. '.rupiah($kost->kost_room_price_day).' / Day';
						 	echo br(1).' - Rp. '.rupiah($kost->kost_room_price_month).' / Month';
						 }
						?>
		            </td>
				</tr>
				<?php
		        if($kost->kost_room_type == '1'){
					?>
			        <tr>
						<td>Check In</td><td> : </td>
			            <td><?php echo form_input('check_in',set_value('check_in'),'readonly="true" id="check_in_monthly"')?>
							<?php echo form_error('check_in', '<label class="error">', '</label>')?>
						</td>
					</tr>
			        <tr>
						<td>Length of Stay</td><td> : </td>
			            <td><?php echo form_input('length_of_stay',set_value('length_of_stay'),'size="1"')?> month
							<?php echo form_error('length_of_stay', '<labesl class="error">', '</label>')?>
						</td>
					</tr>
		        	<?php
		        }
				elseif($kost->kost_room_type == '2'){
					?>
			        <tr>
						<td>Check In</td><td> : </td>
			            <td><?php echo form_input('check_in',set_value('check_in'),'readonly="true" id="check_in"')?>
							<?php echo form_error('check_in', '<label class="error">', '</label>')?>
						</td>
					</tr>
			        <tr>
						<td>Check Out</td><td> : </td>
			            <td><?php echo form_input('check_out',set_value('check_out'),'readonly="true" id="check_out"')?>
							<?php echo form_error('check_out', '<labesl class="error">', '</label>')?>
						</td>
					</tr>
		        	<?php
				}
				else{
					?>
			        <tr>
						<script type="text/javascript">
							/*function change_type(str){
							    if(str == "1"){
							        $(".daily").css("display","none");
							        $(".monthly").css("display","");
							    }
							    else{
							        $(".daily").css("display","");
							        $(".monthly").css("display","none");
							    }
							    $(".hidden_type").html('<input type="hidden" name="room_type_select" value="'+str+'" />');
							    return true;
							}*/
						</script>
						<td colspan="2"></td>
						<td>
							<select name="room_type" onchange="submit()">
								<option value="type_1" <?php echo set_select('room_type', 'type_1');?>>Monthly</option>
								<option value="type_2" <?php echo set_select('room_type', 'type_2');?>>Daily</option>
							</select>
						</td>
					</tr>
					<?php
					$room_type = $this->input->post('room_type');
					if($room_type == 'type_1' OR $room_type == ''){
					?>
				        <tr class="monthly">
							<td>Check In</td><td> : </td>
				            <td><?php echo form_input('check_in',set_value('check_in'),'readonly="true" id="check_in_monthly"')?>
								<?php echo form_error('check_in', '<label class="error">', '</label>')?>
							</td>
						</tr>
				        <tr class="monthly">
							<td>Length of Stay</td><td> : </td>
				            <td><?php echo form_input('length_of_stay',set_value('length_of_stay'),'size="1"')?> month
								<?php echo form_error('length_of_stay', '<labesl class="error">', '</label>')?>
							</td>
						</tr>
					<?php
					}
					elseif($room_type == 'type_2'){
					?>
				        <tr class="daily">
							<td>Check In</td><td> : </td>
				            <td><?php echo form_input('check_in',set_value('check_in'),'readonly="true" id="check_in"')?>
								<?php echo form_error('check_in', '<label class="error">', '</label>')?>
							</td>
						</tr>
				        <tr class="daily">
							<td>Check Out</td><td> : </td>
				            <td><?php echo form_input('check_out',set_value('check_out'),'readonly="true" id="check_out"')?>
								<?php echo form_error('check_out', '<labesl class="error">', '</label>')?>
							</td>
						</tr>
		        	<?php
		        	}
				}
				?>
		        <tr>
					<td>Special Request <i>(optional)</i></td><td> : </td>
		            <td><?php $special_request = array('name'=>'special_request','value'=>set_value('special_request'),'cols'=>'20','rows'=>'3'); echo form_textarea($special_request)?></td>
				</tr>
		        <tr>
					<td></td><td> : </td>
		        	<td><?php echo $cap_img?><?php echo form_input('captcha',set_value('captcha'),'id="captcha" maxlength="6"')?><?php echo $cap_msg?></td>
				</tr>
		        <tr>
					<td colspan="2"></td>
		        	<td><input type="submit" name="register" value="register" /></td>
				</tr>
	        </table>
	        </fieldset>
	        </form>
	        </div>
	        <?php
		}
		?>
    	</div>
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/date-picker/jquery-ui.css" />
        <script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui.js"></script>
		<script type="text/javascript">
		$(function() {
			/* -------------------
			   * birthday
			   **
			------------------- */
			$( "#birthday" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true,yearRange: "c-60:c"});
			/* -------------------
			   * check date
			   **
			------------------- */
			// check in
			$("#check_in").datepicker({dateFormat: 'yy-mm-dd',beforeShowDay: disableRangeOfDays,changeMonth: true,changeYear: true,minDate: 0,maxDate: 3,
				onClose: function(selectedDate){
					$( "#check_out" ).datepicker( "option", "minDate: 1", selectedDate )
					//$("#check_out").datepicker("option", "minDate: 0", selectedDate);
					// price_check
					/*if(document.getElementById("check_out").value != ""){
						var form_data = $("form").serialize();
						$.ajax({
							beforeSend	: function(){
								$("#wait").css("display","block");
							},
							type		: "POST",
							url			: document.getElementById("data_url").value,
							data		: form_data,
							success		: function(result){
								$("#response").html(result);
								$("#wait").css("display","none");
							}
						});
					}*/
				}
			});
			// check out
			$("#check_out").datepicker({dateFormat: 'yy-mm-dd',beforeShowDay: disableRangeOfDays,changeMonth: true,changeYear: true,minDate: 1,
				onClose: function(selectedDate){
					$("#check_in").datepicker("option", "maxDate: 3", selectedDate);
					// price_check
					/*if(document.getElementById("check_in").value != ""){
						var form_data = $("form").serialize();
						$.ajax({
							beforeSend	: function(){
								$("#wait").css("display","block");
							},
							type		: "POST",
							url			: document.getElementById("data_url").value,
							data		: form_data,
							success		: function(result){
								$("#response").html(result);
								$("#wait").css("display","none");
							}
						});
					}*/
				}
			});			
			// check in
			$("#check_in_monthly").datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true,minDate: 0,maxDate: 3});
			// disable a range of dates
			var disabledDaysRange = [[
			<?php
			$this->db->join('tb_kost_room', 'tb_rent_kost.kost_room_id = tb_rent_kost.kost_room_id');
			$this->db->where('tb_rent_kost.kost_room_id', $this->session->userdata('kost_room_id'));
			$this->db->where('rent_status !=', '3');
			$kost_rent = $this->db->get('tb_rent_kost')->result();
			foreach($kost_rent as $a){
				?>
				"<?php echo date("m-d-Y", strtotime($a->check_in))?> to <?php echo date("m-d-Y", strtotime($a->check_out))?>",
				<?php	
			}
			?>
			]];
			function disableRangeOfDays(d){
				for(var i = 0; i < disabledDaysRange.length; i++){
					if($.isArray(disabledDaysRange[i])){
						for(var j = 0; j < disabledDaysRange[i].length; j++){
							var r = disabledDaysRange[i][j].split("to");
							r[0] = r[0].split("-");
							r[1] = r[1].split("-");
							if(new Date(r[0][2], (r[0][0]-1), r[0][1]) <= d && d <= new Date(r[1][2], (r[1][0]-1), r[1][1])){
								return [false];
							}
						}
					}
					else{
						if(((d.getMonth()+1) + '-' + d.getDate() + '-' + d.getFullYear()) == disabledDaysRange[i]){
							return [false];
						}
					}
				}
				return [true];
			}
		});
        </script>      
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>