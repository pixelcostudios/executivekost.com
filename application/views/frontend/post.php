<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">      
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/lightbox/jquery.lightbox-0.5.css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>javascripts/lightbox/jquery.lightbox-0.5.js"></script>
        <script type="text/javascript">
        $(function() {
            $('.image a').lightBox({
                imageLoading: '<?php echo base_url(); ?>javascripts/lightbox/images/lightbox-ico-loading.gif',
                imageBtnPrev: '<?php echo base_url(); ?>javascripts/lightbox/images/lightbox-btn-prev.gif',
                imageBtnNext: '<?php echo base_url(); ?>javascripts/lightbox/images/lightbox-btn-next.gif',
           });
        });
		</script>
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a href="<?php echo base_url()?>category/<?php echo $category_name?>.html"><?php echo $category_title?></a> / <a class="current">Detail Post</a></span>
        <?php
		$flashmessage = $this->session->flashdata('message');
		echo !empty($flashmessage)?'<p class="message fadeout">'.$flashmessage.'</p>': '';
		?>
        <div id="social-media">
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
        <a class="addthis_button_tweet"></a>
        <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
        <a class="addthis_button_compact"></a>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50f0cbe33d8a334b"></script>
        <!-- AddThis Button END -->
        </div>
        <?php
		echo heading($post_title,1);
		if(!empty($img1->image)) $url = base_url().$img1->dir.'/'.$img1->image; else $url = "";
        echo '<div class="image"><a href="'.$url.'">';
		echo isset($img1->image)?img(array('src'=>base_url().$img1->dir.'/'.$img1->image,'alt'=>$post_title,'title'=>$post_title)):'';
		echo '</a></div>';
		if(count($img2) > 0){
			foreach($img2 as $a){
				echo '<div class="rel-img image"><a href="'.base_url().$a->dir.'/'.$a->image.'">';
				echo img(array('src'=>''.base_url().$a->dir.'/'.thumb($a->image).'','class'=>'thumb','alt'=>$post_title,'title'=>$post_title));
				echo '</a></div>';
			}
		}
        ?>
        <div class="clr"></div>
		<?php        
		$r = explode('<iframe', $post_content);
		if (isset($r[1])){
			$a = explode('</iframe>', $r[1]);
			$attr = explode('src="',$a[0]);
			echo '<iframe width="418" height="235" src="'.$attr[1].'></iframe>';
			echo br(1).$a[1];
		}		
		else{
			echo $post_content;
		}
		?>        
    	</div>        
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
        <div id="testimoni">
            <h1>Testimoni</h1>
            <?php
			foreach($comments as $a){
            	echo '<div class="testimoni">';
            	echo '<h2>'.$a->name.', '.date("F j, Y",strtotime($a->comment_date)).'</h2>';
            	echo '<p>'.$a->comment_content.'</p>';
            	echo '</div>';
			}			
			?>
        	<div id="comment-form">
            <form method="post" action="<?php echo base_url()?>send_comment">
            <?php			
			echo form_hidden('post_id',''.$post_id.'');
			echo form_hidden('slug',''.$post_name.'');
			echo form_hidden('lang',$this->session->userdata('lang'));
			?>
            <table>
            <tr>
            <td>
			<?php echo form_input('name',''.set_value('name').'','placeholder="NAMA"')?>
			<?php echo form_error('name', '<br /><label class="error">', '</label>')?></td>
            <td>
            <?php echo form_input('email',''.set_value('email').'','placeholder="EMAIL"')?>
			<?php echo form_error('email', '<br /><label class="error">', '</label>')?>
            </td>
            </tr>
            <tr>
            <td colspan="2"><img src="<?php echo base_url()?>stylesheets/images/testimoni-icon.jpg" /><textarea name="comment" rows="3"placeholder="COMMENT ..."><?php echo set_value('comment')?></textarea><?php echo '<br />'.form_error('comment', '<label class="error">', '</label>')?>
            </td>
            </tr>
            <tr>
            <td colspan="2" style="padding-left: 65px;"><?php echo $cap_img?><?php echo form_input('captcha',''.set_value('captcha').'','id="captcha" maxlength="6"')?><input type="submit" name="submit" value="SEND" /></td>
            </tr>
            <tr>
            <td colspan="2" style="padding-left: 65px;"><?php echo $cap_msg?></td>
            </tr>
            </table>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="clr"></div>
</div>