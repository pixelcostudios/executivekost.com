<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    <?php if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";?>
    
    <div id="single">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url();?>">Home</a> / <a href="<?php echo base_url();?>site_map">Site Map</a></span>  
        <h1>Top Menu : </h1>
        <ul>
        	<li><a href="<?php echo base_url()?>lang/id">Ekost Indonesia</a></li>
            <li><a href="<?php echo base_url()?>lang/en">Ekost English</a></li>
        	<li><a href="<?php echo base_url()?>page/<?php if($lang=='') echo 'tentang-e-kost'; else echo 'about-e-kost';?>.html">About Us</a></li>
            <li><a href="<?php echo base_url()?>page/<?php if($lang=='') echo 'hubungi-kami'; else echo 'contact-us';?>.html">Contact Us</a></li>
            <li><a href="<?php echo base_url()?>page/faq.html">FAQ</a></li>
            <li><a href="<?php echo base_url()?>testimonial">Testimonial</a></li>
            <?php if($this->session->userdata('member_login')==TRUE){?> 
            <li><a href="#"> | <?php echo $this->session->userdata('member_username')?></a>
            <ul>
            	<li><a href="<?php echo base_url()?>member">Profile</a></li>
            	<li><a href="<?php echo base_url()?>member/logout">Logout</a></li>
            </ul>
            </li>
            <?php } else{ ?>
            <li><a href="<?php echo base_url()?>member/login">My Account</a></li>
            <?php } ?>
        </ul>
        
        <h1>Main Menu : </h1>
        <ul>
            <li><a href="<?php echo base_url()?>">Home</a></li>
            <li><a href="<?php echo base_url()?>">About Us</a>
            <ul>
            <?php
            $page = $this->site_model->get_data("page_name".$lang." as page_name,page_title".$lang." as page_title","tb_pages","page_status = '1' AND page_id NOT IN (6)")->result();
            foreach($page as $p){
                echo '<li>'.anchor(base_url().'page/'.$p->page_name.'.html',$p->page_title).'</li>';
            }
            ?>
            </ul>
            </li>
            <li><a href="#">Location</a>
            <ul>
            <?php
            $menu = $this->site_model->get_data('',"tb_daerah",'','',"daerah_title ASC")->result();
            foreach($menu as $a){
                echo '<li><a href="'.base_url().'kost/'.$a->daerah_name.'.html">'.$a->daerah_title.'</a></li>';
            }
            ?>
            </ul>
            </li>
            <li><a href="#">Services</a>    
            <ul>
            <?php
            $menu = $this->site_model->get_data("post_name".$lang." as post_name,post_title".$lang." as post_title","tb_posts","category_id = '6'")->result();
            foreach($menu as $a){
                echo '<li><a href="'.base_url().$a->post_name.'.html">'.$a->post_title.'</a></li>';
            }
            ?>
            </ul>
            </li>
            <li><a href="#">Gallery</a>   
            <ul>     
            <?php
            $menu = $this->site_model->get_data("category_name".$lang." as category_name,category_title".$lang." as category_title","tb_category","category_group = '2'")->result();
            foreach($menu as $a){
                echo '<li><a href="'.base_url().'category/'.$a->category_name.'.html">'.$a->category_title.'</a></li>';
            }
            ?>
            </ul>
            </li>
            <li><a href="<?php echo base_url()?>kost">Reservation</a></li>
            <li><a href="#">City Scape</a>
            <ul>
            <?php
            $menu = $this->site_model->get_data("post_name".$lang." as post_name,post_title".$lang." as post_title","tb_posts","category_id = '10'")->result();
            foreach($menu as $a){
                echo '<li><a href="'.base_url().$a->post_name.'.html">'.$a->post_title.'</a></li>';
            }
            ?>
            </ul>
            </li>
            <li><a href="http://blog.executivekost.com/" target="_blank">Blog</a></li>
            <li><a href="<?php echo base_url()?>feed/subscriber">Subscribe Newsletter</a></li>
        </ul>
        
        <h1>Category Menu :</h1>
        <ul>
		<?php
        $menu = $this->site_model->get_data("category_name".$lang." as category_name,category_title".$lang." as category_title","tb_category","category_id IN (3,4)")->result();
        foreach($menu as $a){
            echo '<li><a href="'.base_url().'category/'.$a->category_name.'.html">'.$a->category_title.'</a></li>';
        }
        ?>
        </ul>
    	</div>        
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>
