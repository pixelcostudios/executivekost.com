<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a href="<?php echo base_url()?>kost/detail/<?php echo $this->session->userdata('kost_name')?>.html">Kost</a> / <a class="current">Reservation</a></span>
        <?php
		$flashmessage = $this->session->flashdata('message');
		echo !empty($flashmessage)?'<p class="message fadeout">'.$flashmessage.'</p>': '';
		?>
        <div id="reservation">
        <ul>
        <li><a href="<?php echo base_url()?>member/order">Order Status</a></li>
        <li class="active"><a href="<?php echo base_url()?>member/profile">Personal Information</a></li>
        <li style="float: right;"><a href="<?php echo base_url()?>member/logout">Logout</a></li>
        <div class="clr"></div>
        </ul>
        <div class="clr"></div>
        
        <link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/date-picker/jquery-ui.css" />
        <script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui.js"></script>
		<script type="text/javascript">
			$(function() {
				$( "#birthday" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
			});
        </script>
        <form method="post" action="<?php echo base_url()?>member/update_info">
        <input type="hidden" name="member_id" value="<?php echo $member_id?>" /> 
        <fieldset>
        <legend>Personal Information</legend>
        <table class="reservation">
        <tr><td width="120">Name</td><td> : </td><td><?php echo form_input('name',''.set_value('name', isset($name) ? $name : '').'')?><?php echo form_error('name', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>Identity</td><td> : </td>
        <td>
        <select name="identity">
        <option value="ktm" <?php echo set_select('identity', 'ktm', isset($identity) && $identity == 'ktm' ? TRUE : FALSE);?>>KTM</option>
        <option value="ktp" <?php echo set_select('identity', 'ktp', isset($identity) && $identity == 'ktp' ? TRUE : FALSE);?>>KTP</option>
        <option value="sim" <?php echo set_select('identity', 'sim', isset($identity) && $identity == 'sim' ? TRUE : FALSE);?>>SIM</option>
        <option value="passport" <?php echo set_select('identity', 'passport', isset($identity) && $identity == 'passport' ? TRUE : FALSE);?>>Passport</option>
        </select>
        </td>
        </tr>
        <tr><td>Identity Number</td><td> : </td><td><?php echo form_input('identity_no',''.set_value('identity_no', isset($identity_no) ? $identity_no : '').'')?><?php echo form_error('identity_no', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>Birthday</td><td> : </td><td><?php echo form_input('birthday',''.set_value('birthday', isset($birthday) ? $birthday : '').'','id="birthday"')?><?php echo form_error('birthday', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>Phone</td><td> : </td><td><?php echo form_input('phone',''.set_value('phone', isset($phone) ? $phone : '').'')?><?php echo form_error('phone', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>Email</td><td> : </td><td><?php echo form_input('email',''.set_value('email', isset($email) ? $email : '').'')?><?php echo form_error('email', '<br /><label class="error">', '</label>')?></td></tr>
        <?php $address = array('name'=>'address','value'=>''.set_value('address',isset($address) ? $address : '').'','cols'=>'20','rows'=>'3'); ?>
        <tr><td>Address</i></td><td> : </td><td><?php echo form_textarea($address)?></td></tr>
        <tr><td colspan="2"></td><td><input type="submit" name="submit" value="update" /></tr>
        </table>
        </fieldset>
        </form>            
        <form method="post" action="<?php echo base_url()?>member/update_pass">
        <input type="hidden" name="member_id" value="<?php echo $member_id?>" /> 
        <fieldset>
        <legend>Change Username & Password</legend>
        <table class="reservation">
        <tr><td width="110">Present Password</td><td> : </td><td><?php echo form_password('old_password',''.set_value('old_password').'')?><?php echo form_error('old_password', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>Username</td><td> : </td><td><?php echo form_input('username',''.set_value('username', isset($username) ? $username : '').'')?><?php echo form_error('username', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>New Password</td><td> : </td><td><?php echo form_password('new_password',''.set_value('new_password').'')?><?php echo form_error('new_password', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td>Confirm Password</td><td> : </td><td><?php echo form_password('conf_password',''.set_value('conf_password').'')?><?php echo form_error('conf_password', '<br /><label class="error">', '</label>')?></td></tr>
        <tr><td colspan="2"></td><td><input type="submit" name="submit" value="update" /></tr>
        </table>
        </fieldset>
        </form>
        </div>
        </div>  
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>