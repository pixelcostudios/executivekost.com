<?php
if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
?>
	<div id="header">
    	<div id="social">
        <?php $forhead = $this->site_model->get_data('',"tb_options","option_id = '1'")->row();?>
        <a href="<?php echo $forhead->twitter?>"><img src="<?php echo base_url()?>stylesheets/images/social-twitter.jpg" /></a>
        <a href="<?php echo $forhead->facebook?>"><img src="<?php echo base_url()?>stylesheets/images/social-fb.jpg" /></a>
        <!--<div id="fb-root"></div>
		<script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-like" data-href="http://www.facebook.com/ekost.jogja" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>-->
        </div>
        <div id="header-menu">
        <ul>
        	<li><a href="<?php echo base_url()?>lang/id"><img src="<?php echo base_url()?>stylesheets/images/lang-id.jpg" /></a><a href="<?php echo base_url()?>lang/en"><img src="<?php echo base_url()?>stylesheets/images/lang-en.jpg" /></a></li>
        	<!--<li><a href="<?php //echo base_url()?>page/<?php //if($lang=='') echo 'tentang-e-kost'; else echo 'about-e-kost';?>.html">About Us</a></li>-->
            <li><a href="<?php echo base_url()?>page/<?php if($lang=='') echo 'hubungi-kami'; else echo 'contact-us';?>.html">Contact Us</a></li>
            <li><a href="<?php echo base_url()?>page/faq.html">FAQ</a></li>
            <li><a href="<?php echo base_url()?>testimonial">Testimonial</a></li>
            <?php if($this->session->userdata('member_login')==TRUE){?> 
            <li><a href="#"> | <?php echo $this->session->userdata('member_username')?></a>
            <ul>
            	<li><a href="<?php echo base_url()?>member">Profile</a></li>
            	<li><a href="<?php echo base_url()?>member/logout">Logout</a></li>
            </ul>
            </li>
            <?php } else{ ?>
            <li><a href="<?php echo base_url()?>member/login">My Account</a></li>
            <?php } ?>
            <li><form method="post" action="<?php echo base_url()?>search"><input type="text" name="search" placeholder="search ..." /></form></li>
        </ul>
        </div>
	<div class="clr"></div>
    </div>
    
    <div id="menu">
    <ul id="navbar">
    	<li><a href="<?php echo base_url()?>">Home</a></li>
    	<li><a href="<?php echo base_url()?>">About Us</a>
        <ul>
        <?php
		$page = $this->site_model->get_data("page_name".$lang." as page_name,page_title".$lang." as page_title","tb_pages","page_status = '1' AND page_id NOT IN (6)")->result();
		foreach($page as $p){
			echo '<li>'.anchor(base_url().'page/'.$p->page_name.'.html',$p->page_title).'</li>';
		}
		?>
        </ul>
        </li>
        <li><a href="#">Location</a>
        <ul>
        <?php
		$menu = $this->site_model->get_data('',"tb_daerah",'','',"daerah_title ASC")->result();
		foreach($menu as $a){
			echo '<li><a href="'.base_url().'kost/'.$a->daerah_name.'.html">'.$a->daerah_title.'</a></li>';
		}
		?>
        </ul>
        </li>
        <li><a href="#">Services</a>    
        <ul>
        <?php
		$menu = $this->site_model->get_data("post_name".$lang." as post_name,post_title".$lang." as post_title","tb_posts","category_id = '6'")->result();
		foreach($menu as $a){
			echo '<li><a href="'.base_url().$a->post_name.'.html">'.$a->post_title.'</a></li>';
		}
		?>
        </ul>
        </li>
        <li><a href="#">Gallery</a>   
        <ul>     
        <?php
		$menu = $this->site_model->get_data("category_name".$lang." as category_name,category_title".$lang." as category_title","tb_category","category_group = '2'")->result();
		foreach($menu as $a){
			echo '<li><a href="'.base_url().'category/'.$a->category_name.'.html">'.$a->category_title.'</a></li>';
		}
		?>
        </ul>
        </li>
        <li><a href="<?php echo base_url()?>kost">Reservation</a></li>
        <li><a href="#">City Scape</a>
        <ul>
        <?php
		$menu = $this->site_model->get_data("post_name".$lang." as post_name,post_title".$lang." as post_title","tb_posts","category_id = '10'")->result();
		foreach($menu as $a){
			echo '<li><a href="'.base_url().$a->post_name.'.html">'.$a->post_title.'</a></li>';
		}
		?>
        </ul>
        </li>
        <li><a href="http://blog.executivekost.com/" target="_blank">Blog</a></li>
        <li><a href="<?php echo base_url()?>feed/subscriber">Subscribe Newsletter</a></li>
    </ul>
	<div class="clr"></div>
    <div id="menu-bg"></div>
    </div>