<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="archive">
    	<?php
		if($category->category_type == '2'){
		?>
        <div id="content-gallery">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a class="current"><?php echo $category->category_title?></a></span>    
        <div class="clr"></div>    
        <?php
		foreach($archive as $a){
			echo '<div class="gallery">';
			$image = $this->site_model->get_img("parent_id = '".$a['post_id']."' AND relation = 'post'","1")->row();
			if(count($image)!='0'){
				echo anchor(base_url().$a['post_name'].'.html',img(array('src'=>base_url().$image->dir.'/'.thumb($image->image),'alt'=>$a['post_title'],'title'=>$a['post_title'])));
			}
			else{
				$x = explode('http://www.youtube.com/embed/', $a['post_content']);
				if(isset($x[1])){
					$videoID = explode('"',$x[1]);			
					parse_str(parse_url('http://www.youtube.com/watch?v='.$videoID[0].'', PHP_URL_QUERY), $params);
					if(strlen($params['v'])>0){
						echo anchor(base_url().$a['post_name'].'.html',img(array('src'=>'http://i3.ytimg.com/vi/'.$params['v'].'/default.jpg','alt'=>$a['post_title'],'title'=>$a['post_title'])));
					}
				}
			}
			echo '<div class="caption">'.anchor(base_url().$a['post_name'].'.html',$a['post_title'],array('title'=>$a['post_title'])).'</div>';
			echo '</div>';
		}
		?>
        <?php echo ! empty($pagination) ? '<div class="page_num">' . $pagination . '</div>' : ''; ?> 
        </div>
        <?php
		}
		else{
		?>
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a class="current"><?php echo $category->category_title?></a></span>
        <?php
		foreach($archive as $a){
			echo '<div class="archive">';
			echo heading(anchor(base_url().$a['post_name'].'.html',$a['post_title'],array('title'=>$a['post_title'])),1);
			echo '<p>'.summary($a['post_content'],30).'</p>';
			echo '</div>';
		}
		?>
        <?php echo ! empty($pagination) ? '<div class="page_num">' . $pagination . '</div>' : ''; ?> 
    	</div>        
        
		<?php
        $this->load->view('frontend/sidebar-right');
		}
		?>       
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>