<div id="main-frame">
<?php $this->load->view('frontend/sidebar-left');?>
<div id="content">
	<?php $this->load->view('frontend/menu-top');?>
    
    <div id="single">
    	<div id="content-single">
    	<span id="navigation"><a href="<?php echo base_url()?>">Home</a> / <a class="current">Detail Page</a></span>
        <div id="social-media">
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
        <a class="addthis_button_tweet"></a>
        <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
        <a class="addthis_button_compact"></a>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50f0cbe33d8a334b"></script>
        <!-- AddThis Button END -->
        </div>
        <?php
		echo heading($page_title,1);
		echo isset($image->image)?img(array('src'=>base_url().$image->dir.'/'.$image->image,'alt'=>$page_title,'title'=>$page_title)):'';
		echo '<p>'.$page_content.'</p>';
		
		if($contact=='y'){
        $message = $this->session->flashdata('message');
        echo ! empty($message) ? '<div class="message fadeout">' . $message . '</div>' : '';
        ?>
        <div id="comment">
        <form action="<?php echo base_url()?>send_message" method="post" >
        <?php				
        echo form_hidden('slug',''.$page_name.'');
        ?>
        <table>
        <tr>
        <td><label>Name</label><?php echo form_input('name',''.set_value('name'))?>
        <?php echo '<br />'.form_error('name', '<label class="error">', '</label>')?></td>
        <td><label>Email</label><?php echo form_input('email',''.set_value('email'))?>
        <?php echo '<br />'.form_error('email', '<label class="error">', '</label>')?></td>
        </tr>
        <tr><td colspan="2"><label>Subject</label><?php echo form_input('subject',''.set_value('subject'))?>
        <?php echo '<br />'.form_error('subject', '<label class="error">', '</label>')?></td></tr>
        <tr><td colspan="2"><label>Message</label><textarea name="comment" rows="3"><?php echo set_value('comment')?></textarea><?php echo '<br />'.form_error('comment', '<label class="error">', '</label>')?></td></tr>
        <tr>
        <td><label>Security Code</label><?php echo $cap_img?><?php echo '<label class="error">'.$cap_msg.'</label>'?></td>
		<td><label>&nbsp;</label><?php echo form_input('captcha',''.set_value('captcha').'','size="6" maxlength="6" style="width: auto; font-size:24px; padding: 5px 10px;"')?></td>
        </tr>
        <tr><td colspan="2"><input type="submit" name="submit" value="SUBMIT" /></td>
        </table>
        </form>
        </div>
        <?php } ?> 
    	</div>        
        
		<?php $this->load->view('frontend/sidebar-right');?>
        <div class="clr"></div>
    </div>
</div>
<div class="clr"></div>
</div>