<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<?php
echo form_open_multipart(''.$action.'');
echo form_hidden('user_login_id',''.set_value('user_login_id', isset($user_login_id) ? $user_login_id : $this->uri->segment(4)).'');
?>
<div style="width: 49%; float:left; margin-right: 2%">
<?php
echo '<p><label>:: Nick Name & Username</label></p>';
echo '<p><label>Nick Name</label>'.form_input('nickname',''.set_value('nickname', isset($nickname) ? $nickname : '').'','style="width: 200px"').'</p>';
echo '<p><label>Username</label>'.form_input('username',''.set_value('username', isset($username) ? $username : '').'','style="width: 200px"').'</p>'.form_error('username', '<p class="error">', '</p>');
echo '<p><label>:: Email</label></p>';
echo '<p><label>Email</label>'.form_input('usermail',''.set_value('usermail', isset($usermail) ? $usermail : '').'','style="width: 200px"').'</p>'.form_error('usermail', '<p class="error">', '</p>');
?>
</div>
<div style="width: 49%; float:left;">
<?php
echo '<p><label>:: Password</label></p>';
if($this->session->userdata('form_mode')=='edit' AND $user_login_id == $this->session->userdata('ses_owner_id')){
echo '<p><label>Enter your present password to continue:</label>'.form_password('old_password',''.set_value('old_password').'','style="width: 200px"').'</p>';
echo form_error('old_password', '<p class="error">', '</p>').'<br />';
}
echo '<p><label>New Password</label>'.form_password('new_password',''.set_value('new_password').'','style="width: 200px"').'</p>';
echo form_error('new_password', '<p class="error">', '</p>');
echo '<p><label>Confirm Password</label>'.form_password('conf_password',''.set_value('conf_password').'','style="width: 200px"').'</p>';
echo form_error('conf_password', '<p class="error">', '</p>');
?>
</div>
<div class="clr"></div>
<?php
echo '<br /><p>'.form_submit('','Submit','class="button"').'</p>';
?>
<div class="clr"></div>
</div>
</div>