<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php $options = $this->site_model->get_data('',"tb_options","option_id = '1'")->row(); ?>
<title><?php echo $options->title?> Control Panel Owner</title>
<link type="image/x-icon" rel="shortcut icon" href="<?php echo base_url();?>images/favicon.ico" />
<link href="<?php echo base_url();?>stylesheets/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="topmenu"></div>
<div id="mainlogin">
	<div id="login">
    <h1><?php echo $options->title?> Owner</h1>
    <div class="login-body">	
	<?php
	$attributes = array('name' => 'login_form', 'id' => 'login_form');
	echo form_open(base_url().'owner/process_login', $attributes);
	?>
    <label>Username</label>
    <input type="text" name="username" maxlength="20" value="<?php echo set_value('username');?>" />
    <?php echo form_error('username','<p class="error">','</p>'); ?>
    <label>Password</label>
    <input type="password" name="password" maxlength="20" value="<?php echo set_value('password');?>" />
    <?php echo form_error('password','<p class="error">','</p>'); ?>
	<?php 
	$message = $this->session->flashdata('message');
	echo $message == '' ? '' : '<p class="error">' . $message . '</p>';
    ?>
    <input type="submit" value="Log In" class="button" style="float: right;" />
    <div class="clr"></div>
    </form>
    </div>
    <div class="develop">Development by <a href="#">Git Solution</a></div>
    </div>
</div>
</body>
</html>