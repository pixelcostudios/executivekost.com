<div id="head">
<?php
echo ! empty($h3_title) ? '<h3>' . $h3_title . '</h3>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="invoice">
<div style="width: 49%; float:left; margin-right: 2%">
	<table>
		<tr><th colspan="3">Personal Information</th></tr>
		<tr><td>Name</td>		<td> : </td><td><?php echo $member_name?></td></tr>
		<tr><td>Identity</td>	<td> : </td><td><?php echo $member_identity?> [ <?php echo $member_identity_number?> ]</td></tr>
		<tr><td>Birthday</td>	<td> : </td><td><?php echo date("d F Y",strtotime($member_birthday))?></td></tr>
		<tr><td>Phone</td>		<td> : </td><td><?php echo $member_phone?></td></tr>
		<tr><td>Email</td>		<td> : </td><td><?php echo $member_email?></td></tr>
		<tr><td>Address</td>	<td> : </td><td><?php echo $member_address?></td></tr>
	</table>
<?php 
// confirm
$confirm = $this->site_model->get_data('',"tb_confirms","rent_kost_id = '".$rent_kost_id."'",'',"")->row();
if(count($confirm)!=0 AND $rent_status=='1'){
?>
	<table>
		<tr><th colspan="3">Member Confirm</th></tr>
		<tr><td>Booking Code</td>		<td> : </td><td><?php echo $confirm->booking_code?></td></tr>
		<tr><td>Confirm Date</td>		<td> : </td><td><?php echo date("d F Y | H:i:s",strtotime($confirm->confirm_date))?></td></tr>
		<tr><td>Transfer Date</td>		<td> : </td><td><?php echo date("d F Y | H:i:s",strtotime($confirm->transfer_date))?></td></tr>
		<tr><td>Deposit</td>			<td> : </td><td>Rp. <?php echo rupiah($confirm->deposit)?>,-</td></tr>
		<tr><td>Bank</td>				<td> : </td><td style="text-transform: uppercase;"><?php echo $confirm->bank?></td></tr>
		<tr><td>Bank Account Name</td>	<td> : </td><td><?php echo $confirm->bank_account_number?></td></tr>
		<tr><td>Bank Account Number</td><td> : </td><td><?php echo $confirm->bank_account_name?></td></tr>
		<tr><td>Transfer Message</td>	<td> : </td><td><?php echo $confirm->transfer_message?></td></tr>
	</table>
<?php
}
?>
</div>
<div style="width: 49%; float:left;">
<?php
$exp_date = strtotime("".$time_order."+90 minutes");
if($rent_status=='2'){
	$style = "background-color: #00FF00; color: #FFF";
	$status = "paid";
}
else{
	if($exp_date < now()){
		$style = "background-color: #FF0000; color: #FFF";
		$status = "expired";
	}
	else{
		$style = "background-color: #FFA500; color: #FFF";
		$status = "pending";
	}
}

if($exp_date < now() OR $rent_status == '2'/* OR count($confirm)==0*/ OR $this->session->userdata('ses_owner_level') == 'investor'){}else{
	echo form_open(base_url().'owner/finish');
	echo form_hidden('booking_id', $rent_kost_id);
	echo form_hidden('member_id', $member_id);
}
if($rent_status=='2' AND $this->session->userdata('ses_owner_level') == 'owner'){
	echo form_open(base_url().'owner/update');
	echo form_hidden('rent_kost_id', $rent_kost_id);
}
?>
<table>
	<tr><th colspan="3">Kost Order Information</th></tr>
	<tr><td>Kost Name</td>			<td> : </td><td><?php echo $kost_title?></td></tr>
	<tr><td>Kost Room Number</td>	<td> : </td><td><?php echo $kost_room_number?></td></tr>
	<?php
	if($this->session->userdata('ses_owner_level') == 'owner'){
	?>
		<tr><td>Check In</td>	<td> : </td><td><input type="text" name="check_in" id="check_in" value="<?php echo $check_in?>" /></td></tr>
		<tr><td>Check Out</td>	<td> : </td><td><input type="text" name="check_out" id="check_out" value="<?php echo $check_out?>" /></td></tr>
	<?php
	}
	else{
		?>
		<tr><td>Check In</td>	<td> : </td><td><?php echo date("d F Y",strtotime($check_in))?></td></tr>
		<tr><td>Check Out</td>	<td> : </td><td><?php echo date("d F Y",strtotime($check_out))?></td></tr>
		<?php
	}
	?>
	<tr><td>Total Pay</td>		<td> : </td><td>Rp. <?php echo rupiah($total_pay)?>,-</td></tr>
	<tr><td>Special Request</td><td> : </td><td><?php echo $special_request?></td></tr>
	<?php
	if($rent_status!='3'){
		?>	
		<tr><td>Time Order</td>		<td> : </td><td><?php echo date("d F Y | H:i:s",strtotime($time_order))?></td></tr>
		<tr><td>Order Expired</td>	<td> : </td><td><?php echo date("d F Y | H:i:s",$exp_date)?></td></tr>
		<tr><td>Order Status</td>	<td> : </td><td style="'.$style.'"><?php echo $status?></td></tr>
		<?php
		if($exp_date < now() OR $rent_status=='2'/* OR count($confirm)==0*/ OR $this->session->userdata('ses_owner_level') == 'investor'){
			if($rent_status=='2' AND $this->session->userdata('ses_owner_level') == 'owner'){
				?>
				<tr><td colspan="2"></td><td><input type="submit" value="Update" class="button" /> <span>or</span> <?php echo anchor(base_url().'owner/checkout/'.$rent_kost_id, 'Check Out', 'class="button"')?></td></tr>
				<?php
			}
			else{
				?>
				<tr><td colspan="2"></td><td><?php echo anchor(base_url().'owner/checkout/'.$rent_kost_id, 'Check Out', 'class="button"')?></td></tr>
				<?php
			}			
		}
		else{
			?>
			<input type="hidden" name="member_id" value="<?php echo $member_id?>" />
			<tr><td colspan="2"></td><td><input type="submit" value="Approve" class="button" /></td></tr>
			<?php
		}
	}
?>
</table>
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/date-picker/jquery-ui.css" />
<script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui.js"></script>
<script type="text/javascript">	
	// check in
	$("#check_in").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true,
		onClose: function(selectedDate){
			$( "#check_out" ).datepicker( "option", "minDate: 1", selectedDate )
		}
	});
	// check out
	$("#check_out").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, minDate: 1,
		onClose: function(selectedDate){
			$("#check_in").datepicker("option", selectedDate);
		}
	});	
</script>
<?php
echo '</form>';
?>
</div>
<div class="clr"></div>
</div>
<?php
if($exp_date < now() OR $rent_status=='2'){}else{
	echo '<table style="width: auto;">';
	echo '<tr>';
	echo '<td colspan="2">status : </td>';
	echo '<td><span style="padding:0 5px; background-color: #00FF00;">&nbsp;</span> confirm</td>';
	echo '<td><span style="padding:0 5px; background-color: #FFA500;">&nbsp;</span> pending</td>';
	echo '<td><span style="padding:0 5px; background-color: #FF0000;">&nbsp;</span> expired</td>';
	echo '</tr>';
	echo '</table>';
}
?>
</div>