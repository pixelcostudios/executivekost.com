<div id="head">
<?php
echo ! empty($h3_title) ? '<h3>' . $h3_title . '</h3>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<?php echo form_open_multipart(''.$action.''); ?>
<div id="kost-form">
<p><label>No</label><?php echo form_input('kost_room_number',''.set_value('kost_room_number', isset($kost_room_number) ? $kost_room_number : '').'','style="width: 30px;"')?></p>
<?php echo form_error('title', '<p class="error">', '</p>');?>
<p><label>Type</label><select name="type" style="padding:4px;">
<option value="1" <?php echo set_select('type', '1', isset($type) && $type == '1' ? TRUE : FALSE)?>>Monthly</option>
<option value="2" <?php echo set_select('type', '2', isset($type) && $type == '2' ? TRUE : FALSE)?>>Daily</option>
<option value="3" <?php echo set_select('type', '3', isset($type) && $type == '3' ? TRUE : FALSE)?>>Double</option>
</select></p>
<p><label>Price Day</label><?php echo form_input('price_day',''.set_value('price_day', isset($price_day) ? $price_day : '').'','style="width: 100px;"')?></p>
<p><label>Price Month</label><?php echo form_input('price_month',''.set_value('price_month', isset($price_month) ? $price_month : '').'','style="width: 100px;"')?></p>
<p><label>Title</label><?php echo form_input('title',''.set_value('title', isset($title) ? $title : '').'','style="width: 500px;"')?></p>
</div>
<?php echo form_error('title', '<p class="error">', '</p>');?>
<?php $desc = array('name'=>'des','value'=>''.set_value('des',isset($des) ? $des : '').'','class'=>'area');?>
<p><label class="lang-id">Description</label><?php echo form_textarea($desc)?></p>
<?php $desc_en = array('name'=>'des_en','value'=>''.set_value('des_en',isset($des_en) ? $des_en : '').'','class'=>'area');?>
<p><label class="lang-en">Description</label><?php echo form_textarea($desc_en)?></p>
<script type="text/javascript" language="javascript">
var upload_number = <?php echo count($images)+1 ?>;
function addFileInput() {
	if(upload_number > 5) {
		alert('sorry you can only upload 5 files')
		exit(0);
	}
	var d = document.createElement("div");
	var a = document.createElement("a");
	var file = document.createElement("input");
	var file2 = document.createElement("input");
	var img = document.createElement("img");
	d.setAttribute("id", "f"+upload_number);
	d.setAttribute("style", "clear: both;");
	d.appendChild(file);
	d.appendChild(file2);
	d.appendChild(a);
	a.setAttribute("href", "javascript:removeFileInput('f"+upload_number+"');");
	a.appendChild(img);
	file.setAttribute("type", "file");
	file.setAttribute("name", "image"+upload_number);
	file.setAttribute("style", "margin: 4px 0; float: left;");
	file2.setAttribute("type", "text");
	file2.setAttribute("style", "float: left;");
	file2.setAttribute("name", "caption"+upload_number);
	file2.setAttribute("placeholder", "caption "+upload_number+" . . . .");
	img.setAttribute("src", "<?php echo base_url()?>stylesheets/backend/remove.png");
	img.setAttribute("style", "margin: 4px 0 4px 5px;");
	document.getElementById("moreUploads").appendChild(d);
	upload_number++;
}

function removeFileInput(i) {
	var elm = document.getElementById(i);
	document.getElementById("moreUploads").removeChild(elm);
	upload_number = upload_number -1; // decrement the max file upload counter if the file is removed 
}
</script>
<?php
echo br(1);
if($this->session->userdata('form_mode')=='edit'){
	foreach($images as $a){
		echo '<div class="image-frame"><div class="image">';
		echo '<img src="'.base_url().$a->dir.'/'.thumb($a->image).'" width="145" alt="'.$a->caption.'" title="'.$a->caption.'" />';
		echo '</div>';
		echo anchor(base_url().'owner_backend/kost/'.$kost_id.'/delete_image/'.$a->image_id.'/'.$a->parent_id.'',img(array('src'=>base_url().'stylesheets/backend/remove.png')));
		echo '</div>';
	}
}
?>
<div class="clr">&nbsp;</div>
<p><label class="left">Images</label><a onclick="addFileInput();" style="cursor: pointer;"><img src="<?php echo base_url()?>stylesheets/backend/plus.png" /></a></p>
<div id="moreUploads"></div>
<p class="error">filetype allowed : .gif .jpg .jpeg .png</p>
<?php
echo '<p>'.form_submit('','Submit','class="button"').' <span>atau</span> '.anchor(base_url('owner/'.$this->uri->segment(2).'/'.$this->uri->segment(3)),'kembali').'</p>';
?>
</div>