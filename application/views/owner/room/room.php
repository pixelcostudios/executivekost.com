<div id="head">
<?php
$this->load->view('owner/room_menu');
echo ! empty($h3_title) ? '<h3>' . $h3_title . '</h3>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<?php
if($this->session->userdata('ses_owner_level')!='investor'){?>
<button class="button" onclick="location.href='<?php echo $action?>'" />add</button>
<?php } ?>
<div class="clr"></div>
<?php
echo ! empty($table) ? $table : '';
echo ! empty($pagination) ? "<div class=\"page_num\">" . $pagination . '</div>' : '';
echo '<table style="width: auto;">';
echo '<tr>';
echo '<td><span class="edit"></span> edit</td>';
echo '<td><span class="delete"></span> delete</td>';
echo '</tr>';
echo '</table>';
?>
</div>