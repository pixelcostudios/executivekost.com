<?php
$this->load->view('owner/room_menu');
?>
<div class="head"><h3><?php echo $namakost ?></h3></div>
<div class="head"><h3>Setting Akun</h3></div>
<div id="data">
<form name="form1" method="post" action="<?php echo base_url()."owner/accounting/".$this->uri->segment(3)."/akun/proses-akun" ?>">
  <table width="700" border="0" class="table_list">
    <tr>
      <td width="140">Kode Akun</td>
      <td width="10">&nbsp;</td>
      <td width="536"><input name="kodeakun" type="text" id="kodeakun" value="<?php echo $kodeakun ?>">
      <input name="mode" type="hidden" id="mode" value="<?php echo $mode ?>" />
      <input name="idakun" type="hidden" id="idakun" value="<?php echo $idakun ?>" /></td>
    </tr>
    <tr>
      <td>Jenis Akun</td>
      <td>&nbsp;</td>
      <td><select name="idjenisakun" id="idjenisakun">
      <option value="">[-pilih dahulu-]</option>
      <?php foreach($jenisakun as $list){ ?>
      <option value="<?php echo $list['idjenisakun'] ?>" <?php echo ($list['idjenisakun']==$idjenisakun)?'selected':'' ?>><?php echo $list['namajenisakun'] ?></option>
      <?php } ?>
      </select></td>
    </tr>
    <tr>
      <td>Nama Akun</td>
      <td>&nbsp;</td>
      <td><input type="text" name="namaakun" id="namaakun" value="<?php echo $namaakun ?>"></td>
    </tr>
    <tr>
      <td>Pos Saldo</td>
      <td>&nbsp;</td>
      <td><select name="possaldo" id="possaldo">
      <option value="">[-pilih dahulu-]</option>
      <option value="DB" <?php echo ($possaldo=='DB')?'selected':'' ?>>DEBET</option>
      <option value="KR" <?php echo ($possaldo=='KR')?'selected':'' ?>>KREDIT</option>
      </select></td>
    </tr>
    <tr>
      <td>Pos Laporan</td>
      <td>&nbsp;</td>
      <td><select name="poslaporan" id="poslaporan">
      <option value="">[-pilih dahulu-]</option>
      <option value="NRC" <?php echo ($poslaporan=='NRC')?'selected':'' ?>>NERACA</option>
      <option value="LB" <?php echo ($poslaporan=='LB')?'selected':'' ?>>RUGI LABA</option>
      </select></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="submit" name="button" id="button" value="Submit"></td>
    </tr>
  </table>
</form>
</div>