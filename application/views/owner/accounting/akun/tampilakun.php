<?php
$this->load->view('owner/room_menu');
?>
<div class="head"><h3><?php echo $namakost ?></h3></div>
<div class="head"><h3>Setting Akun</h3></div>
<div id="data">
<input type="button" value="add" class="button" style="float:left; margin:0 5px 0 0;" onclick="location.href='<?php echo base_url().'owner/accounting/'.$this->uri->segment(3).'/akun/input-akun' ?>'" />
<div class="clr"></div>
<table width="880" border="0" class="table_list">
  <tr>
    <th width="102" height="22" align="center">Kode Akun</th>
    <th width="199" align="center">Nama Jenis Akun</th>
    <th width="293" align="center">Nama Akun</th>
    <th width="80" align="center">Pos Saldo</th>
    <th width="80" align="center">Pos Laporan</th>
    <th width="100" align="center">&nbsp;</th>
  </tr>
  <?php
  if(count($akun)==0){
  ?>
  <tr>
    <td colspan="6">&nbsp;</td>
  </tr>
  <?php
  }else{
	  foreach($akun as $list){
	  ?>
  <tr>
    <td><?php echo $list['kodejenisakun'] ?>-<?php echo $list['kodeakun'] ?></td>
    <td><?php echo $list['namajenisakun'] ?></td>
    <td><?php echo $list['namaakun'] ?></td>
    <td align="center"><?php echo ($list['possaldo']=='DB')?'DEBET':(($list['possaldo']=='KR')?'KREDIT':'') ?></td>
    <td align="center"><?php echo ($list['poslaporan']=='NRC')?'Neraca':(($list['poslaporan']=='LB')?'Laba Rugi':'') ?></td>
    <td align="center"><a href="<?php echo base_url()."owner/accounting/".$this->uri->segment(3)."/akun/edit-akun/".$list['idakun'] ?>" class="edit"></a>
    <a href="<?php echo base_url()."owner/accounting/".$this->uri->segment(3)."/akun/delete-akun/".$list['idakun'] ?>" class="delete"></a></td>
  </tr>
  
  <?php
  }}
  ?>
  
  
  
</table>
</div>