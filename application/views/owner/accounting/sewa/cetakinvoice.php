<style type="text/css">
body{margin:0;}
.print {
	width:755.905511811px;
	/*height:861.732283465px;*/
	border:1px solid #999;
	margin:0 auto 10px auto !important;
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;
}
.tabel table{border:1px solid #999;border-collapse:collapse;}
.tabel th{border:1px solid #999;}
.tabel td{border:1px solid #999;}
h1{font-size:18px;font-weight:bold;}
/*.tabel tr{border:1px solid #999;}*/
</style>
<div class="print">
<table width="100%" border="0">
  <tr>
    <td align="center"><h1>KWITANSI</h1></td>
  </tr>
  <tr>
    <td align="right"><?php echo $notransaksi ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0">
      <tr>
        <td width="27%">Sudah terima dari</td>
        <td width="73%"><?php echo $member_name ?></td>
      </tr>
      <tr>
        <td>Jumlah uang</td>
        <td>: <?php echo $deposit ?></td>
      </tr>
      <tr>
        <td>Terbilang</td>
        <td><div style="text-transform:capitalize">: <strong><?php echo $terbilang ?></strong></div></td>
      </tr>
      <tr>
        <td>Untuk pembayaran</td>
        <td>: Sewa kamar pada <?php echo $kost_title ?> <?php echo $kost_room_title ?> tanggal <?php echo $checkin ?> sampai <?php echo $checkout ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td height="127"><table width="100%" border="0">
      <tr>
        <td width="61%">&nbsp;</td>
        <td width="39%"><?php echo $transfer_date ?></td>
      </tr>
      <tr>
        <td height="74">&nbsp;</td>
        <td valign="bottom"><?php echo $petugas ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Yang menerima</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  </table>

</div>