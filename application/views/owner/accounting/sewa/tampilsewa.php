<?php
$this->load->view('owner/room_menu');
?>
<div class="head"><h3>Data Sewa Kamar</h3></div>
<div id="data">
<input type="button" value="add" class="button" style="float:left; margin:0 5px 0 0;" onclick="location.href='<?php echo base_url().'owner/accounting/'.$this->uri->segment(3).'/sewakamar/carimember'?>'" />
<div class="clr"></div>

<table width="880" border="0" class="table_list">
  <tr>
    <th width="84" height="22" align="center">Tanggal Transaksi</th>
    <th width="83" align="center">Bukti Transaksi</th>
    <th width="239" align="center">Member</th>
    <th width="80" align="center">No Kamar</th>
    <th width="81" align="center">Tgl Masuk</th>
    <th width="83" align="center">Tgl Keluar</th>
    <th width="166" align="center">Pembayaran</th>
    <th width="50" align="center">&nbsp;</th>
  </tr>
  <?php
  if(count($sewakamar)==0){
  ?>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <?php
  }else{
	  foreach($sewakamar as $list){
	  ?>
  <tr>
    <td><?php echo $list['tanggalsewakamar'] ?></td>
    <td><?php echo $list['notransaksi'] ?></td>
    <td><?php echo $list['member_name'] ?></td>
    <td align="right"><?php echo $list['kost_room_number'] ?></td>
    <td align="right"><?php echo $list['tanggalmasuk'] ?></td>
    <td align="right"><?php echo $list['tanggalkeluar'] ?></td>
    <td align="right"><?php echo number_format($list['besaran'],0,',','.') ?></td>
    <td align="right"><a href="<?php echo base_url()."owner/accounting/".$this->uri->segment(3)."/sewakamar/edit-sewakamar/".$list['idsewakamar'] ?>" class="edit"></a>
    <a href="<?php echo base_url()."owner/accounting/".$this->uri->segment(3)."/sewakamar/delete-sewakamar/".$list['idsewakamar'] ?>" class="delete"></a></td>
  </tr>
  
  <?php
  }}
  ?>
  </table>
</div>