<style type="text/css">
body{margin:0;}
.print {
	width:755.905511811px;
	/*height:861.732283465px;*/
	border:1px solid #999;
	margin:0 auto 10px auto !important;
	font-family:Arial, Helvetica, sans-serif;
	font-size:10px;
}
.tabel table{border:1px solid #999;border-collapse:collapse;}
.tabel th{border:1px solid #999;}
.tabel td{border:1px solid #999;}
.tabel h1{font-size:14;}
/*.tabel tr{border:1px solid #999;}*/
</style>
<div class="print">
<table width="100%" border="0">
<tr>
<td width="33%">Nama Kost : <?php echo $namakost ?></td>
<td width="33%">&nbsp;</td>
<td width="33%">No. Transaksi : <?php echo $notransaksi ?></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td align="center"><strong><h1>Form Permintaan Belanja</h1></strong></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td colspan="3">Nama : <?php echo $namapenjaga ?></td>
  </tr>
<tr>
  <td>Rencana kebutuhan biaya</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td colspan="3">
  
  <div class="tabel">
  <table width="100%" border="1">
    <tr>
      <td width="3%" rowspan="2" align="center">No</td>
      <td width="11%" rowspan="2" align="center">Tanggal</td>
      <td width="36%" rowspan="2" align="center">Uraian</td>
      <td width="13%" rowspan="2" align="center">satuan</td>
      <td colspan="2" align="center">Jumlah Satuan</td>
      <td width="15%" rowspan="2" align="center">Total</td>
    </tr>
    <tr>
      <td width="7%" align="center">Jumlah</td>
      <td width="15%" align="center">Harga</td>
      </tr>
    <?php
	foreach($detailpengajuan as $list){ ?>
    <tr>
      <td><?php echo $list['no'] ?></td>
      <td align="center"><?php echo $tgltransaksi ?></td>
      <td><?php echo $list['uraian'] ?></td>
      <td><?php echo $list['satuan'] ?></td>
      <td align="center"><?php echo $list['jumlah'] ?></td>
      <td align="right"><?php echo $list['harga'] ?></td>
      <td align="right"><?php echo $list['total'] ?></td>
    </tr>
    
    <?php } ?>
    <tr>
      <td>&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td colspan="4">&nbsp;</td>
      <td align="right"><?php echo $gtotal ?></td>
    </tr>
    <tr>
      <td colspan="7"><div style="text-transform:capitalize"><strong>Terbilang:</strong><?php echo $terbilang ?></div></td>
      </tr>
  </table>
  </div>
  </td>
  </tr>
<tr>
  <td colspan="3">&nbsp;</td>
</tr>
<tr>
  <td colspan="3">Yogyakarta, <?php echo date('d-m-Y') ?></td>
</tr>
<tr>
  <td colspan="3"><table width="100%" border="0">
    <tr>
      <td width="60%">Dibuat oleh,</td>
      <td width="40%">Disetujui pimpinan,</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php echo $namapenjaga ?></td>
      <td><?php echo $namapemilik ?></td>
    </tr>
  </table></td>
  </tr>
</table>
</div>