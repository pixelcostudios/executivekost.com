<?php
$this->load->view('owner/room_menu');
$this->load->model('owner/accounting/mfunction','fungsi');
?>
<div class="head"><h3><?php echo $namakost ?></h3></div>
<div class="head"><h3>Detail Pengajuan Belanja</h3></div>
<div id="data">
<table width="700" border="0">
  <tr>
    <td width="159">Tanggal Pengajuan</td>
    <td width="10">:</td>
    <td width="517"><?php echo $this->fungsi->tgl($pengajuan['tglpengajuan']) ?></td>
  </tr>
  <tr>
    <td>Nama Kost</td>
    <td>:</td>
    <td><?php echo $pengajuan['kost_title'] ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<form method="post" action="<?php echo base_url().'owner/accounting/'.$this->uri->segment(3).'/belanja/owner-detail/proses-persetujuan' ?>">
<table width="730" border="0" class="table_list">
  <tr>
    <th width="195" align="center">Uraian</th>
    <th width="106" align="center">Satuan</th>
    <th width="81" align="center">Jumlah</th>
    <th width="126" align="center">Harga Satuan</th>
    <th width="129" align="center">Total</th>
    <th width="53" align="center">Persetujuan</th>
  </tr>
  <?php 
	$totals=0;
	$i=0;
	foreach($detailpengajuan as $list){ 
	$i++;
	?>
  <tr>
    <td><?php echo $list['uraian'] ?></td>
    <td align="center"><?php echo $list['satuan'] ?></td>
    <td align="center"><?php echo $list['jumlah'] ?></td>
    <td align="right">Rp. <?php echo number_format($list['harga'],0,',','.') ?></td>
    <td align="right"> Rp.
      <?php
	  $total=$list['jumlah']*$list['harga'];
	  $totals+=$total;
	  echo number_format($total,0,',','.');
	  ?></td>
    <td align="center"><input type="checkbox" name="setuju[]" id="setuju" value="<?php echo $list['iddetailpengajuan'] ?>" <?php echo ($list['statusdetail']==1)?'checked':'' ?> /></td>
  </tr>
  <?php } ?>
  <tr>
    <td colspan="6" align="right">Rp. <?php echo number_format($totals,0,',','.'); ?></td>
  </tr>
  <tr>
    <td colspan="6" align="right"><input name="idpengajuanbelanja" type="hidden" id="idpengajuanbelanja" value="<?php echo $this->uri->segment(6) ?>" />      <input type="submit" name="button" id="button" value="Submit" class="button" /></td>
  </tr>
</table>
</form>
</div>