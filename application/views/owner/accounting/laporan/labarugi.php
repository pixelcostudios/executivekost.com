<?php
$this->load->view('owner/room_menu');
?>
<div class="head">
<h3><?php echo $kost_title ?></h3>
<h3>LAPORAN RUGI-LABA</h3>
<h3>Tanggal: <?php echo date('d-m-Y') ?></h3>
</div>
<div id="data">
<br />
<form id="form1" name="form1" method="post" action="<?php echo base_url().'owner/accounting/'.$this->uri->segment(3).'/laporan/rugi-laba'?>">
  <select name="bulan" id="bulan">
  	<option value="">[-pilih dahulu-]</option>
  	<option value="01" <?php echo ($bulan=='01')?'selected':'' ?>>Januari</option>
    <option value="02" <?php echo ($bulan=='02')?'selected':'' ?>>Februari</option>
    <option value="03" <?php echo ($bulan=='03')?'selected':'' ?>>Maret</option>
    <option value="04" <?php echo ($bulan=='04')?'selected':'' ?>>April</option>
    <option value="05" <?php echo ($bulan=='05')?'selected':'' ?>>Mei</option>
    <option value="06" <?php echo ($bulan=='06')?'selected':'' ?>>Juni</option>
    <option value="07" <?php echo ($bulan=='07')?'selected':'' ?>>Juli</option>
    <option value="08" <?php echo ($bulan=='08')?'selected':'' ?>>Agustus</option>
    <option value="09" <?php echo ($bulan=='09')?'selected':'' ?>>September</option>
    <option value="10" <?php echo ($bulan=='10')?'selected':'' ?>>Oktober</option>
    <option value="11" <?php echo ($bulan=='11')?'selected':'' ?>>November</option>
    <option value="12" <?php echo ($bulan=='12')?'selected':'' ?>>Desember</option>
  </select>
  <select name="tahun" id="tahun">
  	<option value="">[-pilih dahulu-]</option>
  	<?php 
		for($i=$minimum;$i<=$maximum;$i++){ ?>
    <option value="<?php echo $i ?>" <?php echo ($tahun==$i)?'selected':'' ?>><?php echo $i ?></option>
    <?php } ?>
  </select>
  <input type="submit" name="button" id="button" class="button" value="Submit" />
</form>
<table width="600" border="0" class="table_list">
  <tr>
    <th width="91">Kode Akun</th>
    <th width="286">Nama Akun</th>
    <th width="99">&nbsp;</th>
    <th width="106">&nbsp;</th>
  </tr>
  <?php if(count($rugilaba)==0){ ?>
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <?php 
  }else{
	
  	foreach($rugilaba as $list){
		if(!$list['bold']){
	  ?>
      
  <tr>
    <td><?php echo $list['kodeakun'] ?></td>
    <td><?php echo $list['namaakun'] ?></td>
    <td align="right"><?php echo ($list['debit']=='')?'0':number_format($list['debit'],0,',','.') ?></td>
    <td align="right">&nbsp;</td>
  </tr>
  <?php }else{?>
  <tr style="font-weight:bold;">
    <td>&nbsp;</td>
    <td> <?php echo strtoupper('TOTAL '.$list['namaakun']) ?></td>
    <td align="right">&nbsp;</td>
    <td align="right"><?php echo ($list['kredit']=='')?'0':number_format($list['kredit'],0,',','.') ?></td>
  </tr>
  
  <?php
  
  }
  
  ?>
  
  
  <?php }}  
  if(count($rugilaba)>0){
  ?>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr style="font-weight:bold;">
    <td>&nbsp;</td>
    <td>Laba Bersih</td>
    <td align="right">&nbsp;</td>
    <td align="right">
    <?php 
	if($total<0)
	echo "<div style=\"color:#f00;\">".number_format($total,0,',','.')."</div>";
    else
    echo number_format($total,0,',','.');
	?>
    </td>
  </tr>
  <?php } ?>
</table>
<br  />
<input type="button" value="Download Excel" class="button" style="float:left; margin:0 5px 0 0;" onclick="location.href='<?php echo base_url().'owner/accounting/'.$this->uri->segment(3).'/laporan/cetak-rugi-laba/'.$bulan.'/'.$tahun ?>'" />
<div class="clr"></div>
</div>