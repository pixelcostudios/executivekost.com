<script type="text/javascript">

/* -----------------------
   click bolak balik
----------------------- */
function yusfangsion(str){
  $("#semua div").attr("style","display:none");
  $("#"+str).attr("style","display:block; float:left; margin-right: 5px;");
}
</script>
<?php
$this->load->view('owner/room_menu');
?>
<div class="head"><h3><?php echo $namakost ?></h3></div>
<div class="head">
  <h3>Data Pendapatan Lain-lain</h3></div>
<div id="data">
<input type="button" value="add" class="button" style="float:left; margin:0 5px 0 0;" onclick="location.href='<?php echo base_url().'owner/accounting/'.$this->uri->segment(3).'/pendapatanlain/input' ?>'" />
<div class="clr"></div>

<br />
<form id="form1" name="form1" method="post" action="<?php echo base_url().'owner/accounting/'.$this->uri->segment(3).'/pendapatanlain/tampil-pendapatanlain'?>">
<table width="880" border="0" class="table_list">
  <tr>
    <td>
    
      </td>
  </tr>
  <tr>
    <td>
    <select name="selectpilih" onchange="yusfangsion(this.value)" style="float:left;margin:0 10px 0 0;" >
    <option value="0">--</option>
    <option value="harian" <?php echo ($selectpilih=='harian')?'selected':'' ?>>Harian</option>
    <option value="bulanan" <?php echo ($selectpilih=='bulanan')?'selected':'' ?>>Bulanan</option>
    <option value="lainnya" <?php echo ($selectpilih=='lainnya')?'selected':'' ?>>Lainnya</option>
    </select>
    
    <div id="semua">
    <div id="harian" style="display:none;">
    Tanggal Transaksi : <input type="text" name="hariantransaksi" id="hariantransaksi" value="<?php echo $hariantransaksi ?>" />  
    </div>
    <div id="bulanan" style="display:none;">
    Tanggal Transaksi : 
      <select name="bulanbulanan" id="bulanbulanan">
      	<option value="">[--pilih--]</option>
      	<option value="01" <?php echo ($bulanbulanan=='01')?'selected':'' ?>>Januari</option>
        <option value="02" <?php echo ($bulanbulanan=='02')?'selected':'' ?>>Februari</option>
        <option value="03" <?php echo ($bulanbulanan=='03')?'selected':'' ?>>Maret</option>
        <option value="04" <?php echo ($bulanbulanan=='04')?'selected':'' ?>>April</option>
        <option value="05" <?php echo ($bulanbulanan=='05')?'selected':'' ?>>Mei</option>
        <option value="06" <?php echo ($bulanbulanan=='06')?'selected':'' ?>>Juni</option>
        <option value="07" <?php echo ($bulanbulanan=='07')?'selected':'' ?>>Juli</option>
        <option value="08" <?php echo ($bulanbulanan=='08')?'selected':'' ?>>Agustus</option>
        <option value="09" <?php echo ($bulanbulanan=='09')?'selected':'' ?>>September</option>
        <option value="10" <?php echo ($bulanbulanan=='10')?'selected':'' ?>>Oktober</option>
        <option value="11" <?php echo ($bulanbulanan=='11')?'selected':'' ?>>November</option>
        <option value="12" <?php echo ($bulanbulanan=='12')?'selected':'' ?>>Desember</option>
      </select>
      <select name="tahunbulanan" id="tahunbulanan">
      	<option value="">[--pilih--]</option>
		<?php for($i=$minimum;$i<=$maximum;$i++){ ?>
			<option value="<?php echo $i ?>" <?php echo ($tahunbulanan==$i)?'selected':''; ?>><?php echo $i ?></option>
        <?php } ?>
      </select>
    </div>
    <div id="lainnya" style="display:none;">
    Tanggal Transaksi : <input type="text" name="laintransaksi1" id="laintransaksi1" value="<?php echo $laintransaksi1 ?>" />  
    s.d 
    <input type="text" name="laintransaksi2" id="laintransaksi2" value="<?php echo $laintransaksi2 ?>"/>
    </div>
    </div>
    
    <input type="submit" name="button" id="button" value="Submit" class="button" /></td>
    </tr>
</table>
</form>


<table width="880" border="0" class="table_list">
  <tr>
    <th width="156" height="22" align="center">Tanggal</th>
    <th width="108" align="center">No Transaksi</th>
    <th width="401" align="center">Uraian</th>
    <th width="121" align="center">Jumlah</th>
    <th width="72" align="center">&nbsp;</th>
  </tr>
  <?php
  if(count($pendapatanlain)==0){
  ?>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <?php
  }else{
	  foreach($pendapatanlain as $list){
	  ?>
  <tr>
    <td><?php echo $list['tanggaltransaksi'] ?></td>
    <td><?php echo $list['notransaksi'] ?></td>
    <td><?php echo $list['uraian'] ?></td>
    <td align="center"><?php echo $list['besaran'] ?></td>
    <td align="center"><a href="<?php echo base_url()."owner/accounting/".$this->uri->segment(3)."/pendapatanlain/edit/".$list['idtransaksi'] ?>" class="edit"></a>
    <a href="<?php echo base_url()."owner/accounting/".$this->uri->segment(3)."/pendapatanlain/delete/".$list['idtransaksi'] ?>" class="delete"></a></td>
  </tr>
  
  <?php
  }}
  ?>
  <tr>
    <td colspan="3" align="right"><b>Total Pendapatan</b></td>
    <td align="right"><b><?php echo $total ?></b></td>
    <td align="center">&nbsp;</td>
  </tr>
  
  
</table>
</div>


<link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/date-picker/jquery-ui.css" />
<script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#hariantransaksi" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
	$( "#laintransaksi1" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
	$( "#laintransaksi2" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
	
	yusfangsion('<?php echo $selectpilih ?>');
</script>