<script>

/* -----------------------
   click bolak balik
----------------------- */
function yusfangsion(str){
  $("#semua div").attr("style","display:none");
  $("#"+str).attr("style","display:block; float:left; margin-right: 5px;");
}
</script>
<?php
$this->load->view('owner/room_menu');
?>
<div class="head"><h3><?php echo $namakost ?></h3></div>
<div class="head"><h3>Transaksi</h3></div>
<div id="data">
<?php
$user=array('owner','guard');
if(in_array($this->session->userdata('ses_owner_level'),$user )){ ?>
<input type="button" value="add" class="button" style="float:left; margin:0 5px 0 0;" onclick="location.href='<?php echo base_url().'owner/accounting/'.$this->uri->segment(3).'/transaksi/input-transaksi'?>'"  />
<?php } ?>
<div class="clr"></div>
<br />
<form id="form1" name="form1" method="post" action="<?php echo base_url().'owner/accounting/'.$this->uri->segment(3).'/transaksi/tampil-transaksi'?>">
<table width="880" border="0" class="table_list">
  <tr>
    <td>
    
      </td>
  </tr>
  <tr>
    <td>
    <select name="selectpilih" onchange="yusfangsion(this.value)" style="float:left;margin:0 10px 0 0;" >
    <option value="0">--</option>
    <option value="harian" <?php echo ($selectpilih=='harian')?'selected':'' ?>>Harian</option>
    <option value="bulanan" <?php echo ($selectpilih=='bulanan')?'selected':'' ?>>Bulanan</option>
    <option value="lainnya" <?php echo ($selectpilih=='lainnya')?'selected':'' ?>>Lainnya</option>
    </select>
    
    <div id="semua">
    <div id="harian" style="display:none;">
    Tanggal Transaksi : <input type="text" name="hariantransaksi" id="hariantransaksi" value="<?php echo $hariantransaksi ?>" />  
    </div>
    <div id="bulanan" style="display:none;">
    Tanggal Transaksi : 
      <select name="bulanbulanan" id="bulanbulanan">
      	<option value="">[--pilih--]</option>
      	<option value="01" <?php echo ($bulanbulanan=='01')?'selected':'' ?>>Januari</option>
        <option value="02" <?php echo ($bulanbulanan=='02')?'selected':'' ?>>Februari</option>
        <option value="03" <?php echo ($bulanbulanan=='03')?'selected':'' ?>>Maret</option>
        <option value="04" <?php echo ($bulanbulanan=='04')?'selected':'' ?>>April</option>
        <option value="05" <?php echo ($bulanbulanan=='05')?'selected':'' ?>>Mei</option>
        <option value="06" <?php echo ($bulanbulanan=='06')?'selected':'' ?>>Juni</option>
        <option value="07" <?php echo ($bulanbulanan=='07')?'selected':'' ?>>Juli</option>
        <option value="08" <?php echo ($bulanbulanan=='08')?'selected':'' ?>>Agustus</option>
        <option value="09" <?php echo ($bulanbulanan=='09')?'selected':'' ?>>September</option>
        <option value="10" <?php echo ($bulanbulanan=='10')?'selected':'' ?>>Oktober</option>
        <option value="11" <?php echo ($bulanbulanan=='11')?'selected':'' ?>>November</option>
        <option value="12" <?php echo ($bulanbulanan=='12')?'selected':'' ?>>Desember</option>
      </select>
      <select name="tahunbulanan" id="tahunbulanan">
      	<option value="">[--pilih--]</option>
		<?php for($i=$minimum;$i<=$maximum;$i++){ ?>
			<option value="<?php echo $i ?>" <?php echo ($tahunbulanan==$i)?'selected':''; ?>><?php echo $i ?></option>
        <?php } ?>
      </select>
    </div>
    <div id="lainnya" style="display:none;">
    Tanggal Transaksi : <input type="text" name="laintransaksi1" id="laintransaksi1" value="<?php echo $laintransaksi1 ?>" />  
    s.d 
    <input type="text" name="laintransaksi2" id="laintransaksi2" value="<?php echo $laintransaksi2 ?>"/>
    </div>
    </div>
    
    <input type="submit" name="button" id="button" value="Submit" class="button" /></td>
    </tr>
</table>
</form>

<table width="880" border="0" class="table_list">
  <tr>
    <th width="84" height="22" align="center">Tanggal</th>
    <th width="83" align="center">Bukti Transaksi</th>
    <th width="239" align="center">Uraian</th>
    <th width="80" align="center">Debet(Rp.)</th>
    <th width="81" align="center">Kredit(Rp.)</th>
    <th width="83" align="center">Pajak(10%)</th>
    <th width="166" align="center">Saldo</th>
    <th width="30" align="center">&nbsp;</th>
  </tr>
  <?php
  if(count($transaksi)==0){
  ?>
  <tr>
    <td colspan="8">&nbsp;</td>
  </tr>
  <?php
  }else{
	  $totdebit=0;
	  $totkredit=0;
	  $totpajak=0;
	  $total=0;
	  foreach($transaksi as $list){
		  
		  $debit=$list['debit'];
		  $kredit=$list['kredit'];
		  $totdebit+=$debit;
	  	  $totkredit+=$kredit;
	  	  $totpajak+=$list['pajak'];
	  	  $total+=($debit-$kredit-$list['pajak']);
	  ?>
  <tr>
    <td><?php echo $list['tanggaltransaksi'] ?></td>
    <td><a href="<?php echo base_url().'owner/accounting/'.$this->uri->segment(3).'/penyesuaian/input/'.$list['idtransaksi'] ?>"><?php echo $list['notransaksi'] ?></a></td>
    <td><?php echo $list['uraian'] ?></td>
    <td align="right"><?php echo ($debit!='')?number_format($debit,0,',','.'):$debit ?></td>
    <td align="right"><?php echo ($kredit!='')?number_format($kredit,0,',','.'):$kredit ?></td>
    <td align="right"><?php echo ($list['pajak']==0)?'':number_format($list['pajak'],0,',','.') ?></td>
    <td align="right">
	<?php 
	if($total<0)
	echo "<div style=\"color:#f00;\">".number_format($total,0,',','.')."</div>";
    else
    echo number_format($total,0,',','.');
	?>
    
    </td>
    <td align="right"><a href="<?php echo base_url()."owner/accounting/".$this->uri->segment(3)."/transaksi/edit-transaksi/".$list['idtransaksi'] ?>" class="edit"></a><br />
    <!--<a href="<?php echo base_url()."owner/accounting/sewakamar/".$this->uri->segment(4)."/delete-transaksi/".$list['idtransaksi'] ?>" class="delete">del</a>-->
		
	</td>
  </tr>
  
  <?php
  }}
  
  if(count($transaksi)>0){
  ?>
  
  <tr style="font-weight:bold">
    <td colspan="3" align="right">Jumlah Penerimaan/Pengeluaran</td>
    <td align="right"><?php echo number_format($totdebit,0,',','.') ?></td>
    <td align="right"><?php echo number_format($totkredit,0,',','.') ?></td>
    <td align="right"><?php echo number_format($totpajak,0,',','.') ?></td>
    <td align="right">
	<?php 
	if($total<0)
	echo "<div style=\"color:#f00;\">".number_format($total,0,',','.')."</div>";
    else
    echo number_format($total,0,',','.');
	?>
    </td>
    <!--<td align="right">&nbsp;</td>-->
  </tr>
  <?php } ?>
</table>
<br  />

<?php 
$link=base_url().'owner/accounting/'.$this->uri->segment(3).'/transaksi/cetak-transaksi/'.$selectpilih;
if($selectpilih=='harian')
	$link.='/'.$hariantransaksi;
elseif($selectpilih=='bulanan')
	$link.='/'.$bulanbulanan.'/'.$tahunbulanan;
else
	$link.='/'.$laintransaksi1.'/'.$laintransaksi2;

?>
	
<input type="button" value="Download Excel" class="button" style="float:left; margin:0 5px 0 0;" onclick="location.href='<?php echo $link ?>'" /><!--.$tanggaltransaksi-->
<div class="clr"></div>
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/date-picker/jquery-ui.css" />
<script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#hariantransaksi" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
	$( "#laintransaksi1" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
	$( "#laintransaksi2" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
	
	yusfangsion('<?php echo $selectpilih ?>');
</script>