<?php
$this->load->view('owner/room_menu');
?>
<div class="head"><h3><?php echo $namakost ?></h3></div>
<div class="head"><h3><?php echo $mode ?> Transaksi</h3></div>
<div id="data">
<form name="form1" method="post" action="<?php echo base_url()."owner/accounting/".$this->uri->segment(3)."/transaksi/proses-transaksi" ?>">
  <table width="700" border="0" class="table_list">
    <tr>
      <td width="140">Tanggal Transaksi:</td>
      <td width="10">&nbsp;</td>
      <td width="536">
	  <input name="tanggaltransaksi" type="text" id="tanggaltransaksi" value="<?php echo $tanggaltransaksi ?>">
      <input name="mode" type="hidden" id="mode" value="<?php echo $mode ?>" />
      <input name="idtransaksi" type="hidden" id="idtransaksi" value="<?php echo $idtransaksi ?>" /></td>
    </tr>
    <tr>
      <td>Jenis Akun</td>
      <td>&nbsp;</td>
      <td>
	  
	  <select name="idakun" id="idakun">
      <option value="">[-pilih dahulu-]</option>
      <?php
	  foreach($akun as $list){
	  ?>
      <option value="<?php echo $list['idakun'] ?>" <?php echo ($list['idakun']==$idakun)?'selected':'' ?>><?php echo $list['kodejenisakun'].'-'.$list['kodeakun'].' '.$list['namaakun'] ?> </option>
      <?php } ?>
      </select></td>
    </tr>
    <tr>
      <td>No. Bukti Transaksi</td>
      <td>&nbsp;</td>
      <td><input type="text" name="nobukti" id="nobukti" value="<?php echo $nobukti ?>"></td>
    </tr>
    <tr>
      <td>Uraian</td>
      <td>&nbsp;</td>
      <td>
	  <?php //if($mode=='input'){?>
	  <textarea name="uraian" id="uraian" cols="45" rows="5"><?php echo $uraian ?></textarea>
	  <?php //}else{ echo $uraian; ?>
	  <!--<input name="uraian" type="hidden" id="uraian" value="<?php echo $uraian ?>" />-->
	  <?php //} ?>
	  </td>
    </tr>
    <tr>
      <td>Jumlah Rp.</td>
      <td>&nbsp;</td>
      <td>
	  <?php if($mode=='input'){?>
	  <input type="text" name="besaran" id="besaran" value="<?php echo $besaran ?>">
	  <?php }else{ echo $besaran; ?>
	  <input name="besaran" type="hidden" id="besaran" value="<?php echo $besaran ?>" />
	  <?php } ?>
	  </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="submit" name="button" id="button" value="Submit"></td>
    </tr>
  </table>
</form>
</div>
<link rel="stylesheet" media="screen" type="text/css" href="<?php echo base_url(); ?>javascripts/date-picker/jquery-ui.css" />
<script type="text/javascript" src="<?php echo base_url()?>javascripts/date-picker/jquery-ui.js"></script>
<script type="text/javascript">
	$( "#tanggaltransaksi" ).datepicker({dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true});
</script>
