<ul id="head-menu">
<li><a href="<?php echo base_url()?>owner/kost/<?php echo $this->uri->segment(3)?>">Room List</a></li>
<?php
		$userkategori= $this->session->userdata('ses_owner_level');
		$kost_id=$this->uri->segment(3);

		if($this->config->item('kost_id')!='')
		{
			if(in_array($kost_id,$this->config->item('kost_id'))){
		?>
<li><a href="#">Financial Report</a>
	<ul>
    <?php
    if(strtolower($userkategori)=='owner'){
	?>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/belanja/tampil-belanja'?>">Pengajuan Belanja</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/transaksi/tampil-transaksi'?>">Transaksi</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/laporan/rugi-laba'?>">Rugi Laba</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/laporan/neraca'?>">Neraca</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/freeze'?>">Freeze</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/unfreeze'?>">Un Freeze</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/jenisakun/tampil-jenisakun' ?>">Jenis Akun Akuntansi</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/akun/tampil-akun' ?>">Akun Akuntansi</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/setting'?>">Setting Akun</a></li>
    <?php }elseif(strtolower($userkategori)=='investor') { ?>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/transaksi/tampil-transaksi'?>">Transaksi</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/laporan/rugi-laba'?>">Rugi Laba</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/laporan/neraca'?>">Neraca</a></li>
    <?php }elseif(strtolower($userkategori)=='guard'){ ?>
	<li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/jenisakun/tampil-jenisakun' ?>">Jenis Akun Akuntansi</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/akun/tampil-akun' ?>">Akun Akuntansi</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/setting'?>">Setting Akun</a></li>
	<li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/belanja'?>">Pengajuan Belanja</a></li>
    <!--<li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/sewakamar/cari-member'?>">Sewa Kamar</a></li>-->
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/pengeluaran'?>">Laporan Pengeluaran</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/pendapatanlain'?>">Pendapatan Lain</a></li>
    <li><a href="<?php echo base_url().'owner/accounting/'.$kost_id.'/transaksi/tampil-transaksi'?>">Transaksi</a></li>
    <?php } ?>
    </ul>
</li>
<?php }} ?>
<div class="clr"></div>
</ul>