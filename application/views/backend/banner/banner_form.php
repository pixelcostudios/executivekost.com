<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<div id="navigation">
<ul>
	<li><a href="<?php echo $data_link?>">List Data</a></li>
    <li class="active"><a href="<?php echo $form_link?>">Form</a></li>
</ul>
<div class="clr"></div>
</div>
<?php
echo form_open_multipart("".$action."");
?>
<div style="width: 515px; float: left; margin-right: 10px;">
<?php
echo '<p><label>Title</label>'.form_input('title',set_value('title', isset($title) ? $title : ''),'onchange="check_title(\''.base_url()."backend/check_title/".$this->session->userdata("check_title").'\',this.value)"').'</p>';
echo form_error('title', '<p class="error">', '</p>');
echo '<p id="response_title" class="error" style="display: none;"></p>';
echo '<p><label>Url</label>'.form_input('url',set_value('url', isset($url) ? $url : '')).'</p>';
echo form_error('url', '<p class="error">', '</p>');
echo '<p><label>Order</label>'.form_input('sequence',set_value('sequence', isset($sequence) ? $sequence : ''),'style="width: 30px;"').'</p>';
echo '<br />';
if($this->session->userdata('form_mode')=='edit'){
	echo '<p><label>Status</label>';
	echo '<font size="-1">publish</font> '.form_radio('publish', '1', set_radio('publish', '1',isset($banner_status) && $banner_status == '1' ? TRUE : FALSE)).' ';
	echo '<font size="-1">unpublish</font> '.form_radio('publish', '0', set_radio('publish', '0', isset($banner_status) && $banner_status == '0' ? TRUE : FALSE));
	echo '</p>';
}
echo '<p>'.form_submit('','Submit','class="button"').'</p>';
?>
</div>
<div style="width: 230px; float: left;">
<?php
if($this->session->userdata("form_mode")=='edit'){
	$old_img = $this->session->userdata("old_img");
	$c = explode(".",$old_img);
	if($c[0]!='swf'){
		echo '<p><img src="'.base_url().'uploads/banners/'.thumb($old_img).'" /></p>';
	}
	else {
		echo "<p><object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" 
		codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0\" width=\"200\">
		<param name=\"movie\" value=\"".base_url()."uploads/banners/".$old_img."\" />
		<param name=\"quality\" value=\"high\" />
		<embed src=\"".base_url()."uploads/banners/".$old_img."\" quality=\"high\" 
		pluginspage=\"http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash\"	type=\"application/x-shockwave-flash\" width=\"200\">
		</embed>
		</object></p>";
	}
}
echo '<p><label>Image</label>'.form_upload('image').'</p>';
echo ! empty($image_error_message) ? '<p class="error">' . $image_error_message . '</p>': '';
?>
</div>
<div class="clr"></div>
</div>
</div>