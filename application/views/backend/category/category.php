<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<div id="navigation">
<ul>
	<li class="active"><a href="<?php echo $data_link?>">List Data</a></li>
    <li><a href="<?php echo $form_link?>">Form</a></li>
</ul>
<div class="clr"></div>
</div>
<?php
echo form_open(''.$set_action.'');
echo form_hidden('uri',$this->uri->segment(3));
echo "<select name=\"category_group\" class=\"select\">";
echo "<option value=\"0\">Uncategorized</option>";
foreach($list_kategori as $a){
	echo "<option value=\"".$a->category_id."\">".$a->category_title." (".$a->category_id.")</option>";
}
echo "</select> ";
echo form_submit('check','group','class="button" style="margin-right:5px;"');
echo form_submit('check','active','class="button" style="margin-right:5px;"');
echo form_submit('check','deactive','class="button"');

echo ! empty($pagination) ? "<div class=\"page_num\">" . $pagination . '</div>' : '';
echo ! empty($table) ? $table : '';
echo ! empty($pagination) ? "<div class=\"page_num\">" . $pagination . '</div>' : '';
?>
</div>
</div>