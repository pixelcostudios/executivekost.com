<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<div id="navigation">
<ul>
	<li><a href="<?php echo $data_link?>">List Data</a></li>
    <li class="active"><a href="<?php echo $form_link?>">Form</a></li>
</ul>
<div class="clr"></div>
</div>
<?php
echo form_open_multipart(''.$action.'');
?>
<div style="width:515px; float:left; margin-right: 10px;">
<?php
echo '<p><label class="lang-id">Title</label>'.form_input('title',set_value('title', isset($title) ? $title : ''),'onchange="check_title(\''.base_url()."backend/check_title/".$this->session->userdata("check_title").'\',this.value)"').'</p>';
echo form_error('title', '<p class="error">', '</p>');
echo '<p id="response_title" class="error" style="display: none;"></p>';
echo '<p><label class="lang-en">Title</label>'.form_input('title_en',set_value('title_en', isset($title_en) ? $title_en : '')).'</p>';
echo form_error('title_en', '<p class="error">', '</p>');
$des = array('name'=>'des','value'=>set_value('des',isset($des) ? $des : ''),'cols'=>'54','rows'=>'5');
echo '<p><label class="lang-id">Description</label>'.form_textarea($des).'</p>';
$des_en = array('name'=>'des_en','value'=>set_value('des_en',isset($des_en) ? $des_en : ''),'cols'=>'54','rows'=>'5');
echo '<p><label class="lang-en">Description</label>'.form_textarea($des_en).'</p>';
echo '<p>'.form_submit('','Submit','class="button"').'</p>';
?>
</div>
<div style="width:230px; float:left;">
<?php
echo '<p><label>Type</label><select name="type" style="padding:4px;">
<option value="1" '.set_select('type', '1', isset($type) && $type == '1' ? TRUE : FALSE).'>Post</option>
<option value="2" '.set_select('type', '2', isset($type) && $type == '2' ? TRUE : FALSE).'>Gallery</option>';
echo '</select></p>';
echo '<p><label>Parent Category</label><select name="category" style="padding:4px;" class="fieldtitle hover">
<option value="0">Parent Category</option>';
if(count($kategori)>0){
    foreach($kategori as $k){
    echo '<option value="'.$k->category_id.'" '.set_select('category',$k->category_id, isset($category_group) && $category_group == $k->category_id ? TRUE : FALSE).'>'.$k->category_title.'</option>';
    }
}
echo '</select></p>';
if(!empty($image)){
echo '<p><label>Image</label></p>
<div style="width:150px; height:100px; margin:0 5px 10px; float:left; position:relative;">
<div style="width:150px; height:100px; overflow:hidden;">
<img src="'.base_url().'uploads/category/'.$image.'" width="150" />
<a href="'.$url.'delete_image/'.$this->session->userdata('category_id').'">      
<img src="'.base_url().'stylesheets/backend/remove.png" style="position:absolute; bottom:-5px; right:-5px;" /></a>
</div></div>';
}
echo '<p>'.form_upload('image').'</p>';
echo form_hidden('oldimg',set_value('oldimg', isset($oldimg) ? $oldimg : ''));
echo ! empty($image_error_message) ? '<p class="error">' . $image_error_message . '</p>': '';
?>
</div>
<div class="clr"></div>
</div>
</div>