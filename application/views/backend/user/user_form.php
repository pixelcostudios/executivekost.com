<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<div id="navigation">
<ul>
	<li><a href="<?php echo $data_link?>">List Data</a></li>
    <li class="active"><a href="<?php echo $form_link?>">Form</a></li>
</ul>
<div class="clr"></div>
</div>
<?php
echo form_open_multipart(''.$action.'');
echo form_hidden('user_login_id',''.set_value('user_login_id', isset($user_login_id) ? $user_login_id : $this->uri->segment(4)).'');
?>
<div style="width: 49%; float:left; margin-right: 2%">
<?php
echo '<p><label>:: Nick Name & Username</label></p>';
echo '<p><label>Nick Name</label>'.form_input('nickname',set_value('nickname', isset($nickname) ? $nickname : ''),'style="width: 200px"').'</p>';
echo '<p><label>Username</label>'.form_input('username',set_value('username', isset($username) ? $username : ''),'onchange="check_title(\''.base_url()."backend/check_title/".$this->session->userdata("check_title").'\',this.value)" style="width: 200px"').'</p>'.form_error('username', '<p class="error">', '</p>');
echo '<p id="response_title" class="error" style="display: none;"></p>';
echo '<p><label>:: Email</label></p>';
echo '<p><label>Email</label>'.form_input('usermail',set_value('usermail', isset($usermail) ? $usermail : ''),'style="width: 200px"').'</p>'.form_error('usermail', '<p class="error">', '</p>');
?>
</div>
<div style="width: 49%; float:left;">
<?php
echo '<p><label>:: Password</label></p>';
if($this->session->userdata('form_mode')=='edit' AND $user_login_id == $this->session->userdata('ses_admin_id')){
echo '<p><label>Enter your present password to continue:</label>'.form_password('old_password',''.set_value('old_password').'','style="width: 200px"').'</p>';
echo form_error('old_password', '<p class="error">', '</p>').'<br />';
}
echo '<p><label>New Password</label>'.form_password('new_password',''.set_value('new_password').'','style="width: 200px"').'</p>';
echo form_error('new_password', '<p class="error">', '</p>');
echo '<p><label>Confirm Password</label>'.form_password('conf_password',''.set_value('conf_password').'','style="width: 200px"').'</p>';
echo form_error('conf_password', '<p class="error">', '</p>');
?>
</div>
<div class="clr"></div>
<?php
if($user_login_id != $this->session->userdata('ses_admin_id')){
	echo '<p><label>:: User Type</label><select name="type" style="padding:4px;">';
	echo '<option value="owner" '.set_select('type', 'owner', isset($type) && $type == 'owner' ? TRUE : FALSE).'>Owner</option>';
	echo '<option value="investor" '.set_select('type', 'investor', isset($type) && $type == 'investor' ? TRUE : FALSE).'>Investor</option>';
	echo '<option value="guard" '.set_select('type', 'guard', isset($type) && $type == 'guard' ? TRUE : FALSE).'>Guard</option>';
	echo '</select></p>';
	echo '<p><label>:: Kost List</label></p>';
	if($this->session->userdata('form_mode')=='edit'){
		echo '<ul>';
		foreach($kosts as $a){
			$check = (array_search($a->kost_id, $owner_kost) === FALSE)? '' : 'checked';
			echo '<li><input type="checkbox" value="'.$a->kost_id.'" name="kost[]" '.$check.' /> '.$a->kost_title.'</li>';
		}
		echo '</ul>';
	}
	else {
		echo '<ul>';
		foreach($kosts as $a){
			echo '<li><input type="checkbox" value="'.$a->kost_id.'" name="kost[]" /> '.$a->kost_title.'</li>';
		}
		echo '</ul>';
	}
}
echo '<br /><p>'.form_submit('','Submit','class="button"').'</p>';
?>
<div class="clr"></div>
</div>
</div>