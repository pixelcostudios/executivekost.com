<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<div id="navigation">
<ul>
	<li><a href="<?php echo $data_link?>">List Data</a></li>
    <li class="active"><a href="<?php echo $form_link?>">Form</a></li>
</ul>
<div class="clr"></div>
</div>
<?php
echo form_open_multipart($action);
echo '<p><label>Nama Daerah</label>'.form_input('title',set_value('title', isset($title) ? $title : '')).'</p>';
echo form_error('title', '<p class="error">', '</p>');
echo '<p>'.form_submit('','Submit','class="button"').'</p>';
?>
</div>
</div>