<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<?php
echo form_open_multipart(''.$action.'');
?>
<div id="tabs_container">
<ul id="tabs">
    <li class="active"><a href="#tab1">ID</a></li>
    <li><a href="#tab2">EN</a></li>
</ul>
</div>
<div id="tabs_content_container">
    <div id="tab1" class="tab_content" style="display: block;">
    <p><label class="lang-id">Title</label><?php echo form_input('title',''.set_value('title', isset($default['title']) ? $default['title'] : '').'')?></p>
	<?php echo form_error('title', '<p class="error">', '</p>')?>
    <?php $des = array('name'=>'des','value'=>''.set_value('des',isset($default['des']) ? $default['des'] : '').'','class'=>'area','style'=>'height: 300px;')?>
    <p><label class="lang-id">Description</label><?php echo form_textarea($des)?></p>
    </div>
    <div id="tab2" class="tab_content">
    <p><label class="lang-en">Title</label><?php echo form_input('title_en',''.set_value('title_en', isset($default['title_en']) ? $default['title_en'] : '').'')?></p>
	<?php echo form_error('title_en', '<p class="error">', '</p>')?>
    <?php $des_en = array('name'=>'des_en','value'=>''.set_value('des_en',isset($default['des_en']) ? $default['des_en'] : '').'','class'=>'area','style'=>'height: 300px;')?>
    <p><label class="lang-en">Description</label><?php echo form_textarea($des_en)?></p>
    </div>
</div>
<script type="text/javascript" language="javascript">
var upload_number = <?php echo count($images)+1 ?>;
function addFileInput() {
	if(upload_number > 5) {
		alert('sorry you can only upload 5 files')
		exit(0);
	}
	var d = document.createElement("div");
	var a = document.createElement("a");
	var file = document.createElement("input");
	var file2 = document.createElement("input");
	var img = document.createElement("img");
	d.setAttribute("id", "f"+upload_number);
	d.setAttribute("style", "clear: both;");
	d.appendChild(file);
	d.appendChild(file2);
	d.appendChild(a);
	a.setAttribute("href", "javascript:removeFileInput('f"+upload_number+"');");
	a.appendChild(img);
	file.setAttribute("type", "file");
	file.setAttribute("name", "image"+upload_number);
	file.setAttribute("style", "margin: 4px 0; float: left;");
	file2.setAttribute("type", "text");
	file2.setAttribute("style", "float: left;");
	file2.setAttribute("name", "caption"+upload_number);
	file2.setAttribute("placeholder", "caption "+upload_number+" . . . .");
	img.setAttribute("src", "<?php echo base_url()?>stylesheets/backend/remove.png");
	img.setAttribute("style", "margin: 4px 0 4px 5px;");
	document.getElementById("moreUploads").appendChild(d);
	upload_number++;
}

function removeFileInput(i) {
	var elm = document.getElementById(i);
	document.getElementById("moreUploads").removeChild(elm);
	upload_number = upload_number -1; // decrement the max file upload counter if the file is removed 
}
</script>
<?php
echo br(1);
if($this->session->userdata('form_mode')=='edit'){
	foreach($images as $a){
		echo '<div class="image-frame"><div class="image">';
		echo '<img src="'.base_url().$a->dir.'/'.thumb($a->image).'" class="image-img" width="145" alt="'.$a->caption.'" title="'.$a->caption.'" />';
		echo '</div>';
		echo anchor(base_url().'backend/page/delete_image/'.$a->image_id.'/'.$a->parent_id.'',img(array('src'=>base_url().'stylesheets/backend/remove.png')));
		echo '</div>';
	}
}
?>
<div class="clr">&nbsp;</div>
<p><label class="left">Images</label><a onclick="addFileInput();" style="cursor: pointer;"><img src="<?php echo base_url()?>stylesheets/backend/plus.png" /></a></p>
<div id="moreUploads"></div>
<p class="error">filetype allowed : .gif .jpg .jpeg .png</p>
<?php
if($this->session->userdata('form_mode')=='edit'){
	echo '<p><label>Status</label><font size="-1">publish</font> '.form_radio('page_status', '1', set_radio('page_status', '1', isset($default['page_status']) && $default['page_status'] == '1' ? TRUE : FALSE)).' <font size="-1">unpublish</font> '.form_radio('page_status', '0', set_radio('page_status', '0', isset($default['page_status']) && $default['page_status'] == '0' ? TRUE : FALSE)).'</p>';
}
echo '<p>'.form_submit('','Submit','class="button"').'</p>';
?>
</div>