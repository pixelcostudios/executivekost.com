<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width">
<?php $options = $this->site_model->get_data('',"tb_options","option_id = '1'")->row(); ?>
<title><?php echo $options->title ?> Control Panel Administrator</title>
<link type="image/x-icon" rel="shortcut icon" href="<?php echo base_url();?>css/backend/favicon.ico" />
<link href="<?php echo base_url();?>stylesheets/backend.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();?>javascripts/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>javascripts/backend-app.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>javascripts/redactor/redactor.js"></script>	
<link rel="stylesheet" href="<?php echo base_url();?>javascripts/redactor/redactor.css" />
</head>

<body>
<div id="header-bg">
<div id="header">
    <div id="header-left">
    <h1><?php echo $options->title ?> Administrator</h1>
    </div>
    <div id="header-right">
    <ul>
    <li><img src="<?php echo base_url();?>stylesheets/backend/avatar.jpg" /></li>
    <li><a href="#">Welcome,<br /><?php echo $this->session->userdata('admin_username'); ?></a>
        <ul>
        <li><a href="<?php echo base_url();?>" target="_blank">Visit Site</a></li>
        <li><a href="<?php echo base_url(); ?>backend/user/edit/<?php echo $this->session->userdata('ses_admin_id'); ?>">Account Setting</a></li>
        <li><a href="<?php echo base_url(); ?>backend/user">Manage User</a></li>
        <li><a href="<?php echo base_url();?>backend/logout" onclick="return confirm('Are you sure you want to log out ?')">Log out</a></li>
        </ul>
    </li>
    </ul>
    </div>
    <div id="header-middle">        
    <ul>
        <?php $this->load->view('backend/nav_top')?>
    	<div class="clr"></div>
    </ul>
    </div>
<div class="clr"></div>
</div>
</div>

<div id="container">
    <div id="sidebar">
        <?php $this->load->view('backend/nav_left')?>
        <div class="clr"></div>
    </div>
    <div id="content">
        <?php $this->load->view($content)?>
    </div>
<div class="clr"></div>
</div>

<div id="footer">copyright &copy; 2013 <b><?php echo $options->title ?></b></div>
</body>
</html>