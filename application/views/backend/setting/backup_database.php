<div id="head">
<?php
echo ! empty($h1_title) ? '<h1>' . $h1_title . '</h1>': '';
echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
$flashmessage = $this->session->flashdata('message');
echo ! empty($flashmessage) ? '<p class="message fadeout">' . $flashmessage . '</p>': '';
?>
</div>
<div id="data">
<div id="data-content">
<?php
echo form_open(''.$action.'');
echo ! empty($table) ? $table : '';
echo br(1).'<p>'.form_submit('','Submit','class="button"').'</p>';
?>
</div>
</div>