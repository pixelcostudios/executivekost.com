<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	/* ---------------------
	   USER
	--------------------- */
	// check login
	function check_login($username,$password){
		$query = $this->db->get_where('tb_users', array('user_login'=>$username,'user_pass'=>md5($password)), 1, 0);
		if($query->num_rows()>0){
			$row = $query->row();		
			return $row->user_level;
		}
		else{
			return '';
		}
	}	
	// check password
	function check_password($id,$password){
		$query = $this->db->get_where('tb_users', array('user_login_id' => $id,'user_pass'=>md5($password)), 1, 0);
		if($query->num_rows()>0)
			return TRUE;
		else
			return FALSE;
	}
	/* ---------------------
	   MEMBER
	--------------------- */
	// check login member
	function check_login_member($username,$password){
		$query = $this->db->get_where('tb_members', array('member_username'=>$username,'member_password'=>md5($password)), 1, 0);
		if($query->num_rows()>0)
			return TRUE;
		else
			return FALSE;
	}
	// check login member
	function check_password_member($id,$password){
		$query = $this->db->get_where('tb_members', array('member_id'=>$id,'member_password'=>md5($password)), 1, 0);
		if($query->num_rows()>0)
			return TRUE;
		else
			return FALSE;
	}
	/* ---------------------
	   DEFAULT
	--------------------- */
	// get data
	function get_data($select="*",$table,$where="",$group="",$order="",$limit=""){
		$a = explode(",",$limit);
		if(!empty($a[0])) $db_limit = $a[0];
		if(!empty($a[1])) $db_offset = $a[1]; else $db_offset = '0';
		$this->db->select($select);
		$this->db->from($table);
		if(!empty($where))
			$this->db->where($where);
		if(!empty($group))
			$this->db->group_by($group);
		if(!empty($order))
			$this->db->order_by($order);
		if(!empty($limit))
			$this->db->limit($db_limit,$db_offset);
		return $this->db->get();
	}
	// get image
	function get_img($where="",$limit=""){
		$a = explode(",",$limit);
		if(!empty($a[0])) $db_limit = $a[0];
		if(!empty($a[1])) $db_offset = $a[1]; else $db_offset = '0';
		$this->db->from("tb_images");
		if(!empty($where))
			$this->db->where($where);
		$this->db->order_by("image_id ASC");
		if(!empty($limit))
			$this->db->limit($db_limit,$db_offset);
		return $this->db->get();
	}
	// check data
	function check_data($title,$table,$title_check){
		$sql = "select ".$title." from ".$table." where ".$title." = '".$title_check."'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
			return FALSE;
		else
			return TRUE;
	}	
	// input data
	function input_data($tabel,$data){
		$str = $this->db->insert_string($tabel, $data); 
		$query = $this->db->query($str);
		return $query;
	}
	// update data
	function update_data($tabel,$data,$where){
		$str = $this->db->update_string($tabel, $data, $where); 
		$query = $this->db->query($str);
		return $query;
	}
	// delete data
	function del_data($table,$data){
		$this->db->delete($table,$data); 
	}
	/* ---------------------
	   OTHER FUNCTION
	--------------------- */
	// related
	function related($table,$title,$slug,$category_id){
		$katasambung = array('ini','itu','sudah','ada','dan', 'maka', 'sedang', 'hingga', 'meski', 'lalu', 'bila', 'sambil', 'atau', 'serta', 'karena', 'jika','apabila', 'lagi pula', 'karena itu', 'andaikata', 'sebab itu','lagi','dengan','tetapi', 'hanya', 'sedangkan', 'biar', 'meski','ke','di','bikin');	
		$a = explode("-",$slug);
		$b = str_replace($katasambung,"",$a);
		
		$sql = "select * ";
		$sql.= "from ".$table.",tb_category where ".$table.".".$title." != '".$slug."' and ( ";
		
		$i = 0;
		foreach($b as $c){
			if($i==0 && $c!='')
				$sql.= "".$table.".".$title." like '%$c%' ";
			elseif($i>0 && $c!='')
				$sql.= " or ".$table.".".$title." like '%$c%'";
			$i++;	
		}
		
		$sql.= ") and ".$table.".category_id = tb_category.category_id and ".$table.".category_id = '".$category_id."'";		
		return $this->db->query($sql);	
	}
	// search
	function search($table,$title,$search,$limit=""){
		$a = explode(" ",$search);
		$sql = "select * ";
		$sql.= "from ".$table." where ";
		
		$i = 0;
		foreach($a as $b){
			if($i==0 && $b!='')
				$sql.= "".$table.".".$title." like '%$b%' ";
			elseif($i>0 && $b!='')
				$sql.= " or ".$table.".".$title." like '%$b%'";
			$i++;	
		}
		
		//$sql.= "and ".$table.".category_id = tb_category.category_id and ".$table.".category_id = '".$category_id."' order by ".$title." asc ";	
		$sql.= "order by ".$title." asc ";	
		if(!empty($limit))
			$sql.= "limit $limit";
		return $this->db->query($sql);	
	}
}