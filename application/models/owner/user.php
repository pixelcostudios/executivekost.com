<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Model{
	var $url;
	var $content;
	function __construct(){
		parent::__construct();
		$this->url = base_url().'owner/user/';
	}
	// manage user
	/*function index(){
		$this->content['h1_title']		= "Manage User";
		$this->content['content'] 		= "backend/default";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url.'add';
		
		$uri = 4;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
				
		$posts = $this->site_model->get_data('',"tb_users","user_level IN ('investor','guard')",'',"user_name ASC","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_users","user_level IN ('investor','guard')")->num_rows();
		
		if($num_rows > 0){
			// Set heading
			$this->table->set_empty("&nbsp;");
			$no 		= array('data'=>'No','width'=>'40');
			$actions 	= array('data'=>'Actions','width'=>'80');
			$this->table->set_heading($no,'User','Status','Email','Username','Password',$actions);
			
			$i = 0 + $offset;
			foreach($posts as $p){
				$owner_kost = $this->site_model->get_data('',"tb_user_kost uk,tb_kosts k","uk.kost_id = k.kost_id AND user_id = '".$p->user_id."'")->result();
				$data = array();
				foreach($owner_kost as $o){
					$data[] = $o->kost_title;					
				}
				if(count($owner_kost)) $kost_title = br(1).'" '.implode(", ",$data).' "'; else $kost_title = "";
				$this->table->add_row(
					++$i,$p->user_name,$p->user_level.' of'.$kost_title,$p->user_email,$p->user_login,$p->user_b,
					anchor($this->url.'edit/'.$p->user_login_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor($this->url.'delete/'.$p->user_login_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"))
				);
			}
			
			$config['base_url'] 			= $this->url."manage/";
			$config['total_rows']		 	= $num_rows;
			$config['per_page']		 		= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination'] 	= $this->pagination->create_links();
			$this->content['table'] 		= $this->table->generate();
		}
		else{
			$this->content['message'] = "Empty Data!";
		}		
		$this->load->view('owner/template',$this->content);
	}
	// add user
	function add(){
		$this->content['h1_title'] 		= "Manage User";
		$this->content['content'] 		= 'owner/user/user_form';
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url.'add';
		$this->content['action'] 		= $this->url.'input';
		$this->content['user_login_id'] = '';
		$this->session->set_userdata('form_mode', 'add');
		$this->session->set_userdata('check_title', "user_login/tb_users/");
		$this->session->set_userdata('check_email', "user_email/tb_users/");
		
		$this->load->view('owner/template',$this->content);		
	}*/
	// edit user
	function edit(){
		$this->content['h1_title'] 		= 'Manage Profile';
		$this->content['content'] 		= 'owner/user/user_form';
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['action'] 		= $this->url.'input';
		$this->session->set_userdata('form_mode', 'edit');
		
		$user = $this->site_model->get_data('',"tb_users","user_login_id = '".$this->uri->segment(4)."'")->row();
		$this->session->set_userdata('check_title', "user_login/tb_users/".$user->user_login);
		$this->session->set_userdata('check_email', "user_email/tb_users/".$user->user_email);
		$this->content['user_login_id'] = $user->user_login_id;
		$this->content['nickname'] 		= $user->user_name;
		$this->content['username'] 		= $user->user_login;
		$this->content['usermail'] 		= $user->user_email;
		$this->content['type'] 			= $user->user_level;
		
		$this->load->view('owner/template',$this->content);		
	}
	// update user
	function input(){
		$this->content['h1_title'] 		= "Manage Profile";
		$this->content['content']		= "owner/user/user_form";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['action'] 		= $this->url."input";
		$this->content['kosts'] 		= $this->site_model->get_data('',"tb_kosts")->result();	
		
		$this->form_validation->set_rules('user_login_id', 'user_login_id', '');
		$this->form_validation->set_rules('nickname', 'nickname', 'required');
		$this->form_validation->set_rules('username', 'username', 'required|callback_check_title['.$this->session->userdata("check_title").']');
		if($this->input->post('usermail') != '')
			$this->form_validation->set_rules('usermail', 'usermail', 'valid_email|callback_check_title['.$this->session->userdata("check_email").']');
		if($this->input->post("new_password")!==''){
			$this->form_validation->set_rules('old_password', 'old password', 'min_length[6]|callback_check_password['.$this->input->post('user_login_id').']');
			$this->form_validation->set_rules('new_password', 'new password', 'min_length[6]');
			$this->form_validation->set_rules('conf_password', 'confirm password', 'required|min_length[6]|callback_check_confirm_password['.$this->input->post("new_password").']');
		}
		$this->form_validation->set_rules('type', 'user type', '');		
		
		$this->content['user_login_id'] = $this->input->post('user_login_id');
		$user_login_id 	= $this->input->post('user_login_id');
		$nickname 		= $this->input->post("nickname");
		$username 		= $this->input->post("username");
		$usermail 		= $this->input->post("usermail");
		$old_password 	= $this->input->post("old_password");
		$new_password 	= $this->input->post("new_password");
		$conf_password 	= $this->input->post("conf_password");
		$password 		= md5($new_password);
		
		if($this->form_validation->run() == TRUE){
			if(!empty($new_password)){
				// data for tb_users
				$data = array(
					'user_login'	=> $username,
					'user_pass'		=> $password,
					'user_b'		=> $new_password,
					'user_name'		=> $nickname,
					'user_email'	=> $usermail);
			}
			else{
				// data for tb_users
				$data = array(
					'user_login'	=> $username,
					'user_name'		=> $nickname,
					'user_email'	=> $usermail);
			}
			$this->site_model->update_data("tb_users",$data,"user_login_id = '".$user_login_id."'");
			// message and redirect
			$this->session->set_flashdata('message', 'Update Success ...');
			redirect($this->url.'edit/'.$user_login_id);
		}
		else{
			$this->load->view('owner/template',$this->content);
		}
	}
}