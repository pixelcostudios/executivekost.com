<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kost extends CI_Model{
	var $url;
	var $content;
	function __construct(){
		parent::__construct();
		$this->url = base_url()."owner/kost/";
	}
	
	// index
	function index(){
		$id = $this->uri->segment(3);
		$this->session->set_userdata('kost_id', $id);
		$this->content['h3_title']		= 'Room List';
		$this->content['content'] 		= 'owner/room/room';
		$this->content['action'] 		= $this->url.$id.'/add';
		
		$uri = 4;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$table = "tb_kost_room ";
		//$table.= "LEFT JOIN tb_rent_kost rk ON kr.kost_room_id = rk.kost_room_id ";
		//$table.= "LEFT JOIN tb_members m ON rk.member_id = m.member_id";
		$room = $this->site_model->get_data('',$table,"kost_id = '".$id."'",'',"kost_room_number ASC","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',$table,"kost_id = '".$id."'")->num_rows();
		
		if($num_rows > 0){
			// Set heading
			$this->table->set_empty("&nbsp;");
			$no 		= array('data'=>'No','width'=>'40');
			//$type 		= array('data'=>'Room Type','width'=>'100');
			//$status 	= array('data'=>'Status','width'=>'100');
			$actions 	= array('data'=>'Actions','width'=>'80');
			$this->table->set_heading($no,'Room Name','Member','Check In','Check Out','Price',$actions);
			
			foreach($room as $r){
				// member name
				$where = "rk.member_id = m.member_id AND rk.kost_room_id = '".$r->kost_room_id."' AND rent_status = '2'";
				$m = $this->site_model->get_data('',"tb_rent_kost rk,tb_members m",$where)->row();
				if(count($m)!='0' AND (strtotime($m->check_in) <= strtotime(date("Y-m-d")) AND strtotime($m->check_out) >= strtotime(date("Y-m-d")))){
					$member_name = anchor(base_url().'owner/check/'.$m->rent_kost_id,$m->member_name);
					$check_in = fdate($m->check_in,2);
					$check_out = fdate($m->check_out,2);
				}
				else{
					$member_name = ""; $check_in = ""; $check_out = "";
				}
				// actions
				if($this->session->userdata('ses_owner_level')!='investor'){
					$actions = anchor($this->url.$r->kost_id.'/edit/'.$r->kost_room_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor($this->url.$r->kost_id.'/delete/'.$r->kost_room_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"))	;
				}
				else{
					$actions = '';
				}
				if($r->kost_room_type == '1')
					$room_price = 'Rp. '.rupiah($r->kost_room_price_month).' /month';
				else
					$room_price = 'Rp. '.rupiah($r->kost_room_price_day).' /day';
				
				$this->table->add_row(
					$r->kost_room_number,
					$r->kost_room_title,
					$member_name,
					$check_in,
					$check_out,
					array('data'=>$room_price,'align'=>'center'),
					$actions);
			}
			
			$config['base_url'] 			= $this->url.$id;
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination'] 	= $this->pagination->create_links();
			$this->content['table'] 		= $this->table->generate();
		}
		else{
			$this->content['message'] = 'Empty Data!';
		}
		
		$this->load->view('owner/template',$this->content);
	}
	// add
	function add(){
		$this->content['h3_title'] 		= anchor($this->url.$this->session->userdata('kost_id'),'room list', array('class' => 'back')).' &raquo; add';
		$this->content['content'] 		= 'owner/room/room_form';
		$this->content['action'] 		= $this->url.'input';
		$this->content['images'] 		= array();
		$this->session->set_userdata('form_mode', 'add');
		
		$this->load->view('owner/template',$this->content);
	}
	// edit
	function edit(){
		$id = $this->uri->segment(5);
		$this->content['h3_title'] 		= anchor($this->url.$this->session->userdata('kost_id'),'room list', array('class' => 'back')).' &raquo; edit';
		$this->content['content'] 		= 'owner/room/room_form';
		$this->content['action'] 		= $this->url.'input';
		$this->session->set_userdata('form_mode', 'edit');
		
		$room = $this->site_model->get_data('',"tb_kost_room","kost_room_id = '$id'")->row();
		
		$this->session->set_userdata('kost_room_id', $room->kost_room_id);
		$this->content['kost_id'] 				= $room->kost_id;
		$this->content['title'] 				= $room->kost_room_title;
		$this->content['kost_room_number']		= $room->kost_room_number;
		$this->content['des'] 					= $room->kost_room_content;
		$this->content['des_en']				= $room->kost_room_content_en;
		$this->content['price_day']				= $room->kost_room_price_day;
		$this->content['price_month']			= $room->kost_room_price_month;
		$this->content['type']					= $room->kost_room_type;
		$this->content['images']				= $this->site_model->get_img("parent_id = '".$room->kost_room_id."' and relation = 'kost_room'","5")->result();
		
		$this->load->view('owner/template',$this->content);			
	}
	// input post
	function input(){
		$this->content['h3_title'] 		= anchor($this->url.$this->session->userdata('kost_id'),'room list', array('class' => 'back')).' &raquo; '.$this->session->userdata('form_mode');
		$this->content['content']		= 'owner/room/room_form';
		$this->content['action']		= $this->url.'input';
		$type_file 						= "gif|jpg|jpeg|png";
		
		$this->form_validation->set_rules('kost_room_number', 'room number', 'required');
		$this->form_validation->set_rules('title', 'title', '');
		$this->form_validation->set_rules('title_en', 'title en', '');
		$this->form_validation->set_rules('des', 'des', '');		
		$this->form_validation->set_rules('des_en', 'des_en', '');
		$this->form_validation->set_rules('price_day', 'price day', 'required');
		$this->form_validation->set_rules('price_month', 'price month', 'required');
		$this->form_validation->set_rules('type', 'type', '');
		
		create_dir('kosts');
		$dir = date("Y").'/'.date("m");
		
		if($this->session->userdata('form_mode')=='add'){	
			$this->content['images'] = array();
			if($this->form_validation->run() == TRUE){
				// data to database
				$data = array(
					'kost_id'				=> $this->session->userdata('kost_id'),
					'kost_room_number'		=> $this->input->post("kost_room_number"),
					'kost_room_title'		=> $this->input->post("title"),
					'kost_room_title_en'	=> $this->input->post("title_en"),
					'kost_room_content'		=> $this->input->post("des"),
					'kost_room_content_en'	=> $this->input->post("des_en"),
					'kost_room_price_day'	=> $this->input->post("price_day"),
					'kost_room_price_month'	=> $this->input->post("price_month"),
					'kost_room_type'		=> $this->input->post('type'));
				// save to database
				$this->site_model->input_data("tb_kost_room",$data);
				// upload image
				$data_id = $this->db->insert_id();
				$title = $this->site_model->get_data('',"tb_kosts","kost_id = '".$this->session->userdata('kost_id')."'")->row();
				$this->fm->multi_upload_img("uploads/kosts/".$dir,$type_file,$title->kost_title.'_room_'.$data_id,$data_id,"kost_room");		
				// message						
				$this->session->set_flashdata('message', '1 data has been saved!');
				redirect($this->url.$this->session->userdata('kost_id'));	
			}
			else {	
				$this->load->view('owner/template',$this->content);								
			}
		}
		elseif($this->session->userdata('form_mode')=='edit'){
			$id = $this->session->userdata('kost_room_id');
			$this->content['images'] = $this->site_model->get_img("parent_id = '".$id."' and relation = 'kost_room'","5")->result();
			if($this->form_validation->run() == TRUE){
				// data to database
				$data = array(
					'kost_id'				=> $this->session->userdata('kost_id'),
					'kost_room_number'		=> $this->input->post("kost_room_number"),
					'kost_room_title'		=> $this->input->post("title"),
					'kost_room_title_en'	=> $this->input->post("title_en"),
					'kost_room_content'		=> $this->input->post("des"),
					'kost_room_content_en'	=> $this->input->post("des_en"),
					'kost_room_price_day'	=> $this->input->post("price_day"),
					'kost_room_price_month'	=> $this->input->post("price_month"),
					'kost_room_type'		=> $this->input->post('type'));
				// save to database
				$this->site_model->update_data("tb_kost_room",$data,"kost_room_id = '".$id."'");
				$title = $this->site_model->get_data('',"tb_kosts","kost_id = '".$this->session->userdata('kost_id')."'")->row();
				$this->fm->multi_upload_img("uploads/kosts/".$dir,$type_file,$title->kost_title.'_room_'.$id,"$id","kost_room");
				// message					
				$this->session->set_flashdata('message', 'data ( room '.$this->input->post("room_number").' '.$title->kost_title.' / '.$this->input->post("title_en").' ) has been update!');
				$this->session->unset_userdata('kost_room_id');
				redirect($this->url.$this->session->userdata('kost_id').'/edit/'.$id);
			}
			else {	
				$this->load->view('owner/template',$this->content);								
			}
		}
	}
	// delete post
	function delete(){
		$id = $this->uri->segment(5);
		$images = $this->site_model->get_data('',"tb_images","parent_id = '".$id."' and relation = 'kost_room'")->result();
		foreach($images as $image){
			if(!empty($image->image)){
				$del	= "./".$image->dir.'/'.$image->image;
				$del2	= "./".$image->dir.'/'.thumb($image->image);
				if(file_exists($del)) unlink($del);
				if(file_exists($del2)) unlink($del2);
			}
			$this->site_model->del_data("tb_images",$data=array('image_id'=>$image->image_id));	
		}
		$this->site_model->del_data("tb_kost_room",$data=array('kost_room_id'=>$id));					
		$this->session->set_flashdata('message', 'data has been delete!');
		redirect($this->url.$this->session->userdata('kost_id'));		
	}
	// delete post image
	function delete_image(){
		$kost_id = $this->uri->segment(3);
		$id = $this->uri->segment(5);
		$kost_room_id = $this->uri->segment(6);
		$image = $this->site_model->get_img("image_id = '".$id."'")->row();
		$del	= "./".$image->dir.'/'.$image->image;
		$del2	= "./".$image->dir.'/'.thumb($image->image);
		if(file_exists($del)) unlink($del);
		if(file_exists($del2)) unlink($del2);
		$this->site_model->del_data("tb_images",$data=array('image_id'=>$id));
		redirect($this->url.$kost_id.'/edit/'.$kost_room_id);	
	}
}