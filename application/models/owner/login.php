<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Model{
	var $content;
	function __construct(){
		parent::__construct();
	}
	
	function index(){		
		if($this->session->userdata('owner_login')==TRUE){
			redirect(base_url().'owner/home');
		}
	}
	
	function process_login(){
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
		
		if($this->form_validation->run() == TRUE){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$user_level = $this->site_model->check_login($username,$password);
			
			if($user_level!=''){
				$user = $this->site_model->get_data('',"tb_users","user_login = '".$username."' AND user_level = '".$user_level."'")->row();
				$data = array('owner_id'=>$user->user_id,'ses_owner_id'=>$user->user_login_id,'owner_username'=>$user->user_name,'ses_owner_level'=>$user->user_level,'owner_login'=>TRUE);
				$this->session->set_userdata($data);
				redirect(base_url().'owner/home');
			}
			else{
				$this->session->set_flashdata('message', 'Invalid Username / Password');
				redirect(base_url().'login/owner');
			}
		}
		else{
			$this->load->view('owner/login');
		}
	}
}