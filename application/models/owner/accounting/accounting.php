<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class accounting extends CI_Model{
	
	function menu(){
		$ses_freeze=$this->session->userdata('ses_freeze');
		$ses_userlevel=$this->session->userdata('ses_owner_level');
		if($ses_freeze=='1' and $ses_userlevel=='guard'){
			$this->load->model('owner/accounting/freeze');
			$this->freeze->tampilfreezing();
		}else{
		
			$mode=$this->uri->segment(4);
			switch($mode){
				case 'belanja':$this->load->model('owner/accounting/permintaanbelanja','pb');
										  $mode2=$this->uri->segment(5);
										  switch($mode2){
											case 'delete-pengajuan-belanja':$this->pb->deletepengajuanbelanja();break;
											case 'cetak-pengajuan-belanja':$this->pb->cetakpengajuanbelanja();break;
											case 'input-jumlah-items':$this->pb->inputjumlahitem();break;
											case 'input-items':$this->pb->inputitem();break;
											case 'confirm-input-items':$this->pb->konfirminputitem();break;
											case 'proses-input-items':$this->pb->prosespengajuan();break;
											case 'guard-detail': $mode3=$this->uri->segment(6);
																  switch($mode3){
																	case 'delete-detail-pengajuan':$this->pb->deleteitemdetailpengajuan();break;
																	case 'input-detail-pengajuan':$this->pb->inputdetailpengajuan();break;
																	case 'proses-input-detail-items':$this->pb->prosesinputdetailpengajuan();break;
																	default:$this->pb->guarddetail();break;
																  }break;
											case 'owner-detail': $mode3=$this->uri->segment(6);
																  switch($mode3){
																	case 'delete-detail-pengajuan':$this->pb->deleteitemdetailpengajuan();break;
																	case 'proses-persetujuan':$this->pb->prosespersetujuan();break;
																	default:$this->pb->ownerdetail();break;
																  }break;
											default:
													$userlevel=$this->session->userdata('ses_owner_level');
													if($userlevel=='guard')
														$this->pb->tampilpengajuan();
													elseif($userlevel=='owner')
														$this->pb->tampilownerpengajuan();
														
													break;
										  }break;
				case 'transaksi':$this->load->model('owner/accounting/transaksi','tr');
								 $mode2=$this->uri->segment(5);
								 switch($mode2){
									 case 'input-transaksi':$this->tr->inputtransaksi();break;
									 case 'edit-transaksi':$this->tr->edittransaksi();break;
									 case 'proses-transaksi':$this->tr->prosestransaksi();break;
									 case 'delete-transaksi':$this->tr->hapustransaksi();break;
									 case 'cetak-transaksi':$this->tr->cetaktransaksi();break;
									 default :$this->tr->tampiltransaksi();break;
								 }break;
				case 'setting':$this->load->model('owner/accounting/settingpajak','sp');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input':$this->sp->input();break;
								 case 'edit':$this->sp->edit();break;
								 case 'proses':$this->sp->proses();break;
								 //case 'delete':$this->sp->hapus();break;
								 default :$this->sp->tampil();break;
							 }break;			  
				case 'jenisakun':$this->load->model('owner/accounting/jenisakun');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input-jenisakun':$this->jenisakun->inputjenisakun();break;
								 case 'edit-jenisakun':$this->jenisakun->editjenisakun();break;
								 case 'proses-jenisakun':$this->jenisakun->prosesjenisakun();break;
								 case 'delete-jenisakun':$this->jenisakun->hapusjenisakun();break;
								 default :$this->jenisakun->tampiljenisakun();break;
							 }break;			  
				case 'akun':$this->load->model('owner/accounting/akun');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input-akun':$this->akun->inputakun();break;
								 case 'edit-akun':$this->akun->editakun();break;
								 case 'proses-akun':$this->akun->prosesakun();break;
								 case 'delete-akun':$this->akun->hapusakun();break;
								 default :$this->akun->tampilakun();break;
							 }break;			  
				case 'sewakamar':$this->load->model('owner/accounting/sewakamar','sewa');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'carimember':$this->sewa->carimember();break;
								 case 'proses-carimember':$this->sewa->prosescarimember();break;
								 case 'input-sewakamar':$this->sewa->inputsewa();break;
								 case 'edit-sewakamar':$this->sewa->editsewa();break;
								 case 'proses':$this->sewa->prosessewa();break;
								 //case 'delete':$this->sewa->hapus();break;
								 default :$this->sewa->carimember();break;
							 }break;			  
				case 'transaksi':$this->load->model('owner/accounting/transaksi','tr');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input-transaksi':$this->tr->inputtransaksi();break;
								 case 'edit-transaksi':$this->tr->edittransaksi();break;
								 case 'delete-transaksi':$this->tr->hapustransaksi();break;
								 case 'proses-transaksi':$this->tr->prosestransaksi();break;
								 default:$this->tr->tampiltransaksi();
							 };break;
				case 'laporan':$this->load->model('owner/accounting/laporan','lp');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'rugi-laba':$this->lp->rugilaba();break;
								 case 'cetak-rugi-laba':$this->lp->cetakrugilaba();break;
								 case 'cetak-neraca':$this->lp->cetakneraca();break;
								 default:$this->lp->neraca();
							 };break;
				case 'pengeluaran':$this->load->model('owner/accounting/pengeluaran');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input':$this->pengeluaran->inputpengeluaran();break;
								 case 'edit':$this->pengeluaran->editpengeluaran();break;
								 case 'delete':$this->pengeluaran->deletepengeluaran();break;
								 case 'proses':$this->pengeluaran->prosespengeluaran();break;
								 default:$this->pengeluaran->tampilpengeluaran();
							 };break;
				case 'pendapatanlain':$this->load->model('owner/accounting/pendapatanlain');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input':$this->pendapatanlain->inputpendapatan();break;
								 case 'edit':$this->pendapatanlain->editpendapatan();break;
								 case 'proses':$this->pendapatanlain->prosespendapatan();break;
								 case 'delete':$this->pendapatanlain->deletependapatan();break;
								 default :$this->pendapatanlain->tampilpendapatan();break;
							 }break;
				case 'penyesuaian':$this->load->model('owner/accounting/penyesuaian');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input':$this->penyesuaian->input();break;
								 case 'edit':$this->penyesuaian->edit();break;
								 case 'proses':$this->penyesuaian->proses();break;
								 case 'delete':$this->penyesuaian->delete();break;
								 default :$this->penyesuaian->tampil();break;
							 }break;
				case 'diskon':$this->load->model('owner/accounting/diskon');
							$mode2=$this->uri->segment(5);
							 switch($mode2){
								 case 'input':$this->diskon->input();break;
								 //case 'edit':$this->penyesuaian->edit();break;
								 case 'proses':$this->diskon->proses();break;
								 //case 'delete':$this->penyesuaian->delete();break;
								 default :$this->diskon->tampil();break;
							 }break;
				case 'freeze':$this->load->model('owner/accounting/freeze');
							$this->freeze->freezing();
							break;
				case 'unfreeze':$this->load->model('owner/accounting/freeze');
							$this->freeze->unfreezing();
							break;
				case 'cetakinvoice':$this->load->model('owner/accounting/mfunction');
							$this->mfunction->cetakkwitansi($this->uri->segment(5));
							break;
			}
		}
	
	
	}
}
