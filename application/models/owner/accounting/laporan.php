<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class laporan extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
		$this->load->model('owner/accounting/mfunction','fungsi');
		$this->content['namakost']=$this->fungsi->getnamekost($this->kost_id);
    }
	
	function rugilaba(){
		$bulan=$this->input->post('bulan');
		$tahun=$this->input->post('tahun');
		
		$bulan=($bulan=='')?date('m'):$bulan;
		$tahun=($tahun=='')?date('Y'):$tahun;
		
		$query = $this->db->get('tb_settingakun');
		$a=$query->row_array();
		$idakunsewa=$a['idakunsewa'];
		$idakunpajak=$a['idakunpajak'];
		
		$query = $this->db->query('select min(year(tanggaltransaksi)) as minimum from tb_transaksi');
		$a=$query->row_array();
		$minimum=$a['minimum'];
		
		$query = $this->db->query('select max(year(tanggaltransaksi)) as maximum from tb_transaksi');
		$a=$query->row_array();
		$maximum=$a['maximum'];
		
		
		$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$this->db->where(array('tb_akun.poslaporan'=>'LB'));
		//$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id,'tb_akun.poslaporan'=>'LB'));
		$qr=$this->db->get('tb_akun');
		$h=$qr->result_array();
		
		
		$hasil=array();
		$namajenis='';
		$totdebit=0;
		$totkredit=0;
		$totpajak=0;
		$total=0;
		$totaljenis=0;
	
		foreach($h as $list){
			$idakun=$list['idakun'];
			$kodeakun=$list['kodejenisakun'].'-'.$list['kodeakun'];
			$namaakun=$list['namaakun'];
			$namajenisakun=$list['namajenisakun'];
			$possaldo=$list['possaldo'];
			
			if($namajenis!='' AND $namajenis!=$namajenisakun)
			{
				$a=array();
				$a['idakun']=$idakun;
				$a['kodeakun']=$kodeakun;
				$a['namaakun']=$namajenis;
				$a['debit']='';
				$a['kredit']=$totaljenis;
				$a['bold']=TRUE;
				$hasil[]=$a;
				$totaljenis=0;
			}
			$namajenis=$namajenisakun;
			
			if($idakunsewa==$idakun){
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakun.' and kost_id='. $this->kost_id .' and month(tanggaltransaksi)='.$bulan.' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();
				
				if($h2['jmlbesaran']=='')
					$besaran=0;
				else
					$besaran=$h2['jmlbesaran'];
					
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakunpajak.' and kost_id='. $this->kost_id .' and month(tanggaltransaksi)='.$bulan.' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();
				
				if($h2['jmlbesaran']=='')
					$besaranpajak=0;
				else
					$besaranpajak=$h2['jmlbesaran'];
					
				$jmlbesaran=$besaran+$besaranpajak;
				
			}else{
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakun.' and kost_id='. $this->kost_id .' and month(tanggaltransaksi)='.$bulan.' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();

				if($h2['jmlbesaran']=='')
					$jmlbesaran=0;
				else
					$jmlbesaran=$h2['jmlbesaran'];
					
			}
			
			
			if($possaldo=='DB'){
				$debit=$jmlbesaran;
				$kredit=0;
			}else{
				$debit=0;
				$kredit=$jmlbesaran;
			}
		  
			$totdebit+=$debit;
			$totkredit+=$kredit;
			
			$totaljenis+=$jmlbesaran;		
					
			$a=array();
			$a['idakun']=$idakun;
			$a['kodeakun']=$kodeakun;
			$a['namaakun']=$namaakun;
			$a['debit']=$jmlbesaran;
			$a['kredit']='';
			$a['bold']=FALSE;
			$hasil[]=$a;
			
			
		}
		
		if(count($h)>0){
			$a=array();
			$a['idakun']=$idakun;
			$a['kodeakun']=$kodeakun;
			$a['namaakun']=$namajenis;
			$a['debit']='';
			$a['kredit']=$totaljenis;
			$a['bold']=TRUE;
			$hasil[]=$a;
		}
		
		$total=$totdebit - $totkredit;
		$this->content['bulan']=$bulan;
		$this->content['tahun']=$tahun;
		$this->content['minimum']=$minimum;
		$this->content['maximum']=$maximum;
		$this->content['total']=$total;
		$this->content['kost_title']=$this->fungsi->getnamekost($this->kost_id);
		$this->content['rugilaba']=$hasil;
		$this->content['content']='owner/accounting/laporan/labarugi';
		$this->load->view('owner/template',$this->content);
		
	}
	

	function neraca(){
		$tahun=$this->input->post('tahun');
		$tahun=($tahun=='')?date('Y'):$tahun;
		
		$query = $this->db->get('tb_settingakun');
		$a=$query->row_array();
		$idakunsewa=$a['idakunsewa'];
		$idakunpajak=$a['idakunpajak'];
		
		$query = $this->db->query('select min(year(tanggaltransaksi)) as minimum from tb_transaksi');
		$a=$query->row_array();
		$minimum=$a['minimum'];
		
		$query = $this->db->query('select max(year(tanggaltransaksi)) as maximum from tb_transaksi');
		$a=$query->row_array();
		$maximum=$a['maximum'];
		
		$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$this->db->where(array('tb_akun.poslaporan'=>'NRC'));
		//$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id,'tb_akun.poslaporan'=>'NRC'));
		$qr=$this->db->get('tb_akun');
		$h=$qr->result_array();
		
		$hasil=array();
		$namajenis='';
		$totdebit=0;
		$totkredit=0;
		$total=0;
		$totaljenis=0;
		
		foreach($h as $list){
			$idakun=$list['idakun'];
			$kodeakun=$list['kodejenisakun'].'-'.$list['kodeakun'];
			$namaakun=$list['namaakun'];
			$namajenisakun=$list['namajenisakun'];
			$possaldo=$list['possaldo'];
			if($namajenis!='' AND $namajenis!=$namajenisakun)
			{
				$a=array();
				$a['idakun']=$idakun;
				$a['kodeakun']=$kodeakun;
				$a['namaakun']=$namajenis;
				$a['debit']='';
				$a['kredit']=$totaljenis;
				$a['bold']=TRUE;
				$hasil[]=$a;
				$totaljenis=0;
				
			}
			
			$namajenis=$namajenisakun;
			
			if($idakunsewa==$idakun){
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakun.' and kost_id='.$this->kost_id.' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();
				if($h2['jmlbesaran']=='')
					$besaran=0;
				else
					$besaran=$h2['jmlbesaran'];
				
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakunpajak.' and kost_id='.$this->kost_id.' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();
				if($h2['jmlbesaran']=='')
					$besaranpajak=0;
				else
					$besaranpajak=$h2['jmlbesaran'];
				
				$jmlbesaran=$besaran+$besaranpajak;
				
			}else{
				
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakun.' and kost_id='.$this->kost_id.' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();

				if($h2['jmlbesaran']=='')
					$jmlbesaran=0;
				else
					$jmlbesaran=$h2['jmlbesaran'];
					
			}
			
			if($possaldo=='DB'){
				$debit=$jmlbesaran;
				$kredit=0;
			}else{
				$debit=0;
				$kredit=$jmlbesaran;
			}
		  
			$totdebit+=$debit;
			$totkredit+=$kredit;
			
			$totaljenis+=$jmlbesaran;		
					
			$a=array();
			$a['idakun']=$idakun;
			$a['kodeakun']=$kodeakun;
			$a['namaakun']=$namaakun;
			$a['debit']=$jmlbesaran;
			$a['kredit']='';
			$a['bold']=FALSE;
			$hasil[]=$a;
			
		}
		
		if(count($h)>0){
			$a=array();
			$a['idakun']=$idakun;
			$a['kodeakun']=$kodeakun;
			$a['namaakun']=$namajenis;
			$a['debit']='';
			$a['kredit']=$totaljenis;
			$a['bold']=TRUE;
			$hasil[]=$a;
		}
		
		$total=$totdebit - $totkredit;
		$this->content['tahun']=$tahun;
		$this->content['minimum']=$minimum;
		$this->content['maximum']=$maximum;
		$this->content['total']=$total;
		$this->content['kost_title']=$this->fungsi->getnamekost($this->kost_id);
		$this->content['neraca']=$hasil;
		$this->content['content']='owner/accounting/laporan/neraca';
		$this->load->view('owner/template',$this->content);
		
	}
	
	function cetakrugilaba(){	 
		$bulan=($this->uri->segment(6)!='')?$this->uri->segment(6):date('m');
		$tahun=($this->uri->segment(7)!='')?$this->uri->segment(7):date('Y');
		
		$this->content['kost_title']=$this->fungsi->getnamekost($this->kost_id);
		$this->load->library('excel');
		
		$sharedStyle1 = new PHPExcel_Style();
		$sharedStyle2 = new PHPExcel_Style();
		$sharedStyle3 = new PHPExcel_Style();
		$sharedStyle1->applyFromArray(
			 array('borders' => array(
			 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			 ),
		));
		
		$sharedStyle2->applyFromArray(
			 array('borders' => array(
			 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			 ),
		));
		
		$sharedStyle3->applyFromArray(
			 array('borders' => array(
			 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			 ),
		));
		
		
		$this->excel->setActiveSheetIndex(0);
		
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Laporan Laba Rugi');
		
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);

		
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', $this->content['kost_title']);
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
		
		$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A2', 'Laporan Rugi Laba');
		$this->excel->getActiveSheet()->mergeCells('A2:D2');
		
		$this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(12);
		//make the font become bold
		//$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A3', 'bulan: '.$this->fungsi->namabulan($bulan).' Tahun: '.$tahun);
		$this->excel->getActiveSheet()->mergeCells('A3:D3');
		$this->excel->getActiveSheet()->getStyle('A1:A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setSize(12);
		$this->excel->getActiveSheet()->setCellValue('A5', 'Kode Akun');
		
		$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setSize(12);
		$this->excel->getActiveSheet()->setCellValue('B5', 'Nama Akun');
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "A5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "B5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "C5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "D5");
		//$this->excel->getActiveSheet()->getStyle('A5')->applyFromArray($sharedStyle1);

		$query = $this->db->get('tb_settingakun');
		$a=$query->row_array();
		$idakunsewa=$a['idakunsewa'];
		$idakunpajak=$a['idakunpajak'];
				
		$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$this->db->where(array('tb_akun.poslaporan'=>'LB'));
		//$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id,'tb_akun.poslaporan'=>'LB'));
		$qr=$this->db->get('tb_akun');
		$h=$qr->result_array();
		
		$hasil=array();
		$namajenis='';
		$total=0;
		$totdebit=0;
		$totkredit=0;
		$totaljenis=0;
		$baris=5;
		$i=0;
		foreach($h as $list){
			//$baris++;
			$idakun=$list['idakun'];
			$kodeakun=$list['kodejenisakun'].'-'.$list['kodeakun'];
			$namaakun=$list['namaakun'];
			$namajenisakun=$list['namajenisakun'];
			$possaldo=$list['possaldo'];
			if($namajenis!='' AND $namajenis!=$namajenisakun)
			{
				$a=array();
				$a['idakun']=$idakun;
				$a['kodeakun']=$kodeakun;
				$a['namaakun']=$namajenis;
				$a['debit']='';
				$a['kredit']=$totaljenis;
				$a['bold']=TRUE;
				$hasil[]=$a;
				$totaljenis=0;
			}
			
			$namajenis=$namajenisakun;
			
			if($idakunsewa==$idakun){
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakun.' and kost_id='. $this->kost_id .' and month(tanggaltransaksi)='.$bulan.' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();
				
				if($h2['jmlbesaran']=='')
					$besaran=0;
				else
					$besaran=$h2['jmlbesaran'];
					
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakunpajak.' and kost_id='. $this->kost_id .' and month(tanggaltransaksi)='.$bulan.' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();
				
				if($h2['jmlbesaran']=='')
					$besaranpajak=0;
				else
					$besaranpajak=$h2['jmlbesaran'];
					
				$jmlbesaran=$besaran+$besaranpajak;
			}else{
					
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakun.' and kost_id='. $this->kost_id .' and month(tanggaltransaksi)='.$bulan.' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();
				
				if($h2['jmlbesaran']=='')
					$jmlbesaran=0;
				else
					$jmlbesaran=$h2['jmlbesaran'];
			}
			
			if($possaldo=='DB'){
				$debit=$jmlbesaran;
				$kredit=0;
			}else{
				$debit=0;
				$kredit=$jmlbesaran;
			}
		  
			$totdebit+=$debit;
			$totkredit+=$kredit;
			
			$totaljenis+=$jmlbesaran;		
					
			$a=array();
			$a['idakun']=$idakun;
			$a['kodeakun']=$kodeakun;
			$a['namaakun']=$namaakun;
			$a['debit']=$jmlbesaran;
			$a['kredit']='';
			$a['bold']=FALSE;
			$hasil[]=$a;
			
		}
		
		if(count($h)>0){
			$a=array();
			$a['idakun']=$idakun;
			$a['kodeakun']=$kodeakun;
			$a['namaakun']=$namajenis;
			$a['debit']='';
			$a['kredit']=$totaljenis;
			$a['bold']=TRUE;
			$hasil[]=$a;
		}
		
		$total=$totdebit - $totkredit;
		
		
		
		foreach($hasil as $list){
			$baris++;
			$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "A".$baris);
			$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "B".$baris);
			$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "C".$baris);
			$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "D".$baris);
						
			
			if($list['bold']){
				$this->excel->getActiveSheet()->getStyle('A'.$baris.':D'.$baris)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('A'.$baris, '');
				$this->excel->getActiveSheet()->setCellValue('B'.$baris, 'TOTAL '.$list['namaakun']);
				
				$this->excel->getActiveSheet()->setCellValue('C'.$baris, '');
				$this->excel->getActiveSheet()->setCellValue('D'.$baris, $list['kredit']);
			}else{
				
				$this->excel->getActiveSheet()->setCellValue('A'.$baris, $list['kodeakun']);
				$this->excel->getActiveSheet()->setCellValue('B'.$baris, $list['namaakun']);
				
				$this->excel->getActiveSheet()->setCellValue('C'.$baris, $list['debit']);
				$this->excel->getActiveSheet()->setCellValue('D'.$baris, '');
			}
			
		}
				
		$baris++;
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "A".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "B".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "C".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "D".$baris);
		
		$baris++;
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle3, "A".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle3, "B".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle3, "C".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle3, "D".$baris);
		
		$this->excel->getActiveSheet()->getStyle('A'.$baris.':D'.$baris)->getFont()->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('B'.$baris, 'Laba Bersih');
		$this->excel->getActiveSheet()->setCellValue('D'.$baris, $total);
		
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Laporan Laba Rugi.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	function cetakneraca(){	 
		$tahun=($this->uri->segment(7)!='')?$this->uri->segment(7):date('Y');
		
		$this->content['kost_title']=$this->fungsi->getnamekost($this->kost_id);
		$this->load->library('excel');
		
		$sharedStyle1 = new PHPExcel_Style();
		$sharedStyle2 = new PHPExcel_Style();
		$sharedStyle3 = new PHPExcel_Style();
		$sharedStyle1->applyFromArray(
			 array('borders' => array(
			 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			 ),
		));
		
		$sharedStyle2->applyFromArray(
			 array('borders' => array(
			 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			 ),
		));
		
		$sharedStyle3->applyFromArray(
			 array('borders' => array(
			 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			 ),
		));
		
		
		$this->excel->setActiveSheetIndex(0);
		
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Laporan Neraca');
		
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);

		
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', $this->content['kost_title']);
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
		
		$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A2', 'Neraca');
		$this->excel->getActiveSheet()->mergeCells('A2:D2');
		
		$this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(12);
		//make the font become bold
		//$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A3', ' Tahun: '.$tahun);
		$this->excel->getActiveSheet()->mergeCells('A3:D3');
		$this->excel->getActiveSheet()->getStyle('A1:A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$this->excel->getActiveSheet()->getStyle('A5')->getFont()->setSize(12);
		$this->excel->getActiveSheet()->setCellValue('A5', 'Kode Akun');
		
		$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setSize(12);
		$this->excel->getActiveSheet()->setCellValue('B5', 'Nama Akun');
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "A5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "B5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "C5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "D5");
		//$this->excel->getActiveSheet()->getStyle('A5')->applyFromArray($sharedStyle1);

		$query = $this->db->get('tb_settingakun');
		$a=$query->row_array();
		$idakunsewa=$a['idakunsewa'];
		$idakunpajak=$a['idakunpajak'];
		
		$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$this->db->where(array('tb_akun.poslaporan'=>'NRC'));
		//$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id,'tb_akun.poslaporan'=>'NRC'));
		$qr=$this->db->get('tb_akun');
		$h=$qr->result_array();
		
		$hasil=array();
		$namajenis='';
		$total=0;
		$totdebit=0;
		$totkredit=0;
		$totaljenis=0;
		$baris=5;
		$i=0;
		foreach($h as $list){
			//$baris++;
			$idakun=$list['idakun'];
			$kodeakun=$list['kodejenisakun'].'-'.$list['kodeakun'];
			$namaakun=$list['namaakun'];
			$namajenisakun=$list['namajenisakun'];
			$possaldo=$list['possaldo'];
			if($namajenis!='' AND $namajenis!=$namajenisakun)
			{
				$a=array();
				$a['idakun']=$idakun;
				$a['kodeakun']=$kodeakun;
				$a['namaakun']=$namajenis;
				$a['debit']='';
				$a['kredit']=$totaljenis;
				$a['bold']=TRUE;
				$hasil[]=$a;
				$totaljenis=0;
			}
			
			$namajenis=$namajenisakun;
			
			if($idakunsewa==$idakun){
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakun.' and kost_id='. $this->kost_id .' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();
				//print_r($h2);
				if($h2['jmlbesaran']=='')
					$besaran=0;
				else
					$besaran=$h2['jmlbesaran'];
				
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakunpajak.' and kost_id='. $this->kost_id .' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();
				//print_r($h2);
				if($h2['jmlbesaran']=='')
					$besaranpajak=0;
				else
					$besaranpajak=$h2['jmlbesaran'];
				
				$jmlbesaran=$besaran+$besaranpajak;
			}else{
				
				$qr2=$this->db->query('select sum(besaran) as jmlbesaran from tb_transaksi where idakun='.$idakun.' and kost_id='. $this->kost_id .' and year(tanggaltransaksi)='.$tahun);
				$h2=$qr2->row_array();
				//print_r($h2);
				if($h2['jmlbesaran']=='')
					$jmlbesaran=0;
				else
					$jmlbesaran=$h2['jmlbesaran'];
			}
			
			if($possaldo=='DB'){
				$debit=$jmlbesaran;
				$kredit=0;
			}else{
				$debit=0;
				$kredit=$jmlbesaran;
			}
		  
			$totdebit+=$debit;
			$totkredit+=$kredit;
			
			$totaljenis+=$jmlbesaran;		
					
			$a=array();
			$a['idakun']=$idakun;
			$a['kodeakun']=$kodeakun;
			$a['namaakun']=$namaakun;
			$a['debit']=$jmlbesaran;
			$a['kredit']='';
			$a['bold']=FALSE;
			$hasil[]=$a;
		}
			
		if(count($h)>0){
			$a=array();
			$a['idakun']=$idakun;
			$a['kodeakun']=$kodeakun;
			$a['namaakun']=$namajenis;
			$a['debit']='';
			$a['kredit']=$totaljenis;
			$a['bold']=TRUE;
			$hasil[]=$a;
		}
		
		$total=$totdebit - $totkredit;	
		
		foreach($hasil as $list){
			$baris++;
			$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "A".$baris);
			$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "B".$baris);
			$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "C".$baris);
			$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "D".$baris);
			
			if($list['bold']){
				$this->excel->getActiveSheet()->getStyle('A'.$baris.':D'.$baris)->getFont()->setBold(true);
				$this->excel->getActiveSheet()->setCellValue('A'.$baris, '');
				$this->excel->getActiveSheet()->setCellValue('B'.$baris, 'TOTAL '.$list['namaakun']);
				
				$this->excel->getActiveSheet()->setCellValue('C'.$baris, '');
				$this->excel->getActiveSheet()->setCellValue('D'.$baris, $list['kredit']);
			}else{
				
				$this->excel->getActiveSheet()->setCellValue('A'.$baris, $list['kodeakun']);
				$this->excel->getActiveSheet()->setCellValue('B'.$baris, $list['namaakun']);
				
				$this->excel->getActiveSheet()->setCellValue('C'.$baris, $list['debit']);
				$this->excel->getActiveSheet()->setCellValue('D'.$baris, '');
			}
		}
						
		$baris++;
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "A".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "B".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "C".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "D".$baris);
		
		$baris++;
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle3, "A".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle3, "B".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle3, "C".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle3, "D".$baris);
		
		$this->excel->getActiveSheet()->getStyle('A'.$baris.':D'.$baris)->getFont()->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('B'.$baris, 'Laba Bersih');
		$this->excel->getActiveSheet()->setCellValue('D'.$baris, $total);
		
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='Laporan Neraca.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
}
?>