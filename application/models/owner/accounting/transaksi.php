<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class transaksi extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
		$this->load->model('owner/accounting/mfunction','fungsi');
		$this->content['namakost']=$this->fungsi->getnamekost($this->kost_id);
    }
	
	function getakun(){
		$query = $this->db->get('tb_settingakun');
		$a=$query->row_array();
		$idakunsewa=$a['idakunsewa'];
		$idakunpajak=$a['idakunpajak'];
		$idakunpendapatanlain=$a['idakunpendapatanlain'];
		$idakunpengeluaran=$a['idakunpengeluaran'];
		$wherein=array(1=>$idakunsewa,2=>$idakunpajak,3=>$idakunpendapatanlain,4=>$idakunpengeluaran);
		
		$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		//$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id));
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$this->db->where_not_in('tb_akun.idakun',$wherein);
		$qr=$this->db->get('tb_akun');
		
		return $qr->result_array();
	}
	
	function getallakun(){
		/*$query = $this->db->get('tb_settingakun');
		$a=$query->row_array();
		$idakunsewa=$a['idakunsewa'];
		$idakunpajak=$a['idakunpajak'];
		$idakunpendapatanlain=$a['idakunpendapatanlain'];
		$idakunpengeluaran=$a['idakunpengeluaran'];
		$wherein=array(1=>$idakunsewa,2=>$idakunpajak,3=>$idakunpendapatanlain,4=>$idakunpengeluaran);
		*/
		$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		//$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id));
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		//$this->db->where_not_in('tb_akun.idakun',$wherein);
		$qr=$this->db->get('tb_akun');
		
		return $qr->result_array();
	}	
		
	function tampiltransaksi(){
		$hariantransaksi=trim($this->input->post('hariantransaksi'));
		$bulanbulanan=trim($this->input->post('bulanbulanan'));
		$tahunbulanan=trim($this->input->post('tahunbulanan'));
		$laintransaksi1=trim($this->input->post('laintransaksi1'));
		$laintransaksi2=trim($this->input->post('laintransaksi2'));
		$selectpilih=trim($this->input->post('selectpilih'));
		
		if(!preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $hariantransaksi))
			$hariantransaksi=date('Y-m-d');
		if(!preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $laintransaksi1))
			$laintransaksi1=date('Y-m-d');
		if(!preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $laintransaksi2))
			$laintransaksi2=date('Y-m-d');
		if($bulanbulanan=='')
			$bulanbulanan=date('m');
		if($tahunbulanan=='')
			$tahunbulanan=date('Y');
		if($selectpilih!='harian' AND $selectpilih!='bulanan' AND $selectpilih!='lainnya')
			$selectpilih='harian';
			
		
		$this->db->join('tb_akun','tb_akun.idakun=tb_transaksi.idakun');
		$this->db->order_by('tb_transaksi.tanggaltransaksi','asc');
		if($selectpilih=='harian')	
			$this->db->where(array('tb_transaksi.kost_id'=>$this->kost_id,'tb_transaksi.tanggaltransaksi'=>$hariantransaksi));
		elseif($selectpilih=='bulanan')
			$this->db->where(array('tb_transaksi.kost_id'=>$this->kost_id,'month(tb_transaksi.tanggaltransaksi)'=>$bulanbulanan,'year(tb_transaksi.tanggaltransaksi)'=>$tahunbulanan));
		else
			$this->db->where(array('tb_transaksi.kost_id'=>$this->kost_id,'tb_transaksi.tanggaltransaksi >='=>$laintransaksi1,'tb_transaksi.tanggaltransaksi <='=>$laintransaksi2));
		
		$this->db->order_by('tb_transaksi.tanggaltransaksi','tb_transaksi.notransaksi asc');
		$this->db->order_by('tb_transaksi.notransaksi','asc');	
		$qr=$this->db->get('tb_transaksi');
		$a=$qr->result_array();
		
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		$idakunsewa=$h['idakunsewa'];
		$idakunpajak=$h['idakunpajak'];
		$idakundiskon=$h['idakundiskon'];
		
		$h=array();
		foreach($a as $list){
			if($list['idakun']!=$idakunpajak AND $list['idakun']!=$idakundiskon){
				$notransaksi=$list['notransaksi'];
				$idakun=$list['idakun'];
				$idtransaksi=$list['idtransaksi'];
				$possaldo=$list['possaldo'];
				
				$b=array();
				$b['idtransaksi']=$list['idtransaksi'];
				$b['tanggaltransaksi']=$this->fungsi->tglindo($list['tanggaltransaksi']);
				$b['notransaksi']=$list['notransaksi'];
				$b['idakun']=$list['idakun'];
				$b['uraian']=$list['uraian'];
				$b['possaldo']=$list['possaldo'];
				$besaran=$list['besaran'];
				
				if($idakun==$idakunsewa){
					$this->db->select('besaran');
					$this->db->where(array('parent_id'=>$idtransaksi,'idakun'=>$idakunpajak));
					$qr=$this->db->get('tb_transaksi');
					$h2=$qr->row_array();
					if(count($h2)>0){
						$b['pajak']=$h2['besaran'];
					}else{
						$this->db->select('besaran');
						$this->db->where(array('notransaksi'=>$notransaksi,'idakun'=>$idakunpajak));
						$qr=$this->db->get('tb_transaksi');
						$h2=$qr->row_array();
						if(count($h2)>0)
							$b['pajak']=$h2['besaran'];
						else{
							$b['pajak']=0;
						}
					}
					//$b['besaran']=$besaran+$b['pajak'];
					$besaran+=$b['pajak'];
				}else{
					//$b['besaran']=$besaran;
					$b['pajak']=0;
				}
				
				if($list['possaldo']=='DB'){
		  			$b['debit']=$besaran;
					$b['kredit']='';
		  		}else{
			  		$b['debit']='';
					$b['kredit']=$besaran;
		  		}
		  
				$h[]=$b;
				
				
				//proses penyesuaian
				$this->db->where('idtransaksi',$idtransaksi);
				$qr3=$this->db->get('tb_penyesuaian');
				$h3=$qr3->result_array();
				
				if(count($h3)>0){
					foreach($h3 as $list3){
						$b=array();
						$b['idtransaksi']=$idtransaksi;
						$b['tanggaltransaksi']=$this->fungsi->tglindo($list3['tanggaltransaksi']);
						$b['notransaksi']=$notransaksi;
						$b['idakun']=$idakun;
						$b['uraian']=$list3['uraian'];
						$b['possaldo']=$list3['possaldo'];
						$besaran=$list3['besaran'];
						$b['pajak']=0;
						
						if($list3['possaldo']=='DB'){
		  					$b['debit']=$besaran;
							$b['kredit']='';
		  				}else{
			  				$b['debit']='';
							$b['kredit']=$besaran;
		  				}
						
						$h[]=$b;
					}
					
					
				}
				
				//diskon punya	
				$this->db->join('tb_akun','tb_akun.idakun=tb_transaksi.idakun');
				$this->db->order_by('tb_transaksi.tanggaltransaksi','asc');
				$this->db->where('parent_id',$idtransaksi);
				$qr4=$this->db->get('tb_transaksi')->result_array();
				
				foreach($qr4 as $list4){
					$b=array();
					$b['idtransaksi']=$list4['idtransaksi'];
					$b['tanggaltransaksi']=$this->fungsi->tglindo($list4['tanggaltransaksi']);
					$b['notransaksi']=$list4['notransaksi'];
					$b['idakun']=$list4['idakun'];
					$b['uraian']=$list4['uraian'];
					$b['possaldo']=$list4['possaldo'];
					
					$besaran=$list4['besaran'];
				
					$b['pajak']=0;
								
					if($list4['possaldo']=='DB'){
			  			$b['debit']=$besaran;
						$b['kredit']='';
			  		}else{
				  		$b['debit']='';
						$b['kredit']=$besaran;
			  		}
			  
					$h[]=$b;
				}
			}
		}
		
		$this->content['minimum']=$this->fungsi->getminyeartransaction();
		$this->content['maximum']=$this->fungsi->getmaxyeartransaction();
		$this->content['hariantransaksi']=$hariantransaksi;
		$this->content['bulanbulanan']=$bulanbulanan;
		$this->content['tahunbulanan']=$tahunbulanan;
		$this->content['laintransaksi1']=$laintransaksi1;
		$this->content['laintransaksi2']=$laintransaksi2;
		$this->content['selectpilih']=$selectpilih;
		
		$this->content['transaksi']=$h;
		$this->content['content']='owner/accounting/transaksi/tampiltransaksi';
		$this->load->view('owner/template',$this->content);
	}
	
	function inputtransaksi(){
		$this->content['akun']=$this->getakun();
		$this->content['mode']='input';
		$this->content['tanggaltransaksi']=date('Y-m-d');
		$this->content['idtransaksi']='';
		$this->content['idakun']='';
		$this->content['nobukti']='';
		$this->content['uraian']='';
		$this->content['besaran']='';
		
		$this->content['content']='owner/accounting/transaksi/forminputtransaksi';
		$this->load->view('owner/template',$this->content);
	}
	
	function edittransaksi(){
		
		$this->content['akun']=$this->getallakun();
		$idtransaksi=$this->uri->segment(6);
		$this->db->where(array('idtransaksi'=>$idtransaksi));
		$qr=$this->db->get('tb_transaksi');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/transaksi/tampil-transaksi','refresh');
		else{
			$this->content['mode']='edit';
			$this->content['tanggaltransaksi']=$h['tanggaltransaksi'];
			$this->content['idtransaksi']=$h['idtransaksi'];
			$this->content['nobukti']=$h['notransaksi'];
			$this->content['uraian']=$h['uraian'];
			$this->content['idakun']=$h['idakun'];
			$this->content['besaran']=$h['besaran'];
					
			$this->content['content']='owner/accounting/transaksi/forminputtransaksi';
			$this->load->view('owner/template',$this->content);
		}
	}
	
	function hapustransaksi(){
		$idtransaksi=$this->uri->segment(6);
		$this->db->where(array('idtransaksi'=>$idtransaksi));
		$qr=$this->db->get('tb_transaksi');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/transaksi/tampil-transaksi','refresh');
		else{
			$where=array('idtransaksi'=>$idtransaksi);
			$this->db->delete('tb_transaksi', $where); 
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/transaksi/tampil-transaksi','refresh');
		}
	}
	
	function prosestransaksi(){
		$mode=$this->input->post('mode');
		$idtransaksi=$this->input->post('idtransaksi');
		$nobukti=$this->input->post('nobukti');
		$uraian=$this->input->post('uraian');
		$idakun=$this->input->post('idakun');
		$besaran=$this->input->post('besaran');
		$tanggaltransaksi=$this->input->post('tanggaltransaksi');
		
		$this->content['mode']=$mode;
		$this->content['tanggaltransaksi']=$tanggaltransaksi;
		$this->content['idtransaksi']=$idtransaksi;
		$this->content['nobukti']=$nobukti;
		$this->content['uraian']=$uraian;
		$this->content['idakun']=$idakun;
		$this->content['besaran']=$besaran;
		
		//$this->form_validation->set_rules('tanggaltransaksi', 'Tanggal transaksi', 'required');
		//$this->form_validation->set_rules('nobukti', 'No Bukti', 'required');
		$this->form_validation->set_rules('idakun', 'Jenis Akun', 'required');
		if($mode=='edit'){
			$this->form_validation->set_rules('besaran', 'Besaran', 'required');
			$this->form_validation->set_rules('uraian', 'Uraian', 'required');
		}
		
		$this->form_validation->set_message('required', '%s masih kosong');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			if($mode=='input')
				$this->content['akun']=$this->getakun();
			else
				$this->content['akun']=$this->getallakun();
				
			$this->content['content']='owner/accounting/transaksi/forminputtransaksi';
			$this->load->view('owner/template',$this->content);
		}else{
			
			$userid=$this->session->userdata('ses_owner_id');
			//$this->db->where(array('guard_id'=>$userid));
			//$qr=$this->db->get('tb_kosts');
			//$h=$qr->row_array();
			//$kost_id=$h['kost_id'];
			$kost_id=$this->uri->segment(3);
			$namfield=array();
			if($mode=='input')
			$namafield=array('kost_id'=>$kost_id,'tanggaltransaksi'=>$tanggaltransaksi,'notransaksi'=>$nobukti,'uraian'=>$uraian,'idakun'=>$idakun,'besaran'=>$besaran);
			else
			$namafield=array('kost_id'=>$kost_id,'tanggaltransaksi'=>$tanggaltransaksi,'notransaksi'=>$nobukti,'idakun'=>$idakun,'uraian'=>$uraian);
			//if($pajak=='Y')
			//	$jmlpajak=$besaran*0.01;
			//else
			//	$jmlpajak=0;
						
			//$namafield['pajak']=$jmlpajak;
			
			if($mode=='input'){
				$this->db->insert('tb_transaksi', $namafield);
			}
			else{
				$where=array('idtransaksi'=>$idtransaksi);
				$this->db->where($where);
				$this->db->update('tb_transaksi', $namafield);
			}
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/transaksi/tampil-transaksi','refresh');
		}
	}
	
	function cetaktransaksi(){
		$selectpilih=$this->uri->segment(6);
		if($selectpilih=='harian'){
			$hariantransaksi=$this->uri->segment(7);
		}elseif($selectpilih=='bulanan'){
			$bulanbulanan=$this->uri->segment(7);
			$tahunbulanan=$this->uri->segment(8);
		}
		elseif($selectpilih=='lainnya'){
			$laintransaksi1=$this->uri->segment(7);
			$laintransaksi2=$this->uri->segment(8);
		}else{
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/transaksi/tampil-transaksi','refresh');
		}
		
		//$tanggaltransaksi=$this->uri->segment(6);
		
		$this->db->join('tb_akun','tb_akun.idakun=tb_transaksi.idakun');
		$this->db->order_by('tb_transaksi.tanggaltransaksi','asc');
		
		//if(!preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $tanggaltransaksi)){
		//	redirect(base_url().'owner/accounting/'.$this->kost_id.'/transaksi/tampil-transaksi','refresh');
		
		if($selectpilih=='harian')	
			$this->db->where(array('tb_transaksi.kost_id'=>$this->kost_id,'tb_transaksi.tanggaltransaksi'=>$hariantransaksi));
		elseif($selectpilih=='bulanan')
			$this->db->where(array('tb_transaksi.kost_id'=>$this->kost_id,'month(tb_transaksi.tanggaltransaksi)'=>$bulanbulanan,'year(tb_transaksi.tanggaltransaksi)'=>$tahunbulanan));
		else
			$this->db->where(array('tb_transaksi.kost_id'=>$this->kost_id,'tb_transaksi.tanggaltransaksi >='=>$laintransaksi1,'tb_transaksi.tanggaltransaksi <='=>$laintransaksi2));
			
		$qr=$this->db->get('tb_transaksi');
		$a=$qr->result_array();
			
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		$idakunsewa=$h['idakunsewa'];
		$idakunpajak=$h['idakunpajak'];
		$idakundiskon=$h['idakundiskon'];
		
		$h=array();
		foreach($a as $list){
			if($list['idakun']!=$idakunpajak AND $list['idakun']!=$idakundiskon){
				$notransaksi=$list['notransaksi'];
				$idakun=$list['idakun'];
				
				$b=array();
				$idtransaksi=$list['idtransaksi'];
				$b['idtransaksi']=$list['idtransaksi'];
				$b['tanggaltransaksi']=$this->fungsi->tgl($list['tanggaltransaksi']);
				$b['notransaksi']=$list['notransaksi'];
				$b['idakun']=$list['idakun'];
				$b['uraian']=$list['uraian'];
				$b['possaldo']=$list['possaldo'];
				$besaran=$list['besaran'];
				
				if($idakun==$idakunsewa){
					$this->db->select('besaran');
					$this->db->where(array('parent_id'=>$idtransaksi,'idakun'=>$idakunpajak));
					$qr=$this->db->get('tb_transaksi');
					$h2=$qr->row_array();
					if(count($h2)>0){
						$b['pajak']=$h2['besaran'];
					}else{
						$this->db->select('besaran');
						$this->db->where(array('notransaksi'=>$notransaksi,'idakun'=>$idakunpajak));
						$qr=$this->db->get('tb_transaksi');
						$h2=$qr->row_array();
						if(count($h2)>0)
							$b['pajak']=$h2['besaran'];
						else{
							$b['pajak']=0;
						}
					}
					//$b['besaran']=$besaran+$b['pajak'];
					$besaran+=$b['pajak'];
				}else{
					//$b['besaran']=$besaran;
					$b['pajak']=0;
				}
				
				if($list['possaldo']=='DB'){
					$b['debit']=$besaran;
					$b['kredit']='';
				}else{
					$b['debit']='';
					$b['kredit']=$besaran;
				}
		  
				$h[]=$b;
				
				
				//proses penyesuaian
				$this->db->where('idtransaksi',$idtransaksi);
				$qr3=$this->db->get('tb_penyesuaian');
				$h3=$qr3->result_array();
				
				if(count($h3)>0){
					foreach($h3 as $list3){
						$b=array();
						$b['idtransaksi']=$idtransaksi;
						$b['tanggaltransaksi']=$this->fungsi->tglindo($list3['tanggaltransaksi']);
						$b['notransaksi']=$notransaksi;
						$b['idakun']=$idakun;
						$b['uraian']=$list3['uraian'];
						$b['possaldo']=$list3['possaldo'];
						$besaran=$list3['besaran'];
						$b['pajak']=0;
						
						if($list3['possaldo']=='DB'){
		  					$b['debit']=$besaran;
							$b['kredit']='';
		  				}else{
			  				$b['debit']='';
							$b['kredit']=$besaran;
		  				}
						
						$h[]=$b;
					}
					
					
				}
				
				//diskon punya	
				$this->db->join('tb_akun','tb_akun.idakun=tb_transaksi.idakun');
				$this->db->order_by('tb_transaksi.tanggaltransaksi','asc');
				$this->db->where('parent_id',$idtransaksi);
				$qr4=$this->db->get('tb_transaksi')->result_array();
				
				foreach($qr4 as $list4){
					$b=array();
					$b['idtransaksi']=$list4['idtransaksi'];
					$b['tanggaltransaksi']=$this->fungsi->tglindo($list4['tanggaltransaksi']);
					$b['notransaksi']=$list4['notransaksi'];
					$b['idakun']=$list4['idakun'];
					$b['uraian']=$list4['uraian'];
					$b['possaldo']=$list4['possaldo'];
					
					$besaran=$list4['besaran'];
				
					$b['pajak']=0;
								
					if($list4['possaldo']=='DB'){
			  			$b['debit']=$besaran;
						$b['kredit']='';
			  		}else{
				  		$b['debit']='';
						$b['kredit']=$besaran;
			  		}
			  
					$h[]=$b;
				}
			}
		}
			
		$this->content['kost_title']=$this->fungsi->getnamekost($this->kost_id);
		$this->load->library('excel');
		
		$sharedStyle1 = new PHPExcel_Style();
		$sharedStyle2 = new PHPExcel_Style();
		$sharedStyle3 = new PHPExcel_Style();
		$sharedStyle1->applyFromArray(
			 array('borders' => array(
			 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			 ),
		));
		
		$sharedStyle2->applyFromArray(
			 array('borders' => array(
			 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			 ),
		));
		
		$sharedStyle3->applyFromArray(
			 array('borders' => array(
			 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
			 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			 ),
		));
		
		
		$this->excel->setActiveSheetIndex(0);
		
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Laporan Transaksi');
		
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', $this->content['kost_title']);
		$this->excel->getActiveSheet()->mergeCells('A1:G1');
		
		$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(12);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A2', 'Laporan Transaksi');
		$this->excel->getActiveSheet()->mergeCells('A2:G2');
		
		$this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(12);
		//make the font become bold
		//$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		//set cell A1 content with some text
		if($selectpilih=='harian')
			$this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal Transaksi: '.$this->fungsi->tglindo($hariantransaksi));
		elseif($selectpilih=='bulanan')
			$this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal Transaksi: '.$this->fungsi->namabulan($bulanbulanan).' '.$tahunbulanan);
		else
			$this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal Transaksi: '.$this->fungsi->tglindo($laintransaksi1).' - '.$this->fungsi->tglindo($laintransaksi2));
		$this->excel->getActiveSheet()->mergeCells('A3:G3');
		$this->excel->getActiveSheet()->getStyle('A1:A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$this->excel->getActiveSheet()->getStyle('A5:G5')->getFont()->setSize(12);
		$this->excel->getActiveSheet()->setCellValue('A5', 'Tanggal');
		
		/*$this->excel->getActiveSheet()->getStyle('B5')->getFont()->setSize(12);*/
		$this->excel->getActiveSheet()->setCellValue('B5', 'Bukti Transaksi');
		$this->excel->getActiveSheet()->setCellValue('C5', 'Uraian');
		$this->excel->getActiveSheet()->setCellValue('D5', 'Debit(Rp.)');
		$this->excel->getActiveSheet()->setCellValue('E5', 'Kredit(Rp.)');
		$this->excel->getActiveSheet()->setCellValue('F5', 'Pajak(10%)');
		$this->excel->getActiveSheet()->setCellValue('G5', 'Saldo');
		
		
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "A5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "B5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "C5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "D5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "E5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "F5");
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "G5");
		
		$totdebit=0;
		$totkredit=0;
		$totpajak=0;
		$total=0;
		$baris=5;
		if(count($h)>0){
			foreach($h as $list){
				$debit=$list['debit'];
				$kredit=$list['kredit'];
				$totdebit+=$debit;
				$totkredit+=$kredit;
				$totpajak+=$list['pajak'];
				$total+=($debit-$kredit-$list['pajak']);
	  
				$baris++;
				$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "A".$baris);
				$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "B".$baris);
				$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "C".$baris);
				$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "D".$baris);
				$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "E".$baris);
				$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "F".$baris);
				$this->excel->getActiveSheet()->setSharedStyle($sharedStyle2, "G".$baris);
				
				$this->excel->getActiveSheet()->setCellValue('A'.$baris, $list['tanggaltransaksi']);
				$this->excel->getActiveSheet()->setCellValue('B'.$baris, $list['notransaksi']);
				$this->excel->getActiveSheet()->setCellValue('C'.$baris, $list['uraian']);
				$this->excel->getActiveSheet()->setCellValue('D'.$baris, $list['debit']);
				$this->excel->getActiveSheet()->setCellValue('E'.$baris, $list['kredit']);
				$this->excel->getActiveSheet()->setCellValue('F'.$baris, $list['pajak']);
				$this->excel->getActiveSheet()->setCellValue('G'.$baris, $total);
			}
		}
		
		$baris++;
		$this->excel->getActiveSheet()->mergeCells('A'.$baris.':C'.$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "A".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "B".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "C".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "D".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "E".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "F".$baris);
		$this->excel->getActiveSheet()->setSharedStyle($sharedStyle1, "G".$baris);
		
		$this->excel->getActiveSheet()->getStyle('A'.$baris.':G'.$baris)->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('A'.$baris)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$this->excel->getActiveSheet()->setCellValue('A'.$baris, 'Jumlah penerimaan / pengeluaran');
		$this->excel->getActiveSheet()->setCellValue('D'.$baris, $totdebit);
		$this->excel->getActiveSheet()->setCellValue('E'.$baris, $totkredit);
		$this->excel->getActiveSheet()->setCellValue('F'.$baris, $totpajak);
		$this->excel->getActiveSheet()->setCellValue('G'.$baris, $total);
				
		
		$filename='Laporan Transaksi.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	
	
}