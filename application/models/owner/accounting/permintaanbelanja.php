<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class permintaanbelanja extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
		$this->load->model('owner/accounting/mfunction','fungsi');
		$this->content['namakost']=$this->fungsi->getnamekost($this->kost_id);
    }
	
	function tampilpengajuan(){
		if($this->session->userdata('ses_owner_level')=='guard'){
			$userid=$this->session->userdata('ses_owner_id');
			$this->db->join('tb_kosts','tb_pengajuanbelanja.kost_id=tb_kosts.kost_id');
			$this->db->where(array('tb_kosts.kost_id'=>$this->kost_id));
			$this->db->order_by('tglpengajuan','desc');
			$qr=$this->db->get('tb_pengajuanbelanja');
			$h=$qr->result_array();
			
			$hasil=array();
			foreach($h as $list){
				$a=array();
				$a['idpengajuanbelanja']=$list['idpengajuanbelanja'];
				if($list['periksa']==0){
					$a['tglpengajuan']='<b>'.$list['tglpengajuan'].'</b>';
					$a['kost_title']='<b>'.$list['kost_title'].'</b>';
					$a['periksa']='<b>'.($list['periksa']==0)?'Belum diperiksa':'Sudah diperiksa'.'</b>';
				}else{
					
					$a['tglpengajuan']=$list['tglpengajuan'];
					$a['kost_title']=$list['kost_title'];
					$a['periksa']=($list['periksa']==0)?'Belum diperiksa':'Sudah diperiksa';
				}
				$idpengajuan=$list['idpengajuanbelanja'];
				$this->db->where(array('idpengajuanbelanja'=>$idpengajuan));
				$qr=$this->db->get('tb_detailpengajuan');
				$h1=$qr->result_array();
				$total=0;
				foreach($h1 as $list1){
					$jumlah=$list1['jumlah'];
					$harga=$list1['harga'];
					$total+=($jumlah*$harga);
				}
				if($list['periksa']==0){
					$a['total']='<b>Rp '.number_format($total,0,',','.').'</b>';
				}else{
					$a['total']=$total;
				}
				$hasil[]=$a;
			}
		}
		$this->content['hasil']=$hasil;
		$this->content['content']='owner/accounting/pengajuan/guardpengajuanbelanja';
		$this->load->view('owner/template',$this->content);		
	}
	
	function tampilownerpengajuan(){
		if($this->session->userdata('ses_owner_level')=='owner'){
			$userid=$this->session->userdata('ses_owner_id');
			$this->db->join('tb_kosts','tb_pengajuanbelanja.kost_id=tb_kosts.kost_id');
			$this->db->where(array('tb_kosts.kost_id'=>$this->kost_id));
			$qr=$this->db->get('tb_pengajuanbelanja');
			$h=$qr->result_array();
			
			$hasil=array();
			foreach($h as $list){
				$a=array();
				$a['idpengajuanbelanja']=$list['idpengajuanbelanja'];
				$a['periksa']=$list['periksa'];
				if($list['periksa']==0){
					$a['tglpengajuan']='<b>'.$list['tglpengajuan'].'</b>';
					$a['kost_title']='<b>'.$list['kost_title'].'</b>';
					$a['ketperiksa']='<b>'.($list['periksa']==0)?'Belum diperiksa':'Sudah diperiksa'.'</b>';
				}else{
					
					$a['tglpengajuan']=$list['tglpengajuan'];
					$a['kost_title']=$list['kost_title'];
					$a['ketperiksa']=($list['periksa']==0)?'Belum diperiksa':'Sudah diperiksa';
				}
				$idpengajuan=$list['idpengajuanbelanja'];
				$this->db->where(array('idpengajuanbelanja'=>$idpengajuan));
				$qr=$this->db->get('tb_detailpengajuan');
				$h1=$qr->result_array();
				$total=0;
				foreach($h1 as $list1){
					$jumlah=$list1['jumlah'];
					$harga=$list1['harga'];
					$total+=($jumlah*$harga);
				}
				if($list['periksa']==0){
					$a['total']='<b>Rp '.number_format($total,0,',','.').'</b>';
				}else{
					$a['total']=$total;
				}
				$hasil[]=$a;
			}
		}
		$this->content['hasil']=$hasil;
		$this->content['content']='owner/accounting/pengajuan/ownerpengajuanbelanja';
		$this->load->view('owner/template',$this->content);
			
	}
	
	function inputjumlahitem(){
		$this->content['content']='owner/accounting/pengajuan/inputjumlahitem';
		$this->load->view('owner/template',$this->content);
	}
	
	function forminputitem($jmlitem){
		$tgl=date('d/m/Y');
		$this->content['tanggal']=$tgl;
		$this->content['jmlitem']=$jmlitem;
		$this->content['content']='owner/accounting/pengajuan/inputitem';
		$this->load->view('owner/template',$this->content);
	}
	
	function inputitem(){
		$jmlitem=$this->input->post('jmlitem');
		if($jmlitem==0 OR !is_numeric($jmlitem))
			redirect('owner/accounting/'.$this->kost_id.'/belanja/input-jumlah-items','refresh');
		else{
			$this->forminputitem($jmlitem);
		}
		
	}
	
	function konfirminputitem(){
		$jmlitem=$this->input->post('jmlitem');
		$uraian=$this->input->post('uraian');
		$satuan=$this->input->post('satuan');
		$jumlah=$this->input->post('jumlah');
		$harga=$this->input->post('harga');
		
		$tgl=date('d/m/Y');
		$this->content['tanggal']=$tgl;
		$this->content['jmlitem']=$jmlitem;
		$this->content['uraian']=$uraian;
		$this->content['satuan']=$satuan;
		$this->content['jumlah']=$jumlah;
		$this->content['harga']=$harga;
		
		$this->content['content']='owner/accounting/pengajuan/konfirminputitem';
		$this->load->view('owner/template',$this->content);
	}
	
	function prosespengajuan(){
		$jmlitem=$this->input->post('jmlitem');
		$uraian=$this->input->post('uraian');
		$satuan=$this->input->post('satuan');
		$jumlah=$this->input->post('jumlah');
		$harga=$this->input->post('harga');
		
		$ses_level=$this->session->userdata('ses_owner_level');
		$userid=$this->session->userdata('owner_id');
		//$userid=$this->session->userdata('ses_owner_id');
		$tglpengajuan=date('Y-m-d');
		$nopengajuan=$this->fungsi->createnobelanja();
		/*$this->db->select('kost_id');
		$this->db->where(array('guard_id'=>$userid));
		$qr=$this->db->get('tb_kosts');
		$h=$qr->row_array();
		$kostid=$h['kost_id'];*/
		
		$data=array();
		$data['kost_id']=$this->kost_id;
		$data['user_id']=$userid;
		$data['nopengajuan']=$nopengajuan;
		$data['tglpengajuan']=$tglpengajuan;
		$this->db->insert('tb_pengajuanbelanja',$data);
		$idpengajuan=$this->db->insert_id();
		
		
		for($i=1;$i<=count($uraian);$i++){
			$data=array();
			$data['idpengajuanbelanja']=$idpengajuan;
			$data['uraian']=$uraian[$i];
			$data['satuan']=$satuan[$i];
			$data['jumlah']=$jumlah[$i];
			$data['harga']=$harga[$i];
			$this->db->insert('tb_detailpengajuan',$data);
		}
		redirect('owner/accounting/'.$this->kost_id.'/belanja','refresh');
	}
	
	function guarddetail(){
		$idpengajuan=$this->uri->segment(6);
		if($idpengajuan=='')
			redirect('owner/accounting/'.$this->kost_id.'/belanja','refresh');
		else{
			$this->db->join('tb_kosts','tb_pengajuanbelanja.kost_id=tb_kosts.kost_id');
			$this->db->where(array('tb_pengajuanbelanja.idpengajuanbelanja'=>$idpengajuan));
			$qr=$this->db->get('tb_pengajuanbelanja');
			$h=$qr->row_array();
			
			if(count($h)==0)
				redirect('owner/accounting/'.$this->kost_id.'/belanja','refresh');
			else{
				$this->content['pengajuan']=$h;
				
				$this->db->where(array('idpengajuanbelanja'=>$idpengajuan));
				$qr=$this->db->get('tb_detailpengajuan');
				$h1=$qr->result_array();
				$this->content['detailpengajuan']=$h1;
				
				$this->content['content']='owner/accounting/pengajuan/guarddetailpengajuanbelanja';
				$this->load->view('owner/template',$this->content);
			}
		}
	}
	
	function ownerdetail(){
		$idpengajuan=$this->uri->segment(6);
		if($idpengajuan=='')
			redirect('owner/accounting/'.$this->kost_id.'/belanja','refresh');
		else{
			$this->db->join('tb_kosts','tb_pengajuanbelanja.kost_id=tb_kosts.kost_id');
			$this->db->where(array('tb_pengajuanbelanja.idpengajuanbelanja'=>$idpengajuan));
			$qr=$this->db->get('tb_pengajuanbelanja');
			$h=$qr->row_array();
			
			if(count($h)==0)
				redirect('owner/accounting/'.$this->kost_id.'/belanja','refresh');
			else{
				$this->content['pengajuan']=$h;
				
				$this->db->where(array('idpengajuanbelanja'=>$idpengajuan));
				$qr=$this->db->get('tb_detailpengajuan');
				$h1=$qr->result_array();
				$this->content['detailpengajuan']=$h1;
				
				$this->content['content']='owner/accounting/pengajuan/ownerdetailpengajuanbelanja';
				$this->load->view('owner/template',$this->content);
			}
		}
	}
	
	function deleteitemdetailpengajuan(){
		$iddetailpengajuan=$this->uri->segment(7);
		
		if($iddetailpengajuan=='')
			redirect('owner/accounting/'.$this->kost_id.'/belanja/guard-detail/'.$this->uri->segment(5),'refresh');
		else{
			$this->db->where(array('iddetailpengajuan'=>$iddetailpengajuan));
			$qr=$this->db->get('tb_detailpengajuan');
			$h=$qr->row_array();
			if(count($h)==0)
				redirect('owner/accounting/'.$this->kost_id.'/belanja','refresh');
			else{
				$idpengajuan=$h['idpengajuanbelanja'];
				$this->db->where(array('iddetailpengajuan'=>$iddetailpengajuan));
				$this->db->delete('tb_detailpengajuan');
				redirect('owner/accounting/'.$this->kost_id.'/belanja/guard-detail/'.$idpengajuan,'refresh');
			}
		}
	}
	
	function inputdetailpengajuan(){
		//$iddetailpengajuan=$this->uri->segment(7);
		$idpengajuan=$this->uri->segment(7);
		if($idpengajuan=='')
			redirect('owner/accounting/'.$this->kost_id.'/belanja/','refresh');
		else{
			
			$this->content['idpengajuan']=$idpengajuan;
			$this->content['uraian']='';
			$this->content['satuan']='';
			$this->content['jumlah']='';
			$this->content['harga']='';
			
			$this->content['content']='owner/accounting/pengajuan/forminputdetailitem';
			$this->load->view('owner/template',$this->content);
		}
	}
	
	
	function prosesinputdetailpengajuan(){
		$idpengajuan=$this->input->post('idpengajuan');
		$uraian=$this->input->post('uraian');
		$satuan=$this->input->post('satuan');
		$jumlah=$this->input->post('jumlah');
		$harga=$this->input->post('harga');
		
		$this->form_validation->set_rules('uraian', 'Uraian', 'required');
		$this->form_validation->set_rules('satuan', 'Satuan', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required|numeric');
		$this->form_validation->set_rules('harga', 'harga', 'required|numeric');
		
		$this->form_validation->set_message('required', '%s masih kosong');
		$this->form_validation->set_message('numeric', '%s harus dalam bentuk angka');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->content['idpengajuan']=$idpengajuan;
			$this->content['uraian']=$uraian;
			$this->content['satuan']=$satuan;
			$this->content['jumlah']=$jumlah;
			$this->content['harga']=$harga;
			
			$this->content['content']='owner/accounting/pengajuan/forminputdetailitem';
			$this->load->view('owner/template',$this->content);
		}else{
		
			$data['idpengajuanbelanja']=$idpengajuan;
			$data['uraian']=$uraian;
			$data['satuan']=$satuan;
			$data['jumlah']=$jumlah;
			$data['harga']=$harga;
			$this->db->insert('tb_detailpengajuan',$data);
		
			redirect('owner/accounting/'.$this->kost_id.'/belanja/guard-detail/'.$idpengajuan,'refresh');
		}

	}
	
	function deletepengajuanbelanja(){
		$idpengajuan=$this->uri->segment(5);
		if($idpengajuan=='')
			redirect('owner/accounting/'.$this->kost_id.'/belanja','refresh');
		else{
			$this->db->where(array('idpengajuanbelanja'=>$idpengajuan));
			$qr=$this->db->get('tb_detailpengajuan');
			$h=$qr->result_array();
			if(count($h)>0){
				$this->db->where(array('idpengajuanbelanja'=>$idpengajuan));
				$this->db->delete('tb_detailpengajuan');
			}
			
			$this->db->where(array('idpengajuanbelanja'=>$idpengajuan));
			$this->db->delete('tb_pengajuanbelanja');
			redirect('owner/accounting/'.$this->kost_id.'/belanja','refresh');
		}
	}
	
	function cetakpengajuanbelanja(){
		$data=array();
		
		$idpengajuan=$this->uri->segment(6);
		$userid=$this->session->userdata('owner_id');
		
		$this->db->where(array('user_id'=>$userid));
		$qr=$this->db->get('tb_users');
		$row=$qr->row_array();
		$namapenjaga=$row['user_name'];
		
		$this->db->where(array('tb_user_kost.kost_id'=>$this->kost_id,'tb_users.user_level'=>'owner'));
		$this->db->join('tb_users','tb_users.user_id=tb_user_kost.user_id');
		$qr=$this->db->get('tb_user_kost');
		$row=$qr->row_array();
		$namapemilik=$row['user_name'];
		
		$this->db->where(array('kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_kosts');
		$row=$qr->row_array();
		$namakost=$row['kost_title'];
		
		$this->db->where(array('idpengajuanbelanja'=>$idpengajuan));
		$qr=$this->db->get('tb_pengajuanbelanja');
		$row=$qr->row_array();
		$notransaksi=$row['nopengajuan'];
		$tgltransaksi=$this->fungsi->tglindo($row['tglpengajuan']);
		
		$this->db->where(array('idpengajuanbelanja'=>$idpengajuan));
		$qr=$this->db->get('tb_detailpengajuan');
		$hasil=$qr->result_array();
		
		$b=array();
		$i=0;
		$gtotal=0;
		foreach($hasil as $list){
			if($list['statusdetail']==1){
				$i++;
				$total=$list['jumlah']*$list['harga'];
				$a=array();
				$a['no']=$i;
				$a['uraian']=$list['uraian'];
				$a['satuan']=$list['satuan'];
				$a['jumlah']=$list['jumlah'];
				$a['harga']=number_format($list['harga'],0,",",".");
				$a['total']=number_format($total,0,",",".");
				$b[]=$a;
				$gtotal+=$total;
			}
		}
		$data['namakost']=$namakost;
		$data['namapemilik']=$namapemilik;
		$data['namapenjaga']=$namapenjaga;
		$data['notransaksi']=$notransaksi;
		$data['tgltransaksi']=$tgltransaksi;
		$data['detailpengajuan']=$b;
		$data['gtotal']=number_format($gtotal,0,",",".");
		$data['terbilang']=$this->fungsi->terbilang($gtotal);
		$this->load->library('pdf');
     	$html = $this->load->view('owner/accounting/pengajuan/cetakpengajuanbelanja',$data,TRUE);
     	$this->pdf->pdf_create($html,'welcome');
		
	}
	
	function prosespersetujuan(){
		$setuju=$this->input->post('setuju');
		$idpengajuanbelanja=$this->input->post('idpengajuanbelanja');
		
		if(is_array($setuju)){
			$this->db->where(array('idpengajuanbelanja'=>$idpengajuanbelanja));
			$qr=$this->db->get('tb_detailpengajuan');
			$h=$qr->result_array();
			print_r($h);
			foreach($h as $list){
				if(in_array($list['iddetailpengajuan'],$setuju))
					$datasimpan['statusdetail']='1';
				else
					$datasimpan['statusdetail']='0';
					
				$this->db->where(array('iddetailpengajuan'=>$list['iddetailpengajuan']));
				$this->db->update('tb_detailpengajuan',$datasimpan);
			}
		}
		$this->db->where(array('idpengajuanbelanja'=>$idpengajuanbelanja));
		$this->db->update('tb_pengajuanbelanja',array('periksa'=>'1'));
			
		redirect('owner/accounting/'.$this->kost_id.'/belanja/tampil-belanja');
	}
	
}