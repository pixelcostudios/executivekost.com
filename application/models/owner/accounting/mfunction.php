<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class mfunction extends CI_Model{
	var $kost_id;
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
		
    }
	
	function createnotransaksi($kost_id=''){
		$romawi=array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V',6=>'VI',7=>'VII',8=>'VIII',9=>'IX',10=>'X',11=>'XI',12=>'XII');
		if($kost_id!='')
			$kostid=$kost_id;
		else
			$kostid=$this->kost_id;
		
		$year=date('Y');
		$month=$romawi[(int)date('m')];
		
		$qr2=$this->db->query('select sk.notransaksi 
							   from tb_sewakamar sk
							   left join tb_kost_room kr on kr.kost_room_id = sk.kost_room_id
							   where year(sk.tanggalsewakamar)='.$year.' and kr.kost_id='.$kostid.' order by sk.notransaksi desc');
		$h2=$qr2->row_array();
		if(count($h2)>0){
			$notrans=substr($h2['notransaksi'],10);
			if(!is_numeric($notrans))
				$notrans=1;
			else
				$notrans++;
				
			$notransaksi=str_pad($notrans,10,'0', STR_PAD_LEFT).'/'.$month.'/'.$year;
		}
		else
			$notransaksi='0000000001'.'/'.$month.'/'.$year;
		//echo $notransaksi;
		return $notransaksi;
		
	}
	
	function createnobelanja(){
		$romawi=array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V',6=>'VI',7=>'VII',8=>'VIII',9=>'IX',10=>'X',11=>'XI',12=>'XII');
		$year=date('Y');
		$month=$romawi[(int)date('m')];
		$nokostid=str_pad($this->kost_id,3,'0', STR_PAD_LEFT);
		
		$qr2=$this->db->query('select nopengajuan from tb_pengajuanbelanja where year(tglpengajuan)='.$year.' and kost_id='.$this->kost_id.' order by nopengajuan desc');
		$h2=$qr2->row_array();
		if(count($h2)>0){
			//$notransaksi=substr($h2['notransaksi'],-10);
			$nopengajuan=str_pad(substr($h2['nopengajuan'],10)+1,10,'0', STR_PAD_LEFT).'/BLJ/'.$nokostid.'/'.$month.'/'.$year;
		}
		else
			$nopengajuan='0000000001/BLJ/'.$nokostid.'/'.$month.'/'.$year;
		//echo $notransaksi;
		return $nopengajuan;
		
	}
	
	function getminyeartransaction(){
		$query = $this->db->query('select min(year(tanggaltransaksi)) as minimum from tb_transaksi');
		$a=$query->row_array();
		return $a['minimum'];
	}
	
	function getmaxyeartransaction(){
		$query = $this->db->query('select max(year(tanggaltransaksi)) as maximum from tb_transaksi');
		$a=$query->row_array();
		return $a['maximum'];
	}
	
	function namabulan($bln){
		$bulan=array("00"=>"00","01"=>"Januari", "02"=>"Februari", "03"=>"Maret", "04"=>"April", "05"=>"Mei", "06"=>"Juni", "07"=>"Juli", "08"=>"Agustus", "09"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
		return $bulan[$bln];
	}
	
	function tgl($tgl){
		$day=array("Sun"=>"Minggu", "Mon"=>"Senin", "Tue"=>"Selasa", "Wed"=>"Rabu", "Thu"=>"Kamis", "Fri"=>"Jumat", "Sat"=>"Sabtu");
		$bulan=array("00"=>"00","01"=>"Januari", "02"=>"Februari", "03"=>"Maret", "04"=>"April", "05"=>"Mei", "06"=>"Juni", "07"=>"Juli", "08"=>"Agustus", "09"=>"September", "10"=>"Oktober", "11"=>"November", "12"=>"Desember");
		$a=explode("-",$tgl);
		$bln=$bulan[$a[1]];
		return "$a[2] $bln $a[0]";
	}
	
	function tglindo($tgl){
		$a=explode("-",$tgl);
		//$bln=$bulan[$a[1]];
		return "$a[2]-$a[1]-$a[0]";
	}
	
	function getnamekost($kost_id){
		$this->db->select('kost_title');
		$this->db->where(array('kost_id'=>$kost_id));
		$qr=$this->db->get('tb_kosts');
		$h=$qr->row_array();
		if(count($h)==0)
			$nama='';
		else
			$nama=$h['kost_title'];
			
		return $nama;
	}
	
	function terbilang($jumlah){
		$angka=array(1=>'satu',2=>'dua',3=>'tiga',4=>'empat',5=>'lima',6=>'enam',7=>'tujuh',8=>'delapan',9=>'sembilan',10=>'sepuluh',11=>'sebelas',12=>'dua belas',13=>'tiga belas',14=>'empat belas',15=>'lima belas',16=>'enam belas',17=>'tujuh belas',18=>'delapan belas',19=>'sembilan belas');
		
		
		$milyar=floor($jumlah/1000000000);
		$sisa=$jumlah% 1000000000;
		$juta=floor($sisa/1000000);
		$sisa=$sisa% 1000000;
		$ribu=floor($sisa/1000);
		$sisa=$sisa% 1000;
		$ratus=floor($sisa/100);
		$sisa=$sisa% 100;
		$puluh=floor($sisa/10);
		$satuan=$sisa% 10;
		
		if($puluh==1){
			$puluh=$puluh*10+$satuan;
			$satuan=0;
		}
		
		
		$str='';
		$ratusmilyar=floor($milyar/100);
		$sisamilyar=$milyar%100;
		$puluhmilyar=floor($sisamilyar/10);
		$satuanmilyar=$sisamilyar%10;
		
		$psnratus=($ratusmilyar>0)?(($ratusmilyar==1)?'seratus ': $angka[$ratusmilyar].' ratus '):'';
		$psnpuluh=($puluhmilyar>0)?$angka[$puluhmilyar].' puluh ':'';
		$psnsatuan=($satuanmilyar>0)?$angka[$satuanmilyar].' ':'';
		
		$str.=($psnratus=='' AND $psnpuluh=='' AND $psnsatuan=='')?'':$psnratus.$psnpuluh.$psnsatuan.' milyar ';
		
		$ratusjuta=floor($juta/100);
		$sisajuta=$juta%100;
		$puluhjuta=floor($sisajuta/10);
		$satuanjuta=$sisajuta%10;
		
		if($puluhjuta==1){
			$puluhjuta=$puluhjuta*10+$satuanjuta;
			$satuanjuta=0;
		}
		
		$psnratus=($ratusjuta>0)?(($ratusjuta==1)?'seratus ':$angka[$ratusjuta].' ratus '):'';
		$psnpuluh=($puluhjuta>0)?(($puluhjuta>9)?$angka[$puluhjuta]:$angka[$puluhjuta].' puluh '):'';
		$psnsatuan=($satuanjuta>0)?$angka[$satuanjuta].' ':'';
				
		$str.=($psnratus=='' AND $psnpuluh=='' AND $psnsatuan=='')?'':$psnratus.$psnpuluh.$psnsatuan.' juta ';
		
		$ratusribu=floor($ribu/100);
		$sisaribu=$ribu%100;
		$puluhribu=floor($sisaribu/10);
		$satuanribu=$sisaribu%10;
		
		if($puluhribu==1){
			$puluhribu=$puluhribu*10+$satuanribu;
			$satuanribu=0;
		}
		
		$psnratus=($ratusribu>0)?(($ratusribu==1)?'seratus ':$angka[$ratusribu].' ratus '):'';
		$psnpuluh=($puluhribu>0)?(($puluhribu>9)?$angka[$puluhribu]:$angka[$puluhribu].' puluh '):'';
		$psnsatuan=($satuanribu>0)?(($ratusribu==0 AND $puluhribu==0 AND $satuanribu==1)?'seribu ':$angka[$satuanribu].' '):'';
		
		$str.=($psnratus=='' AND $psnpuluh=='' AND $psnsatuan=='')?'':(($ratusribu==0 AND $puluhribu==0 AND $satuanribu==1)?$psnsatuan:$psnratus.$psnpuluh.$psnsatuan.' ribu ');
		
		$str.=($ratus>0)?(($ratus==1)?'seratus ':$angka[$ratus].' ratus '):'';
		$str.=($puluh>0)?(($puluh>9)?$angka[$puluh]:$angka[$puluh].' puluh '):'';
		$str.=($satuan>0)?$angka[$satuan]:'';
		
		return $str;
		
	}
	
	function transaksikeuangan($booking_id=''){
		if($booking_id!=''){
			$x = $this->site_model->get_data('',"tb_rent_kost","rent_kost_id = '".$this->input->post('booking_id')."'")->row();
			$room = $this->site_model->get_data('',"tb_kost_room","kost_room_id = '".$x->kost_room_id."'")->row();
						
			$bookingcode 	= $x->booking_code;
			$member_id 		= $x->member_id;
			$transfer_date 	= $x->check_in;
			$kost_room_id	= $x->kost_room_id;
			$checkin		= $x->check_in;
			$checkout		= $x->check_out;
			$deposit 		= $x->total_pay;
			$kost_id		= $room->kost_id;
			$kost_room_title = $room->kost_room_title;
			$bulanan		= $room->kost_room_price_month;
			$harian			= $room->kost_room_price_day;
			/*$namfield=array();
			$namafield=array('tanggalsewakamar'=>$transfer_date,'kost_room_id'=>$kost_room_id,'member_id'=>$member_id,'tanggalmasuk'=>$checkin,'tanggalkeluar'=>$checkout,'besaran'=>$deposit);
			
			
			//$notransaksi=$this->fungsi->createnotransaksi($kost_id);
			$namafield['notransaksi']=$notransaksi;
			$this->db->insert('tb_sewakamar', $namafield);*/
			
			$tmptgl=$checkin;
			
			
			
			$qr=$this->db->get('tb_settingakun');
			$h=$qr->row_array();
			$idakun=$h['idakunsewa'];
			$idakunpajak=$h['idakunpajak'];
			
			/*$date1 = new DateTime(date('Y-m-d', strtotime($checkin)));
			$date2 = new DateTime(date('Y-m-d',strtotime($checkout)));
			//$date2 = new DateTime(date('Y-m-d',strtotime("+1 days",strtotime($tgl2))));
			$interval = $date1->diff($date2);
			
			$tahun	= $a.=$interval->y;	
			$bulan	= $a.=$interval->m;
			$harian	= $a.=$interval->d;
			
			$totbulan	= $bulan + ($tahun * 12);*/
			
			$a=$this->fm->selisih_tgl($checkin,$checkout);
			$bulan=$a['bulan'];
			$hari=$a['hari'];
			
			for($i=1;$i<$bulan;$i++){
				$pajak=($bulanan*10/100);
				$pendapatan=$bulanan-$pajak;	
				
				$namafield2=array();
				$namafield2['kost_id']=$kost_id;
				$namafield2['tanggaltransaksi']=$tmptgl;
				$namafield2['notransaksi']=$bookingcode;//$notransaksi;
				$namafield2['idakun']=$idakun;
				$namafield2['besaran']=$pendapatan;
				$namafield2['uraian']='Pendapatan sewa '.$kost_room_title.' untuk 1 bulan' ;
				$this->db->insert('tb_transaksi', $namafield2);
				$idtransaksi=$this->db->insert_id();
						
				$namafield2=array();
				$namafield2['kost_id']=$kost_id;
				$namafield2['tanggaltransaksi']=$tmptgl;
				$namafield2['notransaksi']=$bookingcode;//$notransaksi;
				$namafield2['parent_id']=$idtransaksi;
				$namafield2['idakun']=$idakunpajak;
				$namafield2['besaran']=$pajak;
				$namafield2['uraian']='Pajak sewa '.$kost_room_title.' untuk 1 bulan';
				$this->db->insert('tb_transaksi', $namafield2);			
				
				$a=explode('-',$tmptgl);
				$tmptahun	= $a[0];
				$tmpbulan	= $a[1];
				$tmphari	= $a[2];
				$b=mktime(0,0,0,((int)$tmpbulan+1),$tmphari,$tmptahun);
				$tmptgl=date('Y-m-d',$b);
			} 
			
			$total=$bulanan+($harian*$hari);
			$pajaktotal=($total*10/100);
			$pendapatantotal=$total-$pajaktotal;	
			
			$namafield2=array();
			$namafield2['kost_id']=$kost_id;
			$namafield2['tanggaltransaksi']=$tmptgl;
			$namafield2['notransaksi']=$bookingcode;//$notransaksi;
			$namafield2['idakun']=$idakun;
			$namafield2['besaran']=$pendapatantotal;
			$namafield2['uraian']='Pendapatan sewa '.$kost_room_title.' untuk 1 bulan '.$hari.' hari' ;
			$this->db->insert('tb_transaksi', $namafield2);
			$idtransaksi=$this->db->insert_id();
					
			$namafield2=array();
			$namafield2['kost_id']=$kost_id;
			$namafield2['tanggaltransaksi']=$tmptgl;
			$namafield2['notransaksi']=$bookingcode;//$notransaksi;
			$namafield2['parent_id']=$idtransaksi;
			$namafield2['idakun']=$idakunpajak;
			$namafield2['besaran']=$pajaktotal;
			$namafield2['uraian']='Pajak sewa '.$kost_room_title.' untuk 1 bulan '.$hari.' hari' ;
			$this->db->insert('tb_transaksi', $namafield2);			
			
					
			
			
			
		}
	}
	
	function cetakkwitansi($rent_kost_id=''){
		if($rent_kost_id!=''){

			$this->db->select('tb_confirms.booking_code, tb_members.member_name, tb_kosts.kost_title, tb_kost_room.kost_room_title, tb_rent_kost.check_in, tb_rent_kost.check_out, tb_confirms.deposit, tb_confirms.transfer_date');
			$this->db->join('tb_confirms','tb_rent_kost.booking_code=tb_confirms.booking_code');
			$this->db->join('tb_kosts','tb_kosts.kost_id=tb_confirms.kost_id');
			$this->db->join('tb_kost_room','tb_kost_room.kost_room_id=tb_confirms.kost_room_id');
			$this->db->join('tb_members','tb_members.member_id=tb_confirms.member_id');
			$this->db->where(array('tb_rent_kost.rent_kost_id'=>$rent_kost_id));
			$qr=$this->db->get('tb_rent_kost');
			$h=$qr->row_array();
			
			$temp=explode(' ',$h['transfer_date']);
			$transfer_date=$this->tglindo($temp[0]);
			$data['petugas'] 		 = $this->session->userdata('owner_username');
			$data['member_name'] 	 = $h['member_name'];
			$data['notransaksi'] 	 = $h['booking_code'];
			$data['kost_title'] 	 = $h['kost_title'];
			$data['kost_room_title'] = $h['kost_room_title'];
			$data['transfer_date'] 	 = $transfer_date;
			$data['checkin']		 = $this->tgl($h['check_in']);
			$data['checkout']		 = $this->tgl($h['check_out']);
			$deposit 				 = $h['deposit'];
			$data['deposit'] 		 = number_format($deposit,0,',','.');
			$data['terbilang']		 = $this->terbilang($deposit).' rupiah';
			
			$this->load->library('pdf');
     		$html = $this->load->view('owner/accounting/sewa/cetakinvoice',$data,TRUE);
     		$this->pdf->pdf_create($html,'invoice');
		
		}
	}
	
	
}