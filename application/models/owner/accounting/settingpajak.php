<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class settingpajak extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
		$this->load->model('owner/accounting/mfunction','fungsi');
		$this->content['namakost']=$this->fungsi->getnamekost($this->kost_id);
    }
	
	function getakun(){
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		//$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_akun');
		return $qr->result_array();	
	}
	
	function tampil(){
		//$this->db->where(array('kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		
		if(count($h)>0){
			$idakunpajak=$h['idakunpajak'];
			$idakunsewa=$h['idakunsewa'];
			$idakunpengeluaran=$h['idakunpengeluaran'];
			$idakunpendapatanlain=$h['idakunpendapatanlain'];
			$idakundiskon=$h['idakundiskon'];
			$idsetting=$h['idsetting'];
			$a['idsetting']=$idsetting;
			$this->db->where(array('tb_akun.idakun'=>$idakunpajak));
			$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
			$qr=$this->db->get('tb_akun');
			$h=$qr->row_array();
			if(count($h)>0)
				$a['akunpajak']=$h['kodejenisakun'].'-'.$h['kodeakun'];
			else
				$a['akunpajak']='';
			
			$this->db->where(array('tb_akun.idakun'=>$idakunsewa));
			$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
			$qr=$this->db->get('tb_akun');
			$h=$qr->row_array();
			if(count($h)>0)
				$a['akunsewa']=$h['kodejenisakun'].'-'.$h['kodeakun'];
			else
				$a['akunsewa']='';
				
			$this->db->where(array('tb_akun.idakun'=>$idakunpengeluaran));
			$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
			$qr=$this->db->get('tb_akun');
			$h=$qr->row_array();
			if(count($h)>0)
				$a['akunpengeluaran']=$h['kodejenisakun'].'-'.$h['kodeakun'];
			else
				$a['akunpengeluaran']='';
			
			$this->db->where(array('tb_akun.idakun'=>$idakundiskon));
			$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
			$qr=$this->db->get('tb_akun');
			$h=$qr->row_array();
			if(count($h)>0)
				$a['akundiskon']=$h['kodejenisakun'].'-'.$h['kodeakun'];
			else
				$a['akundiskon']='';
					
			$this->db->where(array('tb_akun.idakun'=>$idakunpendapatanlain));
			$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
			$qr=$this->db->get('tb_akun');
			$h=$qr->row_array();
			if(count($h)>0)
				$a['akunpendapatanlain']=$h['kodejenisakun'].'-'.$h['kodeakun'];
			else
				$a['akunpendapatanlain']='';
				
				
		}else
			$a=array();
		$this->content['settingpajak']=$a;
		$this->content['content']='owner/accounting/settingpajak/tampilsettingpajak';
		$this->load->view('owner/template',$this->content);
	}
	
	function input(){
		$this->content['akun']=$this->getakun();
		$this->content['mode']='input';
		$this->content['idakunpajak']='';
		$this->content['idakunsewa']='';
		$this->content['idakunpengeluaran']='';
		$this->content['idakundiskon']='';
	
		$this->content['content']='owner/accounting/settingpajak/forminputsettingpajak';
		$this->load->view('owner/template',$this->content);
	}
	
	function edit(){
		
		$idsetting=$this->uri->segment(6);
		$this->db->where(array('idsetting'=>$idsetting));
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner/accounting/'.$this->uri->segment(3).'/settingpajak','refresh');
		else{
			$this->content['akun']=$this->getakun();
			$this->content['mode']='edit';
			$this->content['idsetting']=$h['idsetting'];
			$this->content['idakunpajak']=$h['idakunpajak'];
			$this->content['idakunsewa']=$h['idakunsewa'];
			$this->content['idakunpendapatanlain']=$h['idakunpendapatanlain'];
			$this->content['idakunpengeluaran']=$h['idakunpengeluaran'];
			$this->content['idakundiskon']=$h['idakundiskon'];
		
			$this->content['content']='owner/accounting/settingpajak/forminputsettingpajak';
			$this->load->view('owner/template',$this->content);
		}
	}
	
	/*function hapus(){
		$idakun=$this->uri->segment(5);
		$this->db->where(array('idakun'=>$idakun));
		$qr=$this->db->get('tb_akun');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner/accounting/akun','refresh');
		else{
			$where=array('idakun'=>$idakun);
			$this->db->delete('tb_akun', $where); 
			redirect(base_url().'owner/accounting/akun','refresh');
		}
	}*/
	
	function proses(){
		$mode=$this->input->post('mode');
		$idsetting=$this->input->post('idsetting');
		$idjenisakunpajak=$this->input->post('idjenisakunpajak');
		$idjenisakunsewa=$this->input->post('idjenisakunsewa');
		$idakunpendapatanlain=$this->input->post('idakunpendapatanlain');
		$idjenisakunpengeluaran=$this->input->post('idjenisakunpengeluaran');
		$idakundiskon=$this->input->post('idakundiskon');

		$this->content['akun']=$this->getakun();
		$this->content['mode']=$mode;
		$this->content['idjenisakunpajak']=$idjenisakunpajak;
		$this->content['idjenisakunsewa']=$idjenisakunsewa;
		$this->content['idakunpendapatanlain']=$idakunpendapatanlain;
		$this->content['idjenisakunpengeluaran']=$idjenisakunpengeluaran;
		$this->content['idakundiskon']=$idakundiskon;
		
		$this->form_validation->set_rules('idjenisakunpajak', 'Jenis akun pajak', 'required');
		$this->form_validation->set_rules('idjenisakunsewa', 'Jenis akun sewa kamar', 'required');
		$this->form_validation->set_rules('idakunpendapatanlain', 'Jenis akun pendapatan lain', 'required');
		$this->form_validation->set_rules('idjenisakunpengeluaran', 'Jenis akun pengeluaran kost', 'required');
		$this->form_validation->set_rules('idakundiskon', 'Jenis akun diskon kost', 'required');
		$this->form_validation->set_message('required', '%s masih kosong');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->content['content']='owner/accounting/settingpajak/forminputsettingpajak';
			$this->load->view('owner/template',$this->content);
		}else{
			
			$namfield=array();
			$namafield=array('idakunpajak'=>$idjenisakunpajak,'idakunsewa'=>$idjenisakunsewa,'idakunpendapatanlain'=>$idakunpendapatanlain,'idakunpengeluaran'=>$idjenisakunpengeluaran,'idakundiskon'=>$idakundiskon);
					
			if($mode=='input'){
				$this->db->insert('tb_settingakun', $namafield);
			}
			else{
				$where=array('idsetting'=>$idsetting);
				$this->db->where($where);
				$this->db->update('tb_settingakun', $namafield);
			}
			redirect(base_url().'owner/accounting/'.$this->uri->segment(3).'/setting','refresh');
		}
	}
}