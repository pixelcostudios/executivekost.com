<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class jenisakun extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
		$this->load->model('owner/accounting/mfunction','fungsi');
		$this->content['namakost']=$this->fungsi->getnamekost($this->kost_id);
    }
	
	function tampiljenisakun(){
		//$this->db->where(array('kost_id'=>$this->kost_id));
		$this->db->order_by('kodejenisakun','asc');
		$qr=$this->db->get('tb_jenisakun');
		$h=$qr->result_array();
		$this->content['jenisakun']=$h;
		$this->content['content']='owner/accounting/jenisakun/tampiljenisakun';
		$this->load->view('owner/template',$this->content);
	}
	
	function inputjenisakun(){
		$this->content['mode']='input';
		$this->content['idjenisakun']='';
		$this->content['kodejenisakun']='';
		$this->content['namajenisakun']='';
		
		$this->content['content']='owner/accounting/jenisakun/forminputjenisakun';
		$this->load->view('owner/template',$this->content);
	}
	
	function editjenisakun(){
		
		$idjenisakun=$this->uri->segment(6);
		$this->db->where(array('idjenisakun'=>$idjenisakun));
		$qr=$this->db->get('tb_jenisakun');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/jenisakun','refresh');
		else{
			$this->content['mode']='edit';
			$this->content['idjenisakun']=$h['idjenisakun'];
			$this->content['kodejenisakun']=$h['kodejenisakun'];
			$this->content['namajenisakun']=$h['namajenisakun'];
		
			$this->content['content']='owner/accounting/jenisakun/forminputjenisakun';
			$this->load->view('owner/template',$this->content);
		}
	}
	
	function hapusjenisakun(){
		$idjenisakun=$this->uri->segment(6);
		$this->db->where(array('idjenisakun'=>$idjenisakun));
		$qr=$this->db->get('tb_jenisakun');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/jenisakun','refresh');
		else{
			$where=array('idjenisakun'=>$idjenisakun);
			$this->db->delete('tb_jenisakun', $where); 
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/jenisakun','refresh');
		}
	}
	
	function prosesjenisakun(){
		$mode=$this->input->post('mode');
		$idjenisakun=$this->input->post('idjenisakun');
		$kodejenisakun=$this->input->post('kodejenisakun');
		$namajenisakun=$this->input->post('namajenisakun');
		
		$this->content['mode']=$mode;
		$this->content['idjenisakun']=$idjenisakun;
		$this->content['kodejenisakun']=$kodejenisakun;
		$this->content['namajenisakun']=$namajenisakun;
				
		$this->form_validation->set_rules('kodejenisakun', 'Kode jenis akun', 'required');
		$this->form_validation->set_rules('namajenisakun', 'Nama jenis akun', 'required');
		$this->form_validation->set_message('required', '%s masih kosong');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->content['content']='owner/accounting/jenisakun/forminputjenisakun';
			$this->load->view('owner/template',$this->content);
		}else{
			
			$namfield=array();
			$namafield=array('kodejenisakun'=>$kodejenisakun,'namajenisakun'=>$namajenisakun);
					
			if($mode=='input'){
				$namafield['kost_id']=$this->kost_id;
				$this->db->insert('tb_jenisakun', $namafield);
			}
			else{
				$where=array('idjenisakun'=>$idjenisakun);
				$this->db->where($where);
				$this->db->update('tb_jenisakun', $namafield);
			}
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/jenisakun','refresh');
		}
	}
}