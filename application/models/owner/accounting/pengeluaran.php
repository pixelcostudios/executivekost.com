<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class pengeluaran extends CI_Model{
	var $content;
	var $kost_id;
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->kost_id=$this->uri->segment(3);
		$this->load->model('owner/accounting/mfunction','fungsi');
		$this->content['namakost']=$this->fungsi->getnamekost($this->kost_id);
    }
	
	function getjenisakun(){
		$this->db->order_by('kodejenisakun','asc');
		//$this->db->where(array('kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_jenisakun');
		return $qr->result_array();	
	}
	
	function tampilpengeluaran(){
		$hariantransaksi=trim($this->input->post('hariantransaksi'));
		$bulanbulanan=trim($this->input->post('bulanbulanan'));
		$tahunbulanan=trim($this->input->post('tahunbulanan'));
		$laintransaksi1=trim($this->input->post('laintransaksi1'));
		$laintransaksi2=trim($this->input->post('laintransaksi2'));
		$selectpilih=trim($this->input->post('selectpilih'));
		
		if(!preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $hariantransaksi))
			$hariantransaksi=date('Y-m-d');
		if(!preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $laintransaksi1))
			$laintransaksi1=date('Y-m-d');
		if(!preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $laintransaksi2))
			$laintransaksi2=date('Y-m-d');
		if($bulanbulanan=='')
			$bulanbulanan=date('m');
		if($tahunbulanan=='')
			$tahunbulanan=date('Y');
		if($selectpilih!='harian' AND $selectpilih!='bulanan' AND $selectpilih!='lainnya')
			$selectpilih='harian';
		/*$this->db->order_by('tb_jenisakun.kodejenisakun','asc');
		$this->db->order_by('tb_akun.kodeakun','asc');
		$this->db->join('tb_jenisakun','tb_akun.idjenisakun=tb_jenisakun.idjenisakun');
		$this->db->where(array('tb_jenisakun.kost_id'=>$this->kost_id));
		$qr=$this->db->get('tb_akun');
		$h=$qr->result_array();
		$this->content['akun']=$h;*/
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		$idakunpengeluaran=$h['idakunpengeluaran'];
		
		$this->db->order_by('tb_transaksi.tanggaltransaksi','asc');
		//$this->db->where(array('kost_id'=>$this->kost_id,'idakun'=>$idakunpengeluaran));
		if($selectpilih=='harian')	
			$this->db->where(array('tb_transaksi.kost_id'=>$this->kost_id, 'idakun'=>$idakunpengeluaran, 'tb_transaksi.tanggaltransaksi'=>$hariantransaksi));
		elseif($selectpilih=='bulanan')
			$this->db->where(array('tb_transaksi.kost_id'=>$this->kost_id, 'idakun'=>$idakunpengeluaran, 'month(tb_transaksi.tanggaltransaksi)'=>$bulanbulanan,'year(tb_transaksi.tanggaltransaksi)'=>$tahunbulanan));
		else
			$this->db->where(array('tb_transaksi.kost_id'=>$this->kost_id, 'idakun'=>$idakunpengeluaran, 'tb_transaksi.tanggaltransaksi >='=>$laintransaksi1,'tb_transaksi.tanggaltransaksi <='=>$laintransaksi2));
		
		$this->db->order_by('tb_transaksi.tanggaltransaksi','tb_transaksi.notransaksi asc');
		$this->db->order_by('tb_transaksi.notransaksi','asc');
		$qr=$this->db->get('tb_transaksi');
		$h=$qr->result_array();
		
		$a=array();
		$total=0;
		foreach($h as $list){
			$b=array();
			$b['idtransaksi']=$list['idtransaksi'];
			$b['tanggaltransaksi']=$this->fungsi->tgl($list['tanggaltransaksi']);
			$b['notransaksi']=$list['notransaksi'];
			$b['uraian']=$list['uraian'];
			$b['besaran']=number_format($list['besaran'],0,",",".");
			$total+=$list['besaran'];
			$a[]=$b;
		}
		
		
		$this->content['minimum']=$this->fungsi->getminyeartransaction();
		$this->content['maximum']=$this->fungsi->getmaxyeartransaction();
		$this->content['hariantransaksi']=$hariantransaksi;
		$this->content['bulanbulanan']=$bulanbulanan;
		$this->content['tahunbulanan']=$tahunbulanan;
		$this->content['laintransaksi1']=$laintransaksi1;
		$this->content['laintransaksi2']=$laintransaksi2;
		$this->content['selectpilih']=$selectpilih;
		
		$this->content['pengeluaran']=$a;
		$this->content['total']=number_format($total,0,",",".");
		$this->content['content']='owner/accounting/pengeluaran/tampilpengeluaran';
		$this->load->view('owner/template',$this->content);
	}
	
	function inputpengeluaran(){
		$this->content['mode']='input';
		$this->content['idtransaksi']='';
		$this->content['tanggaltransaksi']=date('Y-m-d');
		$this->content['notransaksi']='';
		$this->content['uraian']='';
		$this->content['besaran']='';
		
		$this->content['content']='owner/accounting/pengeluaran/forminputpengeluaran';
		$this->load->view('owner/template',$this->content);
	}
	
	function editpengeluaran(){
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		$idakunpengeluaran=$h['idakunpengeluaran'];
		
		$idtransaksi=$this->uri->segment(6);
		$this->db->where(array('idtransaksi'=>$idtransaksi,'idakun'=>$idakunpengeluaran));
		$qr=$this->db->get('tb_transaksi');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner/accounting/pengeluaran','refresh');
		else{
			$this->content['mode']='edit';
			$this->content['idtransaksi']=$h['idtransaksi'];
			$this->content['tanggaltransaksi']=$h['tanggaltransaksi'];
			$this->content['notransaksi']=$h['notransaksi'];
			$this->content['uraian']=$h['uraian'];
			$this->content['besaran']=$h['besaran'];
			
			$this->content['content']='owner/accounting/pengeluaran/forminputpengeluaran';
			$this->load->view('owner/template',$this->content);
		}
	}
	
	function deletepengeluaran(){
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		$idakunpengeluaran=$h['idakunpengeluaran'];
		
		$idtransaksi=$this->uri->segment(6);
		$this->db->where(array('idtransaksi'=>$idtransaksi,'idakun'=>$idakunpengeluaran));
		$qr=$this->db->get('tb_transaksi');
		$h=$qr->row_array();
		if(count($h)==0)
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/pengeluaran','refresh');
		else{
			$where=array('idtransaksi'=>$idtransaksi);
			$this->db->delete('tb_transaksi', $where); 
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/pengeluaran','refresh');
		}
	}
	
	function prosespengeluaran(){
		$qr=$this->db->get('tb_settingakun');
		$h=$qr->row_array();
		$idakunpengeluaran=$h['idakunpengeluaran'];
		
		$mode=$this->input->post('mode');
		$idtransaksi=$this->input->post('idtransaksi');
		$tanggaltransaksi=$this->input->post('tanggaltransaksi');
		$notransaksi=$this->input->post('notransaksi');
		$uraian=$this->input->post('uraian');
		$besaran=$this->input->post('besaran');

		$this->content['mode']=$mode;
		$this->content['idtransaksi']=$idtransaksi;
		$this->content['tanggaltransaksi']=$tanggaltransaksi;
		$this->content['notransaksi']=$notransaksi;
		$this->content['uraian']=$uraian;
		$this->content['besaran']=$besaran;
		
		$this->form_validation->set_rules('tanggaltransaksi', 'Tanggal transaksi', 'required');
		$this->form_validation->set_rules('notransaksi', 'No transaksi', 'required');
		$this->form_validation->set_rules('uraian', 'Uraian', 'required');
		$this->form_validation->set_rules('besaran', 'Besaran', 'required');
		$this->form_validation->set_message('required', '%s masih kosong');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->content['content']='owner/accounting/pengeluaran/forminputpengeluaran';
			$this->load->view('owner/template',$this->content);
		}else{
			
			$namfield=array();
			$namafield=array('tanggaltransaksi'=>$tanggaltransaksi,'notransaksi'=>$notransaksi,'uraian'=>$uraian,'besaran'=>$besaran,'kost_id'=>$this->kost_id,'idakun'=>$idakunpengeluaran);
					
			if($mode=='input'){
				$this->db->insert('tb_transaksi', $namafield);
			}
			else{
				$where=array('idtransaksi'=>$idtransaksi);
				$this->db->where($where);
				$this->db->update('tb_transaksi', $namafield);
			}
			redirect(base_url().'owner/accounting/'.$this->kost_id.'/pengeluaran','refresh');
		}
	}
}