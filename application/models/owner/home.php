<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Model{
	var $url = 'owner/';
	var $content;
	function __construct(){
		parent::__construct();
		$this->load->model('site_model','',TRUE);
	}
	
	/* ------------------
	   index
	------------------ */
	function index(){
		/* ------------------
		   order check
		------------------ */
		$this->db->where('rent_status', '1');
		$order_check = $this->db->get('tb_rent_kost')->result();
		foreach($order_check as $x){
			$now = date("Y-m-d H:i:s");
			if(strtotime("".$x->time_order."+90 minutes") <= strtotime($now)){
				$this->db->where('rent_kost_id', $x->rent_kost_id);
				$this->db->update('tb_rent_kost', array('rent_status'=>'4'));
			}
		}
		
		$order_check2 = $this->site_model->get_data('',"tb_rent_kost","rent_status = '2'")->result();
		foreach($order_check as $x){
			if($x->check_out < date("Y-m-d"))
			$this->site_model->update_data("tb_rent_kost",array('rent_status'=>'3'),"rent_kost_id = '".$x->rent_kost_id."'");
		}
		
		/* ------------------
		   send birthday
		------------------ */
		$option 	= $this->site_model->get_data('',"tb_options","option_id = '1'")->row();
		$message 	= $this->site_model->get_data('',"tb_notifications","notification_id = '4'")->row();
		$today = date("Y-m-d");
		$where = "member_birthday = '".$today."'";
		$member = $this->site_model->get_data('',"tb_members",$where)->result();
		foreach($member as $a){
			$this->email->from($option->email, 'Executive Kost');
			$this->email->to($a->member_email);
			$this->email->subject("Happy Birthday ".$a->member_name);
			$this->email->message($message->notification_value);					
			$this->email->send();
		}
		
		/* ------------------
		   data order
		------------------ */
		$this->content['h3_title']		= 'daftar reservasi executive kost';
		$this->content['content'] 		= 'owner/home';
		
		$uri = 3;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
				
		$select = "rk.rent_kost_id,rk.booking_code,m.member_id,m.member_name,k.kost_title,kr.kost_room_number,rk.time_order,rk.check_in,rk.rent_status";
		$table = "tb_rent_kost rk ";
		$table.= "LEFT JOIN tb_members m ON m.member_id = rk.member_id ";
		$table.= "LEFT JOIN tb_kost_room kr ON kr.kost_room_id = rk.kost_room_id ";
		$table.= "LEFT JOIN tb_kosts k ON k.kost_id = kr.kost_id ";
		$table.= "LEFT JOIN tb_user_kost uk ON uk.kost_id = k.kost_id";
		$where = "uk.user_id = '".$this->session->userdata('owner_id')."' AND rk.rent_status != '3' AND rk.rent_status != '4'";
		$order = $this->site_model->get_data($select,$table,$where,"rk.rent_kost_id","rk.time_order DESC","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data($select,$table,$where,"rk.rent_kost_id","rk.time_order DESC")->num_rows();	
		
		if($num_rows > 0){
			// Set heading
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No','Order.Code','Name','Date Order','Check In','Kost Name','Room','Option');
			
			$i = 0 + $offset;
			foreach($order as $o){
				$i++;
				$time_order = fdate($o->time_order);
				$check_in = fdate($o->check_in,2);
				if($o->rent_status == '2'){
					$cell = array('data'=>'<span class="paid">'.$o->member_name.'</span>');
				}
				else {
					$confirm = $this->site_model->get_data('',"tb_confirms","rent_kost_id = '".$o->rent_kost_id."'")->num_rows();
					if($confirm == '1')
						$cell = array('data'=>'<span class="confirm">'.$o->member_name.'</span>');
					else
						$cell = array('data'=>'<span class="pending">'.$o->member_name.'</span>');
				}
				
				//if($o->rent_status == '1') $cell2 = array('data'=>$time_order,'style'=>'background-color: #FFA500; color: #FFF;');
				//if($o->rent_status == '2') $cell2 = array('data'=>$time_order,'style'=>'background-color: #00FF00; color: #FFF;');
				// actions
				if($this->session->userdata('ses_owner_level')!='investor'){
					$actions = anchor(base_url().$this->url.'check/'.$o->rent_kost_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor(base_url().$this->url.'delete/'.$o->rent_kost_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"));
				}
				else{
					$actions = '';
				}
					
				$this->table->add_row($i,'#'.$o->booking_code,$cell,$time_order,$check_in,$o->kost_title,array('data'=>$o->kost_room_number,'align'=>'center'),array('data'=>$actions,'align'=>'center'));
			}
			
			$config['base_url']				= base_url().$this->url.'/home';
			$config['total_rows']			= $num_rows;
			$config['per_page']				= $limit;
			$config['uri_segment']			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination']	= $this->pagination->create_links();
			$this->content['table'] 		= $this->table->generate();
		}
		else{
			$this->content['message'] = 'Empty Data!';
		}
		
		$this->load->view('owner/template',$this->content);
	}
	
	/* ------------------
	   search
	------------------ */
	function search(){
		$this->content['h3_title']		= 'result ticket';
		$this->content['content'] 		= 'ticket/home';
		
		if($this->input->post("search")!=""){
			$hasilcari = $this->site_model->search("tb_booking",$this->input->post("type"),$this->input->post("search"))->result();		
			if(count($hasilcari)>0){	
				// Set template tabel, untuk efek selang-seling tiap baris
				$tmpl = array('table_open'=>'<table class="table_list">','row_alt_start'=>'<tr class="zebra">','row_alt_end'=>'</tr>');
				$this->table->set_template($tmpl);
		
				// Set heading untuk tabel
				$this->table->set_empty("&nbsp;");
				$this->table->set_heading('Order.Code','Name','Kost Name','Room Number','Date Order','Option');
				
				$i = 0 + $offset;
				foreach($order as $o){
					$time_order = date("d F Y H:i:s", strtotime($o->time_order));
									
					$this->table->add_row('#'.$o->booking_code,$o->member_name,$o->kost_title,$o->kost_room_number,$time_order,
						anchor(base_url().$this->url.'check/'.$o->booking_id,'check',array('class'=>'edit')).' '.
						anchor(base_url().$this->url.'delete/'.$o->booking_id,'delete',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')")));
				}
				$this->content['table'] = $this->table->generate();
			}
			else {
				$this->content['message'] = 'Sorry, no data found';
			}
		}
		else {
			$this->content['message'] = 'Sorry, no data found';
		}
		$this->load->view('owner/template',$this->content);
	}
	
	/* ------------------
	   check
	------------------ */
	function check(){
		$id = $this->uri->segment(3);
		$this->content['h3_title']		= 'check order';
		$this->content['content']		= 'owner/check';
		$this->content['action']		= base_url().$this->url.'input';
		$this->session->set_userdata('form_mode', 'edit');
		
		$select = "rk.rent_kost_id,rk.booking_code,rk.time_order,rk.check_in,rk.check_out,rk.total_pay,rk.rent_status,rk.special_request,";
		$select.= "m.member_id,m.member_name,m.member_identity,m.member_identity_number,m.member_birthday,m.member_phone,m.member_email,m.member_address,";
		$select.= "k.kost_title,kr.kost_room_number";
		$table = "tb_rent_kost rk ";
		$table.= "LEFT JOIN tb_members m ON m.member_id = rk.member_id ";
		$table.= "LEFT JOIN tb_kost_room kr ON kr.kost_room_id = rk.kost_room_id ";
		$table.= "LEFT JOIN tb_kosts k ON k.kost_id = kr.kost_id ";
		$order = $this->site_model->get_data($select,$table,"rk.rent_kost_id = '".$id."'")->row();
		// order information
		$this->content['rent_kost_id']			 	= $order->rent_kost_id;
		$this->content['booking_code'] 				= $order->booking_code;
		$this->content['time_order'] 				= $order->time_order;
		$this->content['check_in'] 					= $order->check_in;
		$this->content['check_out']					= $order->check_out;
		$this->content['total_pay']					= $order->total_pay;
		$this->content['rent_status']				= $order->rent_status;
		$this->content['special_request']			= $order->special_request;
		$this->content['kost_title'] 				= $order->kost_title;
		$this->content['kost_room_number']			= $order->kost_room_number;
		// member information
		$this->content['member_id']					= $order->member_id;
		$this->content['member_name']				= $order->member_name;
		$this->content['member_identity']			= $order->member_identity;
		$this->content['member_identity_number']	= $order->member_identity_number;
		$this->content['member_birthday']			= $order->member_birthday;
		$this->content['member_phone'] 				= $order->member_phone;
		$this->content['member_email'] 				= $order->member_email;
		$this->content['member_address']			= $order->member_address;
		
		$this->load->view('owner/template',$this->content);			
	}
	
	/* ------------------
	   finish
	------------------ */
	function finish(){
		$this->site_model->update_data("tb_rent_kost",array('rent_status'=>'2'),"rent_kost_id = '".$this->input->post('booking_id')."'");
		$this->load->model('owner/accounting/mfunction','fungsi');
		$this->fungsi->transaksikeuangan($this->input->post('booking_id'));
/*		$x = $this->site_model->get_data('',"tb_rent_kost","rent_kost_id = '".$this->input->post('booking_id')."'")->row();
		$room = $this->site_model->get_data('',"tb_kost_room","kost_room_id = '".$x->kost_room_id."'")->row();
		$bookingcode = $x->booking_code;
		$confirm = $this->site_model->get_data('',"tb_confirms","booking_code = '".$bookingcode."'")->row();
		if(count($confirm)!='0'){
		}
		else
			echo "<script>alert('asd')</script>";*/			
				
		redirect(base_url().'/owner/home');
	}
	
	/* ------------------
	   change date
	------------------ */
	function update(){
		$id = $this->input->post('rent_kost_id');
		$this->db->where('rent_kost_id', $id);
		$data = array(
			'check_in'	=> $this->input->post('check_in'),
			'check_out'	=> $this->input->post('check_out')
		);
		$this->db->update('tb_rent_kost', $data);
		$this->session->set_flashdata('message', 'Data has been update.');
		redirect(base_url().'owner/check/'.$id);
	}
	
	/* ------------------
	   checkout
	------------------ */
	function checkout(){
		$this->db->where('rent_kost_id', $this->uri->segment(3));
		$this->db->update('tb_rent_kost', array('rent_status'=>'3'));
		$this->session->set_flashdata('message', 'Success Check Out.');
		redirect(base_url().'owner/home');
	}
	
	/* ------------------
	   delete order
	------------------ */
	function delete(){
		$this->site_model->del_data("tb_rent_kost","rent_kost_id = '".$this->uri->segment(3)."'");
		redirect(base_url().'/owner/home');
	}
}