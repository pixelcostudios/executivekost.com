<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting extends CI_Model{
	var $url = "backend/setting/";
	var $content;
	function __construct(){
		parent::__construct();
	}
	// edit setting
	function general(){
		$this->content['h1_title'] 		= 'edit setting "title", "description", "keyword" web.';
		$this->content['content']		= "backend/setting/setting_form";
		$this->content['action'] 		= base_url().$this->url."update_setting";
		
		/*$option = $this->site_model->get_data('',"tb_option","option_type = '1'")->result();
		foreach($option as $a){
			$this->content['default'][$a->option_name] = $a->option_value;
		}*/		
		$option = $this->site_model->get_data('',"tb_options","option_id = '1'")->row();
		
		$this->session->set_userdata('option_id', $option->option_id);
		$this->content['default']['title'] 			= $option->title;
		$this->content['default']['title_en'] 		= $option->title_en;
		$this->content['default']['description'] 	= $option->des;
		$this->content['default']['description_en'] = $option->des_en;
		$this->content['default']['keyword'] 		= $option->keyword;
		$this->content['default']['keyword_en'] 	= $option->keyword_en;
		$this->content['default']['email_admin'] 	= $option->email;
		$this->content['default']['image'] 			= $option->image;
		$this->content['default']['op_1'] 			= $option->op_1;
		$this->content['default']['op_2'] 			= $option->op_2;
		$this->content['default']['facebook'] 		= $option->facebook;
		$this->content['default']['twitter'] 		= $option->twitter;		
		$this->load->view('backend/template',$this->content);			
	}
	// update setting
	function update_setting(){
		$id = $this->session->userdata('option_id');
		$this->content['h1_title'] 		= 'edit setting "title", "description", "keyword" web.';
		$this->content['content']		= "backend/setting/setting_form";
		$this->content['action'] 		= base_url().$this->url.'update_setting';
		
		$this->form_validation->set_rules('email', 'email', '');
		$this->form_validation->set_rules('title', 'title', '');
		$this->form_validation->set_rules('title_en', 'title_en', '');
		$this->form_validation->set_rules('des', 'des', '');	
		$this->form_validation->set_rules('des_en', 'des_en', '');	
		$this->form_validation->set_rules('keyword', 'keyword', '');
		$this->form_validation->set_rules('keyword_en', 'keyword_en', '');
		$this->form_validation->set_rules('op_1', 'op_1', '');
		$this->form_validation->set_rules('op_2', 'op_2', '');
		$this->form_validation->set_rules('facebook', 'facebook', '');
		$this->form_validation->set_rules('twitter', 'twitter', '');
		
		$type_file = "gif|jpg|jpeg|png|swf";
			
		if(!empty($_FILES['image']['name'])){
			$config['upload_path'] 		= "./uploads/";
			$config['allowed_types'] 	= $type_file;
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('image')){
				$this->content['message'] = $this->upload->display_errors('','');
				$this->load->view('backend/template',$this->content);
			}	
			else {
				if($this->input->post("oldimg")!=''){
					$image = $this->site_model->get_data('',"tb_options","option_id = '$id'")->row();
					$del	= "./uploads/".$image->image;
					$del2	= "./uploads/".thumb($image->image);
					if(file_exists($del)) unlink($del);
					if(file_exists($del2)) unlink($del2);
				}
				$cek = $this->fm->upload_img($type_file,$this->input->post("title"));
				$namafile = slug($this->input->post("title")).$cek['ext'];
				$data = array(
					'title'			=> $this->input->post("title"),
					'title_en'		=> $this->input->post("title_en"),
					'des'			=> $this->input->post("des"),
					'des_en'		=> $this->input->post("des_en"),
					'keyword'		=> $this->input->post("keyword"),
					'keyword_en'	=> $this->input->post("keyword_en"),
					'email'			=> $this->input->post("email"),
					'op_1'			=> $this->input->post("op_1"),
					'op_2'			=> $this->input->post("op_2"),
					'facebook'		=> $this->input->post("facebook"),
					'twitter'		=> $this->input->post("twitter"),
					'image'			=> $namafile);
				// save to database
				$this->site_model->update_data("tb_options",$data,"option_id = '$id'");						
				$this->session->set_flashdata('message', 'setting has been update!');
				$this->session->unset_userdata('option_id');
				redirect(base_url().$this->url."general/");
			}	
		}
		else{
			$data = array(
				'title'			=> $this->input->post("title"),
				'title_en'		=> $this->input->post("title_en"),
				'des'			=> $this->input->post("des"),
				'des_en'		=> $this->input->post("des_en"),
				'keyword'		=> $this->input->post("keyword"),
				'keyword_en'	=> $this->input->post("keyword_en"),
				'email'			=> $this->input->post("email"),
				'op_1'			=> $this->input->post("op_1"),
				'op_2'			=> $this->input->post("op_2"),
				'facebook'		=> $this->input->post("facebook"),
				'twitter'		=> $this->input->post("twitter"));
			// save to database
			$this->site_model->update_data("tb_options",$data,"option_id = $id");						
			$this->session->set_flashdata('message', 'setting has been update!');
			$this->session->unset_userdata('option_id');
			redirect(base_url().$this->url."general/");
		}
	}
}