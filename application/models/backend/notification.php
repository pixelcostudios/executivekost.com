<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Notification extends CI_Model{
	var $url;
	var $content;
	function __construct(){
		parent::__construct();
		$this->url = base_url()."backend/setting/notification/";
	}
	
	function index(){
		$this->content['h1_title']		= "Notivication";
		$this->content['content'] 		= "backend/default";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url.'add';
		
		$uri = 4;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$data 		= $this->site_model->get_data('',"tb_notifications",'','',"notification_id desc","$limit,$offset")->result();
		$num_rows 	= $this->site_model->get_data('',"tb_notifications",'')->num_rows();
		
		if($num_rows > 0){
			// Set heading
			$this->table->set_empty("&nbsp;");
			$no 		= array('data'=>'No','width'=>'40');
			$actions 	= array('data'=>'Actions','width'=>'10%');
			$value1 	= array('data'=>'Value','width'=>'45%');
			$value2 	= array('data'=>'','width'=>'45%');
			$this->table->set_heading($no,'Name',$value1,$value2,$actions);
			
			$i = 0 + $offset;
			foreach($data as $a){		
				$this->table->add_row(
					++$i.'.',$a->notification_name,$a->notification_value,$a->notification_value_en,
					anchor($this->url.'edit/'.$a->notification_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor($this->url.'delete/'.$a->notification_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"))
				);
			}
			
			$config['base_url'] 			= $this->url;
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination'] 	= $this->pagination->create_links();
			$this->content['table'] 		= $this->table->generate();
		}
		else{
			$this->content['message'] = "Empty Data!";
		}		
		$this->load->view('backend/template',$this->content);
	}
	// add page
	function add(){
		$this->content['h1_title']		= "Notivication";
		$this->content['content'] 		= "backend/notification/notification_form";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url.'add';
		$this->content['action'] 		= $this->url."input";
		$this->content['images'] 		= array();
		$this->session->set_userdata('form_mode', 'add');
		$this->session->set_userdata('check_title', "notification_name/tb_notifications/");
		
		$this->load->view('backend/template',$this->content);
	}
	// edit page
	function edit(){
		$this->content['h1_title']		= "Notivication";
		$this->content['content'] 		= "backend/notification/notification_form";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url.'add';
		$this->content['action'] 		= $this->url."input";
		$this->session->set_userdata('form_mode', 'edit');
		
		$data = $this->site_model->get_data('',"tb_notifications","notification_id = '".$this->uri->segment(5)."'")->row();
		
		$this->session->set_userdata('notification_id', $data->notification_id);
		$this->session->set_userdata('check_title', "notification_name/tb_notifications/".$data->notification_name."/");
		$this->content['default']['title'] 			= $data->notification_name;
		$this->content['default']['value'] 			= $data->notification_value;
		$this->content['default']['value_en']		= $data->notification_value_en;
		
		$this->load->view('backend/template',$this->content);			
	}
	// input post
	function input(){
		$this->content['h1_title']		= "Notivication";
		$this->content['content']		= "backend/notification/notification_form";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url.'add';
		$this->content['action']		= $this->url."input";
		$type_file 						= "gif|jpg|jpeg|png";
		
		$this->form_validation->set_rules('title', 'title', 'required|callback_check_title['.$this->session->userdata("check_title").']');
		$this->form_validation->set_rules('value', 'value', '');		
		$this->form_validation->set_rules('value_en', 'value_en', '');
				
		if($this->session->userdata('form_mode')=='add'){	
			$this->content['images'] = array();
			if($this->form_validation->run() == TRUE){
				// data to database
				$data = array(
					'notification_name'		=> $this->input->post("title"),
					'notification_value'	=> $this->input->post("value"),
					'notification_value_en'	=> $this->input->post("value_en"));
				// save to database
				$this->site_model->input_data("tb_notifications",$data);	
				// message						
				$this->session->set_flashdata('message', '1 Data has been saved!');
				redirect($this->url);
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
		elseif($this->session->userdata('form_mode')=='edit'){
			$id = $this->session->userdata('notification_id');
			$this->content['images'] = $this->site_model->get_img("parent_id = '".$id."' and relation = 'page'","10")->result();
			if($this->form_validation->run() == TRUE){
				// data to database
				$data = array(
					'notification_name'		=> $this->input->post("title"),
					'notification_value'	=> $this->input->post("value"),
					'notification_value_en'	=> $this->input->post("value_en"));
				// save to database
				$this->site_model->update_data("tb_notifications",$data,"notification_id = '".$id."'");
				// message					
				$this->session->set_flashdata('message', 'Data ( '.$this->input->post("title").' ) has been update!');
				$this->session->unset_userdata('notification_id');
				redirect($this->url."edit/".$id);
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
	}
	// delete post
	function delete(){
		$this->site_model->del_data("tb_notifications",$data=array('notification_id'=>$this->uri->segment(5)));
		$this->session->set_flashdata('message', 'Data has been delete!');
		redirect($this->url);
	}
}