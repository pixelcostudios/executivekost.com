<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Model{
	var $content;
	function __construct(){
		parent::__construct();
	}
	
	function index(){
		$this->content['content'] 		= 'backend/home';
		
		// Lastest Post
		$posts = $this->site_model->get_data('',"tb_posts p,tb_category c","p.category_id = c.category_id",'',"post_modified desc","5,0")->result();
		// Set template tabel, untuk efek selang-seling tiap baris
		$tmpl = array('table_open'=>'<table>');
		$this->table->set_template($tmpl);

		// Set heading untuk tabel
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('Lastest Update Post');
		
		foreach($posts as $p){
			$date = date("d/m/Y", strtotime($p->post_modified));
			$time = date("H:i", strtotime($p->post_modified));
			$post_date = $date.' at '.$time;
			
			$this->table->add_row(
			'<label class="lang-id">'.anchor(base_url().$p->post_name,$p->post_title.br(1)).'</label>
			<label class="lang-en">'.anchor(base_url().$p->post_name_en,$p->post_title_en).'</label>'.
			'category : '.$p->category_title.' | update : '.$post_date.', '.anchor(base_url().'backend/post/edit/'.$p->category_id.'/'.$p->post_id,'&nbsp;',array('class'=>'edit')));
		}
		$this->content['table_posts'] = $this->table->generate();
		
		// Lastest Comments
		$comments = $this->site_model->get_data('',"tb_comments",'','',"comment_id desc","5,0")->result();
		// Set template tabel, untuk efek selang-seling tiap baris
		$tmpl = array('table_open'=>'<table>');
		$this->table->set_template($tmpl);

		// Set heading untuk tabel
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('Recent Comments');
		
		foreach($comments as $c){
			$date = date("d/m/Y", strtotime($c->comment_date));
			$time = date("H:i", strtotime($c->comment_date));
			$post_date = '<b>'.$date.'</b> at <b>'.$time.'</b>';
			
			$this->table->add_row('From '.$c->name.' | '.$post_date.br(1).$c->comment_content);
		}
		$this->content['table_comments'] = $this->table->generate();
		$this->load->view('backend/template',$this->content);
	}
}