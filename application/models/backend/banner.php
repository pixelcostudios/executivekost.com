<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Banner extends CI_Model{
	var $url;
	var $content;
	function __construct(){
		parent::__construct();
		$this->url = base_url()."backend/setting/banner/";
	}
	// view banner
	function index(){
		$id = $this->uri->segment(4);
		$this->session->set_userdata('position', $id);
		$this->content['h1_title']		= "Manage Banner";
		$this->content['content'] 		= "backend/default";
		$this->content['data_link'] 	= $this->url.$id;
		$this->content['form_link'] 	= $this->url.$id."/add";
		
		$uri = 5;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$banners = $this->site_model->get_data('',"tb_banners","position = '".$id."'",'',"banner_id","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_banners","position = '".$id."'")->num_rows();
		
		if($num_rows > 0){
			// Set heading
			$this->table->set_empty("&nbsp;");
			$no 		= array('data'=>'No','width'=>'40');
			$banner 	= array('data'=>'Image Banner','width'=>'170');
			$order 		= array('data'=>'Order','width'=>'80');
			$actions 	= array('data'=>'Actions','width'=>'80');
			$this->table->set_heading($no,$banner,'Title',$order,$actions);
			
			$i = 0 + $offset;
			foreach($banners as $banner){
				$date = date("d/m/Y", strtotime($banner->upload_date));
				$time = date("H:i", strtotime($banner->upload_date));
				$upload_date = $date." | ".$time." WIB";
				$ext = explode(".",$banner->banner_image);
				
				if($banner->banner_status==1) $banner_status = "publish"; else $banner_status = "unpublish";
				
				if($ext[1]!='swf'){
					$img = '<img src="'.base_url().'uploads/banners/'.thumb($banner->banner_image).'" width="150" />';
				}
				else{
					$img = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" 
					codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="150">
					<param name="movie" value="'.base_url().'uploads/banners/'.$banner->banner_image.'" />
					<param name="quality" value="high" />
					<embed src="'.base_url().'uploads/banners/'.$banner->image.'" quality="high" 
					pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash"
					type="application/x-shockwave-flash" width="150">
					</embed>
					</object>';
				}
				$this->table->add_row(++$i,$img,$banner->banner_title.'<br />'.$upload_date.'<br />'.$banner_status,$banner->sequence,
					anchor($this->url.$id.'/edit/'.$banner->banner_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor($this->url.$id.'/delete/'.$banner->banner_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"))
				);
			}
			
			$config['base_url'] 			= $this->url.'/'.$id.'/';
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination'] 	= $this->pagination->create_links();
			$this->content['table'] 		= $this->table->generate();
		}
		else{
			$this->content['message'] = "Empty Data!";
		}
		
		$this->load->view('backend/template',$this->content);
	}
	// add banner
	function add(){
		$this->content['h1_title'] 		= "Manage Banner";
		$this->content['content'] 		= "backend/banner/banner_form";
		$this->content['data_link'] 	= $this->url.$this->uri->segment(4);
		$this->content['form_link'] 	= $this->url.$this->uri->segment(4)."/add";
		$this->content['action'] 		= $this->url."input";
		
		$this->session->set_userdata('form_mode', 'add');		
		$this->session->set_userdata('check_title', "banner_title/tb_banners/");
		
		$this->load->view('backend/template',$this->content);
	}
	// edit banner
	function edit(){
		$this->content['h1_title'] 		= "Manage Banner";
		$this->content['content']		= "backend/banner/banner_form";
		$this->content['data_link'] 	= $this->url.$this->uri->segment(4);
		$this->content['form_link'] 	= $this->url.$this->uri->segment(4)."/add";
		$this->content['action'] 		= $this->url."input";
		
		$this->session->set_userdata('form_mode', 'edit');		
		$banner = $this->site_model->get_data('',"tb_banners","banner_id = '".$this->uri->segment(6)."'")->row();		
		$this->session->set_userdata('banner_id', $banner->banner_id);
		$this->session->set_userdata('old_img', $banner->banner_image);
		$this->session->set_userdata('check_title', "banner_title/tb_banners/".$banner->banner_title."/");
		$this->content['title'] 		= $banner->banner_title;
		$this->content['url'] 			= $banner->banner_url;
		$this->content['sequence'] 		= $banner->sequence;
		$this->content['banner_status'] = $banner->banner_status;
		
		$this->load->view('backend/template',$this->content);
	}
	// input banner
	function input(){
		$this->content['h1_title'] 		= "Manage Banner";
		$this->content['content']		= "backend/banner/banner_form";
		$this->content['data_link'] 	= $this->url.$this->session->userdata("position");
		$this->content['form_link'] 	= $this->url.$this->session->userdata("position")."/add";
		$this->content['action']		= $this->url."input";
		$type_file 						= "gif|jpg|jpeg|png";
		
		$this->form_validation->set_rules('title', 'title', 'required|callback_check_title['.$this->session->userdata("check_title").']');
		$this->form_validation->set_rules('url', 'url', 'required');
		$this->form_validation->set_rules('sequence', 'sequence', '');		
		$this->form_validation->set_rules('publish', 'publish', '');
		$this->form_validation->set_rules('image', 'image', '');
		
		if($this->session->userdata('form_mode')=='add'){	
			if($this->form_validation->run() == TRUE){
				$config['upload_path'] 		= "./uploads/banners/";
				$config['allowed_types'] 	= $type_file;
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('image')){
					$this->content['image_error_message'] = $this->upload->display_errors('','');
					$this->load->view('backend/template',$this->content);
				}	
				else {
					$cek = $this->fm->upload_img($type_file,$this->input->post("title"));
					$namafile = slug($this->input->post("title")).$cek['ext'];
					$data = array(
						'banner_title'			=> $this->input->post("title"),
						'banner_name'			=> slug($this->input->post("title")),
						'banner_url'			=> $this->input->post("url"),
						'upload_date'			=> date("Y-m-d H:i:s"),
						'position'				=> $this->input->post("position"),
						'sequence'				=> $this->input->post("sequence"),
						'banner_image'			=> $namafile);
					// save to database
					$this->site_model->input_data("tb_banners",$data);						
					$this->session->set_flashdata('message', '1 Data has been saved!');
					redirect($this->url.$this->input->post("position"));
				}
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
		elseif($this->session->userdata('form_mode')=='edit'){
			$id = $this->session->userdata('banner_id');
			if($this->form_validation->run() == TRUE){
				if(!empty($_FILES['image']['name'])){
					$config['upload_path'] 		= "./uploads/banners/";
					$config['allowed_types'] 	= $type_file;
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload('image')){
						$this->content['image_error_message'] = $this->upload->display_errors('','');
						$this->load->view('backend/template',$this->content);
					}	
					else {
						if($this->session->userdata("oldimg")!=''){
							$image = $this->site_model->get_data('',"tb_banners","banner_id = '".$id."'")->row();
							$del	= "./uploads/banners/".$image->banner_image;
							$del2	= "./uploads/banners/".thumb($image->banner_image);
							if(file_exists($del)) unlink($del);
							if(file_exists($del2)) unlink($del2);
						}
						$cek = $this->fm->upload_img($type_file,$this->input->post("title"));
						$namafile = slug($this->input->post("title")).$cek['ext'];						
						
						$data = array(
							'banner_title'			=> $this->input->post("title"),
							'banner_name'			=> slug($this->input->post("title")),
							'banner_url'			=> $this->input->post("url"),
							'position'				=> $this->input->post("position"),
							'sequence'				=> $this->input->post("sequence"),
							'banner_image'			=> $namafile,
							'banner_status'			=> $this->input->post("publish"));
						// save to database
						$this->site_model->update_data("tb_banners",$data,"banner_id = '".$id."'");
						$this->session->set_flashdata('message', 'Data ( '.$this->input->post("title").' ) has been update!');
						$this->session->unset_userdata('banner_id');
						redirect($this->url.$this->input->post("position")."/edit"."/".$id);
					}	
				}
				else{
					$data = array(
						'banner_title'			=> $this->input->post("title"),
						'banner_name'			=> slug($this->input->post("title")),
						'banner_url'			=> $this->input->post("url"),
						'position'				=> $this->input->post("position"),
						'sequence'				=> $this->input->post("sequence"),
						'banner_status'			=> $this->input->post("publish"));
					// save to database
					$this->site_model->update_data("tb_banners",$data,"banner_id = '".$id."'");
					$this->session->set_flashdata('message', 'Data ( '.$this->input->post("title").' ) has been update!');
					$this->session->unset_userdata('banner_id');
					redirect($this->url.$this->input->post("position")."/edit"."/".$id);
				}
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
	}
	// delete banner
	function delete(){
		$id = $this->uri->segment(6);
		$image = $this->site_model->get_data('',"tb_banners","banner_id = '".$id."'")->row();
		if(!empty($image->banner_image)){
			$del	= "./uploads/banners/".$image->banner_image;
			$del2	= "./uploads/banners/".thumb($image->banner_image);
			if(file_exists($del)) unlink($del);
			if(file_exists($del2)) unlink($del2);
		}
		$this->site_model->del_data("tb_banners",$data=array('banner_id'=>$id));					
		$this->session->set_flashdata('message', 'Data has been delete!');
		redirect($this->url."banner/".$this->session->userdata('position'));		
	}	
}