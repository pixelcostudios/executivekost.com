<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends CI_Model{
	var $url;
	var $content;
	function __construct(){
		parent::__construct();
		$this->url =  base_url().'backend/setting/category/';
	}
	// view category
	function index(){
		$this->content['h1_title']		= "Category";
		$this->content['content'] 		= "backend/category/category";
		$this->content['list_kategori'] = $this->site_model->get_data('',"tb_category")->result();
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['set_action'] 	= $this->url."set_category";
		
		$uri = 4;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 25;
		
		$category = $this->site_model->get_data('',"tb_category",'','',"category_id asc","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_category")->num_rows();
		
		if($num_rows > 0){
			// Set heading
			$this->table->set_empty("&nbsp;");
			$cek 		= array('data'=>''.form_checkbox('cek[]', '', '','onClick="toggleChecked(this)"').'','width'=>'40');
			$no 		= array('data'=>'ID','width'=>'40');
			$actions 	= array('data'=>'Actions','width'=>'80');
			$this->table->set_heading($cek,$no,'Title','Type','Group','Status',$actions);
			
			$i = 0 + $offset;
			foreach($category as $c){
				$tab_a = array ("1","2");
				$tab_b = array ("post","gallery");
				$category_type = str_replace($tab_a,$tab_b,$c->category_type);
				
				$kategori = $this->site_model->get_data('',"tb_category","category_id = '".$c->category_group."'")->row();
				$this->table->add_row(
					form_checkbox('cek[]', ''.$c->category_id.'', '','onClick="toggleController(this)"'),
					$c->category_id,
					$c->category_title.'/'.$c->category_title_en ,
					$category_type,
					isset($kategori->category_title) ? $kategori->category_title : '',
					$c->category_status,
					anchor($this->url.'edit/'.$c->category_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor($this->url.'delete/'.$c->category_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"))
				);
			}
			
			$config['base_url'] 			= $this->url;
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination'] 	= $this->pagination->create_links();
			$this->content['table'] 		= $this->table->generate();
		}
		else{
			$this->content['message'] = 'Empty Data!';
		}
		
		$this->load->view('backend/template',$this->content);
	}
	// set category
	function set_category(){
		$check = $this->input->post("check");
		$cek = $this->input->post("cek");
		$category_group = $this->input->post("category_group");
		$uri = $this->input->post("uri");
		if($check=='active') { $set = 'category_status'; $active = 'active'; }
		if($check=='deactive') { $set = 'category_status'; $active = 'deactive'; }
		if($check=='group') { $set = 'category_group'; $active = $category_group; }		
		
		$jumlah = count($cek);
		for($i=0; $i<$jumlah; $i++){
			$data = array($set => $active);
			$this->site_model->update_data("tb_category",$data,"category_id = '".$cek[$i]."'");
		}
		redirect($this->url.$uri);
	}
	// add category
	function add(){
		$this->content['h1_title']		= "Category";
		$this->content['content'] 		= "backend/category/category_form";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['set_action'] 	= $this->url."set_category";
		$this->content['action'] 		= $this->url."input";
		$this->content['kategori'] 		= $this->site_model->get_data('',"tb_category")->result();
		$this->session->set_userdata('form_mode', 'add');
		$this->session->set_userdata('check_title', "category_title/tb_category/");
		
		$this->load->view('backend/template',$this->content);
	}
	// edit category
	function edit(){
		$this->content['h1_title']		= "Category";
		$this->content['content']		= "backend/category/category_form";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['set_action'] 	= $this->url."set_category";
		$this->content['action']		= $this->url."input";
		$this->content['url']			= $this->url;
		$this->content['kategori']		= $this->site_model->get_data('',"tb_category")->result();
		$this->session->set_userdata('form_mode', 'edit');
		
		$kategori = $this->site_model->get_data('',"tb_category","category_id = '".$this->uri->segment(5)."'")->row();
		
		$this->session->set_userdata('category_id', $kategori->category_id);
		$this->session->set_userdata('check_title', "category_title/tb_category/".$kategori->category_title."/");
		$this->content['oldimg'] 			= $kategori->category_image;
		$this->content['title'] 			= $kategori->category_title;
		$this->content['title_en'] 			= $kategori->category_title_en;
		$this->content['slug'] 				= $kategori->category_name;
		$this->content['slug_en'] 			= $kategori->category_name_en;
		$this->content['des'] 				= $kategori->category_content;
		$this->content['des_en'] 			= $kategori->category_content_en;
		$this->content['category_group'] 	= $kategori->category_group;
		$this->content['type'] 				= $kategori->category_type;
		$this->content['image'] 			= thumb($kategori->category_image);
		
		$this->load->view('backend/template',$this->content);			
	}
	// input category
	function input(){
		$this->content['h1_title']		= "Category";
		$this->content['content']		= "backend/category/category_form";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['set_action'] 	= $this->url."set_category";
		$this->content['action']		= $this->url."input_category";
		$this->content['url']			= $this->url;
		$this->content['kategori'] 		= $this->site_model->get_data('',"tb_category","category_group = '0'")->result();
		$this->content['oldimg']		= $this->input->post("oldimg");
		$type_file 						= "gif|jpg|jpeg|png";
		
		$this->form_validation->set_rules('title', 'title', 'required|callback_check_title['.$this->session->userdata("check_title").']');
		$this->form_validation->set_rules('title_en', 'title_en', '');
		$this->form_validation->set_rules('des', 'des', '');
		$this->form_validation->set_rules('des_en', 'des_en', '');
		$this->form_validation->set_rules('type', 'type', '');
		$this->form_validation->set_rules('image', 'image', '');
		
		if($this->session->userdata('form_mode')=='add'){	
			if($this->form_validation->run() == TRUE){
				if(!empty($_FILES['image']['name'])){
					$config['upload_path'] 		= "./uploads/category/";
					$config['allowed_types'] 	= $type_file;
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload('image')){
						$this->content['image_error_message'] = $this->upload->display_errors('','');
						$this->load->view('backend/template',$this->content);
					}	
					else {
						$cek = $this->fm->upload_img($type_file,$this->input->post("title"));
						$namafile = slug($this->input->post("title")).$cek['ext'];
					}
				}
				else{
					$namafile = "";
				}		
				$data = array(
					'category_title'		=> $this->input->post("title"),
					'category_title_en'		=> $this->input->post("title_en"),
					'category_name'			=> slug($this->input->post("title")),
					'category_name_en'		=> slug($this->input->post("title_en")),
					'category_content'		=> $this->input->post("des"),
					'category_content_en'	=> $this->input->post("des_en"),
					'category_group'		=> $this->input->post("category"),
					'category_type'			=> $this->input->post("type"),
					'category_image'		=> $namafile);
				// save to database
				$this->site_model->input_data("tb_category",$data);						
				$this->session->set_flashdata('message', '1 Data has been saved!');
				redirect($this->url);
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
		elseif($this->session->userdata('form_mode')=='edit'){
			$id = $this->session->userdata('category_id');
			if($this->form_validation->run() == TRUE){
				if(!empty($_FILES['image']['name'])){
					$config['upload_path'] 		= "./uploads/category/";
					$config['allowed_types'] 	= $type_file;
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload('image')){
						$this->content['image_error_message'] = $this->upload->display_errors('','');
						$this->load->view('backend/template',$this->content);
					}	
					else {
						if($this->input->post("oldimg")!=''){
							$image = $this->site_model->get_data('',"tb_category","category_id = '".$id."'")->row();
							$del	= "./uploads/category/".$image->category_image;
							$del2	= "./uploads/category/".thumb($image->category_image);
							if(file_exists($del)) unlink($del);
							if(file_exists($del2)) unlink($del2);
						}
						$cek = $this->fm->upload_img($type_file,$this->input->post("title"));
						$namafile = slug($this->input->post("title")).$cek['ext'];
						
						$data = array(
							'category_title'		=> $this->input->post("title"),
							'category_title_en'		=> $this->input->post("title_en"),
							'category_name'			=> slug($this->input->post("title")),
							'category_name_en'		=> slug($this->input->post("title_en")),
							'category_content'		=> $this->input->post("des"),
							'category_content_en'	=> $this->input->post("des_en"),
							'category_group'		=> $this->input->post("category"),
							'category_type'			=> $this->input->post("type"),
							'category_image'		=> $namafile);
						// save to database
						$this->site_model->update_data("tb_category",$data,"category_id = '".$id."'");	
						$this->session->set_flashdata('message', 'Data ( '.$this->input->post("title").' / '.$this->input->post("title_en").' ) has been update!');
						$this->session->unset_userdata('category_id');
						redirect($this->url."edit/".$id);
					}	
				}
				else{
					$data = array(
						'category_title'		=> $this->input->post("title"),
						'category_title_en'		=> $this->input->post("title_en"),
						'category_name'			=> slug($this->input->post("title")),
						'category_name_en'		=> slug($this->input->post("title_en")),
						'category_content'		=> $this->input->post("des"),
						'category_content_en'	=> $this->input->post("des_en"),
						'category_group'		=> $this->input->post("category"),
						'category_type'			=> $this->input->post("type"));
					// save to database
					$this->site_model->update_data("tb_category",$data,"category_id = '".$id."'");	
					$this->session->set_flashdata('message', 'Data ( '.$this->input->post("title").' / '.$this->input->post("title_en").' ) has been update!');
					$this->session->unset_userdata('category_id');
					redirect($this->url."edit/".$id);
				}
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
	}
	// delete category
	function delete(){
		$id = $this->uri->segment(5);
		$image = $this->site_model->get_data('',"tb_category","category_id = '".$id."'")->row();
		if(!empty($image->category_image)){
			$del	= "./uploads/category/".$image->category_image;
			$del2	= "./uploads/category/".thumb($image->category_image);
			if(file_exists($del)) unlink($del);
			if(file_exists($del2)) unlink($del2);
		}
		$this->site_model->del_data("tb_category",$data=array('category_id'=>$id));					
		$this->session->set_flashdata('message', 'Data has been delete!');
		redirect($this->url);		
	}
	// delete category image
	function delete_image(){
		$id = $this->uri->segment(5);
		$image = $this->site_model->get_data('',"tb_category","category_id = '".$id."'")->row();
		if(!empty($image->category_image)){
			$del	= "./uploads/category/".$image->category_image;
			$del2	= "./uploads/category/".thumb($image->category_image);
			if(file_exists($del)) unlink($del);
			if(file_exists($del2)) unlink($del2);
		}
		$this->site_model->update_data("tb_category",$data = array('category_image' => ''),"category_id = '".$id."'");					
		$this->session->set_flashdata('message', 'Image has been delete!');
		redirect($this->url."edit/".$id);		
	}
}