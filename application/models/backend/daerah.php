<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Daerah extends CI_Model{
	var $url;
	var $content;
	function __construct(){
		parent::__construct();
		$this->url = base_url()."backend/kost/daerah/";
	}
	// view
	function index(){
		$this->content['h1_title']		= "Manage Daerah";
		$this->content['content'] 		= "backend/default";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add";
		
		$uri = 4;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$daerah = $this->site_model->get_data('',"tb_daerah",'','',"daerah_id desc","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data('',"tb_daerah",'')->num_rows();
		
		if($num_rows > 0){			
			// Set heading
			$this->table->set_empty("&nbsp;");
			$no 		= array('data'=>'No','width'=>'40');
			$date 		= array('data'=>'Date','width'=>'230');
			$actions 	= array('data'=>'Actions','width'=>'80');
			$this->table->set_heading($no,'Daerah',$actions);
			
			$i = 0 + $offset;
			foreach($daerah as $p){			
				$this->table->add_row(
					++$i.'.',
					'<label>'.$p->daerah_title.br(1).'</label>',
					anchor($this->url.'edit/'.$p->daerah_id,'&nbsp;',array('class'=>'edit')).' '.
					anchor($this->url.'delete/'.$p->daerah_id,'&nbsp;',array('class'=>'delete','onclick'=>"return confirm('Are you sure you want to delete this data ?')"))
				);
			}
			
			$config['base_url'] 			= $this->url;
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			
			$this->content['pagination'] 	= $this->pagination->create_links();
			$this->content['table'] 		= $this->table->generate();
		}
		else{
			$this->content['message'] = "Empty Data!";
		}
		
		$this->load->view('backend/template',$this->content);
	}
	// add page
	function add(){
		$this->content['h1_title']		= "Manage Daerah";
		$this->content['content'] 		= "backend/kost/daerah_form";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['action'] 		= $this->url."input";
		$this->session->set_userdata('form_mode', 'add');
		$this->session->set_userdata('check_title', "daerah_title/tb_daerah/");
		
		$this->load->view('backend/template',$this->content);
	}
	// edit page
	function edit(){
		$this->content['h1_title']		= "Manage Daerah";
		$this->content['content'] 		= "backend/kost/daerah_form";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['action'] 		= $this->url."input";
		$this->session->set_userdata('form_mode', 'edit');
		
		$daerah = $this->site_model->get_data('',"tb_daerah","daerah_id = '".$this->uri->segment(5)."'")->row();
		
		$this->session->set_userdata('daerah_id', $daerah->daerah_id);
		$this->session->set_userdata('check_title', "daerah_title/tb_daerah/".$daerah->daerah_title."/");
		$this->content['title'] 		= $daerah->daerah_title;
		
		$this->load->view('backend/template',$this->content);			
	}
	// input post
	function input(){
		$this->content['h1_title']		= "Manage Daerah";
		$this->content['content']		= "backend/kost/daerah_form";
		$this->content['data_link'] 	= $this->url;
		$this->content['form_link'] 	= $this->url."add";
		$this->content['action']		= $this->url.'input';
		
		$this->form_validation->set_rules('title', 'title', 'required|callback_check_title['.$this->session->userdata("check_title").']');
		
		if($this->session->userdata('form_mode')=='add'){	
			$this->content['images'] = array();
			if($this->form_validation->run() == TRUE){
				// data to database
				$data = array(
					'daerah_title'			=> $this->input->post("title"),
					'daerah_name'			=> slug($this->input->post("title")));
				// save to database
				$this->site_model->input_data("tb_daerah",$data);		
				// message						
				$this->session->set_flashdata('message', '1 Data has been saved!');
				redirect($this->url);	
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
		elseif($this->session->userdata('form_mode')=='edit'){
			$id = $this->session->userdata('daerah_id');
			if($this->form_validation->run() == TRUE){
				$data = array(
					'daerah_title'			=> $this->input->post("title"),
					'daerah_name'			=> slug($this->input->post("title")));
				// save to database
				$this->site_model->update_data("tb_daerah",$data,"daerah_id = '".$id."'");
				// message					
				$this->session->set_flashdata('message', 'Data ( '.$this->input->post("title").' ) has been update!');
				$this->session->unset_userdata('daerah_id');
				redirect($this->url."edit/".$id);
			}
			else {	
				$this->load->view('backend/template',$this->content);								
			}
		}
	}
	// delete post
	function delete(){
		$id = $this->uri->segment(5);
		$this->site_model->del_data("tb_daerah",array('daerah_id'=>$id));					
		$this->session->set_flashdata('message', 'Data has been delete!');
		redirect($this->url);		
	}
}