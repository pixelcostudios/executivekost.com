<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search extends CI_Model{
	var $metahead;
	var $content;
	var $footer;
	function __construct(){
		parent::__construct();
	}
	
	function index(){;
		$word = explode(" ",trim($this->input->post("search")));
		$slug = implode("-",$word);
		if($slug==''){
			redirect(base_url());
		}
		redirect(base_url().'searching/'.$slug);
	}
	
	function search(){
		$slug = explode("-",$this->uri->segment(2));
		$word = implode(" ",$slug);
		if($this->session->userdata('lang')=='id'){
			$this->session->set_userdata('lang', 'id');
			$lang = "";
		}
		elseif($this->session->userdata('lang')=='en'){
			$this->session->set_userdata('lang', 'en');
			$lang = "_en";
		}
		
		$uri = 3;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		$limit = 10;
		
		$search = $this->site_model->search("tb_posts","post_title".$lang."",$word,"$offset,$limit")->result();	
		$this->content['search'] = array();
		foreach($search as $a){
			$data['post_title']				= $a->post_title;
			$data['post_name']				= $a->post_name;
			$data['post_content']			= summary($a->post_content,20);
			$this->content['search'][] = $data;
		}
		
		$this->content['word'] = $word;
		
		$num_rows = $this->site_model->search("tb_posts","post_title".$lang."",$word)->num_rows();		
		if($num_rows > 0){
			$config['base_url'] 			= base_url().'searching/'.$this->uri->segment(2).'/';
			$config['total_rows']			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			
			$this->pagination->initialize($config);
			$this->content['pagination']	= $this->pagination->create_links();
		}
	
		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/searching',$this->content);
		$this->load->view('frontend/footer',$this->footer);
		
	}
}