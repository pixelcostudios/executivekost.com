<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends CI_Model{
	var $metahead;
	var $content;
	var $footer;
	function __construct(){
		parent::__construct();
	}
	// index
	function index($mode,$cap_msg=""){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		$slug = html($mode);
		
		// page
		$select = "page_id,page_title".$lang." as page_title,page_name".$lang." as page_name,page_content".$lang." as page_content,contact";
		$page = $this->site_model->get_data($select,"tb_pages","page_name".$lang." = '".$slug."' AND page_status = '1'")->row();
		if(count($page)==0){
			redirect(base_url());
		}
		else{
			// meta head
			$this->metahead['content_title']	= $page->page_title;
			// data page
			$this->content['page_title']	= $page->page_title;
			$this->content['page_name']		= $page->page_name;
			$this->content['page_content']	= $page->page_content;
			$this->content['contact']		= $page->contact;
			$this->content['image']			= $this->site_model->get_img("parent_id = '".$page->page_id."' AND relation = 'page'","1")->row();
			
			$this->content["cap_img"] = $this->fm->_make_captcha();
			$this->content["cap_msg"] = $cap_msg;
			
			$this->load->view('frontend/header',$this->metahead);
			$this->load->view('frontend/page',$this->content);
			$this->load->view('frontend/footer',$this->footer);
		}
	}
	// send message
	function send_message(){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('subject', 'subject', 'required');
		$this->form_validation->set_rules('comment', 'comment', 'required');
		
		$notif = $this->site_model->get_data('',"tb_notifications","notification_name = 'contact_send'")->row();
		$flash_message = $notif->notification_value;
		$flash_message_en = $notif->notification_value_en;
		
		$option = $this->site_model->get_data('',"tb_options","option_id = '1'")->row();
		if($this->form_validation->run() == TRUE){
			$captcha_result = '';
			$data["cap_img"] = $this->fm->_make_captcha();
			if($this->input->post('submit')){
				if($this->fm->_check_capthca()){
					$config['mailtype'] = 'html';
					$this->email->initialize($config);
					
					$this->email->from($this->input->post('email'), $this->input->post('name'));
					$this->email->to($option->email);
					$this->email->subject($this->input->post('subject'));
					$this->email->message($this->input->post('comment'));
					
					$this->email->send();
					$this->session->set_flashdata('message',$flash_message.$lang);
					redirect(base_url().'page/'.$this->input->post('slug').'.html');
				}
				else {
        			$cap_msg = 'Security Code Wrong!';
				}
			}
			$this->index($this->input->post('slug'),$cap_msg);
		}
		else {
			$this->index($this->input->post('slug'));
		}
	}
}