<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Model{
	var $metahead;
	var $content;
	var $footer;
	function __construct(){
		parent::__construct();
	}
	// index
	function index(){
		if($this->session->userdata('lang')=='id')
			$lang = "";
		else
			$lang = "_en";
		// promo
		$select = "post_id,post_title".$lang." as post_title,post_name".$lang." as post_name";
		$where  = "category_id = '5' AND post_title".$lang." != '' AND post_status = '1'";
		$promo = $this->site_model->get_data($select,"tb_posts",$where,'',"post_id desc","5")->result();
		$this->content['promo'] = array();
		foreach($promo as $a){
			$data['post_id']			= $a->post_id;
			$data['post_title']			= $a->post_title;
			$data['post_name']			= $a->post_name;
			$this->content['promo'][]	= $data;
		}		
		// news
		$select = "p.post_id,p.post_title".$lang." as post_title,p.post_name".$lang." as post_name,p.post_content".$lang." as post_content,";
		$select.= "c.category_title".$lang." as category_title,category_name".$lang." as category_name";
		$where  = "p.category_id = c.category_id AND c.category_id = '3' AND post_title".$lang." != '' AND p.post_status = '1'";
		$news = $this->site_model->get_data($select,"tb_posts p,tb_category c",$where,'',"p.post_id desc","1")->result();
		$this->content['news'] = array();
		foreach($news as $a){
			$data['post_id']			= $a->post_id;
			$data['post_title']			= $a->post_title;
			$data['post_name']			= $a->post_name;
			$data['post_content']		= summary($a->post_content,10);	
			$data['category_title']		= $a->category_title;	
			$data['category_name']		= $a->category_name;
			$this->content['news'][]	= $data;
		}
		// artikel
		$select = "p.post_id,p.post_title".$lang." as post_title,p.post_name".$lang." as post_name,p.post_content".$lang." as post_content,";
		$select.= "c.category_title".$lang." as category_title,category_name".$lang." as category_name";
		$where  = "p.category_id = c.category_id AND c.category_id = '4' AND post_title".$lang." != '' AND p.post_status = '1'";
		$artikel = $this->site_model->get_data($select,"tb_posts p,tb_category c",$where,'',"p.post_id desc","1")->result();
		$this->content['artikel'] = array();
		foreach($artikel as $a){
			$data['post_id']			= $a->post_id;
			$data['post_title']			= $a->post_title;
			$data['post_name']			= $a->post_name;
			$data['post_content']		= summary($a->post_content,10);	
			$data['category_title']		= $a->category_title;	
			$data['category_name']		= $a->category_name;
			$this->content['artikel'][]	= $data;
		}
		// comments
		$comments = $this->site_model->get_data("","tb_comments","comment_status = '1'",'',"comment_id desc","1")->result();
		$this->content['comments'] = array();
		foreach($comments as $c){
			$data['name']				= $c->name;
			$data['comment_date']		= fdate($c->comment_date,"2");
			$data['comment_content']	= summary($c->comment_content,15);	
			$this->content['comments'][]	= $data;
		}
		
		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/home',$this->content);
		$this->load->view('frontend/footer',$this->footer);
	}
	// feed
	function feed(){
		header('Content-type: text/xml');
		$option = $this->site_model->get_data('',"tb_options","option_id = '1'")->row();
		echo ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		echo ("<rss version=\"2.0\">\n");
		echo ("<channel>\n");
		echo ("<lastBuildDate>".date("Y, d F H:i:s")."</lastBuildDate>\n");
		echo ("<pubDate>".date("Y, d F H:i:s")."</pubDate>\n");
		echo ("<title>".$option->title."</title>\n");
		echo ("<description><![CDATA[".$option->des."]]></description>\n");
		echo ("<link>".base_url()."</link>\n");
		echo ("<language>en</language>\n");
		echo ("<managingEditor>herda.ramandita@gmail.com</managingEditor>\n");
		echo ("<webMaster>herda.ramandita@gmail.com</webMaster>\n");
		$post = $this->site_model->get_data('',"tb_posts p,tb_category c","p.category_id = c.category_id",'',"post_date desc","5")->result();
		foreach($post as $p){
		echo ("<item>\n");
		echo ("<title>".$p->post_title."</title>\n");
		echo ("<link>".base_url().$p->post_name.'.html'."</link>\n");
		echo ("<description><![CDATA[".summary($p->post_content,100)."]]></description>\n");
		echo ("<guid isPermaLink=\"true\">".base_url().$p->post_name.'.html'."</guid>\n");
		echo ("</item>\n");
		
		}
		echo ("</channel>\n");
		echo ("</rss>\n");
	}
	// subscriber
	function subscriber(){
		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/subscriber',$this->content);
		$this->load->view('frontend/footer',$this->footer);				
	}
	// site map
	function site_map(){
		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/site_map',$this->content);
		$this->load->view('frontend/footer',$this->footer);				
	}
}