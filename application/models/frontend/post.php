<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Post extends CI_Model{
	var $metahead;
	var $content;
	var $footer;
	function __construct(){
		parent::__construct();
	}
	// index
	function index($mode,$cap_msg=""){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		$slug = html($mode);
		// post
		$select = "p.post_id,p.post_title".$lang." as post_title,p.post_name".$lang." as post_name,p.post_content".$lang." as post_content,";
		$select.= "p.meta_des".$lang." as meta_des,p.meta_key".$lang." as meta_key,counter,";
		$select.= "c.category_id,c.category_title".$lang." as category_title,category_name".$lang." as category_name";
		$where = "p.category_id = c.category_id AND post_name".$lang." = '".$slug."' AND post_status = '1'";
		$post = $this->site_model->get_data($select,"tb_posts p,tb_category c",$where)->row();
		if(count($post)==0){
			redirect(base_url());
		}
		else{
			// meta head
			$this->metahead['content_title']	= $post->post_title;
			$this->metahead['content_des'] 		= $post->meta_des;
			$this->metahead['content_keyword']	= $post->meta_key;
			// data post
			$this->content['post_id']		= $post->post_id;
			$this->content['post_title']	= $post->post_title;
			$this->content['post_name']		= $post->post_name;
			$this->content['post_content']	= $post->post_content;
			$this->content['category_title']= $post->category_title;
			$this->content['category_name']	= $post->category_name;
			// image
			$this->content['img1'] = $this->site_model->get_img("parent_id = '".$post->post_id."' AND relation = 'post'","1")->row();
			$this->content['img2'] = $this->site_model->get_img("parent_id = '".$post->post_id."' AND relation = 'post'","4,1")->result();
			// add counter
			$this->site_model->update_data("tb_posts",array('counter'=>$post->counter+1),"post_id = '".$post->post_id."'");
			// comments
			$this->content['comments']		= $this->site_model->get_data('',"tb_comments","parent_id = '".$post->post_id."' AND parent_type = 'post' AND comment_status = '1' AND comment_lang = '".$this->session->userdata('lang')."'",'',"comment_id desc")->result();
			// captcha
			$captcha_result = '';
			$this->content["cap_img"] = $this->fm->_make_captcha();
			$this->content["cap_msg"] = $cap_msg;
			
			if($post->category_id != '10') $template = "post"; else $template = "post_city_scape";
			
			$this->load->view('frontend/header',$this->metahead);
			$this->load->view('frontend/'.$template,$this->content);
			$this->load->view('frontend/footer',$this->footer);
		}
	}
	// archive
	function archive(){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
		$slug = html($this->uri->segment(2));
		// meta head
		$select = "title".$lang." as title,des".$lang." as des,keyword".$lang." as keyword,image";
		$forhead = $this->site_model->get_data($select,"tb_options","option_id = '1'")->row();
		$this->metahead['site_title'] 	= $forhead->title;
		$this->metahead['site_des'] 	= $forhead->des;
		$this->metahead['site_keyword'] = $forhead->keyword;
		$this->metahead['site_logo'] 	= $forhead->image;
		// archive
		$this->content['category'] = $this->site_model->get_data("category_title".$lang." as category_title,category_type","tb_category","category_name".$lang." = '".$slug."'")->row();
		$category = $this->site_model->get_data("category_title".$lang." as category_title,category_type","tb_category","category_name".$lang." = '".$slug."'")->row();	
		$uri = 3;
		$offset = $this->uri->segment($uri);
		if ($offset=='') $offset='0';
		if($category->category_type = '2') $limit = 16; else $limit = 10;
		
		$select = "p.post_id,p.post_title".$lang." as post_title,p.post_name".$lang." as post_name,p.post_content".$lang." as post_content,";
		$select.= "c.category_title".$lang." as category_title";
		$where  = "p.category_id = c.category_id AND c.category_name".$lang." = '".$slug."' AND post_title".$lang." != '' AND p.post_status = '1'";
		$archive = $this->site_model->get_data($select,"tb_posts p,tb_category c",$where,'',"post_date desc","$limit,$offset")->result();
		$num_rows = $this->site_model->get_data($select,"tb_posts p,tb_category c",$where)->num_rows();
		if($num_rows > 0){
			$config['base_url'] 			= base_url()."category/".$slug."/";
			$config['total_rows'] 			= $num_rows;
			$config['per_page'] 			= $limit;
			$config['uri_segment'] 			= $uri;
			$this->pagination->initialize($config);
			$this->content['pagination'] 	= $this->pagination->create_links();
			// data posts
			$this->content['archive'] = array();
			foreach($archive as $a){
				$data['post_id']			= $a->post_id;
				$data['post_title']			= $a->post_title;
				$data['post_name']			= $a->post_name;
				$data['post_content']		= $a->post_content;
				$data['category_title']		= $a->category_title;
				$this->content['archive'][]	= $data;
			}
		}
		
		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/archive',$this->content);
		$this->load->view('frontend/footer',$this->footer);
	}
	// send comment
	function send(){
		if($this->session->userdata('lang')=='id') $lang = ""; else $lang = "_en";
			
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'valid_email');
		$this->form_validation->set_rules('comment', 'comment', 'required');
		
		
		$notif = $this->site_model->get_data('',"tb_notifications","notification_name = 'contact_send'")->row();
		$flash_message = $notif->notification_value;
		$flash_message_en = $notif->notification_value_en;
		
		if($this->form_validation->run()==TRUE){
			if($this->input->post('submit')){
				if($this->fm->_check_capthca()){
					$data = array(
						'parent_id'			=> $this->input->post('post_id'),
						'parent_type'		=> 'post',
						'name'				=> $this->input->post('name'),
						'email'				=> $this->input->post('email'),
						'comment_content'	=> $this->input->post('comment'),
						'comment_lang'		=> $this->input->post('lang'),
						'comment_date'		=> date("Y-m-d H:i:s")
					);
					$this->site_model->input_data("tb_comments",$data);
					$this->session->set_flashdata('message',$flash_message.$lang);
					redirect(base_url().$this->input->post('slug'));
				}
				else {
        			$cap_msg = '<label class="error">Security Code Wrong!</label>';
				}
			}
			$this->index($this->input->post('slug'),$cap_msg);
		}
		else {
			$this->index($this->input->post('slug').'.html');
		}
	}
}