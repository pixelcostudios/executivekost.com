<?php
if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Member extends CI_Model{
	var $metahead;
	var $content;
	var $footer;

	function __construct(){
		parent::__construct();
	}
	/* ------------------
	index
	------------------ */

	function index($cap_msg = ""){
		/* ------------------
		   order check
		------------------ */
		$this->db->where('rent_status', '1');
		$order_check = $this->db->get('tb_rent_kost')->result();
		foreach($order_check as $x){
			$now = date("Y-m-d H:i:s");
			if(strtotime("".$x->time_order."+90 minutes") <= strtotime($now)){
				$this->db->where('rent_kost_id', $x->rent_kost_id);
				$this->db->update('tb_rent_kost', array('rent_status'=>'4'));
			}
		}
		
		$order_check2 = $this->site_model->get_data('',"tb_rent_kost","rent_status = '2'")->result();
		foreach($order_check as $x){
			if($x->check_out < date("Y-m-d"))
			$this->site_model->update_data("tb_rent_kost",array('rent_status'=>'3'),"rent_kost_id = '".$x->rent_kost_id."'");
		}

		if($this->session->userdata('member_login') == TRUE){
			/* ------------------
			   data order
			------------------ */
			$member_id = $this->session->userdata('ses_member_id');
			$this->db->select('tb_kosts.kost_id, tb_kosts.kost_title, tb_kosts.kost_name');
			$this->db->select('tb_kost_room.kost_room_id, tb_kost_room.kost_room_number, tb_kost_room.kost_room_price_day,tb_kost_room.kost_room_price_month');
			$this->db->select('tb_rent_kost.rent_kost_id, tb_rent_kost.booking_code, tb_rent_kost.check_in, tb_rent_kost.check_out, tb_rent_kost.total_pay, tb_rent_kost.rent_status,tb_rent_kost.special_request, tb_rent_kost.time_order');
			//$select    = "k.kost_id,k.kost_title,k.kost_name,kr.kost_room_id,kr.kost_room_number,kr.kost_room_price_day,kr.kost_room_price_month,";
			//$select .= "rk.rent_kost_id,rk.booking_code,rk.check_in,rk.check_out,rk.total_pay,rk.rent_status,rk.special_request,rk.time_order";
			$this->db->join('tb_kost_room', 'tb_rent_kost.kost_room_id = tb_kost_room.kost_room_id');
			$this->db->join('tb_kosts', 'tb_kost_room.kost_id = tb_kosts.kost_id');
			//$where      = "rk.kost_room_id = kr.kost_room_id AND kr.kost_id = k.kost_id AND rk.member_id = '".$member_id."'";
			$this->db->order_by('tb_rent_kost.rent_kost_id', 'DESC');
			$data_order = $this->db->get('tb_rent_kost')->row();
			//$data_order = $this->site_model->get_data($select,"tb_rent_kost rk,tb_kosts k,tb_kost_room kr",$where,'',"rk.rent_kost_id DESC",'1')->row();

			$this->content['kost_id'] 				= $data_order->kost_id;
			$this->content['kost_title'] 			= $data_order->kost_title;
			$this->content['kost_name'] 			= $data_order->kost_name;
			$this->content['kost_room_id'] 			= $data_order->kost_room_id;
			$this->content['kost_room_number'] 		= $data_order->kost_room_number;
			$this->content['kost_room_price_day'] 	= $data_order->kost_room_price_day;
			$this->content['kost_room_price_month'] = $data_order->kost_room_price_month;
			$this->content['rent_kost_id'] 			= $data_order->rent_kost_id;
			$this->content['check_in'] 				= $data_order->check_in;
			$this->content['check_out'] 			= $data_order->check_out;
			$this->content['total_pay'] 			= $data_order->total_pay;
			$this->content['rent_status'] 			= $data_order->rent_status;
			$this->content['special_request'] 		= $data_order->special_request;
			$this->content['time_order'] 			= $data_order->time_order;
			$this->content['booking_code'] 			= $data_order->booking_code;

			if(strtotime(date("Y-m-d")) >= strtotime("".$data_order->check_out."-7 days")){
				//$this->session->set_userdata('booking_code',$data_order->booking_code);
				$kost = $this->site_model->get_data('',"tb_kosts","kost_id = '".$data_order->kost_id."'")->row();
				$this->content['rem_check_in'] = strtotime("".$data_order->check_out."+1 days");
				$this->session->set_userdata('kost_title',$kost->kost_title);
				$this->session->set_userdata('kost_id',$data_order->kost_id);
				$this->session->set_userdata('kost_room_id',$data_order->kost_room_id);
			}
			// captcha
			$captcha_result = '';
			$this->content["cap_img"] = $this->fm->_make_captcha();
			$this->content["cap_msg"] = $cap_msg;

			$this->load->view('frontend/header',$this->metahead);
			$this->load->view('frontend/kost-member-order',$this->content);
			$this->load->view('frontend/footer',$this->footer);
		}
		else{
			$this->load->view('frontend/header',$this->metahead);
			$this->load->view('frontend/kost-login',$this->content);
			$this->load->view('frontend/footer',$this->footer);
		}
	}

	/* ------------------
	member login
	------------------ */
	function login(){
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');

		if($this->form_validation->run() == TRUE){
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if($this->site_model->check_login_member($username,$password) == TRUE){
				$user = $this->site_model->get_data('',"tb_members","member_username = '".$username."'")->row();
				$data = array(
					'ses_member_id'  =>$user->member_id,
					'member_username'=>$user->member_name,
					'member_login'   =>TRUE);
				$this->session->set_userdata($data);
				redirect(base_url().'member/');
			}
			else{
				$this->session->set_flashdata('message', 'Invalid Username / Password');
				redirect(base_url().'member/login');
			}
		}
		else{
			if($this->session->userdata('member_login')){
				redirect(base_url().'member');
			}
			else{
				$this->load->view('frontend/header');
				$this->load->view('frontend/kost-login');
				$this->load->view('frontend/footer');
			}
		}
	}

	/* ------------------
	reservation
	------------------ */
	function reservation($cap_msg = ""){
		$this->content['action'] = base_url().'member/register';
		$this->content['title_member'] = 'Reservation';
		// captcha
		$captcha_result = '';
		$this->content["cap_img"] = $this->fm->_make_captcha();
		$this->content["cap_msg"] = $cap_msg;

		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/kost-reservation',$this->content);
		$this->load->view('frontend/footer',$this->footer);
	}

	/* ------------------
	register / order
	------------------ */
	function register(){
		if($this->session->userdata('lang') == 'id') $lang = "";
		else $lang   = "_en";
		$option = $this->site_model->get_data('',"tb_options","option_id = '1'")->row();
		$notif  = $this->site_model->get_data('',"tb_notifications","notification_name = 'order_send'")->row();
		$bank   = $this->site_model->get_data('',"tb_notifications","notification_name = 'account_number'")->row();
		$limit  = $this->site_model->get_data('',"tb_notifications","notification_name = 'limit'")->row();

		if($this->session->userdata('member_login') == FALSE){
			// login information
			$this->form_validation->set_rules('username', 'username', 'required|callback_username_check');
			$this->form_validation->set_rules('password', 'password', 'required');

			// member information
			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('identity', 'identity', 'required');
			$this->form_validation->set_rules('identity_no', 'identity_no', 'required|callback_identity_check');
			$this->form_validation->set_rules('birthday', 'birthday', '');
			$this->form_validation->set_rules('phone', 'phone', 'required|numeric');
			$this->form_validation->set_rules('email', 'email', 'required|callback_email_check');
			$this->form_validation->set_rules('address', 'address', '');
		}

		// order information
		$this->db->join('tb_kosts', 'tb_kost_room.kost_id = tb_kosts.kost_id');
		$this->db->where('kost_room_id', $this->session->userdata('kost_room_id'));
		$kost = $this->db->get('tb_kost_room')->row();

		if(count($kost) != '0'){
			$this->form_validation->set_rules('room_type', 'room_type', '');
			$room_check_out = $this->input->post('kost_room_id').','.$this->input->post('check_in');
			$this->form_validation->set_rules('check_in', 'check_in', 'required|callback_room_check_in['.$kost->kost_room_id.']');
			// month
			if($kost->kost_room_type == '1'){
				$this->form_validation->set_rules('length_of_stay', 'length of stay', 'required|numeric|is_natural_no_zero');
				$x   = explode("-",$this->input->post('check_in'));
				$per = "month";
				if($x[0] != ''){
					$month    = $x[1] + $this->input->post('length_of_stay');
					$day      = $x[2] - 1;

					$check_in = $this->input->post('check_in');
					$check_out= date("Y-m-d",mktime(0,0,0,$month,$day,$x[0]));
				}
				$total_rent = $month;
				$total_pay  = $kost->kost_room_price_month * $this->input->post('length_of_stay');
			}
			// day
			elseif($kost->kost_room_type == '2'){
				$this->form_validation->set_rules('check_out', 'check out', 'required|callback_room_check_out['.$room_check_out.']');
				if($this->input->post("check_in") != '' AND $this->input->post("check_out") != ''){
					$check_in_date = explode("-",$this->input->post("check_in"));
					$check_out_date= explode("-",$this->input->post("check_out"));
					$total_rent    = $check_out_date[2] - $check_in_date[2];
					$total_pay     = $kost->kost_room_price_day * $total_rent;
				}
				$per      = "day";
				$check_in = $this->input->post('check_in');
				$check_out= $this->input->post("check_out");
			}
			// double
			elseif($kost->kost_room_type == '3'){
				if($this->input->post('room_type') == 'type_1'){
					$this->form_validation->set_rules('length_of_stay', 'length of stay', 'required|numeric|is_natural_no_zero');
					$x   = explode("-",$this->input->post('check_in'));
					$per = "month";
					if($x[0] != ''){
						$month    = $x[1] + $this->input->post('length_of_stay');
						$day      = $x[2] - 1;

						$check_in = $this->input->post('check_in');
						$check_out= date("Y-m-d",mktime(0,0,0,$month,$day,$x[0]));
						
						$total_rent = $month;
						$total_pay  = $kost->kost_room_price_month * $this->input->post('length_of_stay');
					}
				}
				elseif($this->input->post('room_type') == 'type_2'){
					$this->form_validation->set_rules('check_out', 'check out', 'required|callback_room_check_out['.$room_check_out.']');
					if($this->input->post("check_in") != '' AND $this->input->post("check_out") != ''){
						$check_in_date = explode("-",$this->input->post("check_in"));
						$check_out_date= explode("-",$this->input->post("check_out"));
						$total_rent    = $check_out_date[2] - $check_in_date[2];
						$total_pay     = $kost->kost_room_price_day * $total_rent;
					}
					$per      = "day";
					$check_in = $this->input->post('check_in');
					$check_out= $this->input->post("check_out");
				}
			}
		}
		else{
			$total_pay = "";
		}
		$this->form_validation->set_rules('special_request', 'special_request', '');

		if($this->form_validation->run() == TRUE){
			if($this->fm->_check_capthca()){
				if($this->session->userdata('member_login') == FALSE){
					// to tb_members
					$data = array(
						'member_username'       => $this->input->post("username"),
						'member_password'       => md5($this->input->post("password")),
						'member_b'              => $this->input->post("password"),
						'member_name'           => $this->input->post("name"),
						'member_identity'       => $this->input->post("identity"),
						'member_identity_number'=> $this->input->post("identity_no"),
						'member_birthday'       => $this->input->post("birthday"),
						'member_phone'          => $this->input->post("phone"),
						'member_email'          => $this->input->post("email"),
						'member_address'        => mysql_real_escape_string($this->input->post("address")));
					$this->site_model->input_data("tb_members",$data);
					$member_id = $this->db->insert_id();
				}
				else{
					$member_id = $this->session->userdata('ses_member_id');
				}
				// to tb_booking
				$order_code = gen_rand(8);
				$data       = array(
					'booking_code'   => $order_code,
					'member_id'      => $member_id,
					'kost_room_id'   => $kost->kost_room_id,
					'time_order'     => date("Y-m-d H:i:s"),
					'check_in'       => $check_in,
					'check_out'      => $check_out,
					'total_pay'      => $total_pay,
					'rent_status'    => '1',
					'special_request'=> $this->input->post("special_request"));
				$this->site_model->input_data("tb_rent_kost",$data);

				// mail message
				$message = '<style type=text/css>
				body { font-family: Tahoma, Geneva, sans-serif; }
				a { text-decoration: none; color: #333; font-size: 12px; }
				.invoice { clear: both; }
				.invoice h1 { font-size: 14px; color: #333; }
				.invoice p { font-size: 12px; color: #333; }
				.invoice table { border-collapse: collapse; }
				.invoice th,td { padding: 5px 5px 5px 0; font-size: 12px; color: #333; }
				.invoice .table { border-top: 1px solid #999; border-bottom: 1px solid #999; }
				.invoice .table th { text-align: center; }
				</style>';
				$message .= '<div class="invoice">';
				$message .= $notif->notification_value.$lang;
				if($this->session->userdata('member_login') == FALSE){
					$message .= '<h1>Login Information</h1>
					<p>Username: '.$this->input->post("username").'</p>
					<p>Password: '.$this->input->post("password").'</p>';
				}
				$message .= '<h1>Order Information</h1>
				<table>
				<tr>
				<td>Booking Code</td>
				<td colspan="3">: '.$order_code.'</td>
				</tr>
				<tr>
				<td>Kost</td>
				<td colspan="3">: '.$kost->kost_title.'</td>
				</tr>
				<tr>
				<td>Kost Room Number</td>
				<td colspan="3">: '.$kost->kost_room_number.'</td>
				</tr>
				<tr>
				<td>Check In</td>
				<td colspan="3">: '.fdate($check_in,2).'</td>
				</tr>
				<tr>
				<td>Check In</td>
				<td colspan="3">: '.fdate($check_out,2).'</td>
				</tr>
				<tr class="table">
				<th></th>
				<th>Price</th>
				<th>Length</th>
				<th>Total</th>
				</tr>';
				if($kost->kost_room_type == '1'){
					$message .= '<tr><td>Months</td>
					<td>'.rupiah($kost->kost_room_price_month).',-</td>
					<td align="center">'.$total_rent.'</td>
					<td align="right">'.rupiah($price['pay_month']).'</td>
					</tr>';
				}
				else{
					$message .= '<tr><td>Days</td>
					<td>'.rupiah($kost->kost_room_price_day).',-</td>
					<td align="center">'.$total_rent.'</td>
					<td align="right">'.rupiah($total_pay).'</td>
					</tr>';
				}
				$message .= '<tr><td colspan="4"></td></tr>
				<tr class="table">
				<td colspan="3">Total</td>
				<td>Rp. '.rupiah($total_pay).'</td>
				</tr>
				</table>';
				$message .= $bank->notification_value.$lang;
				$message .= '</div>';
				// data member
				$this->db->where('member_id', $member_id);
				$data_member = $this->db->get('tb_members')->row();
				// mail to user
				$config['mailtype'] = 'html';
				$this->email->initialize($config);
				$this->email->from($option->email, 'Executive Kost');
				$this->email->to($data_member->member_email);
				$this->email->subject("Pemesanan Kamar, ".$kost->kost_title);
				$this->email->message($message);
				$this->email->send();
				// mail to owner & guard
				$this->db->join('tb_users', 'tb_users.user_id = tb_user_kost.user_id');
				$this->db->where('tb_user_kost.kost_id', $kost->kost_id);
				$this->db->where_in('user_level', array('owner','guard'));
				$email = $this->db->get('tb_user_kost')->result();
				foreach($email as $email){
					$message = '<style type=text/css>
					body { font-family: Tahoma, Geneva, sans-serif; }
					h1 { font-size: 14px; color: #333; }
					p { font-size: 12px; color: #333; }
					</style>';
					$message .= "<h1>Pemesanan Kost ".$kost->kost_title."</h1>";
					$message .= "<p>Room Number : ".$kost->kost_room_number."</p>";
					$message .= "<p>Name : ".$data_member->member_name."</p>";
					$message .= "<p>Booking Code : ".$order_code."</p>";
					$this->email->from($option->email, 'Executive Kost');
					$this->email->to($email->user_email);
					$this->email->subject("Pemesanan Kamar, ".$kost->kost_title);
					$this->email->message($message);
					$this->email->send();
				}

				$data_session = array(
					'kost_id'     =>'',
					'kost_title'  =>'',
					'kost_name'   =>'',
					'kost_room_id'=>'');
				$this->session->unset_userdata($data_session);
				$this->session->set_flashdata('message', $notif->notification_value.$lang);
				if($this->session->userdata('member_login') == FALSE)
				redirect(base_url().'member/reservation');
				else
				redirect(base_url().'member');
			}
			else{
				$cap_msg = '<span class="error">Security Code Wrong!</span>';
			}
			
			// redirect
			if($this->session->userdata('member_login') == FALSE)
				$this->reservation($cap_msg);
			else
				$this->index($cap_msg);
		}
		else{
			// redirect
			if($this->session->userdata('member_login') == FALSE)
				$this->reservation();
			else
				$this->index();
		}
	}

	/* ------------------
	re order kost
	------------------ */
	function re_order(){
		if($this->session->userdata('lang') == 'id') $lang = "";
		else $lang           = "_en";
		$option         = $this->site_model->get_data('',"tb_options","option_id = '1'")->row();
		$notif          = $this->site_model->get_data('',"tb_notifications","notification_name = 'order_send'")->row();
		$bank           = $this->site_model->get_data('',"tb_notifications","notification_name = 'account_number'")->row();
		$limit          = $this->site_model->get_data('',"tb_notifications","notification_name = 'limit'")->row();

		$kost_id        = $this->session->userdata('kost_id');
		$kost_room_id   = $this->session->userdata('kost_room_id');
		$room_check_out = $this->input->post('kost_room_id').','.$this->input->post('check_in');
		$kost           = $this->site_model->get_data('',"tb_kost_room","kost_room_id = '".$kost_room_id."'")->row();

		$this->form_validation->set_rules('check_in', 'check_in', 'required|callback_room_check_in['.$kost_room_id.']');
		$this->form_validation->set_rules('check_out', 'check out', 'required|callback_room_check_out['.$room_check_out.']');
		$this->form_validation->set_rules('special_request', 'special_request', '');

		$price          = $this->price_check($this->input->post('check_in'),$this->input->post('check_out'),$kost_room_id);
		$check_in       = $this->input->post("check_in");
		$check_out      = $this->input->post("check_out");
		$total_pay      = $price['total_bayar'];

		if(count($kost) != 0){
			if($total_pay > $kost->kost_room_price_month  AND ($price['months'] == 0 AND $price['days'] < $price['result']))
			$data = $price['months']." month ".$price['days']." days<br />total = Rp. ".rupiah($price['total_bayar']).",-".br(1).$limit->notification_value.$lang;
			else
			$data = $price['months']." month ".$price['days']." days<br />total = Rp. ".rupiah($price['total_bayar']).",-";

			$this->session->set_userdata('price_check', $data);
		}

		if($this->form_validation->run() == TRUE){
			if($this->fm->_check_capthca()){
				// to tb_booking
				$order_code = gen_rand(8);
				$data       = array(
					'booking_code'   => $order_code,
					'member_id'      => $this->session->userdata('ses_member_id'),
					'kost_room_id'   => $kost_room_id,
					'time_order'     => date("Y-m-d H:i:s"),
					'check_in'       => $check_in,
					'check_out'      => $check_out,
					'total_pay'      => $total_pay,
					'rent_status'    => '1',
					'special_request'=> $this->input->post("special_request"));
				$this->site_model->input_data("tb_rent_kost",$data);

				// mail message
				$message = '<style type=text/css>
				body { font-family: Tahoma, Geneva, sans-serif; }
				a { text-decoration: none; color: #333; font-size: 12px; }
				#invoice { clear: both; }
				#invoice h1 { font-size: 14px; color: #333; }
				#invoice p { font-size: 12px; color: #333; }
				#invoice table { border-collapse: collapse; }
				#invoice th,td { padding: 5px 5px 5px 0; font-size: 12px; color: #333; }
				#invoice .table { border-top: 1px solid #999; border-bottom: 1px solid #999; }
				#invoice .table th { text-align: center; }
				</style>';
				$message .= '<div id="invoice">';
				$message .= $notif->notification_value.$lang;
				$message .= '<h1>Login Information</h1>
				<p>Username: '.$this->input->post("username").'</p>
				<p>Password: '.$this->input->post("password").'</p>';
				$message .= '<h1>Order Information</h1>
				<table>
				<tr><td>Booking Code</td>		<td>: '.$order_code.'</td></tr>
				<tr><td>Kost</td>				<td>: '.$this->session->userdata('kost_title').'</td></tr>
				<tr><td>Kost Room Number</td>	<td>: '.$kost->kost_room_number.'</td></tr>
				<tr><td>Check In</td>			<td>: '.fdate($check_in,2).'</td></tr>
				<tr><td>Check In</td>			<td>: '.fdate($check_out,2).'</td></tr>
				<tr class="table"><th></th><th>Price</th><th>Length</th><th>Total</th></tr>
				<tr><td>Months</td>
				<td>'.rupiah($kost->kost_room_price_month).',-</td>
				<td align="center">'.$price['months'].'</td>
				<td align="right">'.rupiah($price['pay_month']).'</td></tr>
				<tr><td>Days</td>
				<td>'.rupiah($kost->kost_room_price_day).',-</td>
				<td align="center">'.$price['days'].'</td>
				<td align="right">'.rupiah($price['pay_day']).'</td></tr>
				<tr><td colspan="4"></td></tr>
				<tr class="table"><td colspan="3">Total</td><td>Rp. '.rupiah($price['total_bayar']).'</td></tr>
				</table>';
				$message .= $bank->notification_value.$lang;
				$message .= '</div>';
				// mail to user
				$config['mailtype'] = 'html';
				$this->email->initialize($config);
				$this->email->from($option->email, 'Executive Kost');
				$this->email->to($this->input->post('email'));
				$this->email->subject("Pemesanan Kamar, ".$this->session->userdata('kost_title'));
				$this->email->message($message);
				$this->email->send();
				// mail to owner & guard
				$email = $this->site_model->get_data('',"tb_users u,tb_user_kost uk","u.user_id = uk.user_id AND uk.kost_id = ".$kost_id." AND u.user_level IN ('owner','guard')")->result();
				foreach($email as $a){
					$message = '<style type=text/css>
					body { font-family: Tahoma, Geneva, sans-serif; }
					h1 { font-size: 14px; color: #333; }
					p { font-size: 12px; color: #333; }
					</style>';
					$message .= "<h1>Pemesanan Kost ".$this->session->userdata('kost_title')."</h1>";
					$message .= "<p>Room Number : ".$kost->kost_room_number."</p>";
					$message .= "<p>Name : ".$this->input->post("name")."</p>";
					$message .= "<p>Booking Code : ".$order_code."</p>";
					$this->email->from($option->email, 'Executive Kost');
					$this->email->to($a->user_email);
					$this->email->subject("Pemesanan Kamar, ".$this->session->userdata('kost_title'));
					$this->email->message($message);
					$this->email->send();
				}

				$data_session = array('kost_id'     =>'','kost_title'  =>'','kost_name'   =>'','kost_room_id'=>'');
				$this->session->unset_userdata($data_session);
				$this->session->set_flashdata('message', $notif->notification_value.$lang);
				redirect(base_url().'member');
			}
			else{
				$cap_msg = '<span class="error">Security Code Wrong!</span>';
			}
			$this->index($cap_msg);
		}
		else{
			$this->index();
		}
	}

	/* ------------------
	confirm
	------------------ */
	function confirm(){
		if($this->session->userdata('lang') == 'id') $lang = "";
		else $lang             = "_en";

		$this->form_validation->set_rules('booking_code', 'booking code', 'required|callback_check_booking_code');
		$this->form_validation->set_rules('transfer_date', 'transfer date', 'required');
		$this->form_validation->set_rules('deposit', 'deposit', 'required|numeric');
		$this->form_validation->set_rules('bank', 'bank', 'required');
		$this->form_validation->set_rules('bank_account_name', 'bank account name', 'required');
		$this->form_validation->set_rules('bank_account_number', 'bank account number', 'required');
		$this->form_validation->set_rules('transfer_message', 'transfer message', '');

		$flash_message    = "Terima kasih, konfirmasi Anda akan segera kami proses.";
		$flash_message_en = "Thanks, we will process your confirmation.";

		if($this->form_validation->run() == TRUE){
			$data = array(
				'rent_kost_id'       => $this->input->post("rent_kost_id"),
				'booking_code'       => $this->input->post("booking_code"),
				'transfer_date'      => $this->input->post("transfer_date"),
				'deposit'            => $this->input->post("deposit"),
				'bank'               => $this->input->post("bank"),
				'bank_account_name'  => $this->input->post("bank_account_name"),
				'bank_account_number'=> $this->input->post("bank_account_number"),
				'transfer_message'   => $this->input->post("transfer_message"),
				'confirm_date'       => date("Y-m-d H:i:s"));
			$this->site_model->input_data("tb_confirms",$data);

			$this->session->set_flashdata('message', $flash_message.$lang);
			redirect(base_url().'member');
		}
		else{
			$this->index();
		}
	}

	/* ------------------
	profile
	------------------ */
	function profile(){
		// data member
		$data_member = $this->site_model->get_data('',"tb_members","member_id = '".$this->session->userdata('ses_member_id')."'")->row();
		$this->content['member_id'] = $data_member->member_id;
		$this->content['username'] = $data_member->member_username;
		$this->content['password'] = $data_member->member_password;
		$this->content['name'] = $data_member->member_name;
		$this->content['identity'] = $data_member->member_identity;
		$this->content['identity_no'] = $data_member->member_identity_number;
		$this->content['birthday'] = $data_member->member_birthday;
		$this->content['phone'] = $data_member->member_phone;
		$this->content['email'] = $data_member->member_email;
		$this->content['address'] = $data_member->member_address;

		$this->load->view('frontend/header',$this->metahead);
		$this->load->view('frontend/kost-member-profile',$this->content);
		$this->load->view('frontend/footer',$this->footer);
	}

	/* ------------------
	update member
	------------------ */
	function update_member_information(){
		if($this->session->userdata('lang') == 'id') $lang = "";
		else $lang             = "_en";

		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('identity', 'identity', 'required');
		$this->form_validation->set_rules('identity_no', 'identity_no code', 'required');
		$this->form_validation->set_rules('birthday', 'birthday', 'required');
		$this->form_validation->set_rules('phone', 'phone', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('address', 'address', '');

		$flash_message    = "Data Anda sudah terupdate.";
		$flash_message_en = "Your data already updated.";

		if($this->form_validation->run() == TRUE){
			$data = array(
				'member_name'           => $this->input->post("name"),
				'member_identity'       => $this->input->post("identity"),
				'member_identity_number'=> $this->input->post("identity_no"),
				'member_birthday'       => $this->input->post("birthday"),
				'member_phone'          => $this->input->post("phone"),
				'member_email'          => $this->input->post("email"),
				'member_address'        => $this->input->post("address"));
			$this->site_model->update_data("tb_members",$data,"member_id = '".$this->input->post("member_id")."'");

			$this->session->set_flashdata('message', $flash_message.$lang);
			redirect(base_url().'member/profile');
		}
		else{
			$this->profile();
		}
	}

	/* ------------------
	update password
	------------------ */
	function update_member_password(){
		if($this->session->userdata('lang') == 'id') $lang = "";
		else $lang             = "_en";

		$member_id        = $this->input->post("member_id");
		$username         = $this->input->post("username");
		$old_password     = $this->input->post("old_password");
		$new_password     = $this->input->post("new_password");
		$conf_password    = $this->input->post("conf_password");
		$password         = md5($new_password);

		$this->form_validation->set_rules('name', 'name', '');

		$flash_message    = "Data Anda sudah terupdate.";
		$flash_message_en = "Your data already updated.";

		if($this->form_validation->run() == TRUE){
			if($this->site_model->check_password_member($member_id,$old_password) == TRUE){
				if(!empty($new_password)){
					if($new_password == $conf_password){
						$data = array('member_username'=>$username,'member_password'=>$password,'member_b'       =>$new_password);
						$this->site_model->update_data("tb_members",$data,"member_id = '".$member_id."'");
						$this->session->set_flashdata('message', 'Data was update!');
						redirect(base_url().'member/profile');
					}
					else{
						$this->content['message'] = 'New Password & Confirm Pass do not match!';
						$this->load->view('backend/template',$this->content);
					}
				}
				else{
					$data = array('member_username'=>$username);
					$this->site_model->update_data("tb_members",$data,"member_id = '".$member_id."'");
					$this->session->set_flashdata('message', 'Data was update!');
					redirect(base_url().'member/profile');
				}
			}
			else{
				$this->session->set_flashdata('message', 'Wrong Present Password!');
				redirect(base_url().'member/profile');
			}
		}
		else{
			$this->profile();
		}
	}

	/* ------------------
	price check
	------------------ */
	function price_check($data_check_in = "",$data_check_out = "",$data_kost_room_id = ""){
		$kost = $this->site_model->get_data('',"tb_kost_room","kost_room_id = '".$data_kost_room_id."'")->row();
		if(count($kost) != 0){
			$harga_harian = $kost->kost_room_price_day;
			$harga_bulanan= $kost->kost_room_price_month;

			$data         = $this->fm->selisih_tgl($data_check_in,$data_check_out);

			$result       = $data['result'];
			$days         = $data['hari'];
			$months       = $data['bulan'];
			// harga
			$bayar_harian = $harga_harian * $days;
			if($months != '0')
			$bayar_bulanan = $harga_bulanan * $months;
			else
			$bayar_bulanan = 0;
			$total_bayar   = $bayar_harian + $bayar_bulanan;

			$data          = array('result'     =>$result,'months'     =>$months,'days'       =>$days,'pay_month'  =>$bayar_bulanan,'pay_day'    =>$bayar_harian,'total_bayar'=>$total_bayar);
			return $data;
		}
	}
}