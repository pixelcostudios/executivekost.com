<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fm extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	// captcha
	function _make_captcha(){
		$this->load->helper('captcha');
		$vals = array(
			'img_path'		=> './uploads/captcha/',
			'img_url' 		=> base_url().'uploads/captcha/',
			'img_width' 	=> 150,
			'img_height' 	=> 41,
			'font_path'		=> './stylesheets/army.ttf',
		 );
		// Create captcha
		$cap = create_captcha($vals);
		// Write to DB
		if($cap){
			$data = array(
				'captcha_id' 	=> '',
				'captcha_time' 	=> $cap['time'],
				'ip_address' 	=> $this->input->ip_address(),
				'word' 			=> $cap['word'],
			);
			$query = $this->db->insert_string('tb_captcha',$data);
			$this->db->query($query);
		}
		else {
			return "<span class=\"error\">Umm captcha not work</span>";
		}
		return $cap['image'];
	}
   
	function _check_capthca(){
		// Delete old data ( 2hours)
		$expiration = time()-7200; // Two hour limit
		$this->db->query("DELETE FROM tb_captcha WHERE captcha_time < ".$expiration);	
		
		// Then see if a captcha exists:
		$sql = "SELECT COUNT(*) AS count FROM tb_captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
	 
		if($row->count>0){
			return true;
		}
		return false;
	}
	
	/* upload images */
	function upload_img($type_file,$title,$thumb=TRUE,$w="200",$h="300"){		
		$dataupload = $this->upload->data();	   
		$namafile = slug($title).$dataupload['file_ext'];
		rename($dataupload['full_path'], $dataupload['file_path'].$namafile);
		  
		/* thumb */
		if($thumb==TRUE){
			$configthumb = array();
			$configthumb['source_image'] = $dataupload['file_path'].$namafile;
			$configthumb['create_thumb'] = $thumb;
			$configthumb['maintain_ratio'] = TRUE;
			$configthumb['quality'] = '100%';
			$configthumb['width'] = $w;
			$configthumb['height'] = $h;
			
			$this->load->library('image_lib', $configthumb);
			$this->image_lib->resize();
			$this->image_lib->clear();
		}
		
		$data['ext'] = $dataupload['file_ext'];
		return $data;
	}
	
	/* multi upload image */
	function multi_upload_img($dir,$type_file,$title,$id,$rel,$thumb=TRUE,$w="200",$h="300"){
		$config['upload_path'] = './'.$dir.'/';
		$config['allowed_types'] = $type_file;
		$this->load->library('upload', $config);
		
		$configthumb = array();
		$configthumb['create_thumb'] = $thumb;
		$configthumb['maintain_ratio'] = TRUE;
		$configthumb['quality'] = '100%';
		$configthumb['width'] = $w;
		$configthumb['height'] = $h;
		$this->load->library('image_lib');
		
		for($i = 1; $i <= 10; $i++) {
			$upload = $this->upload->do_upload('image'.$i);
			$data_caption = $this->input->post('caption'.$i);
			if($data_caption!=''){
				$caption = $data_caption;
				$slug = slug($data_caption);
			}
			else{
				$caption = $title.'_'.$i;
				$slug = slug($title).'_'.$i;
			}
			if($upload === FALSE) continue;
			$data = $this->upload->data();
			$ext = $data['file_ext'];
			$file_name = $slug.$ext;
			rename($data['full_path'], $data['file_path'].$file_name);
	
			$uploadedFiles[$i] = $data;
			if($data['is_image'] == 1) {
				$configthumb['source_image'] = $data['file_path'].$file_name;
				$this->image_lib->initialize($configthumb);
				$this->image_lib->resize();
			}
			$this->site_model->input_data("tb_images",$data=array('parent_id'=>$id,'image'=>$file_name,'dir'=>$dir,'relation'=>$rel,'caption'=>$caption));
		}
	}
	
	/* check selisih tangal  */
	function selisih_tgl($data_check_in,$data_check_out){
		/*$get_all_days = strtotime($data_check_out) - strtotime($data_check_in)+1;
		$result = floor(floatval($get_all_days) / (60 * 60 * 24))+1;
		// bulan
		$months = floor($result / 30);
		// hari
		$days = ($result % 30);
		
		return array('result'=>$result,'bulan'=>$months,'hari'=>$days);*/
		
		$date1 = new DateTime(date('Y-m-d', strtotime($data_check_in)));
		$date2 = new DateTime(date('Y-m-d',strtotime($data_check_out)));
		//$date2 = new DateTime(date('Y-m-d',strtotime("+1 $data_check_in",strtotime($tgl2))));
		$interval = $date1->diff($date2);

		//if($interval->y != 0)
		//$years	= $interval->y;
		//if($interval->m != 0)
		$months	= $interval->m + ($interval->y * 12);
		//if($interval->d != 0)		
		$days 	= $interval->d;
		$get_check_in = explode("-", $data_check_in);
		$days_per_months = cal_days_in_month ( CAL_GREGORIAN , $get_check_in[1] , $get_check_in[0] );
		
		return array('result'=>$days_per_months,'bulan'=>$months,'hari'=>$days);
	}
}